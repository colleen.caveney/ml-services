import numpy as np
import tensorflow as tf
from glob import glob
from keras import Model
from keras.models import load_model
import keras.backend as K
from transformers import AutoTokenizer

SAVED_MODELS_PATH = '../../../Downloads/saved_models/**/saved_model/1'
MODEL_NAME_IDX_IN_PATH = 5


class EnsembleModel(Model):

    def __init__(self):
        super().__init__()
        self.models = {}
        for path in glob(SAVED_MODELS_PATH):
            name = path.split('/')[MODEL_NAME_IDX_IN_PATH]
            print(f"loading: {name}")
            self.models[name] = load_model(path)
        self.category_all_model_key = {
            '0': 'distilbert-accessories-wilbert',
            '1':"distilbert-bags-gilbert",
            '2':"distilbert-beauty-dagobert",
            '3':"distilbert-bottoms-albert",
            '4':"distilbert-home-heriberto",
            '5':"distilbert-intimates-albertina",
            '6': "distilbert-jewelry-engelbert",
            '7': "distilbert-office-hubert",
            '8':'other',
            '9':"distilbert-other_clothing-roberto",
            '10': "distilbert-shoes-dilbert",
            '11': "distilbert-swim-bertha",
            '12': "distilbert-tops-roberta"
        }
        
    def call(self, inputs):
        category = self.models['distilbert-category-all-bert'](inputs)
        print(f'cat: {category}')
        category_idx_tf = tf.math.argmax(category['logits'], 1)
        category_idx = K.eval(category_idx_tf)
        print(f"idx: {str(category_idx)}")
        subcategory_model_name = self.category_all_model_key[str(category_idx)]
        sub_category = self.models[subcategory_model_name](inputs)
        sub_category_idx_tf = tf.math.argmax(sub_category['logits'], 1)
        sub_category_idx = eval(sub_category_idx_tf)
        return category_idx, sub_category_idx

model = EnsembleModel()
tokenizer = AutoTokenizer.from_pretrained("distilbert-base-uncased")

def preprocess(inputs):
    list_encoded = tokenizer(inputs, truncation=True, padding=True, return_tensors="tf")
    result = {'input_ids': list_encoded['input_ids'], 'attention_mask': list_encoded['attention_mask']}
    return result

val = preprocess('red vase')
category_idx, subcategory_idx = model.predict(val)
print(category_idx, subcategory_idx)

model.save('ensemble_model')
