GCP_PROJECT = 'leap-platform-dev'
LOCATION = 'us-central1'
CATEGORY_ENDPOINT_ID = '4950392774387040256'
SUBCATEGORY_ENDPOINT_ID_DICT = {
    'Accessories' : "2068089012869922816",
    'Bags, Backpacks & Luggage' : "8625330070321364992",
    'Beauty, Health & Personal Care': "5711501111412654080",
    'Bottoms' : "7157156591798583296",
    'Home' : "6709611378828640256",
    'Intimates' : "6679775031297310720",
    'Jewelry & Watches' : "8701328314033242112",
    'Office & School Supplies' : "6656694083207036928",
    'Other Clothing' : "4373932022083616768",
    'Shoes' : "2451457931149836288",
    'Tops' : "2860722547287130112",
    'Swimsuits & Cover-Ups' : "6955057558520332288"
}
STORAGE_BUCKET = 'leap-model-storage-platform-dev'

KEYS_STORAGE_PREFIX = 'keys'

ALL_CATEGORY_KEY_PATH = 'keys/1.0/category-all-key.csv'

PUBSUB_TOPIC_ID = 'commerce_events'

MODEL_PATH_CONFIG = {
    'Accessories' : 'keys/1.0/accessories-key.csv',
    'Bags, Backpacks & Luggage' : 'keys/1.0/bags-key.csv',
    'Beauty, Health & Personal Care': 'keys/1.0/beauty-key.csv',
    'Bottoms' : 'keys/1.0/bottoms-key.csv',
    'Home' :'keys/1.0/home-key.csv',
    'Intimates' : 'keys/1.0/intimates-key.csv',
    'Jewelry & Watches' : 'keys/1.0/jewelry-key.csv',
    'Office & School Supplies' : 'keys/1.0/office-key.csv',
    'Other Clothing' : 'keys/1.0/other_clothing-key.csv',
    'Shoes' : 'keys/1.0/shoes-key.csv',
    'Tops' : 'keys/1.0/tops-key.csv',
    'Swimsuits & Cover-Ups' : 'keys/1.0/swim-key.csv'
}

TRIGGER_TYPE = 'cloud_event'

# ATTRIBUTE_FILTERS = {'x-leap-event-type': lambda x: x.startswith('product:')}
ATTRIBUTE_FILTERS = {}