import functions_framework
import pandas as pd
import json
from google.cloud import pubsub_v1
# from trigger_serializer import TriggerSerializer
from prediction_service import CloudStorageBucketClient, VertexCategoryPredictionService, DownloadModelPredictionService
# from model import AllCategoryModel, SubCategoryModel, model_path_config
from config import *
from utils import preprocess_product_change_event, trigger_to_dict
from flask import abort


publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(GCP_PROJECT, PUBSUB_TOPIC_ID)


# load 
# cloud_storage_bucket_client = CloudStorageBucketClient(GCP_PROJECT, STORAGE_BUCKET)
# cloud_storage_bucket_client.download_blobs(blob_prefix=KEYS_STORAGE_PREFIX)

CATEGORY_LABEL_KEY = pd.read_csv(ALL_CATEGORY_KEY_PATH)
LABEL_KEY_DICT = {key:pd.read_csv(value) for (key,value) in MODEL_PATH_CONFIG.items()}

category_prediction_service = VertexCategoryPredictionService(GCP_PROJECT, \
    LOCATION, CATEGORY_ENDPOINT_ID, SUBCATEGORY_ENDPOINT_ID_DICT, CATEGORY_LABEL_KEY, LABEL_KEY_DICT)


@functions_framework.cloud_event
# @functions_framework.http
def product_categorization(event):

    try:
        attributes, data = trigger_to_dict(TRIGGER_TYPE, event)
    except Exception as e:
        return abort(400, e)
    
    # filter_out = True
    # for attr in attributes:
    #     if attr in ATTRIBUTE_FILTERS and ATTRIBUTE_FILTERS[attr](attributes[attr]):
    #         filter_out = False
    # if filter_out:
    #     print(f'Message filtered out on attributes')
    #     return

    try:
        product_type = data['product']['product_type']
        product_title = data['product']['title']
    except:
        return abort(400, 'message does not have product.product_type and/or product.title attributes')

    text = preprocess_product_change_event(product_title, product_type)
    
    category, sub_category, tertiary_category = category_prediction_service.predict(text)
    
    # append new fields to original payload
    data['product']['category_prediction'] = category
    data['product']['sub_category_prediction'] = sub_category
    data['product']['tertiary_category_prediction'] = tertiary_category

    # encode for pubsub event
    data =(json.dumps(data)).encode("utf-8")

    # publsih 
    future = publisher.publish(topic_path, data)
    print(f"Published message {future.result()} to {topic_path}")
    return [category, sub_category, tertiary_category]
