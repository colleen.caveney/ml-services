#!/usr/bin/env bash

## logging functions

function log() {
    (>&2 echo -e "${1}")
}

function log_err() {
    log "[ERR] ${1}"
}

function log_warn() {
    log "[WARN] ${1}"
}

function log_info() {
    log "[INFO] ${1}"
}

function die() {
    log_err "${1}"
    exit 1
}

## type helpers

# expects an associate-array as the only argument and will output a string of
# `key='value'`` pairs (space separated) suitable for use env-vars to be
# prefixed to command calls.
function arr2env() {
    declare -n local_arr="$1"
    for k in "${!local_arr[@]}"; do
        printf "%s=%s " "${k}" "${local_arr[$k]@Q}"
    done
}

## python helpers

# tests if pwd is a python project
function lang_is_python() {
    test -f Pipfile
}
