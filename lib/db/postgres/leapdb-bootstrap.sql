DROP DATABASE IF EXISTS leap;
DROP DATABASE IF EXISTS jobs_api;
DROP ROLE IF EXISTS leap;

CREATE ROLE leap LOGIN PASSWORD 'password';

GRANT leap TO postgres;

CREATE DATABASE leap OWNER leap;
CREATE DATABASE jobs_api OWNER leap;

GRANT ALL ON DATABASE leap TO leap;
GRANT ALL ON DATABASE jobs_api TO leap;

\connect leap

CREATE EXTENSION IF NOT EXISTS "pgcrypto";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

\connect jobs_api

CREATE EXTENSION IF NOT EXISTS "pgcrypto";
