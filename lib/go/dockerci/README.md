# DockerCI - GO

The DockerCI - Go library supports a base [docker image](./Dockerfile) for
use by go projects in a continuous integration (CI) environment. The general
intent is provide a system configuration with a baseline set of tools, such as:
`go`, `git`, `make`, etc. in order to avoid needing to perform installation
steps for these tools in a particular project's CI script such that these scripts
need only provide calls to `make <target>`.

The `gcloud` toolkit will also be installed to assist with deployments, though
your specific project will need to provide service-account keys and so forth.

See the [Dockerfile](./Dockerfile) for details on what is specifically
installed.

## Usage

In your project's `.gitlab-ci.yml` file, assuming you have a standard configuration,
you can minimally add the following to get a working CI setup:

```yaml
.conditions:MYPROJECT:
    only:
        changes:
            - "services/MYPROJECT/**"

build:MYPROJECT:
    extends:
        - .image:go # THIS IS THE IMPORTANT LINE
        - .conditions:only-mergable
        - .conditions:MYPROJECT
        - .cache:MYPROJECT
    stage: build
    script:
        - cd services/MYPROJECT
        - make setup
        - make build

test:MYPROJECT:
    stage: test
    extends:
        - .image:go # THIS IS THE IMPORTANT LINE
        - .conditions:only-mergable
        - .conditions:MYPROJECT
        - .cache:app
        - .cache:readonly
    script:
        - cd services/MYPROJECT
        - make test
```

You will need to adjust paths and YAML target names to reflect your service; e.g.
replace `MYPROJECT` with your project's directory name.
