# Provides templating support to any string values such that the value for a
# path can be substituted for a path-template. The path-template takes the form
# `{{path.to.some.key}}`; multple path-templates can be used within the same
# string. Substitutions will be recursively processed.
#
# Given:
#   {
#       "bigquery": {
#           "dataset": "my_dataset",
#           "table": "my_table",
#       },
#       "function": {
#           "env": { "BQTABLE": "{{bigquery.dataset}}.{{bigquery.table}}" }
#       }
#   }
#
# Would produce:
#   {
#       "bigquery": {
#           "dataset": "my_dataset",
#           "table": "my_table",
#       },
#       "function": {
#           "env": { "BQTABLE": "my_dataset.my_table" }
#       }
#   }

def T(root):
    gsub( "{{(?<path>[^}]+)}}"; .path | split(".") as $path | root | getpath($path) | T(root) );

. as $root | walk(
    if type == "string" then
        T($root)
    else
        .
    end
)
