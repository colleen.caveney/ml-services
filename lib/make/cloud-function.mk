ifndef ROOTDIR
ROOTDIR := $(shell git rev-parse --show-toplevel)
endif

ifeq (, $(findstring common.mk, $(MAKEFILE_LIST)))
include $(ROOTDIR)/lib/make/common.mk
endif

.PHONY: cloud-func-dist cloud-func-prepare-remote cloud-func-push

# Note: you will need to define FUNC_FILES for your purposes
#
# Example:
#   FUNC_FILES := main.py const.py requirements.txt
FUNC_FILES := forgot-to-define-FUNC_FILES.file

FUNC_ARCHIVE  := archive.zip
FUNCTION_ARCHIVE_TAG := $(COMMIT)

# this will be used by terraform, as is evident from the output created
cloud-func-dist:
	@rm -f $(FUNC_ARCHIVE)
	@zip -qXr $(FUNC_ARCHIVE) $(FUNC_FILES)
	@echo "{\"archive\":\"$(FUNC_ARCHIVE)\",\"md5\":\"$$(zipinfo -1 $(FUNC_ARCHIVE) | grep -v -E '/$$' | xargs -n 1 cat | md5sum | cut -d ' ' -f 1)\"}"



cloud-func-prepare-remote: require.service-env prohibit-local
	$(eval FUNC_REMOTE_ARCHIVE=gs://$(FUNCTION_BUCKET)/$(SERVICE_NAME)-function/$(FUNCTION_ARCHIVE_TAG).zip)
	@rm -f $(FUNC_ARCHIVE)
	@zip -qXr $(FUNC_ARCHIVE) $(FUNC_FILES)

cloud-func-push: cloud-func-prepare-remote
	@echo "copying function to $(FUNC_REMOTE_ARCHIVE)"
	gsutil -m cp $(FUNC_ARCHIVE) $(FUNC_REMOTE_ARCHIVE)
