## vars
BUILTIN_VARS := $(.VARIABLES) # this needs to be defined first

NAMESPACE := leap
SERVICE_NAME=$(shell basename $(shell pwd))
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)
COMMIT=$(shell git rev-parse --short=8 HEAD)
TIMESTAMP=$(shell date -u +%Y%m%d.%H%M%S)

DOTENV=.env

## infra

# the GCP project and asset bucket do-not and should-not exist
SERVICE_ENV:=$(SERVICE_ENV)
ifndef SERVICE_ENV
SERVICE_ENV=local
endif
GCP_PROJECT=leap-local
ASSETS_HOST=assets.local.leapinc.co
DOCKER_REMOTE_REPO := gcr.io/$(GCP_PROJECT)

## targets

.PHONY: list list-vars noop

noop: # just a noop, useful for `list` target

# list all available make targets
list:
	@$(MAKE) -pRrq -f $(firstword $(MAKEFILE_LIST)) noop 2>/dev/null | \
		awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | \
		sort | \
		egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

list-vars:
	$(foreach v, \
		$(filter-out $(BUILTIN_VARS) BUILTIN_VARS,$(.VARIABLES)), \
		$(info $(v) = $($(v))))
	@echo -n '' # noop

.PHONY: stage prod warehouse

dev:
	@echo "# switching to development environment"
	$(eval GCP_PROJECT=leap-platform-dev)
	$(eval DOCKER_REMOTE_REPO=gcr.io/$(GCP_PROJECT))
	$(eval SERVICE_ENV=dev)
	$(eval FUNCTION_BUCKET=leap-functions-platform-$(SERVICE_ENV))

stage:
	@echo "# switching to staging environment"
	$(eval GCP_PROJECT=leap-platform-stage)
	$(eval DOCKER_REMOTE_REPO=gcr.io/$(GCP_PROJECT))
	$(eval SERVICE_ENV=stage)
	$(eval FUNCTION_BUCKET=leap-functions-platform-$(SERVICE_ENV))

prod:
	@echo "# switching to prod environment"
	$(eval GCP_PROJECT=leap-platform-prod)
	$(eval DOCKER_REMOTE_REPO=gcr.io/$(GCP_PROJECT))
	$(eval SERVICE_ENV=prod)
	$(eval FUNCTION_BUCKET=leap-functions-platform-$(SERVICE_ENV))

warehouse:
	@echo "# switching to warehouse environment"
	$(eval GCP_PROJECT=warehouse-228818)
	$(eval DOCKER_REMOTE_REPO=gcr.io/$(GCP_PROJECT))
	$(eval SERVICE_ENV=warehouse)
	$(eval FUNCTION_BUCKET=leap-functions-$(SERVICE_ENV))

.PHONY: prohibit-local require-local require-dotenv require.service-env

prohibit-local:
	@if [ $(SERVICE_ENV) = "local" ]; then \
		echo "! '$(SERVICE_ENV)' environment does not support this action"; \
		exit 1; \
	fi

require-local:
	@if [ $(SERVICE_ENV) != "local" ]; then \
		echo "! 'local' environment required (not '$(SERVICE_ENV)')"; \
		exit 1; \
	fi

require-dotenv:
	@if [ ! -f $(DOTENV) ]; then \
		echo "! '$(DOTENV)' file required but does not exist"; \
		exit 1; \
	fi

require.service-env:
	@if [ "X$(SERVICE_ENV)X" = "XX" ]; then \
		echo "! SERVICE_ENV required"; \
		exit 1; \
	fi
