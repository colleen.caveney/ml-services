ifndef ROOTDIR
ROOTDIR := $(shell git rev-parse --show-toplevel)
endif

ifeq (, $(findstring common.mk, $(MAKEFILE_LIST)))
include $(ROOTDIR)/lib/make/common.mk
endif

## docker

.PHONY: docker-image docker-build docker-shell docker-run-wsgi docker-push

# global name for the image to manage
DOCKER_IMAGE     := $(NAMESPACE)/$(SERVICE_NAME)
DOCKER_IMAGE_TAG := $(COMMIT)
VERSION          := $(BRANCH)/$(COMMIT)/$(TIMESTAMP)

IMAGE_NAME       := $(SERVICE_NAME)
# container name for local execution only
DOCKER_LOCAL_NAME := local.$(NAMESPACE).$(SERVICE_NAME)

TAR := $(shell which gtar || echo tar)

docker-build:
	$(TAR) -czh -X .dockerignore . | \
	DOCKER_BUILDKIT=1 docker buildx build \
		--ssh default \
		-t $(DOCKER_IMAGE):$(DOCKER_IMAGE_TAG) \
		--build-arg VERSION="${VERSION}" \
		--build-arg LDFLAGS="${LDFLAGS}" -

docker-shell:
	@leap-docker-shell $(DOCKER_IMAGE):$(DOCKER_IMAGE_TAG) $(DOCKER_LOCAL_NAME) /bin/bash

# running as a wsgi

ifndef $(WSGI_PORT)
WSGI_PORT := 8080
endif

WSGI_ARGS := -e LOG_LEVEL=debug -e PORT=$(WSGI_PORT) --name $(DOCKER_LOCAL_NAME)

docker-run-wsgi:
	docker run --rm $(WSGI_ARGS) \
		-p 127.0.0.1:$(WSGI_PORT):$(WSGI_PORT) \
		$(DOCKER_IMAGE):$(DOCKER_IMAGE_TAG)

# image management

docker-push:
	docker tag $(DOCKER_IMAGE):$(DOCKER_IMAGE_TAG) \
		$(DOCKER_REMOTE_REPO)/$(IMAGE_NAME):$(DOCKER_IMAGE_TAG)
	docker tag $(DOCKER_IMAGE):$(DOCKER_IMAGE_TAG) \
		$(DOCKER_REMOTE_REPO)/$(IMAGE_NAME):latest
	docker push $(DOCKER_REMOTE_REPO)/$(IMAGE_NAME):$(DOCKER_IMAGE_TAG)
	docker push $(DOCKER_REMOTE_REPO)/$(IMAGE_NAME):latest
