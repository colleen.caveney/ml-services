ifndef ROOTDIR
ROOTDIR := $(shell git rev-parse --show-toplevel)
endif

ifeq (, $(findstring go.mk, $(MAKEFILE_LIST)))
include $(ROOTDIR)/lib/make/go.mk
endif

ifeq (, $(findstring cloud-function.mk, $(MAKEFILE_LIST)))
include $(ROOTDIR)/lib/make/cloud-function.mk
endif

## running

.PHONY: go.server

go.server: require-local build
	@echo "# running $(SERVICE_NAME)"
	$(TARGET_DIR)/$(SERVICE_NAME)

## distribution

.PHONY: go.cloud-func-push

# default FUNC_FILES; you should define your own if you need to archive another set of files
FUNC_FILES := *.go *.mod *.sum internal

go.cloud-func-push: setup cloud-func-push
