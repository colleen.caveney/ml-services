ifndef ROOTDIR
ROOTDIR := $(shell git rev-parse --show-toplevel)
endif

ifeq (, $(findstring common.mk, $(MAKEFILE_LIST)))
include $(ROOTDIR)/lib/make/common.mk
endif

# Common variables
SHELL =/bin/bash -o pipefail
VERSION      := $(COMMIT)
LDFLAGS      := -X main.build=$(VERSION) -X main.serviceName=$(SERVICE_NAME)
TARGET_DIR   := targets
ALL_PACKAGES := ./...

ifndef MODNAME
MODNAME := leapinc.co/platform
endif
ifndef CMD_PATH
CMD_PATH := ./cmd
endif

OS   := $(shell go env GOOS)
ARCH := $(shell go env GOARCH)
GO   := CGO_ENABLED=0 GOOS=$(OS) GOARCH=$(ARCH) go
CGO  := CGO_ENABLED=1 GOOS=$(OS) GOARCH=$(ARCH) go

GOMOD   := $(GO) mod
GOBUILD := $(GO) build
GORUN   := $(GO) run
GOTEST  := $(GO) test -cover
GOBENCH  := $(GO) test -run ZZZ -bench .

# GO_CMDS is useful in Go monorepo situations with multiple commands in one project
ifneq (., $(CMD_PATH))
GO_CMDS := $(notdir $(shell test -d $(CMD_PATH) && find $(CMD_PATH)/* -maxdepth 1 -type d))
endif

GO_MODULE := $(shell cat go.mod | grep '^module' | awk '{print $$2}')

# Initialization

.PHONY: go.setup

go.setup:
	@$(GOMOD) tidy && $(GOMOD) vendor

ifdef GO_CMDS
.PHONY: $(GO_CMDS) # allows for targeting any service under the $CMD_PATH

$(GO_CMDS):
	@echo "# setting SERVICE_NAME=$@"
	$(eval SERVICE_NAME=$@)
	$(eval CMD_PATH=$(CMD_PATH)/$@)
endif

# Building

.PHONY: build race

build: fmt go.tidy
	@echo "# building $(SERVICE_NAME) from $(CMD_PATH)"
	$(GOBUILD) -ldflags "$(LDFLAGS)" \
        -mod=readonly -v -o $(TARGET_DIR)/$(SERVICE_NAME) \
		$(CMD_PATH)

race:
	@echo "# building $(SERVICE_NAME) from $(CMD_PATH) with race-conditions check"
	$(CGO) build -race -ldflags "$(LDFLAGS)" \
        -mod=readonly -v -o $(TARGET_DIR)/$(SERVICE_NAME) \
		$(CMD_PATH)

# Tools

.PHONY: go.tidy go.tools fmt vet clean dist-clean

go.tidy:
	@echo "# running go mod tidy"
	@$(GOMOD) tidy

# only install tools if tools/ dir exists
go.tools: go.tidy
	@if [ -d tools ]; then \
		@echo "# installing tools from $(GO_MODULE)/tools"; \
		@$(GO) install $$($(GO) list -f '{{join .Imports " " }}' -tags tools $(GO_MODULE)/tools); \
	fi

clean:
	@$(GO) clean --modcache
	rm -fr $(TARGET_DIR)/*

dist-clean: clean
	rm -fr ./vendor

fmt:
	@$(GO) fmt $(ALL_PACKAGES)

vet:
	@$(GO) vet $(ALL_PACKAGES)


# Testing
.PHONY: test

.unit_test:
	@$(CGO) test -cover -race $(ALL_PACKAGES) | grep -v "no test files"

test: .unit_test
