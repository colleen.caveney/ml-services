ifndef ROOTDIR
ROOTDIR := $(shell git rev-parse --show-toplevel)
endif

ifeq (, $(findstring common.mk, $(MAKEFILE_LIST)))
include $(ROOTDIR)/lib/make/common.mk
endif

## localdev management

LEAP_CONFIG=$(HOME)/.config/leap
LEAP_HOME=$(HOME)/.local/share/leap

LOCALDEV=localdev
LOCALDEV_RC=$(LEAP_CONFIG)/$(LOCALDEV).rc
LOCALDEV_HOME=$(LEAP_HOME)/$(LOCALDEV)
LOCALDEV_USER_HOME=$(LOCALDEV_HOME)/home
LOCALDEV_IMAGE=$(NAMESPACE)/$(LOCALDEV)
LOCALDEV_USER=leap

# this def is used to write out the latest rc file
define localdev_rc
NAME=localdev

# local settings
LOCAL_HOME=$(LOCALDEV_USER_HOME)

# container settings
IMAGE_NAME=$(LOCALDEV_IMAGE)
CONTAINER_NAME=$(LOCALDEV)
CONTAINER_USER=$(LOCALDEV_USER)
CONTAINER_UID=$(shell id -u)
CONTAINER_GID=$(shell id -g)

# docker settings
DOCKERFILE=$(ROOTDIR)/$(LOCALDEV).Dockerfile
# one day mac will support "host" networking; until then, we must be specific
EXPOSE_PORTS="3000 3040 8080 8081 9515 22"
DOCKER_RUN_OPTS="--hostname $(LOCALDEV) -v $(ROOTDIR):/leap/platform -v $(LOCALDEV_USER_HOME):/home/$(LOCALDEV_USER)"
endef

export localdev_rc

OS := $(shell uname -s)

ifeq ($(OS),Linux)
	SSH_KEYGEN_CMD=/usr/bin/ssh-keygen -f /home/$(LOCALDEV_USER)/.ssh/id_rsa -t rsa -b 4096 -N ''
else
	SSH_KEYGEN_CMD=/usr/bin/ssh-keygen -f /home/$(LOCALDEV_USER)/.ssh/id_rsa -t rsa -b 4096 -N \'\'
endif

## main targets

# ssh

.PHONY: localdev.ssh.create localdev.ssh.rm localdev.ssh.recreate

localdev.ssh.rm:
	@echo "! remove $(LOCALDEV) ssh key for $(LOCALDEV_USER)"
	rm -f $(LOCALDEV_USER_HOME)/.ssh/id_rsa*

localdev.ssh.create:
	@if [ ! -f $(LOCALDEV_USER_HOME)/.ssh/id_rsa ]; then \
		echo "! generating $(LOCALDEV) ssh key for $(LOCALDEV_USER)"; \
		$(ROOTDIR)/bin/leap-docker-run -c $(LOCALDEV_RC) -- $(SSH_KEYGEN_CMD); \
	fi

localdev.ssh.recreate: localdev.ssh.rm localdev.ssh.create

# build and run

.PHONY: localdev.build localdev.run

# TODO look at https://github.com/mageddo/dns-proxy-server
#      this could get us around using `--net host` and give
#      us the ability to use localdev as the hostname for
#      docker container from the host (our laptop). it's a
#      bit invasive though, in that it takes over your
#      /etc/resolv.conf temporarily :/

localdev.build:
	$(ROOTDIR)/bin/leap-docker-run -bxc $(LOCALDEV_RC)

localdev.run:
	@$(ROOTDIR)/bin/leap-docker-run -c $(LOCALDEV_RC)

# admin

.PHONY: localdev.setup localdev.init localdev.rc

localdev.init:
	@mkdir -p $(LEAP_CONFIG)
	@echo "$$localdev_rc" > $(LEAP_CONFIG)/$(LOCALDEV).rc

	@mkdir -p $(LOCALDEV_USER_HOME)
	@${ROOTDIR}/bin/leap-localdev-home-init $(LOCALDEV_USER_HOME)

localdev.rc:
	@cat $(LOCALDEV_RC)

localdev.setup: localdev.init localdev.build localdev.ssh.create
