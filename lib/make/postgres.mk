ifndef ROOTDIR
ROOTDIR := $(shell git rev-parse --show-toplevel)
endif

## migrations

.PHONY: start-postgres initialize-db

ifndef $(LEAPDB_HOST)
LEAPDB_HOST := 127.0.0.1
endif
ifndef $(LEAPDB_NAME)
LEAPDB_NAME := leap
endif
ifndef $(LEAPDB_USER)
LEAPDB_USER := leap
endif
ifndef $(LEAPDB_PASSWORD)
LEAPDB_PASSWORD := password
endif

start-postgres: require-local
	@echo "# starting postgres"
	sudo service postgresql start

initialize-db: require-local
	@echo "# initialize leap db"
	sudo -u postgres psql -f $(ROOTDIR)/lib/db/postgres/leapdb-bootstrap.sql

psql: require-local
	PGPASSWORD=$(LEAPDB_PASSWORD) psql --username=$(LEAPDB_USER) --dbname=$(LEAPDB_NAME)
