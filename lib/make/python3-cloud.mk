ifndef ROOTDIR
ROOTDIR := $(shell git rev-parse --show-toplevel)
endif

ifeq (, $(findstring python3.mk, $(MAKEFILE_LIST)))
include $(ROOTDIR)/lib/make/python3.mk
endif

ifeq (, $(findstring cloud-function.mk, $(MAKEFILE_LIST)))
include $(ROOTDIR)/lib/make/cloud-function.mk
endif

## testing

# run func locally with an http server

.PHONY: cloud-func-server cloud-func-server-debug

FUNC_SERVER_FILE := server.py

ifndef FLASK_RUN_HOST
FLASK_RUN_HOST := 0.0.0.0
endif
ifndef FLASK_RUN_PORT
FLASK_RUN_PORT := 8080
endif

FUNC_TEST_ENV = SERVICE_ENV=$(SERVICE_ENV)
FUNC_TEST_ENV += $(shell test -f $(DOTENV) && cat $(DOTENV))

cloud-func-server:
	FLASK_APP=$(FUNC_SERVER_FILE) FLASK_RUN_HOST=$(FLASK_RUN_HOST) FLASK_RUN_PORT=$(FLASK_RUN_PORT) $(FUNC_TEST_ENV) \
		$(PIPENV) run python -m gunicorn -c python:gunicorn_config app:APP --reload -w 1

cloud-func-server-debug:
	FLASK_ENV=development \
		FLASK_APP=$(FUNC_SERVER_FILE) FLASK_RUN_HOST=$(FUNC_RUN_HOST) FLASK_RUN_PORT=$(FLASK_RUN_PORT) $(FUNC_TEST_ENV) \
		$(PIPENV) run python -m flask run

# run func locally with as topic subscriber

.PHONY: cloud-func-trigger cloud-func-trigger-debug

cloud-func-trigger:
	$(FUNC_TEST_ENV) $(PIPENV) run python -m trigger

cloud-func-trigger-debug:
	$(FUNC_TEST_ENV) $(PIPENV) run python -m trigger --debug

## distribution

.PHONY: python3-cloud-func-dist python3-cloud-func-push

# default FUNC_FILES; you should define your own if you need to archive another set of files
FUNC_FILES := main.py const.py $(REQUIREMENTS_TXT)

python3-cloud-func-dist: requirements cloud-func-dist

python3-cloud-func-push: require.service-env requirements cloud-func-push
