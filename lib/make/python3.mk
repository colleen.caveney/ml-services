ifndef ROOTDIR
ROOTDIR := $(shell git rev-parse --show-toplevel)
endif

ifeq (, $(findstring common.mk, $(MAKEFILE_LIST)))
include $(ROOTDIR)/lib/make/common.mk
endif

PIPENV           := pipenv
REQUIREMENTS_TXT := requirements.txt
# the pattern for a single test is <file>::<class name>::<function name> so an example would be
# ./tests/api/client/test_client_get.py::TestClientGet::test_failure_client_not_found
PYTEST_PATH	 ?= "./tests/api/client/test_client_get.py::TestClientGet::test_failure_client_not_found"

PYTEST_SINGLE = $(PYTEST_PATH)

.PHONY: python-install repl shell test requirements clean-requirements test-changes clean

# setup pipenv and deps
python-install:
	@$(PIPENV) install -d

# activate a repl
repl:
	@$(PIPENV) run python

# activate a virtual environment in a shell
shell:
	@$(PIPENV) shell

# run tests
test-env:
	@echo "# switching to testing environment"
	$(eval SERVICE_ENV=test)
	$(eval GCP_PROJECT=leap-platform-test)
	$(eval DOCKER_REMOTE_REPO=gcr.io/$(GCP_PROJECT))

TEST_ENV = SERVICE_ENV=$(SERVICE_ENV) FLASK_SKIP_DOTENV=1
PYTEST_ARGS :=

benchmark: test-env
	$(TEST_ENV) $(PIPENV) run python -m pytest -v --disable-warnings -- ./benchmarks

test: test-env
	$(TEST_ENV) $(PIPENV) run python -m pytest -v --disable-warnings $(PYTEST_ARGS) -- ./tests

test-changes: test-env
	$(TEST_ENV) $(PIPENV) run python -m pytest -vv --disable-warnings -- \
		$(shell git status -s -- ./tests/ | grep -vE '^\s*[RD]' | grep -E 'test[^/]*.py$$' | awk '{print $$2}') \
		$(shell git status -s -- ./tests/ | grep -E '^\s*[R]' | grep -E 'test[^/]*.py$$' | awk '{print $$4}')

test-single: test-env
	$(TEST_ENV) pipenv run python -m pytest -v -- $(PYTEST_SINGLE)

# generate output in requirements.txt format file from pipenv
requirements:
	@$(PIPENV) requirements > $(REQUIREMENTS_TXT)

clean-requirements:
	@rm $(REQUIREMENTS_TXT)

clean:
	@$(PIPENV) --rm
