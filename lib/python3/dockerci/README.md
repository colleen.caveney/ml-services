# DockerCI - Python

The DockerCI - Python library supports a base [docker image](./Dockerfile) for
use by python3 projects in a continuous integration (CI) environment. The general
intent is provide a system configuration with a baseline set of tools, such as:
`python3`, `pipenv`, `git`, `make`, etc. in order to avoid needing to perform
installation steps for these tools in a particular project's CI script such that
these scripts need only provide calls to `make <target>`.

The `gcloud` toolkit will also be installed to assist with deployments, though
your specific project will need to provide service-account keys and so forth.

See the [Dockerfile](./Dockerfile) for details on what is specifically
installed.

## Usage

In your project's `.gitlab-ci.yml` file, assuming you have a standard configuration,
you can minimally add the following to get a working CI setup:

```yaml
.conditions:MYPROJECT:
    only:
        changes:
            - "lib/python3/leap/**" # NOTE: optional
            - "services/MYPROJECT/**"

.cache:MYPROJECT:
    cache:
        key:
            files:
                - Pipfile.lock
            prefix: MYPROJECT
        paths:
            - services/MYPROJECT/.venv

build:MYPROJECT:
    extends:
        - .image:python # THIS IS THE IMPORTANT LINE
        - .conditions:only-mergable
        - .conditions:MYPROJECT
        - .cache:MYPROJECT
    stage: build
    script:
        - cd services/MYPROJECT
        - make setup

test:MYPROJECT:
    stage: test
    extends:
        - .image:python # THIS IS THE IMPORTANT LINE
        - .conditions:only-mergable
        - .conditions:MYPROJECT
        - .cache:MYPROJECT
        - .cache:readonly
    script:
        - cd services/myservice
        - make test
```

You will need to adjust paths and YAML target names to reflect your service; e.g.
replace `MYPROJECT` with your project's directory name.
