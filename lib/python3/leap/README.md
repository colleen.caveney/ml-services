# Leap - Python3 Package

Shared Leap python code. Generally this should be installed as an editable package:

```sh
pipenv install -e some/path/to/leap
```

## Development & Testing

Before beginning any new work, make sure that your `pipenv` is up-to-date:

```sh
make setup
```

This will install any missing dependencies (including dev); it won't hurt to run this multiple times. After that, tests are run like so:

```sh
$ make test
```

## TODO:

- complete these docs
