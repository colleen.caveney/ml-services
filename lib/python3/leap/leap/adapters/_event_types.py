"""Leap Event types"""

from enum import unique

from leap.std.enum import TypedEnum


@unique
class EventType(str, TypedEnum):  # subclassing with str makes this type JSON serializable
    """Indicates that the event-type is undefined."""
    UNDEFINED = "undefined"

    """Indicates that there is no operation to be performed.

    This is not the same as UNDEFINED. It is meant to explictly indicate that
    the event-type was recognized, but nothing should happen.
    """
    NOOP = "noop"

    # events

    """ Indicates that a customer has requested their data from a brand. """
    CUSTOMERS_DATA_REQUEST = "customers:data_request"

    """ Indicates that a brand has requested for customer data (and orders) to be purged. """
    CUSTOMERS_REDACT = "customers:redact"

    """ Indicates that a brand uninstalled the app and their data needs to be removed from our database. """
    SHOP_REDACT = "shop:redact"

    """Indicates that a decommission (sometimes 'uninstall') event was triggered."""
    DECOM = "decom"

    """Indicates that a webhook was or is to be regstered."""
    WEBHOOK_REGISTER = "webhook:register"

    """Indicates a request to install webhooks.

    Generally, this is going to be case when a new Adapter is created, an API version
    is updated, or the host endpoint has changed. Could apply to a single Adapter and/or
    Client or could apply to every Adapter for every Client.
    """
    BATCH_WEBHOOK_INSTALL = "batch:webhook:install"

    # order management

    """Indicates that an Order was created."""
    ORDER_CREATE = "order:create"

    """Indicates that an Order's details were updated, but not its line-items."""
    ORDER_UPDATE = "order:update"
    # TODO break this up into possibly multiple, more specific events

    """Indicates that an Order's line-items were modified."""
    ORDER_UPDATE_LI = "order:update:li"

    """Indicates that an Order had one or more payments applied to it."""
    ORDER_PAYMENT = "order:payment"

    """Indicates that an Order was cancelled."""
    ORDER_CANCEL = "order:cancel"

    """Indicates that an Order was deleted."""
    ORDER_DELETE = "order:delete"

    """Indicates that a Fulfillment was created on an Order."""
    FULFILLMENT_CREATE = "fulfillment:create"

    """Indicates that a Fulfillment was updated on an Order."""
    FULFILLMENT_UPDATE = "fulfillment:update"

    """Indicates that a Fulfillment was cancelled for an Order."""
    FULFILLMENT_CANCEL = "fulfillment:cancel"

    """Indicates that a Refund was issued."""
    REFUND_CREATE = "refund:create"

    # possibly @deprecated
    """Indicates that an Payment was captured."""
    PAYMENT_CAPTURE = "payment:capture"

    # product, sku, and inventory management

    """Indicates that a SKU was deleted."""
    SKU_DELETE = "sku:delete"

    """Indicates that a SKU can no longer have Inventory at a Store."""
    INVENTORY_DISCONNECT = "inventory:disconnect"

    """Indicates that a SKU can now have Inventory at a Store."""
    INVENTORY_CONNECT = "inventory:connect"

    """Indicates that inventory level for a SKU at a Store has been adjusted."""
    INVENTORY_ADJUST = "inventory:adjust"

    """Indicates that a product has been updated."""
    PRODUCT_UPDATE = "product:update"

    """Indicates that a product has been created."""
    PRODUCT_CREATE = "product:create"

    """Indicates that a product has been deleted."""
    PRODUCT_DELETE = "product:delete"

    PRODUCT_LISTING_ADD = "product_listings:add"
    PRODUCT_LISTING_UPDATE = "product_listings:update"

    # internal, batch events

    BATCH_INVENTORY_IMPORT = "batch:inventory:import"
    BATCH_PRODUCT_CATALOG_IMPORT = "batch:product_catalog:import"
    BATCH_PRODUCT_CATALOG_REMOVE = "batch:product_catalog:remove"

    BATCH_ORDER_SYNC = "batch:order:sync"

    """Internal event indicating a desire to merge SKU related records."""
    BATCH_SKU_MERGE = "batch:sku:merge"

    """Internal event indicating a desire to "clean up" SKU related records."""
    BATCH_SKU_CLEAN = "batch:sku:clean"

    BATCH_INVENTORY_PROVISION = "batch:inventory:provision"
