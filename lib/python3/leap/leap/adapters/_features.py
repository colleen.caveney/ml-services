"""Leap Adapter features
"""

from enum import Enum, unique

@unique
class Feature(str, Enum): # subclassing with str makes this type JSON serializable
    """Indicates that inventory tracking is enabled."""
    TRACK_INVENTORY = "inventory:track"
