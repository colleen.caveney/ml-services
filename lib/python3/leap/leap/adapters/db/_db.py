import abc
from enum import Enum, unique
import logging
from typing import Dict, Iterator, List, Optional, Tuple, cast

from ._models import Action, AdapterConfig, AdapterLocation, AdapterSKU, AdapterWebhook, Brand, \
     Entity, Job, Location, SKU, SKUInventory, Service, ServiceAccessPrivilege, Store, Subscription, User


@unique
class QueryOp(str, Enum):
    """Indicates which query operation to use."""
    STARTS_WITH = "startsWith"


@unique
class OrderDirection(str, Enum):
    ASCENDING = "ASC"
    DESCENDING = "DESC"

    @classmethod
    def names(cls) -> List[str]:
        return cast(List[str], cls._member_names_)  # pylint: disable=no-member


class DB(abc.ABC):

    # services

    @abc.abstractmethod
    def query_service(self, id: str) -> Optional[Service]:
        """Retrieve a Service given its id."""
        pass

    @abc.abstractmethod
    def save_service(self, service: Service):
        """Save a Service."""
        pass

    # service access privileges

    @abc.abstractmethod
    def service_access_privileges(self, limit: int = 0, **filters: dict) -> Iterator[ServiceAccessPrivilege]:
        """Stream all ServiceAccessPrivileges matching the provided filters.

        If limit is provided and greater-than zero (0), only that many records will be returned.
        """
        pass

    @abc.abstractmethod
    def service_access_privilege(self, **filters: dict) -> Optional[ServiceAccessPrivilege]:
        """Retrieve a ServiceAccessPrivilege matching the provided filters or None.

        With no filters provided, there's no guarantee which ServiceAccessPrivilege will be returned.
        """
        pass

    @abc.abstractmethod
    def save_service_access_privilege(self, sap: ServiceAccessPrivilege):
        """Save a ServiceAccessPrivilege."""
        pass

    @abc.abstractmethod
    def delete_service_access_privilege(self, sap: ServiceAccessPrivilege):
        """Delete a ServiceAccessPrivilege."""
        pass

    # brands

    @abc.abstractmethod
    def brands(self, limit: int = 0, **filters: dict) -> Iterator[Brand]:
        """Stream all brands matching the provided filters

        If limit is provided and greater-than zero (0), only that many records will be returned
        """
        pass

    @abc.abstractmethod
    def brand(self, **filters: dict) -> Optional[Brand]:
        """Retrieve a Brand matching the provided filters or None

        With no filters provided, there's no guarantee which Brand will be returned
        """
        pass

    @abc.abstractmethod
    def query_brand(self, id: str) -> Optional[Brand]:
        """Retrieve a Brand given its id"""
        pass

    @abc.abstractmethod
    def save_brand(self, brand: Brand):
        """Save a Brand"""
        pass

    @abc.abstractmethod
    def delete_brand(self, brand: Brand):
        """Delete a Brand"""
        pass

    # users
    @abc.abstractmethod
    def users(self, limit: int = 0, **filters: dict) -> Iterator[User]:
        """Stream all Users matching the provided filters.

        If limit is provided and greater-than zero (0), only that many records will be returned.
        """
        pass

    @abc.abstractmethod
    def user(self, **filters: dict) -> Optional[User]:
        """Retrieve a User matching the provided filters or None.

        With no filters provided, there's no guarantee which User will be returned.
        """
        pass

    @abc.abstractmethod
    def save_user(self, user: User):
        """Save a User."""
        pass

    @abc.abstractmethod
    def delete_user(self, user: User):
        """Delete a User."""
        pass

    @abc.abstractmethod
    def update_user(self, id: str, update_map: dict):
        """Update a user given a user id"""
        pass

    # adapters

    @abc.abstractmethod
    def adapters(self, limit: int = 0, **filters: dict) -> Iterator[AdapterConfig]:
        """Stream all Adapters matching the provided filters.

        If limit is provided and greater-than zero (0), only that many records will be returned.
        """
        pass

    @abc.abstractmethod
    def adapter(self, **filters: dict) -> Optional[AdapterConfig]:
        """Retrieve an AdapterConfig matching the provided filters or None.

        With no filters provided, there's no guarantee which AdapterConfig will be returned.
        """
        pass

    @abc.abstractmethod
    def query_adapter(self, id: str) -> Optional[AdapterConfig]:
        """Retrieve an AdapterConfig configuration given its id."""
        pass

    @abc.abstractmethod
    def query_adapters_for_client(self, client_id: str, **filters: dict) -> List[AdapterConfig]:
        """Retrieve a list of AdapterConfig configurations for a given client_id.

        Keys and values provided via `filters` will be used to filter results such that
        each key should map to a corresponding property of an AdapterConfig and the
        values should be equal.

        In this way you can, for instance, query for the adapters of a client having
        a specific service_id:

            mydb.query_adapters_for_client("client-001", service_id="shopify")
        """
        pass

    @abc.abstractmethod
    def save_adapter(self, adapter: AdapterConfig):
        """Save an AdapterConfig configuration."""
        pass

    @abc.abstractmethod
    def delete_adapter(self, adapter: AdapterConfig):
        """Delete an AdapterConfig configuration."""
        pass

    # adapter locations

    @abc.abstractmethod
    def adapter_locations(self, limit: int = 0, **filters: dict) -> Iterator[AdapterLocation]:
        """Stream all AdapterLocations matching the provided filters.

        If limit is provided and greater-than zero (0), only that many records will be returned.
        """
        pass

    @abc.abstractmethod
    def adapter_location(self, **filters: dict) -> Optional[AdapterLocation]:
        """Retrieve an AdapterLocation matching the provided filters or None.

        With no filters provided, there's no guarantee which AdapterLocation will be returned.
        """
        pass

    @abc.abstractmethod
    def save_adapter_location(self, adapter_location: AdapterLocation):
        """Save an AdapterLocation."""
        pass

    # adapter webhooks

    @abc.abstractmethod
    def adapter_webhooks(self, limit: int = 0, **filters: dict) -> Iterator[AdapterWebhook]:
        """Stream all AdapterWebhook matching the provided filters.

        If limit is provided and greater-than zero (0), only that many records will be returned.
        """
        pass

    @abc.abstractmethod
    def adapter_webhook(self, **filters: dict) -> Optional[AdapterWebhook]:
        """Retrieve an AdapterWebhook matching the provided filters or None.

        With no filters provided, there's no guarantee which AdapterWebhook will be returned.
        """
        pass

    @abc.abstractmethod
    def save_adapter_webhook(self, webhook: AdapterWebhook):
        """Save an AdapterWebhook."""
        pass

    @abc.abstractmethod
    def delete_adapter_webhook(self, webhook: AdapterWebhook):
        """Delete a AdapterWebhook."""
        pass

    # entities

    @abc.abstractmethod
    def entities(self, limit: int = 0, **filters: dict) -> Iterator[Entity]:
        pass

    @abc.abstractmethod
    def query_entity_for_adapter(self, adapter_id: str, entity_id: str, **filters: dict) -> Optional[Entity]:
        pass

    @abc.abstractmethod
    def save_adapter_entity(self, entity: Entity):
        """Save an Adapter's variant'."""
        pass

    @abc.abstractmethod
    def delete_adapter_entity(self, entity: Entity):
        """Delete an entity."""
        pass

    ## skus and inventory

    @abc.abstractmethod
    def sku(self, id: str) -> Optional[SKU]:
        """Retrieve a SKU given its id."""
        pass

    @abc.abstractmethod
    def skus(self, limit: int = 0, **filters: dict) -> Iterator[SKU]:
        """Stream all SKUs matching the provided filters.

        If limit is provided and greater-than zero (0), only that many records will be returned.
        """
        pass

    @abc.abstractmethod
    def delete_sku(self, sku: SKU):
        """Delete a SKU."""
        pass

    @abc.abstractmethod
    def save_sku(self, sku: SKU):
        """Save a SKU."""
        pass

    @abc.abstractmethod
    def adapter_sku(self, **filters: dict) -> Optional[AdapterSKU]:
        """Retrieve a AdapterSKU matching the provided filters or None.

        With no filters provided, there's no guarantee which AdapterSKU will be returned.
        """
        pass

    @abc.abstractmethod
    def adapter_skus(self, limit: int = 0, **filters: dict) -> Iterator[AdapterSKU]:
        """Stream all AdapterSKU matching the provided filters.

        If limit is provided and greater-than zero (0), only that many records will be returned.
        """
        pass

    @abc.abstractmethod
    def adapter_sku_via_adapter(self, adapter_key: Tuple[str, str]) -> Optional[AdapterSKU]:
        """Retrieve an AdapterSKU given a key representing the Adapter id and the Adapter's sku reference-id.

        The Adapter's sku reference-id is the id of the sku/variant as it's stored in the Service instance the
        Adapter represents (e.g. inventory_item_id for Shopify).
        """
        pass

    @abc.abstractmethod
    def delete_adapter_sku(self, av: AdapterSKU):
        """Delete an AdapterSKU."""
        pass

    @abc.abstractmethod
    def save_adapter_sku(self, av: AdapterSKU):
        """Save an AdapterSKU."""
        pass

    @abc.abstractmethod
    def sku_inventory(self, **filters: dict) -> Optional[SKUInventory]:
        """Retrieve a SKUInventory matching the provided filters or None.

        With no filters provided, there's no guarantee which SKUInventory will be returned.
        """
        pass

    @abc.abstractmethod
    def sku_inventories(self, limit: int = 0, **filters: dict) -> Iterator[SKUInventory]:
        """Stream all SKUInventory records matching the provided filters.

        If limit is provided and greater-than zero (0), only that many records will be returned.
        """
        pass

    @abc.abstractmethod
    def save_sku_inventory(self, inv: SKUInventory):
        """Save a SKUInventory."""
        pass

    @abc.abstractmethod
    def delete_sku_inventory(self, inv: SKUInventory):
        """Delete a SKUInventory."""
        pass

    # locations

    @abc.abstractmethod
    def locations(self, **filters: dict) -> Iterator[Location]:
        """Stream all Locations matching the provided filters."""
        pass

    @abc.abstractmethod
    def query_location(self, id: str) -> Optional[Location]:
        """Retrieve a Location given its id."""
        pass

    @abc.abstractmethod
    def save_location(self, location: Location):
        """Save a Location."""
        pass

    @abc.abstractmethod
    def update_location(self, id: str, update_map: map):
        """Update a location given a location id"""
        pass

    @abc.abstractmethod
    def delete_location(self, location: Location):
        """Delete a Location."""
        pass

    # stores

    @abc.abstractmethod
    def stores(self, **filters: dict) -> Iterator[Store]:
        """Stream all Stores matching the provided filters."""
        pass

    @abc.abstractmethod
    def query_store(self, id: str) -> Optional[Store]:
        """Retrieve a store given its id."""
        pass

    @abc.abstractmethod
    def save_store(self, store: Store):
        """Save a store."""
        pass

    @abc.abstractmethod
    def update_store(self, id: str, update_map: map):
        """Update a store given a store id"""
        pass

    @abc.abstractmethod
    def delete_store(self, store: Store):
        """Delete a store."""
        pass

    # jobs

    @abc.abstractmethod
    def jobs(self, **filters: dict) -> Iterator[Job]:
        """Stream all Jobs matching the provided filters."""
        pass

    @abc.abstractmethod
    def query_job(self, id: str) -> Optional[Job]:
        """Retrieve a Job given its id."""
        pass

    @abc.abstractmethod
    def save_job(self, job: Job):
        """Save a Job."""
        pass

    # subscriptions

    @abc.abstractmethod
    def subscriptions(self, **filters: dict) -> Iterator[Subscription]:
        """Stream all Subscriptions matching the provided filters.

        If limit is provided and greater-than zero (0), only that many records will be returned.
        """
        pass

    @abc.abstractmethod
    def subscription(self, **filters: dict) -> Optional[Subscription]:
        """Retrieve a Subscription matching the provided filters or None.

        With no filters provided, there's no guarantee which Subscription will be returned.
        """
        pass

    @abc.abstractmethod
    def save_subscription(self, subscription: Subscription):
        """Save a Subscription."""
        pass

    # actions

    @abc.abstractmethod
    def actions(self, **filters: dict):
        """Stream all Actions matching the provided filters.

        If limit is provided and greater-than zero (0), only that many records will be returned.
        """
        pass

    @abc.abstractmethod
    def action(self, **filters: dict) -> Optional[Action]:
        """Retrieve a Action matching the provided filters or None.

        With no filters provided, there's no guarantee which Action will be returned.
        """
        pass

    @abc.abstractmethod
    def save_action(self, action: Action):
        """Save an Action."""
        pass
