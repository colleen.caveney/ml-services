from datetime import datetime
from typing import Iterator, List, Optional, Tuple, Union
import uuid

from google.cloud import firestore

from leap.std.hashing.md5 import hash_object
from ._db import DB, OrderDirection, QueryOp
from ._models import Action, AdapterConfig, AdapterLocation, AdapterStore, AdapterSKU, AdapterWebhook, AuditLog, Brand, BrandAsset, \
    BrandUser, Entity, Job, Location, Space, Message, RelatedBrand, SKU, SKUInventory, Service, ServiceAccessPrivilege, Store, \
    Subscription, User
from .interfaces import DBInterface


def _apply_special_fields_from_query(query: firestore.Query, filters: dict) -> firestore.Query:
    if "order_by" in filters:
        # unpack arguments since the direction will be a secondary arg
        order_by = filters.pop("order_by")
        order_direction = list(order_by.values())[0]
        order_key = list(order_by.keys())[0]
        if order_direction == OrderDirection.DESCENDING:
            query = query.order_by(order_key, direction="DESCENDING")
        else:
            query = query.order_by(order_key)
    if "start_after" in filters:
        query = query.start_after(filters.pop("start_after"))
    return query


class FirestoreDB(DB, DBInterface):

    def __init__(self, fs_client: firestore.Client):
        if not fs_client:
            raise ValueError("firestore client not provided")
        self.fs_client = fs_client

    def _query_resource_by_id(self, collection: str, id: str) -> Optional[dict]:
        if not id:
            raise ValueError("id not provided")
        return self.fs_client.document("{0}/{1}".format(collection, id)).get().to_dict()

    def _save_resource(self, collection: str, id: Optional[str], resource: dict):
        doc_ref = self.fs_client.collection(collection).document(id)
        doc_ref.set(resource)

    def _delete_resource(self, collection: str, id: Optional[str]):
        doc_ref = self.fs_client.collection(collection).document(id)
        doc_ref.delete()

    def _query(self, coll_name: str, limit: int = 0, **filters: dict) -> Iterator[firestore.DocumentSnapshot]:
        query = _apply_special_fields_from_query(self.fs_client.collection(coll_name), filters)

        for field, value in filters.items():
            if isinstance(value, set):
                query = query.where(field, "in", list(value))
            elif isinstance(value, tuple):
                if value[0] == QueryOp.STARTS_WITH:
                    query = query.where(field, '>=', value[1]).where(field, '<=', value[1] + '~')
            else:
                query = query.where(field, "==", value)

        if limit:
            query = query.limit(limit)
        return query.stream()

    # services

    def services(self, limit: int = 0, **filters: dict) -> Iterator[Service]:
        for doc in self._query("services", limit, **filters):
            yield Service.build(doc.to_dict())

    def query_service(self, id: str) -> Optional[Service]:
        svc_dict = self._query_resource_by_id("services", id)
        if svc_dict:
            return Service.build(svc_dict)
        return None

    def save_service(self, service: Service):
        service.updated_at = datetime.now()
        self._save_resource("services", service.id, service.to_dict())

    # service access privileges

    def service_access_privileges(self, limit: int = 0, **filters: dict) -> Iterator[ServiceAccessPrivilege]:
        for doc in self._query("service_access_privileges", limit, **filters):
            yield ServiceAccessPrivilege.build(doc.to_dict())

    def service_access_privilege(self, **filters: dict) -> Optional[ServiceAccessPrivilege]:
        for c in self.service_access_privileges(limit=1, **filters):
            return c
        return None

    def save_service_access_privilege(self, sap: ServiceAccessPrivilege):
        sap.updated_at = datetime.now()
        self._save_resource("service_access_privileges", sap.id, sap.to_dict())

    def save_serviceaccessprivilege(self, sap: ServiceAccessPrivilege):
        self.save_service_access_privilege(sap)

    def delete_service_access_privilege(self, sap: ServiceAccessPrivilege):
        self._delete_resource("service_access_privileges", sap.id)

    # brands

    def brands(self, limit: int = 0, **filters: dict) -> Iterator[Brand]:
        for doc in self._query("clients", limit, **filters):
            yield Brand.build(doc.to_dict())

    def brand(self, **filters: dict) -> Optional[Brand]:
        for c in self.brands(limit=1, **filters):
            return c
        return None

    def query_brand(self, id: str) -> Optional[Brand]:
        brand_d = self._query_resource_by_id("clients", id)
        if brand_d:
            return Brand.build(brand_d)
        return None

    def query_brands(self, **filters: dict):
        if 'id' in filters:
            return self.query_brand(filters['id'])
        else:
            return self.brands(**filters)

    def save_brand(self, brand: Brand):
        brand.updated_at = datetime.now()
        self._save_resource("clients", brand.id, brand.to_dict())

    def delete_brand(self, brand: Brand):
        self._delete_resource("clients", brand.id)

    def update_brand(self, brand: Brand):
        self.save_brand(brand)

    # related brands
    def related_brands(self, limit: int = 0, **filters: dict) -> Iterator[Brand]:
        for doc in self._query("related_brands", limit, **filters):
            yield RelatedBrand.build(doc.to_dict())

    def related_brand(self, **filters: dict) -> Optional[Brand]:
        for b in self.related_brands(limit=1, **filters):
            return b
        return None

    def query_related_brands(self, **filters: dict):
        if 'id' in filters:
            return self.related_brand(filters['id'])
        else:
            return self.related_brands(**filters)

    def save_relatedbrand(self, related_brand: RelatedBrand):
        self._save_resource("related_brands", related_brand.id, related_brand.to_dict())

    def delete_relatedbrand(self, related_brand: RelatedBrand):
        self._delete_resource("related_brands", related_brand.id)

    def update_relatedbrand(self, related_brand: RelatedBrand):
        self.save_relatedbrand(related_brand)

    # users

    def users(self, limit: int = 0, **filters: dict) -> Iterator[User]:
        for doc in self._query("users", limit, **filters):
            yield User.build(doc.to_dict())

    def user(self, **filters: dict) -> Optional[User]:
        for cu in self.users(limit=1, **filters):
            return cu
        return None

    def query_users(self, **filters: dict) -> Union[Optional[User], List[User]]:
        if 'id' in filters:
            return self.user(**filters)
        else:
            return self.users(**filters)

    def save_user(self, user: User):
        user.updated_at = datetime.now()
        self._save_resource("users", user.id, user.to_dict())

    def delete_user(self, user: User):
        self._delete_resource("users", user.id)

    def update_user(self, user: User):
        self.save_user(user)

    # brand users

    def client_users(self, limit: int = 0, **filters: dict) -> Iterator[BrandUser]:
        for doc in self._query("client_users", limit, **filters):
            yield BrandUser.build(doc.to_dict())

    def client_user(self, **filters: dict) -> Optional[BrandUser]:
        for cu in self.client_users(limit=1, **filters):
            return cu
        return None

    def query_client_users(self, **filters: dict) -> Union[Optional[BrandUser], List[BrandUser]]:
        if 'id' in filters:
            return self.client_user(**filters)
        else:
            return self.client_users(**filters)

    def query_brand_users(self, **filters: dict) -> Union[Optional[BrandUser], List[BrandUser]]:
        if 'id' in filters:
            return self.client_user(**filters)
        elif 'user_id' in filters and 'brand_id' in filters:
            return self.client_user(**filters)
        else:
            return self.client_users(**filters)

    def save_client_user(self, client_user: BrandUser):
        client_user.updated_at = datetime.now()
        self._save_resource("client_users", client_user.id, client_user.to_dict())

    def save_branduser(self, brand_user: BrandUser):
        self.save_client_user(brand_user)

    def delete_client_user(self, client_user: BrandUser):
        self._delete_resource("client_users", client_user.id)

    def delete_branduser(self, brand_user: BrandUser):
        self._delete_resource("client_users", brand_user.id)

    def update_branduser(self, brand_user: BrandUser):
        self.save_client_user(brand_user)



    # brand assets

    def client_assets(self, limit: int = 0, **filters: dict) -> Iterator[BrandAsset]:
        for doc in self._query("client_assets", limit, **filters):
            yield BrandAsset.build(doc.to_dict())

    def client_asset(self, **filters: dict) -> Optional[BrandAsset]:
        for cu in self.client_assets(limit=1, **filters):
            return cu
        return None

    def save_client_asset(self, client_asset: BrandAsset):
        client_asset.updated_at = datetime.now()
        self._save_resource("client_assets", client_asset.id, client_asset.to_dict())

    def delete_client_asset(self, client_asset: BrandAsset):
        client_asset.deleted = True
        self._save_resource("client_assets", client_asset.id, client_asset.to_dict())

    def query_brand_assets(self, **filters: dict):
        if 'id' in filters:
            return self.client_asset(**filters)
        else:
            return self.client_assets(**filters)

    def save_brandasset(self, asset: BrandAsset):
        self.save_client_asset(asset)

    def delete_brandasset(self, asset: BrandAsset):
        self.delete_client_asset(asset)

    # adapter configs

    def adapters(self, limit: int = 0, **filters: dict) -> Iterator[AdapterConfig]:
        for doc in self._query("adapters", limit, **filters):
            yield AdapterConfig.build(doc.to_dict())

    def adapter(self, **filters: dict) -> Optional[AdapterConfig]:
        for a in self.adapters(limit=1, **filters):
            return a
        return None

    def query_adapter(self, id: str) -> Optional[AdapterConfig]:
        adapter_dict = self._query_resource_by_id("adapters", id)
        if adapter_dict:
            return AdapterConfig.build(adapter_dict)
        return None

    def query_adapters(self, **filters: dict):
        if 'id' in filters:
            return self.query_adapter(filters['id'])
        elif 'brand_id' in filters:
            return self.query_adapters_for_client(filters['brand_id'], **filters)
        else:
            return self.adapters(**filters)

    def query_adapters_for_client(self, client_id: str, **filters: dict) -> List[AdapterConfig]:
        if not client_id:
            raise ValueError("client_id not provided")
        adapters: List[AdapterConfig] = []

        query = self.fs_client.collection("adapters").where("client_id", "==", client_id)
        for field, value in filters.items():
            query = query.where(field, "==", value)

        docs = query.stream()
        for doc in docs:
            adapters.append(AdapterConfig.build(doc.to_dict()))
        return adapters

    def save_adapter(self, adapter: AdapterConfig):
        adapter.updated_at = datetime.now()
        self._save_resource("adapters", adapter.id, adapter.to_dict())

    def save_adapterconfig(self, adapter: AdapterConfig):
        self.save_adapter(adapter)

    def delete_adapter(self, adapter: AdapterConfig):
        self._delete_resource("adapters", adapter.id)
        adapter_webhooks = self._query('adapter_webhooks', adapter_id=adapter.id)
        for i in adapter_webhooks:
            self._delete_resource("adapter_webhooks", i.get('id'))
        adapter_locations = self._query('adapter_locations', adapter_id=adapter.id)
        for i in adapter_locations:
            self._delete_resource("adapter_locations", i.get('id'))

    def delete_adapterconfig(self, adapter: AdapterConfig):
        self.delete_adapter(adapter)


    # adapter locations

    def adapter_locations(self, limit: int = 0, **filters: dict) -> Iterator[AdapterStore]:
        for doc in self._query("adapter_locations", limit, **filters):
            yield AdapterStore.build(doc.to_dict())

    def adapter_location(self, **filters: dict) -> Optional[AdapterStore]:
        for c in self.adapter_locations(limit=1, **filters):
            return c
        return None

    def query_adapter_stores(self, **filters: dict) -> Union[Optional[AdapterStore], List[AdapterStore]]:
        if 'id' in filters:
            return self.adapter_location(**filters)
        else:
            return list(self.adapter_locations(**filters))

    def save_adapter_location(self, adapter_location: AdapterLocation):
        adapter_location.updated_at = datetime.now()
        self._save_resource("adapter_locations", adapter_location.id, adapter_location.to_dict())

    def save_adapterlocation(self, adapter_location: AdapterLocation):
        self.save_adapter_location(adapter_location)

    def save_adapterstore(self, adapter_store: AdapterStore):
        self.save_adapter_location(adapter_store)

    # adapter webhooks

    def adapter_webhooks(self, limit: int = 0, **filters: dict) -> Iterator[AdapterWebhook]:
        for doc in self._query("adapter_webhooks", limit, **filters):
            yield AdapterWebhook.build(doc.to_dict())

    def adapter_webhook(self, **filters: dict) -> Optional[AdapterWebhook]:
        for d in self.adapter_webhooks(limit=1, **filters):
            return d
        return None

    def save_adapter_webhook(self, hook: AdapterWebhook):
        hook.updated_at = datetime.now()
        self._save_resource("adapter_webhooks", hook.id, hook.to_dict())

    def delete_adapter_webhook(self, hook: AdapterWebhook):
        self._delete_resource("adapter_webhooks", hook.id)

    # entities

    def entities(self, limit: int = 0, **filters: dict) -> Iterator[Entity]:
        for doc in self._query("entities", limit, **filters):
            yield Entity.build(doc.to_dict())

    def query_entity_for_adapter(self, adapter_id: str, entity_id: str, **filters: dict) -> Optional[Entity]:
        if not adapter_id:
            raise ValueError("adapter_id not provided")
        elif not entity_id:
            raise ValueError("entity_id not provided")
        entities: List[Entity] = []

        # using backspaces in adapter-id where-clause in case the adapter-id start with a number
        query = self.fs_client.collection("entities"). \
            where("type", "==", "order"). \
            where("adapter_entity_map.`{0}`".format(
                adapter_id), "==", entity_id)
        for field, value in filters.items():
            query = query.where(field, "==", value)

        docs = query.stream()
        for doc in docs:
            entities.append(Entity.build(doc.to_dict()))
        return entities[0] if entities else None

    def save_adapter_entity(self, entity: Entity):
        entity.updated_at = datetime.now()
        self._save_resource("entities", entity.id, entity.to_dict())

    def delete_adapter_entity(self, entity: Entity):
        self._delete_resource("entities", entity.id)

    def save_entity(self, entity: Entity):
        self.save_adapter_entity(entity)

    ## skus and inventory

    def sku(self, id: str) -> Optional[SKU]:
        data = self._query_resource_by_id("skus", id)
        if data:
            return SKU.build(data)
        return None

    def skus(self, limit: int = 0, **filters: dict) -> Iterator[SKU]:
        for doc in self._query("skus", limit, **filters):
            yield SKU.build(doc.to_dict())

    def delete_sku(self, sku: SKU):
        self._delete_resource("skus", sku.id)

    def save_sku(self, sku: SKU):
        sku.updated_at = datetime.now()
        self._save_resource("skus", sku.id, sku.to_dict())

    def adapter_sku(self, **filters: dict) -> Optional[AdapterSKU]:
        for asku in self.adapter_skus(limit=1, **filters):
            return asku
        return None

    def adapter_skus(self, limit: int = 0, **filters: dict) -> Iterator[AdapterSKU]:
        for doc in self._query("adapter_skus", limit, **filters):
            yield AdapterSKU.build(doc.to_dict())

    def adapter_sku_via_adapter(self, adapter_key: Tuple[str, str]) -> Optional[AdapterSKU]:
        if not adapter_key:
            raise ValueError("lookup key not provided")
        adapter_id, av_internal_id = adapter_key
        for doc in self.adapter_skus(limit=1, **{"adapter_id": adapter_id, "keys.inventory_item_id": av_internal_id}):
            return AdapterSKU.build(doc.to_dict())
        return None

    def delete_adapter_sku(self, asku: AdapterSKU):
        self._delete_resource("adapter_skus", asku.id)

    def save_adapter_sku(self, asku: AdapterSKU):
        asku.keys_hash = hash_object(asku.keys)
        asku.updated_at = datetime.now()
        self._save_resource("adapter_skus", asku.id, asku.to_dict())

    def sku_inventory(self, **filters: dict) -> Optional[SKUInventory]:
        for si in self.sku_inventories(limit=1, **filters):
            return si
        return None

    def sku_inventories(self, limit: int = 0, **filters: dict) -> Iterator[SKUInventory]:
        for doc in self._query("sku_inventories", limit, **filters):
            yield SKUInventory.build(doc.to_dict())

    def delete_sku_inventory(self, inv: SKUInventory):
        self._delete_resource("sku_inventories", inv.id)

    def save_sku_inventory(self, inv: SKUInventory):
        inv.updated_at = datetime.now()
        self._save_resource("sku_inventories", inv.id, inv.to_dict())

    # locations

    def locations(self, **filters: dict) -> Iterator[Space]:
        for doc in self._query("locations", limit=0, **filters):
            yield Space.build(doc.to_dict())

    def query_location(self, id: str) -> Optional[Space]:
        location_dict = self._query_resource_by_id("locations", id)
        if location_dict:
            return Space.build(location_dict)
        return None

    def query_spaces(self, **filters: dict):
        if 'id' in filters:
            id = filters['id']
            return self.query_location(id)
        else:
            return self.locations(**filters)

    def save_location(self, location: Space):
        location.updated_at = datetime.now()
        self._save_resource("locations", location.id, location.to_dict())

    def save_space(self, space: Space):
        return self.save_location(space)

    def update_location(self, id: str, update_map: dict):
        location = self.query_location(id)
        location_dict = location.to_dict()
        location_dict.update(update_map)
        self._save_resource("locations", id, location_dict)

    def update_space(self, space: Space):
        if type(space.id) == uuid.UUID:
            space.id = space.id.hex
        return self.update_location(space.id, space.to_dict())

    def delete_location(self, location: Location):
        self._delete_resource("locations", location.id)

    def delete_space(self, space: Space):
        if type(space.id) == uuid.UUID:
            space.id = space.id.hex
        return self.delete_location(space)

    # stores

    def stores(self, **filters: dict) -> Iterator[Store]:
        for doc in self._query("stores", limit=0, **filters):
            yield Store.build(doc.to_dict())

    def query_store(self, id: str) -> Optional[Store]:
        store_dict = self._query_resource_by_id("stores", id)
        if store_dict:
            return Store.build(store_dict)
        return None

    def query_stores(self, **filters: dict):
        if 'id' in filters:
            return self.query_store(filters['id'])
        else:
            return self.stores(**filters)

    def save_store(self, store: Store):
        store.updated_at = datetime.now()
        if store.code:
            self._save_resource("stores", store.code, store.to_dict())
        else:
            self._save_resource("stores", store.id, store.to_dict())

    def update_store(self, store: Store):
        store.updated_at = datetime.now()
        if store.code:
            self._save_resource("stores", store.code, store.to_dict())
        else:
            self._save_resource("stores", store.id, store.to_dict())

    def delete_store(self, store: Store):
        store.deleted = True
        if store.code:
            self._save_resource("stores", store.code, store.to_dict())
        else:
            self._save_resource("stores", store.id, store.to_dict())

    # jobs

    def jobs(self, **filters: dict) -> Iterator[Job]:
        for doc in self._query("jobs", limit=0, **filters):
            yield Job.build(doc.to_dict())

    def query_job(self, id: str) -> Optional[Job]:
        job_dict = self._query_resource_by_id("jobs", id)
        if job_dict:
            return Job.build(job_dict)
        return None

    def save_job(self, job: Job):
        job.updated_at = datetime.now()
        self._save_resource("jobs", job.id, job.to_dict())

    # subscriptions

    def subscriptions(self, limit: int = 0, **filters: dict) -> Iterator[Subscription]:
        for doc in self._query("subscriptions", limit, **filters):
            yield Subscription.build(doc.to_dict())

    def subscription(self, **filters: dict) -> Optional[Subscription]:
        for subscription in self.subscriptions(limit=1, **filters):
            return subscription
        return None

    def save_subscription(self, subscription: Subscription):
        subscription.updated_at = datetime.now()
        self._save_resource("subscriptions", subscription.id, subscription.to_dict())

    # actions

    def actions(self, limit: int = 0, **filters: dict) -> Iterator[Action]:
        for doc in self._query("actions", limit, **filters):
            yield Action.build(doc.to_dict())

    def action(self, **filters: dict) -> Optional[Action]:
        for action in self.actions(limit=1, **filters):
            return action
        return None

    def save_action(self, action: Action):
        action.updated_at = datetime.now()
        self._save_resource("actions", action.id, action.to_dict())

    def query_actions(self, **filters: dict):
        if "brand_id" in filters:
            return self.action(**filters)
        return self.actions(**filters)

    # audit_logs

    def audit_logs(self, limit: int = 0, **filters: dict) -> Iterator[AuditLog]:
        for doc in self._query("audit_logs", limit, **filters):
            yield AuditLog.build(doc.to_dict())

    def audit_log(self, **filters: dict) -> Optional[AuditLog]:
        for audit_log in self.audit_logs(limit=1, **filters):
            return audit_log
        return None

    def save_auditlog(self, audit_log: AuditLog):
        audit_log.updated_at = datetime.now()
        self._save_resource("audit_logs", audit_log.id, audit_log.to_dict())

    def query_audit_logs(self, **filters: dict):
        if 'id' in filters:
            return self.audit_log(**filters)
        return self.audit_logs(**filters)

    # messages

    def messages(self, limit: int = 0, **filters: dict) -> Iterator[Message]:
        for doc in self._query("messages", limit, **filters):
            yield Message.build(doc.to_dict())

    def message(self, **filters: dict) -> Optional[Message]:
        for message in self.messages(limit=1, **filters):
            return message
        return None

    def save_message(self, message: Message):
        message.updated_at = datetime.now()
        self._save_resource("messages", message.id, message.to_dict())

    def query_messages(self, **filters: dict):
        if 'id' in filters:
            return self.message(**filters)
        return self.messages(**filters)
