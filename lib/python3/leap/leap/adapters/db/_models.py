try:
    from pydantic.dataclasses import dataclass
except Exception:
    from dataclasses import dataclass

import logging
import random
import string
import uuid
from dataclasses import asdict, field, fields
from datetime import datetime, timedelta
from enum import unique
from typing import (Any, Callable, ClassVar, Dict, Iterable, List, Optional, Tuple, Union, cast, get_args, get_origin)

import dacite
import leap.std.hashing.md5 as md5
import leap.std.json as json
import leap.std.time
import leap.std.uuid
from dateutil.parser import parse
from faker import Faker
from google.api_core.datetime_helpers import DatetimeWithNanoseconds, to_rfc3339
from leap.adapters import EventType
from leap.std.enum import TypedEnum


def is_valid_uuid(value):
    try:
        uuid.UUID(str(value))
        return True
    except ValueError:
        return False


def _datetime_transformer(d: Union[str, datetime]) -> datetime:
    if isinstance(d, datetime):
        return d
    if isinstance(d, DatetimeWithNanoseconds):
        d = to_rfc3339(d)
    try:
        logging.debug("Parsing ISO8601 datetime")
        date = parse(d)
        return date
    except Exception as err:
        logging.error(f"Unable to parse datetime str: {d}", exc_info=err)


def _uuid_transformer(d: Union[str, uuid.UUID]) -> str:
    if is_valid_uuid(d):
        return str(d).replace('-', '')
    else:
        return d


DaciteHookType = Dict[Any, Callable[[Any], Any]]

DACITE_TYPE_HOOKS: DaciteHookType = {datetime: _datetime_transformer, str: _uuid_transformer}

# value of False allows db records to define fields the models don't support
STRICT_MODE = False

# A key:value representing an adapter-id:entity-id
AdapterKV = Tuple[str, str]

_uuidfactory: leap.std.uuid.UUIDFactory = leap.std.uuid.uuid4_factory()
_datetime_factory: leap.std.time.DatetimeFactory = leap.std.time.datetime_factory()
"""Generates new ids from UUIDs when called."""
id_factory: Callable[[], str] = lambda: _uuidfactory().hex
"""Generates new datetime instances."""
datetime_factory: Callable[[], datetime] = lambda: _datetime_factory()


class Model(json.Serializable):
    """Base methods for all dataclass-based Models."""
    firestore: ClassVar[bool] = True
    platformdb: ClassVar[bool]

    @classmethod
    def field_names(cls) -> List[str]:
        return sorted([f.name for f in fields(cls)])

    def validate_on_submit(self, field_names: List[str]):
        """Validate existence of field when data is submitted

        Args:
            field_names (List[str]): List of field names to check existence

        Raises:
            ValueError: [ {invalid_fields} ] are required to submit
        """
        invalid_fields = []
        for field_name in field_names:
            if not getattr(self, field_name):
                invalid_fields.append(field_name)

        if invalid_fields:
            raise ValueError(f"{invalid_fields} required to submit")

    @classmethod
    def build(cls, data: dict):
        return dacite.from_dict(cls,
                                data,
                                config=dacite.Config(strict=STRICT_MODE, type_hooks=DACITE_TYPE_HOOKS,
                                                     cast=[TypedEnum]))

    @classmethod
    def build_from(cls, m: "Model"):
        return cls.build(m.to_dict())

    def is_optional(field):
        return get_origin(field) is Union and type(None) in get_args(field)

    def is_uuid(field):
        return get_origin(field) is Union and uuid.UUID in get_args(field)

    @classmethod
    def generate_random(cls, seed=None):
        data = {}
        fake = Faker()
        if seed:
            Faker.seed(seed)
            random.seed(seed)
        for f in fields(cls):
            ignore_fields = getattr(cls, 'ignore_fields', [])
            if f.name in ignore_fields:
                continue
            if cls.is_optional(f.type):
                f.type = get_args(f.type)[0]
            value = None
            if get_origin(f.type) is dict:
                value = {}
            elif get_origin(f.type) is list:
                value = []
            elif 'url' in f.name:
                value = fake.url()
            elif f.name == "street_address_1":
                value = fake.street_address()
            elif f.name == "street_address_2":
                value = "Suite 123"
            elif f.name == "state":
                value = fake.state()
            elif f.name == "city":
                value = fake.city()
            elif f.name == "zip_code":
                value = fake.postcode()
            elif f.name == "market":
                value = fake.city() + " Metro"
            elif f.name == "sub_market":
                value = fake.street_name() + " Cluster"
            elif f.type == str:
                value = ''.join(random.choice(string.ascii_letters) for i in range(10))
            elif f.type == bool or f.type == Optional[bool]:
                value = random.choice([True, False])
            elif f.type in [int, float, Any]:
                value = random.randint(1, 100) * 1000
            elif f.type is datetime:
                # if we give a seed, get a reproducible timestamp
                if seed:
                    epoch = random.randint(0, 1670444685)
                    value = datetime.utcfromtimestamp(epoch)
                else:
                    value = datetime.now()
            elif cls.is_uuid(f.type):
                value = uuid.uuid4()
            elif issubclass(f.type, TypedEnum):
                value = random.choice(f.type.values())
            elif issubclass(f.type, Model):
                value = f.type().to_object()
            data[f.name] = value
        return cls.build(data)

    def to_dict(self) -> dict:
        return asdict(self)

    def to_object(self) -> dict:
        d = self.to_json()
        return json.loads(d)

    def to_json(self, indent=None, **kwargs) -> str:
        d = self.to_dict()
        for k, v in d.items():
            if isinstance(v, uuid.UUID):
                d[k] = v.hex
            if isinstance(v, datetime):
                d[k] = v.strftime('%a, %d %b %Y %H:%M:%S GMT')
        return json.dumps(d, indent=indent, **kwargs)

    def to_sql_object(self) -> dict:
        d = self.to_object()
        for k, v in d.items():
            if isinstance(v, list) or isinstance(v, dict):
                d[k] = json.dumps(v)
            if isinstance(v, datetime):
                d[k] = str(v)
        return d

    def copy_attributes(self, attr_1_name, attr_2_name, required=True, default=None):
        """
        During our firestore to postgres migration when the model is created, it could have either attr_1 OR attr_2, or in the rare case
        both exists they should be the same. The required param is used to enforce one of the two attr's must
        exist. Any other configuration is an error case.
        """
        attr_1 = attr_2 = None
        if hasattr(self, attr_1_name):
            attr_1 = getattr(self, attr_1_name)
        if hasattr(self, attr_2_name):
            attr_2 = getattr(self, attr_2_name)
        if attr_1 is None and attr_2 is None:
            if required:
                # If neither is set, that's an error
                raise TypeError(f"A {attr_1_name} or {attr_2_name} must be set")
            else:
                if default:
                    attr_1 = default

        if attr_1 is not None:
            # If attr_1 is set but not attr_2, set attr_2.
            if attr_2 is None:
                attr_2 = attr_1
            else:
                # If attr_2 is already set, and it's anything other than the exact string as attr_1,
                # throw an error because we have no way of knowing which id is correct.
                if attr_2 != attr_1:
                    raise TypeError(f"attr_2 is {attr_2}, attr_1 is {attr_1}. Can not continue with different values")
                # If attr_2 == attr_1, then we are all done
        else:
            # We now know attr_1 is empty because of the above check, so set it to the same as attr_2
            attr_1 = attr_2
        setattr(self, attr_1_name, attr_1)
        setattr(self, attr_2_name, attr_2)


@dataclass
class CustomerMetric(Model):
    bucket: str
    scope: str
    value: float


@dataclass
class Service(Model):
    """A service/integration definition.

    Fields:
        id (str): unique identifier for this service (human readable is fine).

        api_key (str): the service provided api-key value.

        api_secret (str): the service provided api-secret value.

        type (str): the type of service this is; e.g. shopify, newstore, klaviyo, etc.
            Defaults to the value of id.

        handle (str): the name of the service. This only exists in platformdb and seems to match service_id

        oauth_access_scopes (List[str]): a list of access-scopes to be granted when
            when requesting oauth access-tokens. Defaults to empty list.

        config (dict): additional configuration for the Service

        created_at (datetime): the date and time this record was created.

        updated_at (datetime): the date and time this record was last updated.
    """

    id: Union[str, uuid.UUID]
    api_key: str
    api_secret: str
    type: str = field(default_factory=str)
    handle: Optional[str] = None
    oauth_access_scopes: Optional[List[str]] = field(default_factory=list)
    config: Dict[str, Any] = field(default_factory=dict)

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    platformdb: ClassVar[bool] = True

    def __post_init__(self):
        if not self.type:
            # default type to value of id
            self.type = self.id
        if not self.handle:
            self.handle = self.id

    @property
    def landing_page(self) -> Optional[str]:
        return self.config.get("landing_page")

    @property
    def has_sig_check_override(self) -> bool:
        return SigCheckType.has_value(self.config.get("sig_check"))


@unique
class ServiceAccessTier(str, TypedEnum):
    READONLY = "ro"
    FULL = "full"
    FULLER = "fuller"


@dataclass
class ServiceAccessPrivilege(Model):
    """A ServiceAccessPrivilege maps a ServiceAccessTier to a privilege supported by a Service.

    Fields:
        service_id (str): the ID of the Service this privilege is defined in.

        privilege (str): the name of the privilege as it is defined in the Service (e.g. 'write_orders').

        tier (ServiceAccessTier): the ServiceAccessTier this privilege belongs to.

        id (Optional[str]): unique, internal identifier for this ServiceAccessPrivilege. Defaults to
            randomly generated UUID.

        created_at (datetime): the date and time this record was created.

        updated_at (datetime): the date and time this record was last updated.
    """

    service_id: str
    privilege: str
    tier: ServiceAccessTier
    id: Union[str, uuid.UUID] = field(default_factory=id_factory)

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)


@unique
class BrandStatus(str, TypedEnum):
    NEW = "NEW"
    EVAL = "EVAL"
    DEPRIORITIZED = "DEPRIORITIZED"
    LIVE = "LIVE"
    PROSPECTIVE = "PROSPECTIVE"


def _default_sku_fields() -> List[str]:
    return ["handle", "sku", "variant_title"]


@dataclass
class ClientConfig(Model):
    sku_fields: List[str] = field(default_factory=_default_sku_fields)
    omcp_version: Optional[int] = 0


@unique
class IdentityService(str, TypedEnum):
    AUTH0 = "AUTH0"


@dataclass
class User(Model):
    """A record of a User.

    This is a generic model of a User which we obtain via our IdentityService. This
    contains all necessary metadata tied to a User which can be used to link to other
    model elements such as a BrandUser.

    Fields:

        user_id (str): the unique id of the User in the identity-service.

        id (Optional[str]): unique, internal identifier for this User. Defaults to
            randomly generated UUID.

        identity_service (IdentityService): the identity-service this User is managed in.

        email (Optional[str]): email associated with User

        full_name (Optional[str]): full name of User

        display_name (Optional[str]): nickname or display name of User meant for addressing users

        title (Optional[str]): title or position of the given user

        phone_number (Optional[str]): the user's phone number

        is_deleted (bool): denotes whether a user has been deleted or not, when this is true, the
            auth0 user will have been deleted at this point.  The deleted user will be used
            as reference to operations made in admin api

        deleted_at (datetime): defaults to null, the date and time a user has been deleted.

        created_at (datetime): the date and time this service was created.

        updated_at (datetime): the date and time this service was last updated.
    """
    user_id: Optional[str] = None
    id: str = field(default_factory=id_factory)
    external_user_id: Optional[str] = None
    identity_service: IdentityService = IdentityService.AUTH0
    is_deleted: bool = False
    email: Optional[str] = None
    full_name: Optional[str] = None
    phone_number: Optional[str] = None
    title: Optional[str] = None
    display_name: Optional[str] = None
    deleted_at: Optional[datetime] = None

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    ignore_fields: ClassVar[list] = ['user_id']
    platformdb: ClassVar[bool] = True

    @property
    def qualified_user_id(self) -> str:
        return "{}|{}".format(self.identity_service.value.lower(), self.user_id)

    def __post_init__(self) -> None:
        self.copy_attributes("user_id", "external_user_id")


@dataclass
class BrandUser(Model):
    """A binding between a Brand and a User.

    Permissions and roles are intended to be managed via the IdentityService. The binding
    of a User to a Brand is meant only to indicate accessibility.

    Fields:
        brand_id (str): the id of the Brand the User is being given access to.

        user_id (str): the id of the User that is associated to this BrandUser

        id (Optional[str]): unique, internal identifier for this BrandUser. Defaults to
            randomly generated UUID.

        enabled (bool): determines whether a user has access to the available brand_id

        created_at (datetime): the date and time this service was created.

        updated_at (datetime): the date and time this service was last updated.
    """
    brand_id: Optional[str]
    user_id: str
    id: str = field(default_factory=id_factory)
    enabled: bool = True
    client_id: Optional[str] = None

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    platformdb: ClassVar[bool] = True

    # These fields are not in platformdb and should be ignored
    ignore_fields: ClassVar[list] = ['client_id']

    def __post_init__(self):
        self.copy_attributes("brand_id", "client_id")


@unique
class StorageService(str, TypedEnum):
    GCS = "gcs"


@unique
class AssetType(str, TypedEnum):
    FINANCIAL = "FINANCIAL"
    COMMON = "COMMON"
    STORE_DATA = "STORE_DATA"


@dataclass
class BrandAsset(Model):
    """Represents an Asset that a BrandUser has provided/uploaded.

    Fields:
        brand_id (str): the id of the Brand this Asset belongs to.

        created_by_id (str): the id of the User whom provided the Asset.

        bucket (str): the bucket name the Asset was uploaded to.

        path (str): the path to the Asset in the bucket; including the filename.

        name (str): the name of the Asset; generally, this is the name of the file
            provided by the User.

        type (str): the type of Asset from `AssetType`.

        id (Optional[str]): unique, internal identifier for this BrandAsset. Defaults to
            randomly generated UUID.

        storage_service (str): the id of the storage service used to house the Asset.
            Defaults to `StorageService.GCS` (Google Cloud Storage).

        created_at (datetime): the date and time this service was created.

        updated_at (datetime): the date and time this service was last updated.
    """
    brand_id: Optional[str]
    created_by_id: str
    bucket: Union[str, uuid.UUID]
    path: str
    name: str
    type: AssetType
    client_id: Optional[str] = None
    id: Union[str, uuid.UUID] = field(default_factory=id_factory)
    storage_service: StorageService = StorageService.GCS
    deleted: bool = False

    platformdb: ClassVar[bool] = True

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    def __post_init__(self):
        self.copy_attributes("brand_id", "client_id")


@dataclass
class AdapterLocation(Model):
    """A mapping of an Adapter's location to a Leap Store.

    Fields:
        id (Optional[str]): unique, internal identifier for this AdapterLocation. Defaults to
            randomly generated UUID.

        adapter_id (str): the id of the Adapter.

        store_id (str): the canonical id of the Store.

        virtual_store_id/virtual_store_code (Optional[str]): this is to be treated as the primary store-id when
            present. When not present, the store_id is to be used. virtual_store_id is used in FS, while
            virtual_store_code is used in PG.

        adapter_location_id/external_id (str): the id of the store as the Adapter service defines it.
            In Shopify, for instance, this is the location_id. When virtual_store_id is present,
            it can be assumed that multiple AdapterLocation records share this id for the
            bound adapter_id.

        primary (bool): is this mapping the primary owner for the location across the client.
            For the same Store across multiple Adapters for a Client, only one should be
            primary. Should never be True if a virtual_store_id is present.

        enabled (bool): indicates whether this mapping is in use or is maintained more for
            historical reasons.
    """

    adapter_id: str
    store_id: str
    adapter_location_id: Optional[str] = None
    external_id: Optional[str] = None
    primary: bool = None
    is_primary: bool = None
    enabled: bool = True
    id: str = field(default_factory=id_factory)
    virtual_store_id: Optional[str] = None
    virtual_store_code: Optional[str] = None

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    @property
    def is_virtual(self) -> bool:
        return self.virtual_store_id is not None and self.virtual_store_id.strip() != ""

    def __post_init__(self) -> None:
        self.copy_attributes("external_id", "adapter_location_id")
        # TODO: Once we are using PG, make this required
        self.copy_attributes("virtual_store_id", "virtual_store_code", required=False)
        # if we set the default value of these to be True, then we will get mismatches when one is set to True
        # and the other is not set at all. So instead, set to None, then default to True here.
        if self.is_primary is None and self.primary is None:
            self.primary = True
        self.copy_attributes("primary", "is_primary")


class AdapterStore(AdapterLocation):
    platformdb: ClassVar[bool] = True
    # These fields are not in platformdb and should be ignored
    ignore_fields: ClassVar[list] = ['primary', 'virtual_store_id', 'adapter_location_id']

    def __post_init__(self) -> None:
        self.copy_attributes("external_id", "adapter_location_id")
        # TODO: Once we are using PG, make this required
        self.copy_attributes("virtual_store_id", "virtual_store_code", required=False)
        # if we set the default value of these to be True, then we will get mismatches when one is set to True
        # and the other is not set at all. So instead, set to None, then default to True here.
        if self.is_primary is None and self.primary is None:
            self.primary = True
        self.copy_attributes("primary", "is_primary")


@unique
class InventorySyncStrategy(str, TypedEnum):
    ONE = "ONE"
    ALL = "ALL"


@unique
class MerchantType(str, TypedEnum):
    LEAP = "leap"
    BRAND = "brand"
    NA = "na"


@unique
class SigCheckType(str, TypedEnum):
    BYPASS = "bypass"
    OK = "ok"


@dataclass
class AdapterConfigState(Model):
    connected: Optional[bool] = None
    collecting: Optional[bool] = None
    imported: Optional[bool] = None
    tier: Optional[str] = None
    inventory_sync_strategy: Optional[str] = None


@dataclass
class AdapterConfig(Model):
    """A service Adapter configuration for a Client.

    A service Adapter defines an integration with the API of a commerce-platform for
    a client. This includes the secrets needed to authenticate with that API.

    Fields:
        client_id/brand_id (str): the id of the Client/Brand this Adapter is configured for. Firestore is still using
            the old client naming convention, while platformdb is using the new brand convention.

        service_id (str): the id of the service this Adapter is integrating
            with; e.g. "shopify", "newstore", etc. Used to lookup registered Adapter
            implementations.

        service_type (str): the type of the service being used for this adapter;
            e.g. "shopify", "klaviyo", etc.

        instance_id (str): this is a service-agnostic account, domain, instance, etc id.
            For example, with Shopify, if you access your shop with the hostname
            `my-cool-shop.myshopify.com`, the instance_id in this case should be `my-cool-shop`.
            This value should be unique in general, but there may be situations where
            it is not.

        id (Optional[str]): unique, internal identifier for this Adapter. Defaults to
            randomly generated UUID.

        internal (bool): indicates whether the Adapter is internal to Leap; i.e. it is not one
            a customer added, should have control over, or be visible to them.

        source (bool): indicates whether the Adapter can be considered as a source
            of events/data for the client.

        sink (bool): indicates whether the Adapter can be considered a sink to pass
            events/data to for the client.

        config (dict): dictionary of api configuration properties; i.e. api-tokens,
            access-tokens, usernames, password, urls, etc.

        filters (dict): dictionary of service+event specific field filters.

        bq_dataset (Optional[str]): the name of the qualified BigQuery dataset this Adapter can use.
            Name should include the GCP project (e.g. "warehouse.shopify_someclient").

        data_state (dict): dictionary of data properties; i.e. bq_dataset, job_id,
            imported, etc.

        created_at (datetime): the date and time this Adapter was created.

        updated_at (datetime): the date and time this Adapter was last updated.
    """
    service_id: str
    instance_id: str
    service_type: str
    id: str = field(default_factory=id_factory)
    internal: bool = False
    source: bool = True
    sink: bool = True
    config: dict = field(default_factory=dict)
    filters: dict = field(default_factory=dict)
    # TODO: remove and use the data_state field
    bq_dataset: Optional[str] = None
    data_state: dict = field(default_factory=dict)
    track_inventory: Optional[bool] = False
    client_id: Optional[str] = None
    brand_id: Optional[str] = None
    state: Optional[AdapterConfigState] = None

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    platformdb: ClassVar[bool] = True
    ignore_fields: ClassVar[list] = ['service_type', 'bq_dataset', 'client_id', 'state']

    @property
    # A shopify adapter is only connected if there is a config with an adapter token.
    # Other services are automatically connected.
    def is_connected(self) -> bool:
        if self.service_type == 'shopify':
            return bool(self.config.get("access_token"))
        else:
            return True

    @property
    # We're only collecting data for shopify adapters at this point.
    def is_collecting(self) -> bool:
        if self.service_type == 'shopify':
            return self.is_connected and not self.is_imported
        else:
            return False

    @property
    # imported also only applies to shopify adapters
    def is_imported(self) -> bool:
        if self.service_type == 'shopify':
            return bool(self.data_state.get("imported"))
        else:
            return False

    @property
    # tier only applies to shopify adapters
    def tier(self) -> str:
        if (self.service_type == 'shopify' and self.is_connected):
            return self.config.get("tier", "unknown")
        else:
            return "n/a"

    @property
    def inventory_sync_strategy(self) -> InventorySyncStrategy:
        s = self.config.get("inventory_sync_strategy")
        if InventorySyncStrategy.has_value(s):
            return InventorySyncStrategy(s)
        return InventorySyncStrategy.ONE

    @property
    # the merchant type of the shopify adapter - either `leap` or `brand`
    def merchant_type(self) -> str:
        if self.service_type == 'shopify':
            return MerchantType.LEAP if self.internal else MerchantType.BRAND
        else:
            return MerchantType.NA

    @property
    def has_sig_check_override(self) -> bool:
        return SigCheckType.has_value(self.config.get("sig_check"))

    def __post_init__(self) -> None:
        self.copy_attributes("client_id", "brand_id")
        if not self.service_type:
            self.service_type = self.service_id


@dataclass
class AdapterWebhook(Model):
    """AdapterWebhook represents a single webhook installed against an Adapter.

    A service adapter defines an integration with the API of a commerce-platform for
    a client. This includes the secrets needed to authenticate with that API.

    Fields:
        adapter_id (str): the id of the Adapter the webhook is installed on.

        event_type (str): the internal EventType this webhook represents.

        adapter_event_type (str): the Adapter's event name/topic.

        adapter_webhook_id (str): the ID of the webhook in the Adapter's system.

        id (Optional[str]): unique, internal identifier for this AdapterWebhook. Defaults to
            randomly generated UUID.

        omcp_version (int): the OMCP api version used to process the webhook. Defaults to 0.

        created_at (datetime): the date and time this AdapterWebhook was created.

        updated_at (datetime): the date and time this AdapterWebhook was last updated.
    """
    adapter_id: Union[str, uuid.UUID]
    event_type: EventType
    adapter_event_type: str
    adapter_webhook_id: str
    id: Union[str, uuid.UUID] = field(default_factory=id_factory)
    omcp_version: Optional[int] = 0

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)


@dataclass
class Entity(Model):
    """References to the same entity as it exists in multiple systems, mapped to managing adapter(s).
    """

    type: str
    source_adapter_id: str
    id: str = field(default_factory=id_factory)
    adapter_entity_map: Dict[str, str] = field(default_factory=dict)
    line_item_maps: List[Dict[str, str]] = field(default_factory=list)
    fulfillments: List[Dict[str, str]] = field(default_factory=list)
    refunds: List[Dict[str, str]] = field(default_factory=list)

    # TODO deprecate statuses field once we have specific maps for specific fields (e.g. fulfillments, payments)
    statuses: Dict[str, Optional[str]] = field(default_factory=dict)
    hashes: Dict[str, str] = field(default_factory=dict)

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    def entity_id(self, adapter_id: str) -> Optional[str]:
        """Lookup the id of the entity (e.g. order-id) for the given adapter."""
        return self.adapter_entity_map.get(adapter_id)  # pylint: disable=no-member

    def _get_record_by_key(self, records: list, key: AdapterKV) -> Optional[Dict[str, str]]:
        if key:
            for rec in records:
                if rec.get(key[0]) == key[1]:
                    return rec
        return None

    def _add_key_to_record(self, records: list, key: AdapterKV, new_key: AdapterKV) -> Optional[Dict[str, str]]:
        rec = self._get_record_by_key(records, key)
        if rec and new_key:
            rec[new_key[0]] = new_key[1]
            return rec
        return None

    def line_item(self, key: AdapterKV) -> Optional[Dict[str, str]]:
        """Lookup the full line-item record that is mapped to a key (adapter-id, line-item-id).

        Example:
            e = Entity("order", line_item_maps=[
                {"adapter-1": "li-1", "adapter-2": "li-2"},
                {"adapter-1": "li-3", "adapter-2": "li-4"},
            ])
            e.line_item(("adapter-1", "li-3"))
            > {"adapter-1": "li-3", "adapter-2": "li-4"}
        """
        return self._get_record_by_key(self.line_item_maps, key)

    def add_line_item(self, key: AdapterKV, new_key: AdapterKV) -> Optional[Dict[str, str]]:
        return self._add_key_to_record(self.line_item_maps, key, new_key)

    def line_item_id(self, adapter_id: str, key: AdapterKV) -> Optional[str]:
        """Lookup the line-item-id for an adapter that is mapped to some other adapter & line-item-id."""
        li = self._get_record_by_key(self.line_item_maps, key)
        if li:
            return li.get(adapter_id)
        return None

    def fulfillment(self, key: AdapterKV) -> Optional[Dict[str, str]]:
        """Lookup the full fulfillment record that is mapped to a key (adapter-id, fulfillment-id).

        Example:
            e = Entity("order", fulfillments=[
                {"adapter-1": "1234-0", "adapter-2": "2345-0", "status": "cancelled"},
                {"adapter-1": "1234-1", "adapter-2": "2345-1", "status": "success"},
            ])
            e.fulfillment_id(("adapter-1", "1234-1"))
            > {"adapter-1": "1234-1", "adapter-2": "2345-1", "status": "success"}
        """
        return self._get_record_by_key(self.fulfillments, key)

    def add_fulfillment(self, key: AdapterKV, new_key: AdapterKV) -> Optional[Dict[str, str]]:
        return self._add_key_to_record(self.fulfillments, key, new_key)

    def refund(self, key: AdapterKV) -> Optional[Dict[str, str]]:
        """Lookup the full refund record that is mapped to a key (adapter-id, refund-id).

        Example:
            e = Entity("order", refunds=[
                {"adapter-1": "1234-0", "adapter-2": "2345-0"},
                {"adapter-1": "1234-1", "adapter-2": "2345-1"},
            ])
            e.refund_id(("adapter-1", "1234-1"))
            > {"adapter-1": "1234-1", "adapter-2": "2345-1"}
        """
        return self._get_record_by_key(self.refunds, key)

    def add_refund(self, key: AdapterKV, new_key: AdapterKV) -> Optional[Dict[str, str]]:
        return self._add_key_to_record(self.refunds, key, new_key)

    def update_fulfillment_status(self, key: AdapterKV, new_status: str) -> Optional[Dict[str, str]]:
        fulfillment = self.fulfillment(key)
        if fulfillment:
            fulfillment["status"] = new_status
            return fulfillment
        return None

    def update_fulfillment_hash(self, key: AdapterKV, data: dict) -> Optional[str]:
        fulfillment = self.fulfillment(key)
        if fulfillment:
            fulfillment["hash"] = md5.hash_object(data)
            return fulfillment["hash"]
        return None


@dataclass
class EventAction(Model):
    """Captures data related to an action taken in response to an inbound Event."""

    adapter_id: str
    id: str = field(default_factory=id_factory)
    request: dict = field(default_factory=dict)
    response: dict = field(default_factory=dict)

    timestamp: datetime = field(default_factory=datetime_factory)
    latency_ms: float = 0.0

    @property
    def status_code(self) -> int:
        return self.response.get("status_code", 0)

    @property
    def success(self) -> bool:
        return int(self.status_code / 100) == 2

    @property
    def page_info(self) -> Optional[str]:
        """Information related to supporting the next page in a pagination context.

        If `None`, there is no next page.
        """
        return self.response.get("page_info")

    def capture_latency(self) -> float:
        self.latency_ms = (datetime_factory() - self.timestamp) / timedelta(milliseconds=1)
        return self.latency_ms


@dataclass
class Event(Model):
    """A service-agnostic Event."""

    # TODO this and EventAction probably belongs back in process-events

    type: EventType
    id: str = field(default_factory=id_factory)

    # metadata concerning event processing; useful for passing context around
    attrs: Dict[str, object] = field(default_factory=dict)

    # the provided (possibly transformed)
    data: Optional[Dict[str, object]] = field(default_factory=dict)

    # data tracked through processing (can change frequently)
    state: Dict[str, object] = field(default_factory=dict)

    # history of actions taken while processing this Event
    actions: List[EventAction] = field(default_factory=list)

    # the ultimate Entity that is being generated for saving
    entity: Optional[Entity] = None

    timestamp: datetime = field(default_factory=datetime_factory)
    latency_ms: float = 0.0

    @property
    def source_adapter_id(self) -> str:
        return str(self.attrs.get("x-leap-adapter-id"))  # pylint: disable=no-member

    @property
    def omcpv1(self) -> bool:
        return str(self.attrs.get("x-leap-omcp-version", "0")) == "1"  # pylint: disable=no-member

    @property
    def source_locations(self) -> dict:
        return cast(dict, self.attrs.get("source_locations", {}))  # pylint: disable=no-member

    def capture_action(self, action: EventAction) -> EventAction:
        action.capture_latency()
        self.actions.append(action)  # pylint: disable=no-member
        return action

    def capture_latency(self) -> float:
        self.latency_ms = (datetime_factory() - self.timestamp) / timedelta(milliseconds=1)
        return self.latency_ms


@unique
class LocationStatus(str, TypedEnum):
    EVAL = "EVAL"
    DEPRIORITIZED = "DEPRIORITIZED"
    OCCUPIED = "OCCUPIED"
    UNOCCUPIED = "UNOCCUPIED"
    ARCHIVED = "ARCHIVED"

    # @deprecated
    ACTIVE = "ACTIVE"
    PENDING = "PENDING"
    DISABLED = "DISABLED"


@dataclass
class Location(Model):
    """A Location: information regarding a physical retail space that is not bound to a client.

    Fields:
        address (str): the address of the Location

        city (str): the city the Location is in

        state (str): the state the Location is in

        city_code (str): the hex form of the crc32 sum of a lower-cased city+state combination
            @see leap.std.hashing.crc32.hash_object

        zip_code (str): the zip code the Location is in

        latitude (float): the latitude of the Location

        longitude (float): the longitude of the Location

        sqft (int): the primary square footage of the Location

        id (str): a unique identifier tied to the Location

        status_key (LocationStatus): the current status of a given Location

        internal (bool): indicates whether this Location is meant only for internal use or not

        created_at (datetime): the date the Location record was created

        updated_at (datetime): the date the Location record was updated

        unit (str): the unit/suite of the Location

        threshold_rssi (str): wifi signal strength reading

    """
    address: Optional[str]
    city: Optional[str]
    state: Optional[str]
    city_code: str
    zip_code: Optional[str]
    latitude: Any
    longitude: Any
    sqft: Any
    id: Union[str, uuid.UUID] = field(default_factory=id_factory)
    status: Optional[LocationStatus] = None
    status_key: Optional[LocationStatus] = None
    internal: bool = False
    unit: Optional[str] = None
    threshold_rssi: Optional[str] = None

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    platformdb: ClassVar[bool] = True

    @property
    def sqft(self) -> int:
        return self._sqft

    @sqft.setter
    def sqft(self, v: int) -> None:
        if v is None or v == "":
            self._sqft = 0
        else:
            self._sqft = v

    @property
    def latitude(self) -> float:
        return self._latitude

    @latitude.setter
    def latitude(self, v: float) -> None:
        if v is None or v == "":
            self._latitude = 0.0
        else:
            self._latitude = v

    @property
    def longitude(self) -> float:
        return self._longitude

    @longitude.setter
    def longitude(self, v: float) -> None:
        if v is None or v == "":
            self._longitude = 0.0
        else:
            self._longitude = v

    def __post_init__(self):
        if not any([self.status_key, self.status]):
            self.status_key = self.status = LocationStatus.EVAL
        self.copy_attributes("status_key", "status")


class Space(Location):
    # These fields are not in platformdb and should be ignored
    ignore_fields: ClassVar[list] = ['status_key']

    def __post_init__(self):
        if not any([self.status_key, self.status]):
            self.status_key = self.status = LocationStatus.EVAL
        self.copy_attributes("status", "status_key")


@unique
class StoreType(str, TypedEnum):
    WAREHOUSE = "WAREHOUSE"
    POS = "POS"


@dataclass
class Store(Model):
    """A store: the intersection of a location and a client.

    Fields:
        id (str): IN FS a unique identifier tied to the location. Also known as the store-code.
            This will not be generated randomly. Provide a unique value (e.g. "LG-001")
            In PG this is a UUID, and code is the store-code.

        client_id/brand_id (str): the id of a client/brand

        location_id/space_id (str): the id of a location

        deleted (bool): is this Store currently soft deleted or not

        slack_channel_id (Optional[str]): the slack channel id associated with the store-code

        possession_date (Optional[datetime]): the date the client took or will-take posession of the store

        open_date (Optional[datetime]): the date the store opened

        close_date (Optional[datetime]): the date the store closed

        created_at (datetime): the date the store record was created

        updated_at (datetime): the date the store record was updated

        margin (Optional[float]): the stores margin 00.0000

        code (Optional[str]): If this is coming from PG, the id is a UUID, and this is the store code

        shopper_nps_survey_url (Optional[str]): NPS survey url to be sent to end user
    """
    id: Union[str, uuid.UUID]
    client_id: Optional[str] = None
    brand_id: Optional[str] = None
    location_id: Optional[str] = None
    space_id: Optional[str] = None
    deleted: bool = False
    slack_channel_id: Optional[str] = None
    store_type: Optional[StoreType] = None
    type: Optional[StoreType] = None
    possession_date: Optional[datetime] = None
    open_date: Optional[datetime] = None
    close_date: Optional[datetime] = None
    shopper_nps_survey_url: Optional[str] = None

    margin: Optional[float] = None
    code: Optional[str] = None
    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    platformdb: ClassVar[bool] = True
    # TODO remove guid field after UI switches to using store code instead of store id
    guid: ClassVar[uuid.UUID] = None

    # These fields are not in platformdb and should be ignored
    ignore_fields: ClassVar[list] = ['client_id', 'location_id', 'store_type']

    def __post_init__(self) -> None:
        self.copy_attributes("client_id", "brand_id")
        # TODO: Once everything is moved to PG, make this required.
        self.copy_attributes("space_id", "location_id", required=False)
        self.copy_attributes("store_type", "type", required=False, default=StoreType.POS)


@dataclass
class Job(Model):
    """A Job: status, type, and time recorded for a given Job.

    Fields:
        client_id (str): The client id associated with the tasks for a given job

        type (str): The type of job this is e.g. brand-eval

        status (str): The current state of a job

        config (dict): The config used for a given job

        id (str): a unique identifier tied to the job

        created_at (datetime): The time of which the given job has been created

        updated_at (datetime): The time of which the given job has updated its last state
    """
    client_id: str
    type: str
    status: str
    config: dict = field(default_factory=dict)
    id: str = field(default_factory=id_factory)
    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)


## skus and inventory

SSKU_SEPARATOR = "|"


@dataclass
class SKU(Model):
    """A Leap SKU representing a Client's product-sku (aka variant) across multiple Adapters (storefronts).

    Fields:
        client_id (str): the id of the Client this SKU belongs to

        sku (str): a Client-defined value matching the sku in their systems.
            Should be unique to the Client.

        id (str): a unique ID for this SKU. Do not use the `sku` field as the id as it's
            possible that Clients might have matching skus.

        created_at (datetime): the date and time this SKU was created

        updated_at (datetime): the date and time this SKU was last updated
    """
    client_id: str
    sku: str
    id: str = field(default_factory=id_factory)

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    @classmethod
    def format_ssku(cls, *args: Iterable[Any]) -> str:
        return SSKU_SEPARATOR.join([str(a).upper() for a in args])


@dataclass
class AdapterSKU(Model):
    """Binding of a SKU to the Adapter's proprietary reference.

    Fields:
        sku_id (str): the id of the Leap SKU

        adapter_id (str): the id of the Adapter (AdapterConfig)

        keys: a map of the associated sku related metadata, the combonation of this allows
            us to generate our own synthetic SKU.  The keys saved in this section are as follows:
                - product_id
                - variant_id
                - inventory_item_id
                - sku
                - product_type
                - product_vendor

        keys_hash (str): a hash of the combination of the keys map

        id (str): a unique ID for this AdapterSKU. Represents the combination
            of (sku_id, adapter_id, adapter_aku_id) as a unique key.

        created_at (datetime): the date and time this AdapterVariant was created

        updated_at (datetime): the date and time this AdapterVariant was last updated
    """
    sku_id: str
    adapter_id: str
    keys: Dict[str, Optional[str]] = field(default_factory=dict)
    keys_hash: str = field(default_factory=str)
    id: str = field(default_factory=id_factory)

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)


@dataclass
class SKUInventory(Model):
    """Binding of a SKU to a Store in order to track inventory.

    Fields:
        sku_id (str): the id of the Leap SKU

        store_id (str): the id of the Leap Store where inventory exists

        absolute_stock_level (int): the amount of inventory available for this SKU at this Store.
            It's conceivable that this value could drop below zero (0).

        id (str): a unique ID for this SKUInventory. Represents the combination
            of (sku_id, store_id) as a unique key.

        created_at (datetime): the date and time this SKUInventory was created

        updated_at (datetime): the date and time this SKUInventory was last updated
    """
    sku_id: str
    store_id: str
    absolute_stock_level: int
    id: str = field(default_factory=id_factory)

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)


@unique
class SubscriptionType(str, TypedEnum):
    EMAIL = "EMAIL"


@unique
class MessageType(str, TypedEnum):
    EMAIL = "EMAIL"


@unique
class ActionType(str, TypedEnum):
    NEW_BRAND = "NEW_BRAND",
    ADAPTER_ATTEMPT = "ADAPTER_ATTEMPT"
    ADAPTER_CONNECTED = "ADAPTER_CONNECTED"
    ADAPTER_UNINSTALLED = "ADAPTER_UNINSTALLED"
    INITIAL_IMPORT_COMPLETE = "INITIAL_IMPORT_COMPLETE"
    ONBOARD_SURVEY_COMPLETE = "ONBOARD_SURVEY_COMPLETE"
    ASSET_UPLOADED = "ASSET_UPLOADED"
    ADMIN_ASSET_UPLOADED = "ADMIN_ASSET_UPLOADED"
    NEW_USER_CHANGE_PASSWORD = "NEW_USER_CHANGE_PASSWORD"
    BRAND_PROFILE_SAVE = "BRAND_PROFILE_SAVE"
    BRAND_PROFILE_SUBMIT = "BRAND_PROFILE_SUBMIT"
    SELECT_SPACE_SAVE = "SELECT_SPACE_SAVE"


@dataclass
class Subscription(Model):
    """Representation of a Subscription to be used for notifications for a particular Action.

    Fields:
        subscription_type: the SubscriptionType for this Subscription

        action_type: the ActionType this Subscription is subscribed to

        data: a generic dictionary of Subscription data used to define where an Action will be sent
            to.

        id: a unique ID for this Subscription

        created_at (datetime): the date and time this Subscription was created

        updated_at (datetime): the date and time this Subscription was last updated
    """
    subscription_type: Optional[SubscriptionType]
    action_type: ActionType
    data: Dict[str, str] = field(default_factory=dict)
    id: Union[str, uuid.UUID] = field(default_factory=id_factory)
    notification_type: Optional[SubscriptionType] = None

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    def __post_init__(self) -> None:
        self.copy_attributes("subscription_type", "notification_type")


@dataclass
class Action(Model):
    """A recorded Action that can be used for notifications tied to a Subscription

    Fields:
        action_type: the particular type enum being referenced for this Action

        client_id/brand_id: The id of the Client/Brand tied to this Action

        matched_subscription_ids: List of subscription ids tied to this action

        message: a dictionary containing a message and a body to be used as the message delivered
            via matched subsciptions

        id: a unique ID for this Action

        created_at (datetime): the date and time this Subscription was created

        updated_at (datetime): the date and time this Subscription was last updated
    """
    action_type: ActionType
    matched_subscription_ids: List[str]
    client_id: Union[Optional[str], uuid.UUID] = None
    brand_id: Union[Optional[str], uuid.UUID] = None
    message: Dict[str, str] = field(default_factory=dict)
    id: str = field(default_factory=id_factory)

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    platformdb: ClassVar[bool] = True

    def __post_init__(self):
        self.copy_attributes("client_id", "brand_id")


@dataclass
class AuditLog(Model):
    """A record of a message being sent, this keeps track of the Message that the AuditLog is tied to and
    will represent misc properties such as client_id, matched subscriptions within.
        Fields:

        action_type (ENUM): the ActionType tied to this AuditLog entry

        message_id (str): The Message this is tied to

        id (str): a unique ID for this AuditLog entry

        properties (dict[str,str]): these are metadata tied to the AuditLog such as client_id or matched_subscription_ids

        created_at (datetime): the date and time this Message was created

        updated_at (datetime): the date and time this Message was last updated
    """
    action_type: ActionType
    message_id: Union[str, uuid.UUID]
    properties: Dict[str, str] = field(default_factory=dict)
    id: Union[str, uuid.UUID] = field(default_factory=id_factory)

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)
    platformdb: ClassVar[bool] = True


@dataclass
class Message(Model):
    """Representation of a message that is to be sent to the notifications pubsub which will ultimately
    be responsable for the sending of the message.

    Fields:

        action_type (ENUM): the ActionType this message is triggered from

        message_type (ENUM): the MessageType of this particular Message

        message_data (dict[str,str]): a generic dictionary of Subscription data used to define where an Action will be sent
            to.

        recipients (List[str]): the list of intended recipients for this Message

        id (str): a unique ID for this Message

        created_at (datetime): the date and time this Message was created

        updated_at (datetime): the date and time this Message was last updated
    """
    action_type: Union[ActionType, None]
    message_type: MessageType
    recipients: List[str]
    message_data: Dict[str, str] = field(default_factory=dict)
    id: Union[str, uuid.UUID] = field(default_factory=id_factory)

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    platformdb: ClassVar[bool] = True


@dataclass
class ClientOmniChannelInsights(Model):
    client_handle: str
    gross_profit_margin: Optional[float]
    ppv_score: Optional[float]
    fitness_score: Optional[float]
    orders_last_12_months: Optional[int]
    net_sales_last_12_months: Optional[float]
    buyers_last_12_months: Optional[int]
    margin_dollars_last_12_months: Optional[float]
    average_order_value_last_12_months: Optional[float]
    purchase_frequency_last_12_months: Optional[float]
    units_per_transaction_last_12_months: Optional[float]
    lifetime_value_last_12_months: Optional[float]
    refund_rate_last_12_months: Optional[float]
    average_order_value_omni_channel: Optional[float]
    purchase_frequency_omni_channel: Optional[float]
    units_per_transaction_omni_channel: Optional[float]
    lifetime_value_omni_channel: Optional[float]
    refund_rate_omni_channel: Optional[float]
    purchase_frequency_percent_change: Optional[float]
    average_order_value_percent_change: Optional[float]
    refund_rate_percent_change: Optional[float]
    lifetime_value_percent_change: Optional[float]
    top_5_net_sales_percentage: Optional[float]
    top_5_units_sold_percentage: Optional[float]
    total_product_count: Optional[int]
    top_5_product_concentration: Optional[float]
    top_market_city: Optional[str]
    top_market_state_abbreviation: Optional[str]
    total_leap_market_count: Optional[int]

    id: str = field(default_factory=id_factory)
    created_at: datetime = field(default_factory=datetime_factory)


@dataclass
class ClientOmniChannelInsightsMarkets(Model):
    client_handle: str
    market: Optional[str]
    cbsa_code: Optional[str]
    net_sales_last_12_months: Optional[float]
    city: Optional[str]
    state_abbreviation: Optional[str]
    market_net_sales_rank: Optional[int]
    is_leap_market: Optional[bool]

    id: str = field(default_factory=id_factory)
    created_at: datetime = field(default_factory=datetime_factory)


@dataclass
class ClientOmniChannelInsightsTopProductsNetSales(Model):
    client_handle: str
    product_id: Optional[int]
    product_title: Optional[str]
    product_net_sales: Optional[float]
    total_brand_net_sales: Optional[float]
    product_percent_of_net_sales: Optional[float]
    product_net_sales_rank: Optional[int]
    product_count: Optional[int]

    id: str = field(default_factory=id_factory)
    created_at: datetime = field(default_factory=datetime_factory)


@dataclass
class ClientOmniChannelInsightsTopProductsUnitsSold(Model):
    client_handle: str
    product_id: Optional[int]
    product_title: Optional[str]
    product_units_sold: Optional[int]
    total_brand_units_sold: Optional[int]
    product_percent_of_units_sold: Optional[float]
    product_units_sold_rank: Optional[int]
    product_count: Optional[int]

    id: str = field(default_factory=id_factory)
    created_at: datetime = field(default_factory=datetime_factory)


@dataclass
class OmniChannelActiveLeapMarkets(Model):
    cbsa_code: Optional[str]
    market: Optional[str]
    city: Optional[str]
    state_abbreviation: Optional[str]
    market_latitude: Optional[float]
    market_longitude: Optional[float]

    id: str = field(default_factory=id_factory)
    created_at: datetime = field(default_factory=datetime_factory)


@unique
class BrandRole(str, TypedEnum):
    PRODUCTION = "PRODUCTION"
    STAGE = "STAGE"
    DEMO = "DEMO"
    DUPLICATE = "DUPLICATE"
    NEW = "NEW"


@dataclass
class RelatedBrand(Model):
    parent_id: str
    child_id: str
    id: str = field(default_factory=id_factory)
    created_at: datetime = field(default_factory=datetime_factory)
    platformdb: ClassVar[bool] = True


# An abbreviated version of a Brand for when you just need to display the highlights
@dataclass
class BrandLite(Model):
    name: str
    id: str
    brand_code: Optional[str] = None
    role: Optional[BrandRole] = None


# This is a useful abstraction around the related_brands table. The brand_id is the brand a user searched for,
# then the parent/child ids are a full "family tree" for that brand.
@dataclass
class RelatedBrandsGroup(Model):
    brand_id: str
    parent: Optional[BrandLite] = None
    children: Optional[List[BrandLite]] = None


@dataclass
class Brand(Model):
    """A Leap Brand.

    Fields:
        name (str): the name of this brand.

        id (Optional[str]): unique, internal identifier for this Brand. Defaults to
            randomly generated UUID.

        handle (str): a unique canonical handle for each brand to use when querying or joining
            brands.

        deleted (bool): indicates whether this Brand is considered `deleted` from the platform.
            This replaces the `enabled` since enabled could mean a multitude of states

        timezone (Optional[str]): timezone name this Brand operates in
            (https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).
            Defaults to Etc/UTC.

        config (ClientConfig): additional configuration for the Brand

        status_key (BrandStatus): the current status of a given Brand

        website (Optional[str]): the brand website

        contact_name (Optional[str]): the brand contact's name

        contact_email (Optional[str]): the brand contact's email

        contact_phone (Optional[str]): the brand contact's phone

        brand_code (Optional[str]): unique brand code that takes the format of LAAAA
            where "AAAA" are the first four letters from the brands name.

        brand_details/client_details (Dict[str, Any]): brand details

        box_link (Optional[str]): str: Link to box.com folder to upload files for brand

        created_at (datetime): the date and time this record was created.

        updated_at (datetime): the date and time this record was last updated.

        role(BrandRole): The role of the brand. Will mark it as PRODUCTION or DUPLICAT for example.


    """

    name: str
    id: str = field(default_factory=id_factory)
    handle: str = field(default_factory=str)
    deleted: bool = False
    timezone: str = "Etc/UTC"
    config: ClientConfig = field(default_factory=ClientConfig)
    status_key: BrandStatus = BrandStatus.PROSPECTIVE
    website: Optional[str] = None
    contact_name: Optional[str] = None
    contact_email: Optional[str] = None
    contact_phone: Optional[str] = None
    brand_code: Optional[str] = None
    brand_details: Dict[str, Any] = field(default_factory=dict)
    role: Optional[BrandRole] = BrandRole.NEW

    # TODO Remove after we fully migrate to postgres
    client_details: Dict[str, Any] = field(default_factory=dict)
    box_link: Optional[str] = None

    created_at: datetime = field(default_factory=datetime_factory)
    updated_at: datetime = field(default_factory=datetime_factory)

    # Generated dynamically through joining other tables
    adapters: List[AdapterConfig] = field(default_factory=list)
    assets: List[BrandAsset] = field(default_factory=list)
    relations: Optional[List[RelatedBrandsGroup]] = None
    available_spaces_count: Optional[int] = 0

    platformdb: ClassVar[bool] = True
    ignore_fields: ClassVar[list] = ['client_details', 'adapters', 'assets', 'relations', 'available_spaces_count'
                                    ]  # These fields are not in platformdb and should be ignored

    def __post_init__(self):
        if self.client_details and not self.brand_details:
            self.brand_details = self.client_details
        elif self.brand_details and not self.client_details:
            self.client_details = self.brand_details
