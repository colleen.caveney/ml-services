import abc
from typing import List, Optional, Union
from . import _models as models


class ActionInterface(abc.ABC):
    """Interface for CRUD operations with Action objects in db.
    """

    @abc.abstractmethod
    def save_action(self, action: models.Action):
        pass

    @abc.abstractmethod
    def query_actions(self, **filters: dict):
        pass


class MessageInterface(abc.ABC):
    """Interface for CRUD operations with Message objects in db.
    """

    @abc.abstractmethod
    def save_message(self, message: models.Message):
        pass

    @abc.abstractmethod
    def query_messages(self, **filters: dict):
        pass


class AuditLogInterface(abc.ABC):
    """Interface for CRUD operations with Audit Log objects in db.
    """

    @abc.abstractmethod
    def save_auditlog(self, audit_log: models.AuditLog):
        pass

    @abc.abstractmethod
    def query_audit_logs(self, **filters: dict):
        pass


class BrandInterface(abc.ABC):
    """Interface for CRUD operations with Brand"""

    @abc.abstractmethod
    def query_brands(self, **filters: dict):
        pass

    @abc.abstractmethod
    def save_brand(self, brand: models.Brand):
        pass

    @abc.abstractmethod
    def delete_brand(self, brand: models.Brand):
        pass

    @abc.abstractmethod
    def update_brand(self, brand: models.Brand):
        pass


class RelatedBrandInterface(abc.ABC):
    """Interface for CRUD operations with Related Brand"""

    @abc.abstractmethod
    def query_related_brands(self, **filters: dict):
        pass

    @abc.abstractmethod
    def save_relatedbrand(self, related_brand: models.RelatedBrand):
        pass

    @abc.abstractmethod
    def delete_relatedbrand(self, related_brand: models.RelatedBrand):
        pass

    @abc.abstractmethod
    def update_relatedbrand(self, related_brand: models.RelatedBrand):
        pass

class UserInterface(abc.ABC):
    """Interface for CRUD operations with User"""

    @abc.abstractmethod
    def query_users(self, **filters: dict):
        pass

    @abc.abstractmethod
    def save_user(self, user: models.User):
        pass

    @abc.abstractmethod
    def delete_user(self, user: models.User):
        pass

    @abc.abstractmethod
    def update_user(self, user: models.User):
        pass


class BrandUserInterface(abc.ABC):
    """Interface for CRUD operations with BrandUser"""

    @abc.abstractmethod
    def query_brand_users(self, **filters: dict):
        pass

    @abc.abstractmethod
    def save_branduser(self, brand_user: models.BrandUser):
        pass

    @abc.abstractmethod
    def delete_branduser(self, brand_user: models.BrandUser):
        pass

    @abc.abstractmethod
    def update_branduser(self, brand_user: models.BrandUser):
        pass

class BrandAssetInterface(abc.ABC):
    """Interface for CRUD operations with BrandAsset"""

    @abc.abstractmethod
    def query_brand_assets(self, **filters: dict):
        pass

    @abc.abstractmethod
    def save_brandasset(self, asset: models.BrandAsset):
        pass

    @abc.abstractmethod
    def delete_brandasset(self, asset: models.BrandAsset):
        pass

class SpacesInterface(abc.ABC):
    """Interface for CRUD operations with Spaces"""

    @abc.abstractmethod
    def query_spaces(self, **filters: dict):
        pass

    @abc.abstractmethod
    def save_space(self, space: models.Space):
        pass

    @abc.abstractmethod
    def update_space(self, space: models.Space):
        pass

    @abc.abstractmethod
    def delete_space(self, space: models.Space):
        pass

class StoresInterface(abc.ABC):
    """Interface for CRUD operations with Stores"""

    @abc.abstractmethod
    def query_stores(self, **filters: dict):
        pass

    @abc.abstractmethod
    def save_store(self, store: models.Store):
        pass

    @abc.abstractmethod
    def update_store(self, store: models.Store):
        pass

    @abc.abstractmethod
    def delete_store(self, store: models.Store):
        pass
    
class AdaptersInterface(abc.ABC):
    """Interface for CRUD operations with Adapters"""
    
    @abc.abstractmethod
    def query_adapters(self, **filters: dict):
        pass

    @abc.abstractmethod
    def save_adapterconfig(self, adapter: models.AdapterConfig):
        pass

    @abc.abstractmethod
    def delete_adapterconfig(self, adapter: models.AdapterConfig):
        pass

class AdapterStoresInterface(abc.ABC):
    """Interface for CRUD operations with AdapterStores"""

    @abc.abstractmethod
    def query_adapter_stores(self, **filters: dict):
        pass

    @abc.abstractmethod
    def save_adapterstore(self, adapter_store: models.AdapterStore):
        pass


class DBInterface(ActionInterface, MessageInterface, AuditLogInterface, BrandInterface, RelatedBrandInterface, UserInterface, BrandUserInterface, BrandAssetInterface, SpacesInterface, StoresInterface, AdaptersInterface, AdapterStoresInterface):
    """Parent interface just to hold all the other interfaces used for interacting with the database
    """
    pass
