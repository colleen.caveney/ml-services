from typing import Callable, cast, List
from leap.std.uuid import uuid4_factory, UUIDFactory
from leap.std.enum import TypedEnum
from enum import unique

_uuidfactory: UUIDFactory = uuid4_factory()
id_factory: Callable[[], str] = lambda: _uuidfactory().hex

LONG_TEXT_VALUE = 10000
SHORT_TEXT_VALUE = 1000
PERCENTAGE_MAX = 100
BRAND_PROFILE_BRAND_OVERVIEW_REQUIRED_KEYS = ['website']
BRAND_PROFILE_PRODUCT_MERCHANDISING_REQUIRED_KEYS = [
    'warehouses', 'damages_choice', 'rtv_choice', 'return_choice', 'exchange_choice'
]
BRAND_PROFILE_STORE_OPERATIONS_REQUIRED_KEYS = [
    'store_discounts_policy_confirmation', 'store_team_wardrobe_policy_confirmation',
    'store_team_allowance_policy_confirmation', 'store_inventory_counts_policy_confirmation',
    'activation_fund_allowance_policy_confirmation'
]


@unique
class StandardExceptionEnum(str, TypedEnum):
    STANDARD = "STANDARD"
    EXCEPTION = "EXCEPTION"


@unique
class SubmissionType(str, TypedEnum):
    SAVE = "SAVE"
    SUBMIT = "SUBMIT"

    @classmethod
    def names(cls) -> List[str]:
        return cast(List[str], cls._member_names_)


@unique
class NonApplicableEnum(str, TypedEnum):
    NON_APPLICABLE = "NON_APPLICABLE"
