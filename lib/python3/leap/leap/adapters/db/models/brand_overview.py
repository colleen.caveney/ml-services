from pydantic.dataclasses import dataclass
from pydantic import Field as field
from datetime import datetime
from enum import unique
from typing import List, Optional

from leap.adapters.db._models import Model
from leap.std.enum import TypedEnum
from . import BRAND_PROFILE_BRAND_OVERVIEW_REQUIRED_KEYS, LONG_TEXT_VALUE, SHORT_TEXT_VALUE, SubmissionType
from .model_helpers import check_url

CORE_VALUES_LEN = 7
CUSTOMER_PERSONAS_LEN = 3
SALES_CHANNELS_LEN = 4
GOALS_LEN = 4

@unique
class EducationEnum(str, TypedEnum):
    HIGH_SCHOOL = "HIGH_SCHOOL"
    HIGHER_EDUCATION = "HIGHER_EDUCATION"


@unique
class ChannelEnum(str, TypedEnum):
    ECOMMERCE = "ECOMMERCE"
    WHOLESALE = "WHOLESALE"
    PHYSICAL_RETAIL = "PHYSICAL_RETAIL"
    OTHER = "OTHER"


@unique
class GoalEnum(str, TypedEnum):
    CUSTOMER_BASE = "CUSTOMER_BASE"
    PROFITABILITY = "PROFITABILITY"
    INVENTORY_SELL_THROUGH = "INVENTORY_SELL_THROUGH"
    BRAND_AWARENESS = "BRAND_AWARENESS"


@dataclass
class CustomerPersona(Model):
    archetype: str = None
    age_start: int = None
    age_end: int = None
    education: EducationEnum = None
    income_start: int = None
    income_end: int = None
    story: str = None

    def __post_init__(self):
        if self.archetype and len(self.archetype) > SHORT_TEXT_VALUE:
            raise ValueError(f"Customer persona archetype is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.story and len(self.story) > LONG_TEXT_VALUE:
            raise ValueError(f"Customer persona story is too large. Must be less than {LONG_TEXT_VALUE} characters.")


@dataclass
class SalesChannel(Model):
    channel: ChannelEnum = None
    enabled: bool = None
    details: str = None

    def __post_init__(self):
        if self.details and len(self.details) > LONG_TEXT_VALUE:
            raise ValueError(f"Sales channel details is too large. Must be less than {LONG_TEXT_VALUE} characters.")


@dataclass
class SalesGoal(Model):
    goal: GoalEnum = None
    ranking: int = None
    goal_percent: float = None
    date: datetime = None


@dataclass
class BrandOverview(Model):
    website: Optional[str]
    core_values: List[str] = field(default_factory=list)
    customer_personas: List[CustomerPersona] = field(default_factory=list)
    sales_channels: List[SalesChannel] = field(default_factory=list)
    goals: List[SalesGoal] = field(default_factory=list, description="Replace field goals with goal per issue platform#2106", deprecated=True)
    goal: Optional[str] = field(None, description="Replace field goals with goal per issue platform#2106. Max 10000 characters")
    elevator_pitch: Optional[str] = None
    brand_story: Optional[str] = None
    brand_mission: Optional[str] = None
    submission_type: Optional[SubmissionType] = None

    def __post_init__(self):
        if self.submission_type == SubmissionType.SUBMIT.value:
            self.validate_on_submit(BRAND_PROFILE_BRAND_OVERVIEW_REQUIRED_KEYS)
        if self.website:
            if len(self.website) > SHORT_TEXT_VALUE:
                raise ValueError(f"Website is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
            if not check_url(self.website):
                raise ValueError(f"Website {self.website} is not a valid url.")
        if self.elevator_pitch and len(self.elevator_pitch) > SHORT_TEXT_VALUE:
            raise ValueError(f"Elevator Pitch is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.brand_story and len(self.brand_story) > LONG_TEXT_VALUE:
            raise ValueError(f"Brand Story is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.brand_mission and len(self.brand_mission) > LONG_TEXT_VALUE:
            raise ValueError(f"Brand Mission is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if isinstance(self.core_values, list):
            if len(self.core_values) > CORE_VALUES_LEN:
                raise ValueError(f"Too many core values. Only {CORE_VALUES_LEN} allowed maximum")
            for val in self.core_values:
                if len(val) > SHORT_TEXT_VALUE:
                    raise ValueError(f"Core value is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if isinstance(self.customer_personas, list) and len(self.customer_personas) > CUSTOMER_PERSONAS_LEN:
            raise ValueError(f"Too many customer personas. Only {CUSTOMER_PERSONAS_LEN} allowed maximum")
        if isinstance(self.sales_channels, list) and len(self.sales_channels) > SALES_CHANNELS_LEN:
            raise ValueError(f"Too many sales channels. Only {SALES_CHANNELS_LEN} allowed maximum")
        if isinstance(self.goals, list):
            if len(self.goals) > GOALS_LEN:
                raise ValueError(f"Too many goals. Only {GOALS_LEN} allowed maximum")
            self.goals = sorted(self.goals, key=lambda x: x.ranking)
        if isinstance(self.goal, str) and len(self.goal) > LONG_TEXT_VALUE:
            raise ValueError(f"goal is too large. Must be less than {LONG_TEXT_VALUE} characters.")
