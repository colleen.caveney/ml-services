from pydantic.dataclasses import dataclass
from pydantic import Field as field
from datetime import datetime
from typing import Optional, List
from uuid import UUID

from leap.adapters.db._models import Model
from leap.adapters.db._models import User
from . import SHORT_TEXT_VALUE, SubmissionType, id_factory
from .brand_overview import BrandOverview
from .integrations import Integrations
from .marketing import Marketing
from .product_merchandising import ProductMerchandising
from .social import Social
from .store_design import StoreDesign
from .store_operations import StoreOperations
from .visual_merchandising import VisualMerchandising


@dataclass
class BrandProfile(Model):
    brand_id: UUID
    id: UUID = field(default_factory=id_factory)
    created_at: datetime = field(default_factory=datetime.now)
    updated_at: datetime = field(default_factory=datetime.now)
    files_url: Optional[str] = None
    contacts: Optional[List[User]] = None
    overview_updated_by: Optional[UUID] = None
    overview: Optional[BrandOverview] = field(default_factory=BrandOverview)
    visual_merchandising_updated_by: Optional[UUID] = None
    visual_merchandising: Optional[VisualMerchandising] = field(default_factory=VisualMerchandising)
    product_merchandising_updated_by: Optional[UUID] = None
    product_merchandising: Optional[ProductMerchandising] = field(default_factory=ProductMerchandising)
    store_operations_updated_by: Optional[UUID] = None
    store_operations: Optional[StoreOperations] = field(default_factory=StoreOperations)
    submission_type: Optional[SubmissionType] = None
    store_design_updated_by: Optional[UUID] = None
    store_design: Optional[StoreDesign] = field(default_factory=StoreDesign)
    marketing_updated_by: Optional[UUID] = None
    marketing: Optional[Marketing] = field(default_factory=Marketing)
    integrations_updated_by: Optional[UUID] = None
    integrations: Optional[Integrations] = field(default_factory=Integrations)
    social_updated_by: Optional[UUID] = None
    social: Optional[Social] = field(default_factory=Social)

    def __post_init__(self):
        if self.files_url and len(self.files_url) > SHORT_TEXT_VALUE:
            raise ValueError(f"Files url is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
