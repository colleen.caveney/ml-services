from pydantic.dataclasses import dataclass
from pydantic import Field as field

from typing import Optional, Union
import uuid

from leap.adapters.db._models import Model
from . import id_factory

@dataclass
class EphemeralShopifyInstall(Model):
    """A table storing instance_ids of brands that have installed the ephemeral app in Shopify

    Fields:
        id (Optional[str]): unique, internal identifier for this Brand. Defaults to
            randomly generated UUID.

        instance_id (str): this is a service-agnostic account, domain, instance, etc id.
            For example, with Shopify, if you access your shop with the hostname
            `my-cool-shop.myshopify.com`, the instance_id in this case should be `my-cool-shop`.
            This value should be unique in general, but there may be situations where
            it is not.

        access_token (str): Shopify access token
    """

    instance_id: str
    access_token: Optional[str] = None
    id: Union[str, uuid.UUID] = field(default_factory=id_factory)
