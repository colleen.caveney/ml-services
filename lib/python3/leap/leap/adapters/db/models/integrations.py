from pydantic import Field as field
from pydantic.dataclasses import dataclass

from enum import unique
from typing import List, Optional, Union

from leap.adapters.db._models import Model
from leap.std.enum import TypedEnum
from . import LONG_TEXT_VALUE, SHORT_TEXT_VALUE, NonApplicableEnum, SubmissionType

SHORT_TEXT_INTEGRATIONS = ['COMMERCE', 'ERP', 'WMS', 'IMS', 'ACCOUNTING', 'THIRD_PARTY_LOGISTICS']
LONG_TEXT_INTEGRATIONS = ['RETURNS_PORTAL']


@unique
class IntegrationTypeEnum(str, TypedEnum):
    COMMERCE = "COMMERCE"
    ERP = "ERP"
    WMS = "WMS"
    THIRD_PARTY_LOGISTICS = "THIRD_PARTY_LOGISTICS"
    RETURNS_PORTAL = "RETURNS_PORTAL"

@unique
class DeprecatedIntegrationTypeEnum(str, TypedEnum):
    IMS = "IMS"
    ACCOUNTING = "ACCOUNTING"


@dataclass
class Software(Model):
    integration: Union[IntegrationTypeEnum, DeprecatedIntegrationTypeEnum]
    value: Optional[Union[str, NonApplicableEnum]]

    def __post_init__(self):
        if self.value and self.integration.value in SHORT_TEXT_INTEGRATIONS and len(self.value) > SHORT_TEXT_VALUE:
            raise ValueError(
                f"{self.integration.value.lower()} is too large (size={len(self.value)}). Must be less than {SHORT_TEXT_VALUE} characters."
            )
        if self.value and self.integration.value in LONG_TEXT_INTEGRATIONS and len(self.value) > LONG_TEXT_VALUE:
            raise ValueError(
                f"{self.integration.value.lower()} is too large (size={len(self.value)}). Must be less than {LONG_TEXT_VALUE} characters."
            )


@dataclass
class Integrations(Model):
    software: Optional[List[Software]] = field(default_factory=list)
    technical_returns_process: Optional[str] = field(None, description="Rename technical_returns_process to commerce_platform_details per issue platform#2101", deprecated=True)
    commerce_platform_details: Optional[str] = field(None, description="Rename technical_returns_process to commerce_platform_details per issue platform#2101")
    connected_shopify: Optional[bool] = field(None, description="Moved to integrations card per issue platform#2101")
    connected_shopify_more: Optional[str] = field(None, description="Moved to integrations card per issue platform#2101")
    affiliate_marketing_programs: Optional[str] = None
    upcoming_changes: Optional[str] = field(None, description="Removing field per issue platform#2101", deprecated=True)
    submission_type: Optional[SubmissionType] = None

    def __post_init__(self):
        if self.submission_type == SubmissionType.SUBMIT.value:
            set_fields = set([s.integration for s in self.software if s.value])
            missing_integrations = set(IntegrationTypeEnum.values()) - set_fields
            if missing_integrations:
                raise ValueError(f"Missing software integration choices for {sorted(missing_integrations)}")
        if isinstance(self.technical_returns_process, str) and len(self.technical_returns_process) > LONG_TEXT_VALUE:
            raise ValueError(
                f"technical_returns_process is too large (size={len(self.technical_returns_process)}). Must be less than {LONG_TEXT_VALUE} characters."
            )
        if self.affiliate_marketing_programs and len(self.affiliate_marketing_programs) > LONG_TEXT_VALUE:
            raise ValueError(
                f"affiliate_marketing_programs is too large (size={len(self.affiliate_marketing_programs)}). Must be less than {LONG_TEXT_VALUE} characters."
            )
        if isinstance(self.upcoming_changes, str) and len(self.upcoming_changes) > LONG_TEXT_VALUE:
            raise ValueError(
                f"upcoming_changes is too large (size={len(self.upcoming_changes)}). Must be less than {LONG_TEXT_VALUE} characters."
            )
