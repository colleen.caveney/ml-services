from pydantic.dataclasses import dataclass

from enum import unique
from typing import List, Optional

from leap.adapters.db._models import Model
from leap.std.enum import TypedEnum
from . import LONG_TEXT_VALUE, SubmissionType


@unique
class RetailMarketingChannelEnum(str, TypedEnum):
    AFFILIATE = "AFFILIATE"
    CONTENT_MARKETING = "CONTENT_MARKETING"
    DIRECT_MAIL = "DIRECT_MAIL"
    EMAIL = "EMAIL"
    PAID_SEARCH = "PAID_SEARCH"
    PR_EVENTS = "PR_EVENTS"
    RADIO = "RADIO"
    SEO = "SEO"
    SOCIAL = "SOCIAL"
    TV = "TV"
    OTHER = "OTHER"


@dataclass
class RetailMarketingChannel(Model):
    channel: RetailMarketingChannelEnum
    details: Optional[str]
    success: bool

    def __post_init__(self):
        if self.details and len(self.details) > LONG_TEXT_VALUE:
            raise ValueError(
                f"RetailMarketingChannel.details is too large. Must be less than {LONG_TEXT_VALUE} characters.")


@dataclass
class Partner(Model):
    details: Optional[str] = None

    def __post_init__(self):
        if self.details and len(self.details) > LONG_TEXT_VALUE:
            raise ValueError(
                f"Partner.details is too large (size={len(self.details)}). Must be less than {LONG_TEXT_VALUE} characters."
            )


@dataclass
class Marketing(Model):
    retail_marketing_details: Optional[str] = None
    ideal_event_details: Optional[str] = None
    event_dos: Optional[str] = None
    event_donts: Optional[str] = None
    non_profit_partners: Optional[List[Partner]] = None
    influencers: Optional[str] = None
    submission_type: Optional[SubmissionType] = None

    def __post_init__(self):
        if self.retail_marketing_details and len(self.retail_marketing_details) > LONG_TEXT_VALUE:
            raise ValueError(f"retail_marketing_details is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.ideal_event_details and len(self.ideal_event_details) > LONG_TEXT_VALUE:
            raise ValueError(f"ideal_event_details is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.event_dos and len(self.event_dos) > LONG_TEXT_VALUE:
            raise ValueError(f"event_dos is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.event_donts and len(self.event_donts) > LONG_TEXT_VALUE:
            raise ValueError(f"event_donts is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.non_profit_partners and len(self.non_profit_partners) > 10:
            raise ValueError(f"List of non_profit_partners is too long (size={len(self.non_profit_partners)}). Up to 10 allowed")
        if self.influencers and len(self.influencers) > LONG_TEXT_VALUE:
            raise ValueError(f"influencers is too large. Must be less than {LONG_TEXT_VALUE} characters.")
