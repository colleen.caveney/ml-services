from pydantic.dataclasses import dataclass
from pydantic import Field as field
from enum import unique
from typing import List, Optional

from leap.adapters.db._models import Model
from leap.std.enum import TypedEnum
from . import BRAND_PROFILE_PRODUCT_MERCHANDISING_REQUIRED_KEYS, LONG_TEXT_VALUE, PERCENTAGE_MAX, SHORT_TEXT_VALUE, StandardExceptionEnum, SubmissionType

WAREHOUSE_LEN_MAX = 7
PRODUCT_LAUNCH_QUANTITY_MAX = 50000

@unique
class ProductLaunchTimelineEnum(str, TypedEnum):
    WEEKLY = "WEEKLY"
    BI_WEEKLY = "BI_WEEKLY"
    MONTHLY = "MONTHLY"
    QUARTERLY = "QUARTERLY"
    OTHER = "OTHER"


@unique
class ProductCategoryEnum(str, TypedEnum):
    ACCESSORIES = "ACCESSORIES"
    BAGS_BACKPACKS_LUGGAGE = "BAGS_BACKPACKS_LUGGAGE"
    BEAUTY_HEALTH_PERSONAL_CARE = "BEAUTY_HEALTH_PERSONAL_CARE"
    BOTTOMS = "BOTTOMS"
    HOME = "HOME"
    INTIMATES = "INTIMATES"
    JEWELRY_WATCHES = "JEWELRY_WATCHES"
    OFFICE_SCHOOL_SUPPLIES = "OFFICE_SCHOOL_SUPPLIES"
    OTHER_CLOTHING = "OTHER_CLOTHING"
    SHOES = "SHOES"
    SWIMWSUITS_COVER_UPS = "SWIMWSUITS_COVER_UPS"
    TOPS = "TOPS"


@unique
class AccessoriesSubCategoryEnum(str, TypedEnum):
    SUNGLASSES_EYEWEAR_ACCESSORIES = "SUNGLASSES_EYEWEAR_ACCESSORIES"
    HATS_CAPS_HEADWEAR = "HATS_CAPS_HEADWEAR"
    BELTS = "BELTS"
    WALLETS_CARD_CASES__MONEY_ORGANIZERS = "WALLETS_CARD_CASES__MONEY_ORGANIZERS"
    TIES = "TIES"
    COLD_WEATHER_ACCESSORIES = "COLD_WEATHER_ACCESSORIES"
    GOLF_ACCESSORIES = "GOLF_ACCESSORIES"
    SHOE_ACCESSORIES = "SHOE_ACCESSORIES"
    KEYRINGS_KEYCHAINS = "KEYRINGS_KEYCHAINS"
    SUSPENDERS = "SUSPENDERS"
    UMBRELLAS = "UMBRELLAS"


@unique
class BagsBackpacksLuggageSubCategoryEnum(str, TypedEnum):
    BACKPACKS = "BACKPACKS"
    LUGGAGE_TRAVEL_GEAR = "LUGGAGE_TRAVEL_GEAR"
    CLUTCHES_EVENING_BAGS = "CLUTCHES_EVENING_BAGS"
    CROSSBODY_BAGS = "CROSSBODY_BAGS"
    GOLF_BAGS = "GOLF_BAGS"
    LAPTOP_BAGS_CASES = "LAPTOP_BAGS__CASES"
    SATCHELS = "SATCHELS"
    SHOULDER_BAGS = "SHOULDER_BAGS"
    TOTES = "TOTES"
    WRISTLETS = "WRISTLETS"


@unique
class BeautyHealthPersonalSubCategoryEnum(str, TypedEnum):
    MAKEUP = "MAKEUP"
    HAIR_CARE = "HAIR_CARE"
    SKIN_CARE = "SKIN_CARE"
    FRAGRANCES = "FRAGRANCES"


@unique
class BottomsSubCategoryEnum(str, TypedEnum):
    PANTS = "PANTS"
    SHORTS = "SHORTS"
    JEANS = "JEANS"
    SKIRTS = "SKIRTS"
    LEGGINGS = "LEGGINGS"


@unique
class HomeSubCategoryEnum(str, TypedEnum):
    CLOTHING_CLOSET_STORAGES = "CLOTHING_CLOSET_STORAGES"
    RUGS = "RUGS"
    KITCHEN_DINING = "KITCHEN_DINING"
    TOWELS = "TOWELS"
    BATHROOM_ACCESSORIES = "BATHROOM_ACCESSORIES"
    PILLOWS = "PILLOWS"
    BLANKETS_THROWS = "BLANKETS_THROWS"
    HOME_DECOR = "HOME_DECOR"
    BEDDINGS = "BEDDINGS"


@unique
class IntimatesSubCategoryEnum(str, TypedEnum):
    SOCKS_HOSIERY = "SOCKS_HOSIERY"
    UNDERWEAR = "UNDERWEAR"
    SLEEPWEAR_LOUNGEWEAR = "SLEEPWEAR_LOUNGEWEAR"
    BRAS = "BRAS"


@unique
class JewelryWatchesSubCategoryEnum(str, TypedEnum):
    ANKLETS = "ANKLETS"
    DIAMONDS = "DIAMONDS"
    BRACELETS = "BRACELETS"
    WEDDING_ENGAGEMENT = "WEDDING_ENGAGEMENT"
    BROOCHES_PINS = "BROOCHES_PINS"
    NECKLACES = "NECKLACES"
    MENS_ACCESSORIES = "MENS_ACCESSORIES"
    EARRINGS = "EARRINGS"
    JEWELRY_SETS = "JEWELRY_SETS"
    WATCHES = "WATCHES"
    RINGS = "RINGS"


@unique
class OfficeSchoolSuppliesSubCategoryEnum(str, TypedEnum):
    WRITING_CORRECTION_SUPPLIES = "WRITING_CORRECTION_SUPPLIES"
    BINDERS_BINDING_SYSTEMS = "BINDERS_BINDING_SYSTEMS"
    BOOK_COVERS_BOOK_ACCESSORIES = "BOOK_COVERS_BOOK_ACCESSORIES"
    PAPER_PRODUCTS = "PAPER_PRODUCTS"
    ENVELOPES_MAILERS_SHIPPING_SUPPLIES = "ENVELOPES_MAILERS_SHIPPING_SUPPLIES"
    FILING_PRODUCTS = "FILING_PRODUCTS"
    LABELS_ID_BADGES_SUPPLIES = "LABELS_ID_BADGES_SUPPLIES"
    DESK_ACCESSORIES_WORKSPACE_ORGANIZERS = "DESK_ACCESSORIES_WORKSPACE_ORGANIZERS"
    CALENDARS_PLANNERS_PERSONAL_ORGANIZERS = "CALENDARS_PLANNERS_PERSONAL_ORGANIZERS"
    STORE_SIGNS_DISPLAY = "STORE_SIGNS_DISPLAY"


@unique
class OtherClothingSubCategoryEnum(str, TypedEnum):
    DRESSES = "DRESSES"
    CLOTHING_SETS = "CLOTHING_SETS"
    JUMPSUITS_ROMPERS_OVERALLS = "JUMPSUITS_ROMPERS_OVERALLS"


@unique
class ShoesSubCategoryEnum(str, TypedEnum):
    BOOTS = "BOOTS"
    SNEAKERS = "SNEAKERS"
    SANDALS = "SANDALS"
    FLATS = "FLATS"
    LOAFERS_SLIP_ONS = "LOAFERS_SLIP_ONS"
    MULES_CLOGS = "MULES_CLOGS"
    OXFORDS = "OXFORDS"
    PUMPS = "PUMPS"
    SLIPPERS = "SLIPPERS"


@unique
class SwimsuitsCoverUpsSubCategoryEnum(str, TypedEnum):
    BIKINIS = "BIKINIS"
    COVER_UPS = "COVER_UPS"
    ONE_PIECE_SWIMSUITS = "ONE_PIECE_SWIMSUITS"
    OTHER_SWIMSUITS_COVER_UPS = "OTHER_SWIMSUITS_COVER_UPS"
    RASH_GUARDS = "RASH_GUARDS"
    SWIM_TRUNKS_BOARD_SHORTS = "SWIM_TRUNKS_BOARD_SHORTS"
    TANKINIS = "TANKINIS"
    TWO_PIECE_SWIMSUITS = "TWO_PIECE_SWIMSUITS"


@unique
class TopsSubCategoryEnum(str, TypedEnum):
    SUITING_BLAZERS = "SUITING_BLAZERS"
    SHIRTS_TOPS = "SHIRTS_TOPS"
    SWEATERS = "SWEATERS"
    HOODIES_SWEATSHIRTS = "HOODIES_SWEATSHIRTS"
    COATS_JACKETS_VESTS = "COATS_JACKETS_VESTS"


@dataclass
class Warehouse(Model):
    name: Optional[str] = None
    address: Optional[str] = None
    city: Optional[str] = None
    state: Optional[str] = None
    zip_code: Optional[str] = None
    po_box: Optional[str] = None
    latitude: Optional[float] = None
    longitude: Optional[float] = None
    processing_time_start: Optional[int] = None
    processing_time_end: Optional[int] = None
    shipping_time_start: Optional[int] = None
    shipping_time_end: Optional[int] = None

    def __post_init__(self):
        if self.name and len(self.name) > SHORT_TEXT_VALUE:
            raise ValueError(f"Name is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.address and len(self.address) > SHORT_TEXT_VALUE:
            raise ValueError(f"Address is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.city and len(self.city) > SHORT_TEXT_VALUE:
            raise ValueError(f"City is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.state and len(self.state) > SHORT_TEXT_VALUE:
            raise ValueError(f"State is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.zip_code and len(self.zip_code) > SHORT_TEXT_VALUE:
            raise ValueError(f"Zip code is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.po_box and len(self.po_box) > SHORT_TEXT_VALUE:
            raise ValueError(f"PO BOX is too large. Must be less than {SHORT_TEXT_VALUE} characters.")


@dataclass
class DamagesAddress(Model):
    attn: Optional[str] = None
    address: Optional[str] = None
    city: Optional[str] = None
    state: Optional[str] = None
    po_box: Optional[str] = None
    zip_code: Optional[str] = None
    latitude: Optional[float] = None
    longitude: Optional[float] = None

    def __post_init__(self):
        if self.attn and len(self.attn) > SHORT_TEXT_VALUE:
            raise ValueError(f"Attention is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.address and len(self.address) > SHORT_TEXT_VALUE:
            raise ValueError(f"Address is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.city and len(self.city) > SHORT_TEXT_VALUE:
            raise ValueError(f"City is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.state and len(self.state) > SHORT_TEXT_VALUE:
            raise ValueError(f"State is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.zip_code and len(self.zip_code) > SHORT_TEXT_VALUE:
            raise ValueError(f"Zip code is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.po_box and len(self.po_box) > SHORT_TEXT_VALUE:
            raise ValueError(f"PO BOX is too large. Must be less than {SHORT_TEXT_VALUE} characters.")


@dataclass
class Categories(Model):
    accessories: Optional[List[AccessoriesSubCategoryEnum]] = None
    bags_backpacks_luggage: Optional[List[BagsBackpacksLuggageSubCategoryEnum]] = None
    beauty_health_personal_care: Optional[List[BeautyHealthPersonalSubCategoryEnum]] = None
    bottoms: Optional[List[BottomsSubCategoryEnum]] = None
    home: Optional[List[HomeSubCategoryEnum]] = None
    intimates: Optional[List[IntimatesSubCategoryEnum]] = None
    jewelry_watches: Optional[List[JewelryWatchesSubCategoryEnum]] = None
    office_school_supplies: Optional[List[OfficeSchoolSuppliesSubCategoryEnum]] = None
    other_clothing: Optional[List[OtherClothingSubCategoryEnum]] = None
    shoes: Optional[List[ShoesSubCategoryEnum]] = None
    swimsuits_cover_ups: Optional[List[SwimsuitsCoverUpsSubCategoryEnum]] = None
    tops: Optional[List[TopsSubCategoryEnum]] = None
    other: Optional[str] = None

    def __post_init__(self):
        if self.other and len(self.other) > SHORT_TEXT_VALUE:
            raise ValueError(f"Other categories is too large. Must be less than {SHORT_TEXT_VALUE} characters.")


@dataclass
class ProductMerchandising(Model):
    # Required
    damages_choice: Optional[StandardExceptionEnum]
    return_choice: Optional[StandardExceptionEnum]
    exchange_choice: Optional[StandardExceptionEnum]
    warehouses: Optional[List[Warehouse]]
    rtv_choice: Optional[StandardExceptionEnum] = field(description="Rename rtv_choice to return_to_brand_choice per issue platform#2105", deprecated=True)
    return_to_brand_choice: Optional[StandardExceptionEnum] = field(description="Rename rtv_choice to return_to_brand_choice per issue platform#2105")
    damages_address: Optional[DamagesAddress] = None
    rtv_exception: Optional[str] = field(None, description="Rename rtv_exception to return_to_brand_exception per issue platform#2105", deprecated=True)
    return_to_brand_exception: Optional[str] = field(None, description="Rename rtv_exception to return_to_brand_exception per issue platform#2105")
    labeling_confirmation: Optional[bool] = None
    return_exception: Optional[str] = None
    exchange_exception: Optional[str] = None
    connected_shopify: Optional[bool] = field(None, description="Moved to integrations card per issue platform#2101.", deprecated=True)
    connected_shopify_more: Optional[str] = field(None, description="Moved to integrations card per issue platform#2101.", deprecated=True)
    # Not required
    core_assortment_percentage: Optional[int] = None
    seasonal_assortment_percentage: Optional[int] = None
    product_launch_quantity: Optional[int] = None
    product_launch_timeline: Optional[ProductLaunchTimelineEnum] = None
    product_launch_notes: Optional[str] = None
    product_category_include: Optional[Categories] = None
    product_category_excluded: Optional[Categories] = field(None, description="Remove field per issue platform#2105", deprecated=True)
    product_category_new: Optional[Categories] = None
    submission_type: Optional[SubmissionType] = None

    def __post_init__(self):
        if self.submission_type == SubmissionType.SUBMIT.value:
            self.validate_on_submit(BRAND_PROFILE_PRODUCT_MERCHANDISING_REQUIRED_KEYS)
        if self.warehouses and len(self.warehouses) > WAREHOUSE_LEN_MAX:
            raise ValueError(f"Warehouses is too large. Must be less than {WAREHOUSE_LEN_MAX}.")
        if self.core_assortment_percentage and self.core_assortment_percentage > PERCENTAGE_MAX:
            raise ValueError(f"Core percentage is too large. Must be less than {PERCENTAGE_MAX}.")
        if self.seasonal_assortment_percentage and self.seasonal_assortment_percentage > PERCENTAGE_MAX:
            raise ValueError(f"Seasonal percentage is too large. Must be less than {PERCENTAGE_MAX}.")
        if self.product_launch_quantity and self.product_launch_quantity > PRODUCT_LAUNCH_QUANTITY_MAX:
            raise ValueError(f"Product launch is too large. Must be less than {PRODUCT_LAUNCH_QUANTITY_MAX}.")
        if self.product_launch_notes and len(self.product_launch_notes) > LONG_TEXT_VALUE:
            raise ValueError(f"Product launch notes is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if isinstance(self.rtv_exception, str) and len(self.rtv_exception) > LONG_TEXT_VALUE:
            raise ValueError(f"Return to vendor exception is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if isinstance(self.return_to_brand_exception, str) and len(self.return_to_brand_exception) > LONG_TEXT_VALUE:
            raise ValueError(
                f"Return to vendor exception is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.return_exception and len(self.return_exception) > LONG_TEXT_VALUE:
            raise ValueError(f"Return exception is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if isinstance(self.connected_shopify_more, str) and len(self.connected_shopify_more) > SHORT_TEXT_VALUE:
            raise ValueError(f"Connected shopify is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.exchange_exception and len(self.exchange_exception) > LONG_TEXT_VALUE:
            raise ValueError(f"Exchange exception is too large. Must be less than {LONG_TEXT_VALUE} characters.")
