from pydantic.dataclasses import dataclass
from pydantic import Field as field
from enum import unique
from typing import ClassVar, List, Optional
from datetime import datetime

from leap.adapters.db._models import Model
from leap.std.enum import TypedEnum

@unique
class CurrentSpaceConditionEnum(str, TypedEnum):
    CONDITION_0 = "Condition 0"
    CONDITION_1 = "Condition 1"
    CONDITION_2 = "Condition 2"


@unique
class SpaceStatusEnum(str, TypedEnum):
    SPACE_PROPOSED = "Space Proposed"
    NO_LEASE = "No Lease"
    ACTIVE_LEASE = "Active Lease"
    LEASE_NOT_RENEWED = "Lease Not Renewed"


@unique
class SpaceTypeEnum(str, TypedEnum):
    STREET = "Street"
    STREET_WALK_UP = "Street: Walk Up"
    STREET_WALK_DOWN = "Street: Walk Down"
    INDOOR_MALL = "Indoor Mall"
    OUTDOOR_MALL = "Outdoor Mall"
    INDOOR_OUTDOOR_MALL = "Indoor/Outdoor Mall"

@unique
class StoreStageEnum(str, TypedEnum):
    SPACE_UNAVAILABLE = "Space Unavailable"
    SPACE_PASS = "Space Pass"
    POTENTIAL = "Potential"
    CONTRACTING = "Contracting"
    ONBOARDING = "Onboarding"
    LIVE = "Live"
    STORE_CLOSURE = "Store Closure"
    STORE_CLOSED = "Store Closed"


@unique
class StoreProductTypeEnum(str, TypedEnum):
    ACCESSORIES = "Accessories"
    APPAREL = "Apparel"
    ELECTRONICS = "Electronics"
    EVERYDAY = "Everyday"
    HEALTH = "Health"
    HOME = "Home"
    SHOES = "Shoes"
    OTHER = "Other"

@dataclass
class SalesforceSpace(Model):
    salesforce_space_id: str
    brand_handle: str
    brand_guid: Optional[str] = None
    brand_code: Optional[str] = None
    brand_name: Optional[str] = None
    current_space_condition: Optional[CurrentSpaceConditionEnum] = None
    total_foot_traffic_last_twelve_months: Optional[int] = None
    space_media_url: Optional[str] = None
    market: Optional[str] = None
    sub_market: Optional[str] = None
    shopper_average_household_income: Optional[float] = None
    space_status: Optional[SpaceStatusEnum] = None
    space_type: Optional[SpaceTypeEnum] = None
    backroom_square_footage: Optional[int] = None
    sellable_square_footage: Optional[int] = None
    total_square_footage: Optional[float] = None
    state: Optional[str] = None
    city: Optional[str] = None
    street_address_1: Optional[str] = None
    street_address_2: Optional[str] = None
    total_monthly_rent: Optional[float] = None
    zip_code: Optional[str] = None
    selected: Optional[bool] = False
    store_stage: Optional[StoreStageEnum] = None
    store_product_type: Optional[StoreProductTypeEnum] = None
    created_date: Optional[datetime] = field(default_factory=datetime.now)
    image_urls: Optional[List[str]] = field(default_factory=list)
    mall_name: Optional[str] = None
    co_tenants: Optional[List[str]] = field(default_factory=list)
    submarket_vibe: Optional[str] = None

    # TODO Remove this when we have actual submarket_vibe field in SF
    SUBMARKET_VIBE_FILLER_TEXT: ClassVar[str] = "Situated on the high-profile Damen Retail Corridor, this unique converted firehouse is in the heart of Bucktown and less than 500 feet from the Damen Blue Line station with more than 2.2 million riders annually."

    def full_address(self):
        return f"{self.street_address_1} {self.street_address_2}, {self.city}, {self.state}, {self.zip_code}"
