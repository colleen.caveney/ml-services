from pydantic.dataclasses import dataclass
from pydantic import Field as field
from datetime import datetime
from uuid import UUID

from leap.adapters.db._models import Model
from . import id_factory


@dataclass
class SelectSpace(Model):
    salesforce_space_id: str
    brand_id: UUID
    user_id: UUID
    id: UUID = field(default_factory=id_factory)
    created_at: datetime = field(default_factory=datetime.now)
    updated_at: datetime = field(default_factory=datetime.now)
