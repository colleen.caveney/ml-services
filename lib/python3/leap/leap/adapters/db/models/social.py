from pydantic import Field as field
from pydantic.dataclasses import dataclass
from enum import unique
from typing import List, Optional, Union

from leap.adapters.db._models import Model
from leap.std.enum import TypedEnum
from . import SHORT_TEXT_VALUE, NonApplicableEnum, SubmissionType
from .model_helpers import check_url


@unique
class SocialTypeEnum(str, TypedEnum):
    FACEBOOK = "FACEBOOK"
    YOUTUBE = "YOUTUBE"
    INSTAGRAM = "INSTAGRAM"
    TIKTOK = "TIKTOK"
    PINTEREST = "PINTEREST"
    SNAPCHAT = "SNAPCHAT"
    LINKEDIN = "LINKEDIN"
    TWITTER = "TWITTER"


@dataclass
class Platform(Model):
    social: SocialTypeEnum
    value: Optional[Union[str, NonApplicableEnum]]

    def __post_init__(self):
        if self.value:
            if len(self.value) > SHORT_TEXT_VALUE:
                raise ValueError(f"{self.social.value.lower()} url is too large (size={len(self.value)}). Must be less than {SHORT_TEXT_VALUE} characters.")
            if self.value != NonApplicableEnum.NON_APPLICABLE and not check_url(self.value):
                raise ValueError(f"The value {self.value} is not a valid url")


@dataclass
class Social(Model):
    platform: Optional[List[Platform]] = field(default_factory=list)
    submission_type: Optional[SubmissionType] = None

    def __post_init__(self):
        if self.submission_type == SubmissionType.SUBMIT.value:
            set_fields = set([s.social for s in self.platform if s.value])
            missing_social = set(SocialTypeEnum.values()) - set_fields
            if missing_social:
                raise ValueError(f"Missing social platform choices for {sorted(missing_social)}")
