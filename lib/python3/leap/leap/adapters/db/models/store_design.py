from pydantic.dataclasses import dataclass
import re
from enum import unique
from typing import List, Optional

from leap.adapters.db._models import Model
from leap.std.enum import TypedEnum
from . import LONG_TEXT_VALUE, SubmissionType


@unique
class AestheticImageEnum(str, TypedEnum):
    HOMEY_AND_COZY = "HOMEY_AND_COZY"
    RAW_AND_INDUSTRIAL = "RAW_AND_INDUSTRIAL"
    MODERN_AND_MINIMAL = "MODERN_AND_MINIMAL"

@unique
class VibeImageEnum(str, TypedEnum):
    LIGHT_AIRY_AND_BRIGHT = "LIGHT_AIRY_AND_BRIGHT"
    MOODY_AND_DRAMATIC = "MOODY_AND_DRAMATIC"

@unique
class BrandFixtureImageEnum(str, TypedEnum):
    OPTION_1 = "OPTION_1"
    OPTION_2 = "OPTION_2"
    OPTION_3 = "OPTION_3"
    OPTION_4 = "OPTION_4"

@unique
class MetalFixtureFinishMaterialEnum(str, TypedEnum):
    ALUMINUM = "ALUMINUM"
    BLACK = "BLACK"
    GRAPHITE = "GRAPHITE"
    WHITE = "WHITE"

@unique
class MillworkMaterialEnum(str, TypedEnum):
    BEIGEWOOD = "BEIGEWOOD"
    BLACK = "BLACK"
    WHITE = "WHITE"


@dataclass
class StoreDesign(Model):
    brand_colors: Optional[List[str]] = None
    mood_board_aesthetic_image: Optional[AestheticImageEnum] = None
    mood_board_vibe_image: Optional[VibeImageEnum] = None
    key_elements: Optional[str] = None
    additional_comments: Optional[str] = None
    submission_type: Optional[SubmissionType] = None
    brand_fixture_image: Optional[BrandFixtureImageEnum] = None
    metal_fixture_finish_material: Optional[MetalFixtureFinishMaterialEnum] = None
    millwork_material: Optional[MillworkMaterialEnum] = None

    def __post_init__(self):
        if self.key_elements and len(self.key_elements) > LONG_TEXT_VALUE:
            raise ValueError(f"Key elements is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.additional_comments and len(self.additional_comments) > LONG_TEXT_VALUE:
            raise ValueError(f"Additional_comments is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.brand_colors:
            if len(self.brand_colors) > 3:
                raise ValueError(f"Too many brand colors, Only 3 brand colors allowed")
            r = re.compile(r'^#(?:[0-9a-fA-F]{3}){1,2}$')
            invalid_hex_colors = set(self.brand_colors) - set(filter(r.match, self.brand_colors))
            if invalid_hex_colors:
                raise ValueError(f"{sorted(invalid_hex_colors)} are not valid hex colors")
