from pydantic.dataclasses import dataclass
from pydantic import Field as field
from enum import unique
from typing import List, Optional

from leap.adapters.db._models import Model
from leap.std.enum import TypedEnum
from . import BRAND_PROFILE_STORE_OPERATIONS_REQUIRED_KEYS, LONG_TEXT_VALUE, SHORT_TEXT_VALUE, StandardExceptionEnum, SubmissionType

MAX_SELECTION = 10

@unique
class ScentSelectionEnum(str, TypedEnum):
    CLEAN_EARTHY = "CLEAN_EARTHY"
    BEACHY_TROPICAL = "BEACHY_TROPICAL"


@dataclass
class StoreOperations(Model):
    store_discounts_policy_confirmation: Optional[StandardExceptionEnum]
    store_team_wardrobe_policy_confirmation: Optional[StandardExceptionEnum]
    store_team_allowance_policy_confirmation: Optional[StandardExceptionEnum]
    store_inventory_counts_policy_confirmation: Optional[StandardExceptionEnum]
    activation_fund_allowance_policy_confirmation: Optional[StandardExceptionEnum]
    music_genre_selection: Optional[List[str]] = field(default_factory=list, description="Update field music_genre_selection to music_playlist added in issue app#586", deprecated=True)
    music_playlist: Optional[str] = field(None, description="Update field music_genre_selection to music_playlist added in issue app#586")
    music_artist_selection: Optional[List[str]] = field(None, description="Removed field per issue app#586", deprecated=True)
    employee_uniform_details: Optional[str] = field(None, description="New field added in issue app#586. Max 1000 characters.")
    scent_selection: Optional[ScentSelectionEnum] = field(None, description="Update field from scent_selection to store_scent per issue app#586", deprecated=True)
    store_scent: Optional[str] = field(None, description="Update field from scent_selection to store_scent per issue app#586")
    store_discounts_policy_exception: Optional[str] = None
    store_team_wardrobe_policy_exception: Optional[str] = None
    store_team_allowance_policy_exception: Optional[str] = None
    store_inventory_counts_policy_exception: Optional[str] = None
    activation_fund_allowance_policy_exception: Optional[str] = None
    submission_type: Optional[SubmissionType] = None

    def __post_init__(self):
        if self.submission_type == SubmissionType.SUBMIT.value:
            self.validate_on_submit(
                BRAND_PROFILE_STORE_OPERATIONS_REQUIRED_KEYS)
        if isinstance(self.music_genre_selection, list):
            if len(self.music_genre_selection) > MAX_SELECTION:
                raise ValueError(
                    f"Music genre is too large. Must be less than {MAX_SELECTION} options.")
            for m in self.music_genre_selection:
                if len(m) > SHORT_TEXT_VALUE:
                    raise ValueError(
                        f"Music genre choice is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if isinstance(self.music_playlist, str) and len(self.music_playlist) > LONG_TEXT_VALUE:
            raise ValueError(f"music_playlist is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if isinstance(self.music_artist_selection, list):
            if len(self.music_artist_selection) > MAX_SELECTION:
                raise ValueError(f"Music artist is too large. Must be less than {MAX_SELECTION} options.")
            for m in self.music_artist_selection:
                if len(m) > SHORT_TEXT_VALUE:
                    raise ValueError(f"Music artist choice is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if self.store_discounts_policy_exception and len(self.store_discounts_policy_exception) > LONG_TEXT_VALUE:
            raise ValueError(f"Store discount exception is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.store_team_wardrobe_policy_exception and len(self.store_team_wardrobe_policy_exception) > LONG_TEXT_VALUE:
            raise ValueError(f"Store wardrobe exception is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.store_team_allowance_policy_exception and len(self.store_team_allowance_policy_exception) > LONG_TEXT_VALUE:
            raise ValueError(f"Store allowance exception is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.store_inventory_counts_policy_exception and len(self.store_inventory_counts_policy_exception) > LONG_TEXT_VALUE:
            raise ValueError(f"Store inventory count exception is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if self.activation_fund_allowance_policy_exception and len(self.activation_fund_allowance_policy_exception) > LONG_TEXT_VALUE:
            raise ValueError(f"Store activation fund exception is too large. Must be less than {LONG_TEXT_VALUE} characters.")
        if isinstance(self.employee_uniform_details, str) and len(self.employee_uniform_details) > SHORT_TEXT_VALUE:
            raise ValueError(f"employee_uniform_details is too large. Must be less than {SHORT_TEXT_VALUE} characters.")
        if isinstance(self.store_scent, str) and len(self.store_scent) > LONG_TEXT_VALUE:
            raise ValueError(f"store_scent is too large. Must be less than {LONG_TEXT_VALUE} characters.")
