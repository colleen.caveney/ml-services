from pydantic.dataclasses import dataclass
from enum import unique
from typing import Callable, Optional

from leap.adapters.db._models import Model
from leap.std.enum import TypedEnum
from leap.std.uuid import uuid4_factory
from . import SHORT_TEXT_VALUE, SubmissionType

id_factory: Callable[[], str] = lambda: uuid4_factory().hex


@unique
class VisualMerchandisingImageEnum(str, TypedEnum):
    GALLERY = "GALLERY"
    BOUTIQUE = "BOUTIQUE"
    CAPACITY = "CAPACITY"


@unique
class HangerOwnershipEnum(str, TypedEnum):
    MY_BRAND = "MY_BRAND"
    LEAP = "LEAP"
    NA = "NA"


@unique
class HangerMaterialEnum(str, TypedEnum):
    METAL = "METAL"
    WOOD = "WOOD"
    PLASTIC = "PLASTIC"


@unique
class MetalColorsEnum(str, TypedEnum):
    SILVER = "SILVER"
    ROSE_GOLD = "ROSE_GOLD"
    GOLD = "GOLD"


@unique
class WoodColorsEnum(str, TypedEnum):
    BLACK = "BLACK"
    WHITE = "WHITE"
    BLONDE = "BLONDE"
    WALNUT = "WALNUT"

@unique
class PlasticColorsEnum(str, TypedEnum):
    CLEAR = "CLEAR"
    BLACK = "BLACK"


@unique
class ProductPlacementImageEnum(str, TypedEnum):
    COLLECTION_LIFESTYLE = "COLLECTION_LIFESTYLE"
    COLOR_STORY = "COLOR_STORY"
    ITEM_STATEMENT = "ITEM_STATEMENT"


@dataclass
class Material(Model):
    material_type: HangerMaterialEnum
    color: Optional[str] = None

@dataclass
class MetalMaterial(Material):
    material_type = HangerMaterialEnum.METAL
    color: Optional[MetalColorsEnum] = None


@dataclass
class WoodMaterial(Material):
    material_type = HangerMaterialEnum.WOOD
    color: Optional[WoodColorsEnum] = None


@dataclass
class PlasticMaterial(Material):
    material_type = HangerMaterialEnum.PLASTIC
    color: Optional[PlasticColorsEnum] = None


@dataclass
class VisualMerchandising(Model):
    visual_merchandising_image: Optional[VisualMerchandisingImageEnum] = None
    visual_merchandising_note: Optional[str] = None
    product_placement_image: Optional[ProductPlacementImageEnum] = None
    product_placement_note: Optional[str] = None
    hanger_ownership: Optional[HangerOwnershipEnum] = None
    material: Optional[Material] = None
    submission_type: Optional[SubmissionType] = None

    def __post_init__(self):
        if self.visual_merchandising_note and len(self.visual_merchandising_note) > SHORT_TEXT_VALUE:
            raise ValueError("Visual merchandising note is too large")
        if self.product_placement_note and len(self.product_placement_note) > SHORT_TEXT_VALUE:
            raise ValueError("Product placement note is too large")
        self.set_material()

    def set_material(self):
        try:
            if self.material:
                if self.material.material_type == HangerMaterialEnum.METAL:
                    self.material = MetalMaterial(HangerMaterialEnum.METAL, MetalColorsEnum(self.material.color) if self.material.color else None)
                elif self.material.material_type == HangerMaterialEnum.WOOD:
                    self.material = WoodMaterial(HangerMaterialEnum.WOOD, WoodColorsEnum(self.material.color) if self.material.color else None)
                else:
                    self.material = PlasticMaterial(HangerMaterialEnum.PLASTIC, PlasticColorsEnum(self.material.color) if self.material.color else None)
        except ValueError:
            raise ValueError(f"Material type {self.material.material_type} and color {self.material.color} are not a valid combination")
