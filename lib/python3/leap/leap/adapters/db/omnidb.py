import logging
from typing import Dict

from ._db import DB
from ._models import Model
from psycopg2 import errors

class OmniDBManager:
    """Manages multiple DB connections/interfaces and handles CRUD operations to multiple sources
    """

    def __init__(self, **db_interfaces: Dict[str, DB]):
        """
        db_interfaces: dictionary of db interfaces
        Example:
        {
            "firestore": FirestoreDB(),
            "platformdb": PlatformDB()
        }
        """
        self.db_interfaces = db_interfaces
        self.retry_limit = 2

    def add_db_interface(self, name: str, interface):
        """Add db interface to map

        Args:
            name (str): Name of db interface
            interface (DB): db interface object
        """
        self.db_interfaces[name] = interface

    def write(self, model: Model, **kwargs: dict):
        model_name = model.__class__.__name__.lower()
        for db_name, interface in self.db_interfaces.items():
            if getattr(model, db_name, False) and interface:
                try:
                    getattr(interface, f"save_{model_name}")(model)
                    logging.debug(f"Saving {model_name} to {db_name}: {model.to_object()}")
                except (errors.ConnectionException, errors.ConnectionFailure, errors.OperationalError) as e:
                    if kwargs.get("retry", False):
                        raise e
                    logging.info("retrying connection due to sql_proxy dropping connection")
                    self.write(model, retry=True)
                except Exception as err:
                    logging.error(
                        f"Unable to save {model_name} in {db_name}: {model}", exc_info=err)
                    return None
        return model

    def query(self, db_name: str, table: str, **kwargs: dict):
        if db_name in self.db_interfaces:
            interface = self.db_interfaces[db_name]
        else:
            logging.debug(f"Database is not supported for {db_name}")
            return None
        try:
            result = getattr(interface, f"query_{table}")(**kwargs)
            logging.debug(f"Querying {table} from {db_name}")
        except (errors.ConnectionException, errors.ConnectionFailure, errors.OperationalError) as e:
            if kwargs.get("retry", False):
                raise e
            logging.info("retrying connection due to sql_proxy dropping connection")
            self.query(db_name, table, retry=True, **kwargs)
        except Exception as err:
            logging.error(f"Unable to query {table} from {db_name}: {kwargs}", exc_info=err)
            result = None
        return result

    def delete(self, model: Model, **kwargs: dict):
        model_name = model.__class__.__name__.lower()
        for db_name, interface in self.db_interfaces.items():
            if getattr(model, db_name, False) and interface:
                try:
                    getattr(interface, f"delete_{model_name}")(model)
                    logging.debug(
                        f"Deleting {model_name} from {db_name}: {model.to_object()}")
                except (errors.ConnectionException, errors.ConnectionFailure, errors.OperationalError) as e:
                    if kwargs.get("retry", False):
                        raise e
                    logging.info("retrying connection due to sql_proxy dropping connection")
                    self.delete(model, retry=True)
                except Exception as err:
                    logging.error(f"Unable to delete {model_name} in {db_name}: {model}", exc_info=err)
        return model

    def update(self, model: Model, **kwargs: dict):
        model_name = model.__class__.__name__.lower()
        for db_name, interface in self.db_interfaces.items():
            if getattr(model, db_name, False) and interface:
                try:
                    getattr(interface, f"update_{model_name}")(model)
                    logging.debug(
                        f"Updating {model_name} in {db_name}: {model.to_object()}")

                except (errors.ConnectionException, errors.ConnectionFailure, errors.OperationalError) as e:
                    if kwargs.get("retry", False):
                        raise e
                    logging.info("retrying connection due to sql_proxy dropping connection")
                    self.update(model, retry=True)
                except Exception as err:
                    logging.error(
                        f"Unable to update {model_name} in {db_name}: {model}", exc_info=err)
        return model
