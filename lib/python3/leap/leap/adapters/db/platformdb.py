import json
import logging
import os
import time
import uuid
from collections import namedtuple
from dataclasses import dataclass
from datetime import datetime
from math import ceil
from typing import Any, Dict, List, Optional

import pugsql
from psycopg2.extensions import AsIs
from psycopg2.sql import Identifier, Literal, SQL

from leap.adapters import db
from ._models import Action, AdapterConfig, AdapterLocation, AdapterStore, Brand, BrandAsset, BrandLite, BrandUser, \
    ClientOmniChannelInsights, ClientOmniChannelInsightsMarkets, ClientOmniChannelInsightsTopProductsNetSales, \
    ClientOmniChannelInsightsTopProductsUnitsSold, Model, OmniChannelActiveLeapMarkets, RelatedBrand, \
    RelatedBrandsGroup, Service, Store, User
from .interfaces import DBInterface
from .models.brand_profile import BrandProfile
from .models.ephemeral_shopify_install import EphemeralShopifyInstall
from .models.select_space import SelectSpace

# The regex used to search for the name of a brand
REGEX_FOR_NAME_SEARCH = "%{0}%"

PagingMath = namedtuple("PagingMath", ["total_pages", "offset", "current_page"])


def is_valid_uuid(value):
    try:
        uuid.UUID(str(value))
        return True
    except ValueError:
        return False

@dataclass
class PagerResultsInternal(object):
    raw_postgres_data: List[Model]
    count: int
    limit: int
    offset: int
    total_pages: int
    current_page: int

    @staticmethod
    def clean_current_page(current_page: Any, last_page: int) -> int:
        try:
            current_page = int(current_page)
        except ValueError:
            current_page = 1
        if current_page < 1:
            current_page = 1
        if current_page > last_page:
            current_page = last_page
        return current_page

    @staticmethod
    def paging_math(count_val: int, limit: int, current_page: Any) -> PagingMath:
        total_pages = ceil(count_val / limit)
        # Always have at least 1 page to make the math easier
        if total_pages < 1:
            total_pages = 1
        current_page = PagerResultsInternal.clean_current_page(current_page, total_pages)
        offset = limit * (current_page - 1)
        return PagingMath(total_pages, offset, current_page)


class PlatformDB(DBInterface):

    def __init__(self, platformdb_uri: str):
        cur_path = os.path.dirname(os.path.realpath(__file__))
        pugsql_module = pugsql.module(os.path.join(cur_path, 'pugsql/queries/'))
        pugsql_module.connect(platformdb_uri)
        self.pugsql_module = pugsql_module

    # Brands
    def _build_brand(self, brand_dict: Dict, lite: bool = False) -> Brand:
        if not brand_dict:
            return None
        # The model class currently expects a string type for ids, but postgres returns a UUID.
        brand_dict['id'] = brand_dict['id'].hex
        if lite:
            return BrandLite.build(brand_dict)
        else:
            return Brand.build(brand_dict)

    def get_brand_by_id(self, brand_id: str, lite: bool = False) -> Optional[Brand]:
        brands = self.get_brand_by_ids([brand_id], lite)
        return brands[0] if brands else None

    def get_brand_by_filter(self, **filters) -> Optional[Brand]:
        """Get single brand that matches the filters specified

        Args:
            filters (dict): conditions to apply for filtering. Will populate the WHERE clause in SQL

        Returns:
            Optional[Brand]: Brand object retrieved from PlatformDB. If no brand exists, None is returned
        """
        where = self._generate_where_clause(**filters)
        brands = list(self.pugsql_module.get_brand_by_filter(where=AsIs(where)))
        if len(brands) > 1:
            logging.warning("More than one brand returned, only returning first brand in results")
        return self._build_brand(brands[0]) if brands else None

    def get_brand_by_ids(self, brand_ids: List[str], lite: bool = False) -> Optional[List[Brand]]:
        raw_brand: List[Dict[str, str]] = self.pugsql_module.get_brand_by_ids(brand_ids=brand_ids)
        ret_list = []
        for r in raw_brand:
            ret_list.append(self._build_brand(r, lite))
        return ret_list

    def query_brands(self, **filters: dict):
        if 'id' in filters:
            valid_uuid = is_valid_uuid(filters['id'])
            if not valid_uuid:
                logging.error(f"Unable to query with invalid id: {filters['id']}")
                return None
            return self.get_brand_by_id(filters['id'])
        else:
            return self.get_all_brands_paging(**filters)

    def save_brand(self, brand: Brand):
        brand_d = brand.to_sql_object()
        for field in brand.ignore_fields:
            brand_d.pop(field) # remove client_details because it doesn't exist in platformdb
        columns, values = self._generate_insert_statement(brand_d)
        try:
            self.pugsql_module.insert_brand(columns=AsIs(columns), values=AsIs(values))
        except Exception as e:
            logging.error(f"Unable to insert brand {brand.id}: {e}")
            return None
        return brand

    def delete_brand(self, brand: Brand):
        try:
            self.pugsql_module.delete_brand_by_id(brand_id=brand.id)
        except Exception as e:
            logging.error(f"Error deleting brand {brand.id}", exc_info=e)
        return brand

    def update_brand(self, brand: Brand):
        brand_d = brand.to_sql_object()
        for field in brand.ignore_fields:
            brand_d.pop(field) # remove client_details because it doesn't exist in platformdb
        update = self._generate_update_statement(brand_d)
        try:
            self.pugsql_module.update_brand(update=AsIs(update), brand_id=brand.id)
        except Exception as e:
            logging.error(f"Error updating brand {brand.id}", exc_info=e)
        return brand

    # There can be nothing in the where clause, or some combo of name/deleted/status_key.
    def _generate_where_clause(self, name: str = None, deleted: bool = None, status_key: str = None,
                               website: str = None, code: str = None, table_name: str = None, **filters) -> str:
        conditions = []
        i = 0
        if any([name, deleted is not None, status_key, website, code, filters]):
            where_str = ["WHERE "]
        else:
            return ""

        for k, v in filters.items():
            if v is None:
                continue
            where_str.append(f"{k} = {{{i}}}")
            if isinstance(v, list) or isinstance(v, dict):
                conditions.append(Literal(json.dumps(v)))
            else:
                conditions.append(Literal(v))
            i += 1

        if deleted is not None:
            deleted_str = "deleted"
            # In queries that join multiple tables, you have to specify the specific table you want to check deleted on
            if table_name:
                deleted_str = f"{table_name}.{deleted_str}"
            where_str.append(f"{deleted_str} = {{{i}}}")
            conditions.append(Literal(deleted))
            i += 1
        if status_key:
            where_str.append(f"status_key = {{{i}}}")
            conditions.append(Literal(status_key))
            i += 1
        if name:
            where_str.append(f"LOWER(name) SIMILAR TO {{{i}}}")
            conditions.append(Literal(f"%{name.lower()}%"))
            i += 1
        if code:
            where_str.append(f"LOWER(code) SIMILAR TO {{{i}}}")
            conditions.append(Literal(f"%{code.lower()}%"))
            i += 1
        if website:
            where_str.append(f"LOWER(website) SIMILAR TO {{{i}}}")
            conditions.append(Literal(f"%{website.lower()}%"))
            i += 1

        where_clause = where_str.pop(0) + " AND ".join(where_str)
        with self.pugsql_module.engine.connect() as conn:
            return SQL(where_clause).format(*conditions).as_string(conn.connection.cursor())

    def _generate_order_by_clause(self, order_by: str, order_dir: str):
        if order_dir not in ['ASC', 'DESC']:
            raise ValueError("Sort direction must be ASC or DESC")
        with self.pugsql_module.engine.connect() as conn:
            return SQL('ORDER BY {0} {1}').format(SQL(order_by), SQL(order_dir)).as_string(conn.connection.cursor())

    def _generate_insert_statement(self, obj: dict):
        """Generates insert statement to write an object to the database

        Args:
            obj (dict): object that will be inserted into the database

        Returns:
            (tuple): (columns, values)
        """
        sql_str = ','.join([f'{{{i}}}' for i, _ in enumerate(obj.items())])
        columns = [Identifier(key) for key in list(obj.keys())]
        values = []
        for v in list(obj.values()):
            if not isinstance(v, dict):
                values.append(Literal(v))
            else:
                values.append(Literal(json.dumps(v)))
        with self.pugsql_module.engine.connect() as conn:
            column_sql = SQL(sql_str).format(*columns).as_string(conn.connection.cursor())
            values_sql = SQL(sql_str).format(*values).as_string(conn.connection.cursor())
        return column_sql, values_sql

    def _generate_update_statement(self, obj: dict):
        """Generate update statement to update an object in the database

        Args:
            obj (dict): object with fields that will be updated in the database

            SQL({0} = {1}).format(column, value)
        """
        NON_UPDATE_KEYS = ['created_at', 'id', 'updated_at']
        sql_str = []
        values = []
        i = 0
        for k, v in obj.items():
            if k in NON_UPDATE_KEYS:
                continue
            sql_str.append(f'{{{i}}} = {{{i+1}}}')
            values.append(Identifier(k))
            if not isinstance(v, dict):
                values.append(Literal(v))
            else:
                values.append(Literal(json.dumps(v)))
            i += 2
        with self.pugsql_module.engine.connect() as conn:
            update = SQL(','.join(sql_str)).format(*values).as_string(conn.connection.cursor())
        return update

    def get_all_brands_paging(self,
                              order_by: str = 'created_at',
                              order_dir: str = db.OrderDirection.ASCENDING,
                              limit: int = 20,
                              current_page: int = 1,
                              name: str = None,
                              deleted: bool = None,
                              status_key: str = None,
                              website: str = None,
                              lite: bool = None,
                              **filters: dict) -> PagerResultsInternal:
        # Prepare arguments for the query
        where_clause = self._generate_where_clause(name, deleted, status_key, website, **filters)
        order_by_clause = self._generate_order_by_clause(order_by, order_dir)

        # Get the total number of rows
        raw_count = self.pugsql_module.get_all_brands_count(where=AsIs(where_clause))
        count_val = raw_count.get('count', 0)

        # Do all the math for paging
        paging_math = PagerResultsInternal.paging_math(count_val, limit, current_page)
        total_pages = paging_math.total_pages
        offset = paging_math.offset
        current_page = paging_math.current_page

        # Get the brand rows and prepare them to be returned
        raw_brand_list: List[Dict[str, str]] = self.pugsql_module.get_all_brands(order_by=AsIs(order_by_clause),
                                                                                 limit=limit,
                                                                                 offset=offset,
                                                                                 where=AsIs(where_clause))
        ret_list: List[Brand] = []
        for brand in raw_brand_list:
            ret_list.append(self._build_brand(brand, lite))
        ret_struct = PagerResultsInternal(raw_postgres_data=ret_list,
                                          count=count_val,
                                          limit=limit,
                                          offset=offset,
                                          total_pages=total_pages,
                                          current_page=current_page)
        return ret_struct

    # Related Brands
    def _build_related_brand(self, related_brand_dict: dict):
        if not related_brand_dict:
            return None
        # The model class currently expects a string type for ids, but postgres returns a UUID.
        related_brand_dict['id'] = related_brand_dict['id'].hex
        related_brand_dict['parent_id'] = related_brand_dict['parent_id'].hex
        related_brand_dict['child_id'] = related_brand_dict['child_id'].hex
        return RelatedBrand.build(related_brand_dict)

    def _build_related_brand_group(self, brand_id: str, raw_group: dict, brands: List[Brand]) -> RelatedBrandsGroup:
        parent = None
        children = []
        for b in brands:
            if b.id == raw_group["parent_id"].hex:
                parent = b
            else:
                children.append(b)
        return RelatedBrandsGroup(brand_id=brand_id, parent=parent, children=children)

    def get_related_brands_group(self, brand_id: str) -> List[RelatedBrandsGroup]:
        raw_group = self.pugsql_module.get_related_group(id=brand_id)
        ret_list: List[RelatedBrandsGroup] = []
        # TODO: Currently this relation will contains the OG brand. Not sure if it makes sense to remove it or keep it in there, will need to talk to APP to see what they want/need.
        for r in raw_group:
            ids = []
            ids.extend(r.get("children_ids"))
            ids.append(r.get("parent_id"))
            brands = self.get_brand_by_ids(ids, True)
            ret_list.append(self._build_related_brand_group(brand_id, r, brands))
        return ret_list

    def get_all_related_brands(self):
        raw_related_brands_list: List[Dict[str, str]] = self.pugsql_module.get_all_related_brands()
        ret_list: List[RelatedBrand] = []
        for related in raw_related_brands_list:
            ret_list.append(self._build_related_brand(related))

        return ret_list

    def get_related_brands(self,
                           parent_id: str = None,
                           child_id: str = None,
                           **filters: dict) -> List[RelatedBrand]:
        # Prepare arguments for the query
        where_clause = self._generate_where_clause(child_id=child_id, parent_id=parent_id)

        # Get the brand rows and prepare them to be returned
        raw_related_brands_list: List[Dict[str, str]] = self.pugsql_module.get_related_brands(where=AsIs(where_clause))
        ret_list: List[RelatedBrand] = []
        for related in raw_related_brands_list:
            ret_list.append(self._build_related_brand(related))

        return ret_list

    def query_related_brands(self, **filters: dict):
        if 'id' in filters:
            return self._build_related_brand(self.pugsql_module.get_related_brand_by_id(id=filters['id']))
        else:
            return self.get_related_brands(**filters)

    def save_relatedbrand(self, related_brand: RelatedBrand):
        self.pugsql_module.insert_related_brand(
            id=related_brand.id,
            parent_id=related_brand.parent_id,
            child_id=related_brand.child_id)

    def update_relatedbrand(self, related_brand: RelatedBrand):
        try:
            self.pugsql_module.update_related_brand(
                id=related_brand.id,
                parent_id=related_brand.parent_id,
                child_id=related_brand.child_id)
        except Exception as err:
            logging.error(f"Unable to update related_brand {str(err)}")

    def delete_relatedbrand(self, related_brand: RelatedBrand):
        return self.pugsql_module.delete_related_brand_by_id(id=related_brand.id)

    # Adapters
    def _build_adapter_config(self, adapter_dict: Dict[str, Any]) -> AdapterConfig:
        if not adapter_dict:
            return None
        # The model class currently expects a string type for ids, but postgres returns a UUID.
        adapter_dict['id'] = adapter_dict['id'].hex
        adapter_dict['brand_id'] = adapter_dict['brand_id'].hex
        adapter_dict['service_id'] = adapter_dict['service_id'].hex
        return AdapterConfig.build(adapter_dict)

    def get_adapter_by_id(self, adapter_id: str) -> AdapterConfig:
        raw_adapter: Dict[str, str] = self.pugsql_module.get_adapter_by_id(adapter_id=adapter_id)
        return self._build_adapter_config(raw_adapter)

    def get_adapters_for_brand(self, brand_id: str, internal: bool) -> List[AdapterConfig]:
        raw_adapters: List[Dict[str, str]] = self.pugsql_module.get_all_adapters_for_brand(brand_id=brand_id,
                                                                                           internal=internal)
        ret_list: List[AdapterConfig] = []
        for adapter in raw_adapters:
            ret_list.append(self._build_adapter_config(adapter))
        return ret_list

    def get_adapter_for_brand_by_id(self, brand_id: str, adapter_id: bool) -> AdapterConfig:
        raw_adapter: Dict[str, str] = self.pugsql_module.get_adapter_for_brand_by_id(brand_id=brand_id,
                                                                                     adapter_id=adapter_id)
        return self._build_adapter_config(raw_adapter)

    def update_adapter_config(self, adapter: AdapterConfig):
        adapter.updated_at = datetime.now()
        self.pugsql_module.update_adapter_config(adapter_id=adapter.id, config=str(adapter.config))

    def upsert_adapter(self, adapter: AdapterConfig):
        obj = adapter.to_object()
        for field in adapter.ignore_fields:
            obj.pop(field)
        if not is_valid_uuid(obj['service_id']):
            obj['service_id'] = self.get_service_by_filter(handle=obj['service_id']).id
        columns, values = self._generate_insert_statement(obj)
        update = self._generate_update_statement(obj)
        self.pugsql_module.upsert_adapter(adapter_id=adapter.id, columns=AsIs(columns), values=AsIs(values), update=AsIs(update))

    def get_all_adapters_paging(self, order_by: str = 'created_at', order_dir: str = db.OrderDirection.DESCENDING, limit: int = 1000, current_page: int = 1, **filters) -> PagerResultsInternal:
        order_by_clause = self._generate_order_by_clause(order_by, order_dir)
        where = self._generate_where_clause(**filters)
        # Get the total number of rows
        raw_count = self.pugsql_module.get_all_adapters_count(where=AsIs(where))
        count_val = raw_count.get('count', 0)


        # Do all the math for paging
        paging_math = PagerResultsInternal.paging_math(count_val, limit, current_page)
        total_pages = paging_math.total_pages
        offset = paging_math.offset
        current_page = paging_math.current_page

        raw_adapters: List[Dict[str, str]] = self.pugsql_module.get_all_adapters(order_by=AsIs(order_by_clause),
                                                                                 where=AsIs(where),
                                                                                 limit=limit,
                                                                                 offset=offset)
        ret_list: List[AdapterConfig] = []
        for adapter in raw_adapters:
            ret_list.append(self._build_adapter_config(adapter))
        ret_struct = PagerResultsInternal(raw_postgres_data=ret_list,
                                          count=count_val,
                                          limit=limit,
                                          offset=offset,
                                          total_pages=total_pages,
                                          current_page=current_page)
        return ret_struct

    def query_adapters(self, **filters: dict):
        if 'id' in filters:
            return self.get_adapter_by_id(filters['id'])
        return self.get_all_adapters_paging(**filters)

    def save_adapterconfig(self, adapter: AdapterConfig):
        return self.upsert_adapter(adapter)

    def delete_adapterconfig(self, adapter: AdapterConfig):
        """Delete adapter and all associated adapter objects (adapter_stores, webhooks, etc)

        Args:
            adapter (AdapterConfig): Adapter object to store in platformdb
        """
        return self.pugsql_module.delete_adapter(adapter_id=adapter.id)

    # Brand Assets
    def _build_brand_assets(self, brand_asset_dict: Dict[str, str]) -> BrandAsset:
        if not brand_asset_dict:
            return None
        brand_asset_dict['id'] = brand_asset_dict['id'].hex
        brand_asset_dict['brand_id'] = brand_asset_dict['brand_id'].hex
        brand_asset_dict['created_by_id'] = brand_asset_dict['created_by_id'].hex
        return BrandAsset.build(brand_asset_dict)

    def get_brand_assets_for_brand(self, brand_id: str, deleted: bool = False) -> List[BrandAsset]:
        raw_brand_assets: List[Dict[str, str]] = self.pugsql_module.get_brand_assets_for_brand(brand_id=brand_id,
                                                                                               deleted=deleted)
        ret_list = []
        for brand_asset in raw_brand_assets:
            ret_list.append(self._build_brand_assets(brand_asset))
        return ret_list

    def get_brand_asset_for_brand_by_id(self, brand_id: str, id: str):
        raw_brand_asset = self.pugsql_module.get_brand_asset_for_brand_by_id(brand_id=brand_id, asset_id=id)
        return self._build_brand_assets(raw_brand_asset) if raw_brand_asset else None

    def query_brand_assets(self, **filters: dict):
        if 'id' in filters and 'brand_id' in filters:
            return self.get_brand_asset_for_brand_by_id(**filters)
        else:
            return self.get_brand_assets_for_brand(**filters)

    def save_brandasset(self, brand_asset: BrandAsset):
        return self.pugsql_module.save_brand_asset(**brand_asset.to_dict())

    def delete_brandasset(self, asset: BrandAsset):
        return self.set_deleted_brand_asset(asset)

    def set_deleted_brand_asset(self, asset: BrandAsset):
        asset.deleted = True
        self.pugsql_module.set_deleted_brand_asset(asset_id=asset.id)
        return asset

    def get_all_brand_assets_paging(self, order_by: str, order_dir: str, limit: int, current_page: int) -> PagerResultsInternal:
        # Generate order_by
        order_by_clause = self._generate_order_by_clause(order_by, order_dir)
        # Get the total number of rows
        raw_count = self.pugsql_module.get_all_brand_assets_count()
        count_val = raw_count.get('count', 0)

        # Do all the math for paging
        paging_math = PagerResultsInternal.paging_math(count_val, limit, current_page)
        total_pages = paging_math.total_pages
        offset = paging_math.offset
        current_page = paging_math.current_page

        # Get the rows and prepare them to be returned
        raw_brand_assets_list: List[Dict[str,
                                         str]] = self.pugsql_module.get_all_brand_assets(order_by=AsIs(order_by_clause),
                                                                                         limit=limit,
                                                                                         offset=offset)
        ret_list: List[db.BrandAsset] = []
        for brand_asset in raw_brand_assets_list:
            ret_list.append(self._build_brand_assets(brand_asset))
        ret_struct = PagerResultsInternal(raw_postgres_data=ret_list,
                                          count=count_val,
                                          limit=limit,
                                          offset=offset,
                                          total_pages=total_pages,
                                          current_page=current_page)
        return ret_struct

    # Actions
    def _build_action(self, action_dict: Dict[str, str]) -> Action:
        if not action_dict:
            return None
        action_dict['id'] = action_dict['id'].hex
        # client_id and brand_id are not required
        if action_dict.get('client_id'):
            action_dict['client_id'] = action_dict['client_id'].hex
        if action_dict.get('brand_id'):
            action_dict['brand_id'] = action_dict['brand_id'].hex
        return Action.build(action_dict)

    def get_all_actions_paging(self, order_by: str, order_dir: str, limit: int, current_page: int) -> PagerResultsInternal:
        # Generate order_by
        order_by_clause = self._generate_order_by_clause(order_by, order_dir)
        # Get the total number of rows
        raw_count = self.pugsql_module.get_all_actions_count()
        count_val = raw_count.get('count', 0)

        # Do all the math for paging
        paging_math = PagerResultsInternal.paging_math(count_val, limit, current_page)
        total_pages = paging_math.total_pages
        offset = paging_math.offset
        current_page = paging_math.current_page

        # Get the rows and prepare them to be returned
        raw_action_list: List[Dict[str, str]] = self.pugsql_module.get_all_actions(order_by=AsIs(order_by_clause),
                                                                                   limit=limit,
                                                                                   offset=offset)
        ret_list: List[Action] = []
        for action in raw_action_list:
            ret_list.append(self._build_action(action))
        ret_struct = PagerResultsInternal(raw_postgres_data=ret_list,
                                          count=count_val,
                                          limit=limit,
                                          offset=offset,
                                          total_pages=total_pages,
                                          current_page=current_page)
        return ret_struct

    def save_action(self, action: Action):
        """Save action to db.

        Need to convert message: dict to string in order to avoid psycopg2.ProgrammingError: can't adapt type 'dict'
        Need to convert matched_subscription_ids: dict to string in order to avoid (psycopg2.errors.DatatypeMismatch) column "matched_subscription_ids" is of type jsonb but expression is of type text[]
        """
        obj = action.to_object()
        obj['matched_subscription_ids'] = json.dumps(obj['matched_subscription_ids'])
        obj['message'] = json.dumps(obj['message'])
        # Check if brand exists before saving
        timeout = time.time() + 5 # 5 second timeout
        brand = self.get_brand_by_id(action.brand_id)
        while time.time() < timeout:
            if brand:
                break
            brand = self.get_brand_by_id(action.brand_id)
            time.sleep(1)
        try:
            self.pugsql_module.insert_action(obj)
        except Exception as err:
            logging.error(f"Unable to insert action {str(err)}")

    def query_actions(self, **filters: dict):
        if 'brand_id' in filters:
            row = self.pugsql_module.get_action_by_brand_id(brand_id=filters['brand_id'])
            if row:
                return self._build_action(row)
            else:
                logging.debug(f"No action found for brand {filters['brand_id']}")
                return None
        return self.get_all_actions_paging(**filters)

    # Adapter Stores
    def _build_adapter_stores(self, adapter_store_dict: Dict[str, str]) -> AdapterStore:
        if not adapter_store_dict:
            return None
        adapter_store_dict['id'] = adapter_store_dict['id'].hex
        adapter_store_dict['adapter_id'] = adapter_store_dict['adapter_id'].hex
        adapter_store_dict['store_id'] = adapter_store_dict['store_id'].hex
        return AdapterStore.build(adapter_store_dict)

    def get_all_adapter_stores_paging(self, order_by: str = "created_at", order_dir: str = db.OrderDirection.DESCENDING, limit: int = 1000, current_page: int = 1, **filters: dict) -> PagerResultsInternal:
        # Generate order_by
        order_by_clause = self._generate_order_by_clause(order_by, order_dir)
        # Generate where clause
        where = self._generate_where_clause(**filters)

        # Get the total number of rows
        raw_count = self.pugsql_module.get_all_adapter_stores_count()
        count_val = raw_count.get('count', 0)

        # Do all the math for paging
        paging_math = PagerResultsInternal.paging_math(count_val, limit, current_page)
        total_pages = paging_math.total_pages
        offset = paging_math.offset
        current_page = paging_math.current_page

        # Get the rows and prepare them to be returned
        raw_adapter_stores_list: List[Dict[str, str]] = self.pugsql_module.get_all_adapter_stores(
            where=AsIs(where), order_by=AsIs(order_by_clause), limit=limit, offset=offset)
        ret_list: List[AdapterLocation] = []
        for adapter_store in raw_adapter_stores_list:
            ret_list.append(self._build_adapter_stores(adapter_store))
        ret_struct = PagerResultsInternal(raw_postgres_data=ret_list,
                                          count=count_val,
                                          limit=limit,
                                          offset=offset,
                                          total_pages=total_pages,
                                          current_page=current_page)
        return ret_struct

    def get_adapter_store_by_id(self, adapter_store_id):
        adapter_store = self.pugsql_module.get_adapter_store_by_id(adapter_store_id=adapter_store_id)
        return self._build_adapter_stores(adapter_store)

    def query_adapter_stores(self, **filters) -> PagerResultsInternal:
        if 'id' in filters:
            return self.get_adapter_store_by_id(filters['id'])
        else:
            return self.get_all_adapter_stores_paging(**filters)

    def upsert_adapter_store(self, adapter_store: AdapterStore):
        obj = adapter_store.to_object()
        for field in adapter_store.ignore_fields:
            obj.pop(field)
        if not is_valid_uuid(obj['store_id']):
            obj['store_id'] = self.get_store_by_code(obj['store_id']).guid
        columns, values = self._generate_insert_statement(obj)
        update = self._generate_update_statement(obj)
        self.pugsql_module.upsert_adapter_store(adapter_id=adapter_store.id, columns=AsIs(
            columns), values=AsIs(values), update=AsIs(update))

    def save_adapterstore(self, adapter_store: AdapterStore):
        self.upsert_adapter_store(adapter_store)

    # Services
    def _build_service(self, service_dict: Dict[str, str]) -> Service:
        if not service_dict:
            return None
        # The model class currently expects a string type for ids, but postgres returns a UUID.
        service_dict['id'] = service_dict['id'].hex
        return Service.build(service_dict)

    def get_service_by_id(self, service_id: str) -> Optional[Service]:
        raw_service = self.pugsql_module.get_service_by_id(service_id=service_id)
        return self._build_service(raw_service)

    def get_service_by_filter(self, **filters) -> Optional[Service]:
        """Get single service that matches the filters specified

        Args:
            filters (dict): conditions to apply for filtering. Will populate the WHERE clause in SQL

        Returns:
            Optional[Service]: Service object retrieved from PlatformDB. If no service exists, None is returned
        """
        where = self._generate_where_clause(**filters)
        services = list(
            self.pugsql_module.get_service_by_filter(where=AsIs(where)))
        if len(services) > 1:
            logging.warning(
                "More than one service returned, only returning first service in results")
        return self._build_service(services[0]) if services else None

    def get_all_services(self):
        raw_data = self.pugsql_module.get_all_services()
        ret_list = []
        for row in raw_data:
            data = dict(row)
            data['id'] = data['handle']
            ret_list.append(db.Service.build(data))
        return ret_list

    def save_service(self, service: Service):
        obj = service.to_sql_object()
        if not is_valid_uuid(service.id):
            obj.pop('id')

        columns, values = self._generate_insert_statement(obj)
        try:
            self.pugsql_module.insert_service(
                columns=AsIs(columns), values=AsIs(values))
        except Exception as err:
            logging.error(f"Unable to insert service: {obj}", exc_info=err)


    # Stores
    def _build_store(self, store_dict: Dict[str, str]) -> Store:
        if not store_dict:
            return None

        # TODO update this block when frontend has updated to use code instead of id
        store_dict['guid'] = store_dict['id']
        store_dict['id'] = store_dict['code']

        if store_dict.get('client_id'):
            store_dict['client_id'] = store_dict['client_id'].hex
        if store_dict.get('brand_id'):
            store_dict['brand_id'] = store_dict['brand_id'].hex
        if store_dict.get('store_id'):
            store_dict['store_id'] = store_dict['store_id'].hex
        if store_dict.get('space_id'):
            store_dict['space_id'] = store_dict['space_id'].hex
        if store_dict.get('margin'):
            # For whatever reason pugsql thinks margin is a Decimal type which causes rounding errors later.
            # Make it an explicit float for pairty purposes.
            store_dict['margin'] = float(store_dict['margin'])
        store = Store.build(store_dict)
        # TODO Remove this line when frontend hasu pdated to use code instead of id
        store.guid = store_dict['guid']
        return store

    def get_store_by_id(self, store_id: str) -> Optional[Store]:
        raw_store = self.pugsql_module.get_store_by_id(store_id=store_id)
        return self._build_store(raw_store)

    def get_store_by_code(self, store_code: str) -> Optional[Store]:
        raw_store = self.pugsql_module.get_store_by_code(store_code=store_code)
        return self._build_store(raw_store)

    def get_store_by_space_id(self, space_id: str) -> Optional[Store]:
        raw_store = self.pugsql_module.get_store_by_space_id(space_id=space_id)
        return self._build_store(raw_store)

    def get_all_stores_paging(self, order_by: str = 'created_at',
                              order_dir: str = db.OrderDirection.DESCENDING,
                              limit: int = 1000,
                              current_page: int = 1,
                              code: str = None,
                              **filters: dict) -> PagerResultsInternal:
        where = self._generate_where_clause(code=code, **filters)
        # Generate order_by
        order_by_clause = self._generate_order_by_clause(order_by, order_dir)
        # Get the total number of rows
        raw_count = self.pugsql_module.get_all_stores_count(where=AsIs(where))
        count_val = raw_count.get('count', 0)

        # Do all the math for paging
        paging_math = PagerResultsInternal.paging_math(count_val, limit, current_page)
        total_pages = paging_math.total_pages
        offset = paging_math.offset
        current_page = paging_math.current_page

        # Get the rows and prepare them to be returned
        raw_stores_list: List[Dict[str, str]] = self.pugsql_module.get_all_stores(order_by=AsIs(order_by_clause),
                                                                                  where=AsIs(where),
                                                                                  limit=limit,
                                                                                  offset=offset)
        ret_list: List[Store] = []
        for store in raw_stores_list:
            ret_list.append(self._build_store(store))
        ret_struct = PagerResultsInternal(raw_postgres_data=ret_list,
                                          count=count_val,
                                          limit=limit,
                                          offset=offset,
                                          total_pages=total_pages,
                                          current_page=current_page)
        return ret_struct

    def get_all_admin_stores_paging(self,
                                    code: str = None,
                                    deleted: bool = None,
                                    order_by: str = 'created_at',
                                    order_dir: str = db.OrderDirection.DESCENDING,
                                    limit: int = 1000,
                                    current_page: int = 1) -> PagerResultsInternal:
        where = self._generate_where_clause(code=code, deleted=deleted, table_name="stores")
        # Get the total number of rows
        raw_count = self.pugsql_module.get_all_admin_stores_count(where=AsIs(where))
        count_val = raw_count.get('count', 0)
        # Generate order_by, since our query joins multiple tables we have to specify which table to use
        order_by_clause = self._generate_order_by_clause("stores."+order_by, order_dir)

        # Do all the math for paging
        paging_math = PagerResultsInternal.paging_math(count_val, limit, current_page)
        total_pages = paging_math.total_pages
        offset = paging_math.offset
        current_page = paging_math.current_page

        raw_store_list = self.pugsql_module.get_all_admin_stores(limit=limit,
                                                                 offset=offset,
                                                                 where=AsIs(where),
                                                                 order_by=AsIs(order_by_clause))
        stores_list = []
        for row in raw_store_list:
            store = row['store']
            # TODO remove this line when frontend has updated to use code instead of id
            store['id'] = store['code']

            # TODO remove this line when we are fully off firestore
            store['space']['status_key'] = store['space'].get('status', None)
            store['location']['status_key'] = store['location'].get('status', None)
            stores_list.append(store)
        ret_struct = PagerResultsInternal(
            raw_postgres_data=stores_list,
            count=count_val,
            limit=limit,
            offset=offset,
            total_pages=total_pages,
            current_page=current_page
        )
        return ret_struct

    def query_stores(self, **filters: dict):
        if 'id' in filters:
            if is_valid_uuid(filters['id']):
                return self.get_store_by_id(filters['id'])
            else:
                return self.get_store_by_code(filters['id'])
        else:
            return self.get_all_stores_paging(**filters)

    def save_store(self, store: db.Store):
        obj = store.to_sql_object()
        if not is_valid_uuid(store.id):
            store.code = store.id
            obj['code'] = store.id
            obj['id'] = uuid.uuid4().hex

        for field in store.ignore_fields:
            obj.pop(field)
        columns, values = self._generate_insert_statement(obj)
        try:
            self.pugsql_module.insert_store(
                columns=AsIs(columns), values=AsIs(values))
        except Exception as err:
            logging.error(f"Unable to insert store: {obj}", exc_info=err)

    def update_store(self, store: db.Store):
        obj = store.to_sql_object()
        if not is_valid_uuid(store.id):
            store.code = store.id
            obj['code'] = store.id
            obj['id'] = uuid.uuid4().hex
        for field in store.ignore_fields:
            obj.pop(field)
        update = self._generate_update_statement(obj)
        try:
            self.pugsql_module.update_store(
                update=AsIs(update), store_code=store.code)
        except Exception as err:
            logging.error(f"Unable to update store: {obj}", exc_info=err)
        return store

    def delete_store(self, store: db.Store):
        if not is_valid_uuid(store.id):
            store.code = store.id
        self.pugsql_module.delete_store(store_code=store.code)
        return store

    # Users
    def _build_users(self, users_dict: Dict[str, Any]) -> User:
        if not users_dict:
            return None
        users_dict['id'] = users_dict['id'].hex
        return User.build(users_dict)

    def get_all_users_paging(self, order_by: str = "created_at", order_dir: str = db.OrderDirection.DESCENDING, limit: int = 1000, current_page: int = 1, **filters: dict) -> PagerResultsInternal:
        # Generate order_by
        order_by_clause = self._generate_order_by_clause(order_by, order_dir)
        # Get the total number of rows
        raw_count = self.pugsql_module.get_all_users_count()
        count_val = raw_count.get('count', 0)

        # Do all the math for paging
        paging_math = PagerResultsInternal.paging_math(count_val, limit, current_page)
        total_pages = paging_math.total_pages
        offset = paging_math.offset
        current_page = paging_math.current_page

        where = self._generate_where_clause(**filters)
        # Get the rows and prepare them to be returned
        raw_users_list: List[Dict[str, str]] = self.pugsql_module.get_all_users(where=AsIs(where),
                                                                                order_by=AsIs(order_by_clause),
                                                                                limit=limit,
                                                                                offset=offset)
        ret_list: List[User] = []
        for user in raw_users_list:
            ret_list.append(self._build_users(user))
        ret_struct = PagerResultsInternal(raw_postgres_data=ret_list,
                                          count=count_val,
                                          limit=limit,
                                          offset=offset,
                                          total_pages=total_pages,
                                          current_page=current_page)
        return ret_struct

    def create_test_user(self, user: User):
        return self.pugsql_module.create_test_user(id=user.id,
                                                   identity_service=user.identity_service,
                                                   external_user_id=user.external_user_id)

    def get_user_by_id(self, id: str):
        ret_val = self.pugsql_module.get_user_by_id(id=id)
        return self._build_users(ret_val)

    def query_users(self, **filters: dict):
        if 'id' in filters:
            return self.get_user_by_id(**filters)
        else:
            return self.get_all_users_paging(**filters)

    def save_user(self, user: User):
        obj = user.to_sql_object()
        try:
            self.pugsql_module.insert_user(obj)
        except Exception as err:
            logging.error(f"Unable to insert user: {obj}", exc_info=err)
        return user

    def delete_user(self, user: User):
        try:
            self.pugsql_module.delete_user_by_id(user_id=user.id)
        except Exception as e:
            logging.error(f"Error deleting user {user.id}", exc_info=e)
        return user

    def update_user(self, user: User):
        user_d = user.to_sql_object()
        for field in user.ignore_fields:
            user_d.pop(field)
        update = self._generate_update_statement(user_d)
        try:
            self.pugsql_module.update_user(update=AsIs(update), user_id=user.id)
        except Exception as e:
            logging.error(f"Error updating user {user.id}", exc_info=e)
        return user

    # Brand Users
    def _build_brand_users(self, brand_users_dict: Dict[str, Any]) -> BrandUser:
        if not brand_users_dict:
            return None
        brand_users_dict['id'] = brand_users_dict['id'].hex
        brand_users_dict['user_id'] = brand_users_dict['user_id'].hex
        brand_users_dict['brand_id'] = brand_users_dict['brand_id'].hex
        return BrandUser.build(brand_users_dict)

    def get_all_brand_users_paging(self, order_by: str = 'created_at', order_dir: str = db.OrderDirection.DESCENDING, limit: int = 5000, current_page: int = 1, **filters) -> PagerResultsInternal:
        # Generate order_by
        order_by_clause = self._generate_order_by_clause(order_by, order_dir)
        where = self._generate_where_clause(**filters)
        # Get the total number of rows
        raw_count = self.pugsql_module.get_all_brand_users_count(where=AsIs(where))
        count_val = raw_count.get('count', 0)

        # Do all the math for paging
        paging_math = PagerResultsInternal.paging_math(count_val, limit, current_page)
        total_pages = paging_math.total_pages
        offset = paging_math.offset
        current_page = paging_math.current_page

        # Get the rows and prepare them to be returned
        raw_brand_users_list: List[Dict[str,
                                        str]] = self.pugsql_module.get_all_brand_users(where=AsIs(where),
                                                                                       order_by=AsIs(order_by_clause),
                                                                                       limit=limit,
                                                                                       offset=offset)
        ret_list: List[BrandUser] = []
        for brand_user in raw_brand_users_list:
            ret_list.append(self._build_brand_users(brand_user))
        ret_struct = PagerResultsInternal(raw_postgres_data=ret_list,
                                          count=count_val,
                                          limit=limit,
                                          offset=offset,
                                          total_pages=total_pages,
                                          current_page=current_page)
        return ret_struct

    def get_brand_user_by_user_id_and_brand_id(self, user_id: str, brand_id: str):
        ret_val = self.pugsql_module.get_brand_user_by_user_id_and_brand_id(user_id=user_id, brand_id=brand_id)
        return self._build_brand_users(ret_val)

    def get_brand_user_by_id(self, id: str):
        ret_val = self.pugsql_module.get_brand_user_by_id(id=id)
        return self._build_brand_users(ret_val)

    def query_brand_users(self, **filters: dict):
        if 'user_id' in filters and 'brand_id' in filters:
            return self.get_brand_user_by_user_id_and_brand_id(**filters)
        elif 'id' in filters:
            return self.get_brand_user_by_id(**filters)
        else:
            return self.get_all_brand_users_paging(**filters)

    def save_branduser(self, brand_user: BrandUser):
        brand_user_d = brand_user.to_sql_object()
        for field in brand_user.ignore_fields:
            brand_user_d.pop(field)
        columns, values = self._generate_insert_statement(brand_user_d)
        try:
            self.pugsql_module.insert_brand_user(
                columns=AsIs(columns), values=AsIs(values))
        except Exception as e:
            logging.error(f"Unable to insert brand user {brand_user.id}", exc_info=e)
            return None
        return brand_user

    def delete_branduser(self, brand_user: BrandUser):
        self.pugsql_module.delete_brand_user_by_id(brand_user_id=brand_user.id)
        return brand_user

    def update_branduser(self, brand_user: BrandUser):
        obj = brand_user.to_sql_object()
        for field in brand_user.ignore_fields:
            obj.pop(field)
        update = self._generate_update_statement(obj)
        try:
            self.pugsql_module.update_brand_user(update=AsIs(update), brand_user_id=brand_user.id)
        except Exception as err:
            logging.error(f"Unable to update space: {obj}", exc_info=err)
        return brand_user

    # Omni
    def _build_client_omni_channel_insights(self, omni_dict: Dict[str, Any]) -> ClientOmniChannelInsights:
        if not omni_dict:
            return None
        omni_dict['id'] = str(omni_dict['id'])
        return ClientOmniChannelInsights.build(omni_dict)

    def get_client_omni_channel_insights(self, brand_handle: str) -> Optional[ClientOmniChannelInsights]:
        raw_omni_insights = self.pugsql_module.get_client_omni_channel_insights(brand_handle=brand_handle)
        if raw_omni_insights:
            return self._build_client_omni_channel_insights(raw_omni_insights)

    def _build_client_omni_channel_insights_markets(self, omni_dict: Dict[str,
                                                                          Any]) -> ClientOmniChannelInsightsMarkets:
        if not omni_dict:
            return None
        omni_dict['id'] = str(omni_dict['id'])
        return ClientOmniChannelInsightsMarkets.build(omni_dict)

    def get_client_omni_channel_insights_markets(self, brand_handle: str,
                                                 limit: int) -> List[ClientOmniChannelInsightsMarkets]:
        raw_omni_markets = self.pugsql_module.get_client_omni_channel_insights_markets(brand_handle=brand_handle,
                                                                                       limit=limit)
        ret_list: List[ClientOmniChannelInsightsMarkets] = []
        for omni in raw_omni_markets:
            ret_list.append(self._build_client_omni_channel_insights_markets(omni))
        return ret_list

    def _build_client_omni_channel_insights_top_products_net_sales(
            self, omni_dict: Dict[str, Any]) -> ClientOmniChannelInsightsTopProductsNetSales:
        if not omni_dict:
            return None
        omni_dict['id'] = str(omni_dict['id'])
        return ClientOmniChannelInsightsTopProductsNetSales.build(omni_dict)

    def get_client_omni_channel_insights_top_products_net_sales(
            self, brand_handle: str, limit: int) -> List[ClientOmniChannelInsightsTopProductsNetSales]:
        raw_omni_net_sales = self.pugsql_module.get_client_omni_channel_insights_top_products_net_sales(
            brand_handle=brand_handle, limit=limit)
        ret_list = []
        for omni in raw_omni_net_sales:
            ret_list.append(self._build_client_omni_channel_insights_top_products_net_sales(omni))
        return ret_list

    def _build_client_omnichannel_insights_products_units_sold(
            self, omni_dict: Dict[str, Any]) -> ClientOmniChannelInsightsTopProductsUnitsSold:
        if not omni_dict:
            return None
        omni_dict['id'] = str(omni_dict['id'])
        return ClientOmniChannelInsightsTopProductsUnitsSold.build(omni_dict)

    def get_client_omnichannel_insights_products_units_sold(
            self, brand_handle: str, limit: int) -> List[ClientOmniChannelInsightsTopProductsUnitsSold]:
        raw_omni_units = self.pugsql_module.get_client_omnichannel_insights_products_units_sold(
            brand_handle=brand_handle, limit=limit)
        ret_list = []
        for omni in raw_omni_units:
            ret_list.append(self._build_client_omnichannel_insights_products_units_sold(omni))
        return ret_list

    def _build_omni_channel_active_leap_markets(self, omni_dict: Dict[str, Any]) -> OmniChannelActiveLeapMarkets:
        if not omni_dict:
            return None
        omni_dict['id'] = str(omni_dict['id'])
        return OmniChannelActiveLeapMarkets.build(omni_dict)

    def get_omni_channel_active_leap_markets(self) -> List[OmniChannelActiveLeapMarkets]:
        raw_omni_active_markets = self.pugsql_module.get_omni_channel_active_leap_markets()
        ret_list = []
        for omni in raw_omni_active_markets:
            ret_list.append(self._build_omni_channel_active_leap_markets(omni))
        return ret_list

    # Brand Profile
    def get_brand_profile_by_brand_id(self, brand_id: str) -> BrandProfile:
        """Get brand profile data when given a brand id
        Returns:
            BrandProfile
        """
        raw_brand_profile: Dict[str, Any] = self.pugsql_module.get_brand_profile_by_brand_id(brand_id=brand_id)
        if not raw_brand_profile:
            return None
        brand_profile = BrandProfile.build(raw_brand_profile)
        brand_profile.files_url = self.get_brand_box_link_by_id(brand_id=brand_id)
        return brand_profile

    def get_brand_box_link_by_id(self, brand_id: str):
        raw_files_url = self.pugsql_module.get_brand_box_link_by_id(brand_id=brand_id)
        if not raw_files_url or not [*raw_files_url.values()][0]:
            return None
        return [*raw_files_url.values()][0]

    def update_brand_box_link_by_id(self, brand_id: str, box_link: str):
        affected = self.pugsql_module.update_brand_box_link_by_id(brand_id=brand_id, box_link=box_link)
        return affected

    def get_brand_profile_by_id(self, id: str) -> BrandProfile:
        """Get brand profile data when given an id
        Returns:
            BrandProfile
        """
        brand_profile: Dict[str, Any] = self.pugsql_module.get_brand_profile_by_id(id=id)
        if not brand_profile:
            return None
        return BrandProfile.build(brand_profile)

    def insert_brand_profile(self, brand_profile: BrandProfile) -> BrandProfile:
        return self.pugsql_module.insert_brand_profile(
            brand_id=brand_profile.brand_id,
            overview_updated_by=brand_profile.overview_updated_by,
            overview=brand_profile.overview.to_json(),
            visual_merchandising_updated_by=brand_profile.visual_merchandising_updated_by,
            visual_merchandising=brand_profile.visual_merchandising.to_json(),
            product_merchandising_updated_by=brand_profile.product_merchandising_updated_by,
            product_merchandising=brand_profile.product_merchandising.to_json(),
            store_operations_updated_by=brand_profile.store_operations_updated_by,
            store_operations=brand_profile.store_operations.to_json(),
            store_design_updated_by=brand_profile.store_design_updated_by,
            store_design=brand_profile.store_design.to_json(),
            marketing_updated_by=brand_profile.marketing_updated_by,
            marketing=brand_profile.marketing.to_json(),
            integrations_updated_by=brand_profile.integrations_updated_by,
            integrations=brand_profile.integrations.to_json(),
            social_updated_by=brand_profile.social_updated_by,
            social=brand_profile.social.to_json(),
        )

    def put_brand_profile_by_id(self, brand_id: str, brand_profile: BrandProfile):
        brand_profile.updated_at = datetime.now()
        if brand_id != brand_profile.brand_id.hex:
            raise ValueError("Unable to update profile with different brand ids")
        return self.pugsql_module.update_brand_profile_by_brand_id(
            brand_id=brand_id,
            updated_at=brand_profile.updated_at,
            overview_updated_by=brand_profile.overview_updated_by,
            overview=brand_profile.overview.to_json(),
            visual_merchandising_updated_by=brand_profile.visual_merchandising_updated_by,
            visual_merchandising=brand_profile.visual_merchandising.to_json(),
            product_merchandising_updated_by=brand_profile.product_merchandising_updated_by,
            product_merchandising=brand_profile.product_merchandising.to_json(),
            store_operations_updated_by=brand_profile.store_operations_updated_by,
            store_operations=brand_profile.store_operations.to_json(),
            store_design_updated_by=brand_profile.store_design_updated_by,
            store_design=brand_profile.store_design.to_json(),
            marketing_updated_by=brand_profile.marketing_updated_by,
            marketing=brand_profile.marketing.to_json(),
            integrations_updated_by=brand_profile.integrations_updated_by,
            integrations=brand_profile.integrations.to_json(),
            social_updated_by=brand_profile.social_updated_by,
            social=brand_profile.social.to_json(),
        )

    def get_all_subscriptions(self) -> List[db.Subscription]:
        raw_data = self.pugsql_module.get_all_subscriptions()
        ret_list = []
        for row in raw_data:
            ret_list.append(db.Subscription.build(row))
        return ret_list

    def get_all_spaces(self, **filters: dict) -> List[db.Space]:
        where = self._generate_where_clause(**filters)
        raw_data = self.pugsql_module.get_all_spaces(where=AsIs(where))
        ret_list = []
        for row in raw_data:
            data = dict(row)
            data['status_key'] = data.pop('status')
            ret_list.append(db.Space.build(data))
        return ret_list

    def get_space_by_id(self, space_id: str):
        try:
            row = self.pugsql_module.get_space_by_id(space_id=space_id)
        except Exception as err:
            logging.error("Unable to query data from spaces", exc_info=err)
            return None
        if row:
            return db.Space.build(row)
        return None

    def query_spaces(self, **filters: dict) -> List[db.Space]:
        if 'id' in filters:
            return self.get_space_by_id(filters['id'])
        return self.get_all_spaces(**filters)

    def save_space(self, space: db.Space):
        obj = space.to_sql_object()
        for field in space.ignore_fields:
            obj.pop(field)
        columns, values = self._generate_insert_statement(obj)
        try:
            self.pugsql_module.insert_space(columns=AsIs(columns), values=AsIs(values))
        except Exception as err:
            logging.error(f"Unable to insert space: {obj}", exc_info=err)
        return space

    def update_space(self, space: db.Space):
        obj = space.to_sql_object()
        for field in space.ignore_fields:
            obj.pop(field)
        update = self._generate_update_statement(obj)
        try:
            self.pugsql_module.update_space(update=AsIs(update), space_id=space.id)
        except Exception as err:
            logging.error(f"Unable to update space: {obj}", exc_info=err)
        return space

    def delete_space(self, space: db.Space):
        self.pugsql_module.delete_space(space_id=space.id)
        return space

    def get_all_messages(self) -> List[db.Message]:
        raw_data = self.pugsql_module.get_all_messages()
        ret_list = []
        for row in raw_data:
            ret_list.append(db.Message.build(row))
        return ret_list

    def save_message(self, message: db.Message):
        obj = message.to_sql_object()
        try:
            self.pugsql_module.insert_message(obj)
        except Exception as err:
            logging.error(f"Unable to insert message: {obj}", exc_info=err)

    def query_messages(self, **filters: dict):
        if filters:
            where = self._generate_where_clause(None, None, None, **filters)
            return self.pugsql_module.get_messages(where=AsIs(where))
        return self.get_all_messages()

    def save_auditlog(self, audit_log: db.AuditLog):
        obj = audit_log.to_sql_object()
        try:
            self.pugsql_module.insert_audit_log(obj)
        except Exception as err:
            logging.error(f"Unable to insert audit log {obj}", exc_info=err)
        pass

    def get_all_audit_logs(self) -> List[db.AuditLog]:
        raw_data = self.pugsql_module.get_all_audit_logs()
        ret_list = []
        for row in raw_data:
            ret_list.append(db.AuditLog.build(row))
        return ret_list

    def query_audit_logs(self, **filters: dict):
        if filters:
            where = self._generate_where_clause(None, None, None, **filters)
            return self.pugsql_module.get_audit_logs(where=AsIs(where))
        return self.get_all_audit_logs()

    def get_all_adapter_webhooks(self) -> List[db.AdapterWebhook]:
        raw_data = self.pugsql_module.get_all_adapter_webhooks()
        ret_list = []
        for row in raw_data:
            ret_list.append(db.AdapterWebhook.build(row))
        return ret_list

    def get_all_service_access_privileges(self) -> List[db.ServiceAccessPrivilege]:
        raw_data = self.pugsql_module.get_all_service_access_privileges()
        ret_list = []
        for row in raw_data:
            ret_list.append(db.ServiceAccessPrivilege.build(row))
        return ret_list

    def insert_ephemeral_shopify_install(self, ephemeral_shopify_install: EphemeralShopifyInstall) -> EphemeralShopifyInstall:
        return self.pugsql_module.insert_ephemeral_shopify_install(
            instance_id=ephemeral_shopify_install.instance_id,
            access_token=ephemeral_shopify_install.access_token
        )

    def get_ephemeral_app_install(self, instance_id: str) -> EphemeralShopifyInstall:
        result = self.pugsql_module.get_ephemeral_app_install(instance_id=instance_id)

        return db.EphemeralShopifyInstall.build(result)

    def is_ephemeral_app_installed(self, instance_id: str) -> bool:
        results = self.pugsql_module.get_ephemeral_app_install(instance_id=instance_id)
        if results:
            return True

        return False

    # Select a Space
    def _build_select_space(self, select_space_dict: Dict) -> Optional[SelectSpace]:
        if not select_space_dict:
            return None
        return SelectSpace.build(select_space_dict)

    def get_select_space_by_brand_id(self, brand_id: str) -> List[SelectSpace]:
        raw_data = self.pugsql_module.get_select_space_by_brand_id(brand_id=brand_id)
        ret_list = []
        for row in raw_data:
            ret_list.append(self._build_select_space(row))
        return ret_list

    def get_select_space_by_id(self, id: str):
        raw_data = self.pugsql_module.get_select_space_by_id(id=id)
        return self._build_select_space(raw_data)

    def get_select_space_by_salesforce_space_id(self, salesforce_space_id: str):
        raw_data = self.pugsql_module.get_select_space_by_salesforce_space_id(salesforce_space_id=salesforce_space_id)
        return self._build_select_space(raw_data)

    def save_selectspace(self, select_space: SelectSpace):
        try:
            self.pugsql_module.insert_select_space(
                id=select_space.id,
                brand_id=select_space.brand_id,
                user_id=select_space.user_id,
                salesforce_space_id=select_space.salesforce_space_id)
        except Exception as err:
            logging.error(f"Unable to insert select_space {str(err)}")

    def upsert_salesforce_space_image_urls(self, salesforce_space_id: str, image_urls: List[str]):
        try:
            self.pugsql_module.upsert_salesforce_space_image_urls(
                salesforce_space_id=salesforce_space_id,
                image_urls=json.dumps(image_urls))
        except Exception as err:
            logging.error(f"Unable to upsert space image urls for space: {salesforce_space_id}", exc_info=err)

    def delete_salesforce_space_image_urls(self):
        self.pugsql_module.delete_salesforce_space_images()

    def get_salesforce_space_image_urls_by_space_id(self, salesforce_space_id: str):
        try:
            ret = self.pugsql_module.get_salesforce_space_image_urls_by_space_id(salesforce_space_id=salesforce_space_id)
            if ret:
                return ret['image_urls']
            else:
                return []
        except Exception as err:
            logging.error(f"Unable to get space image urls for space: {salesforce_space_id}", exc_info=err)
