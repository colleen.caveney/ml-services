-- :name get_all_adapter_stores_count :one
SELECT count(*)
FROM adapter_stores
;

-- :name get_adapter_store_by_id :one
SELECT *
FROM adapter_stores
WHERE id = :adapter_store_id
;

-- :name get_all_adapter_stores :many
SELECT *
FROM adapter_stores
:where
:order_by
LIMIT :limit OFFSET :offset
;

-- :name upsert_adapter_store :affected
INSERT INTO adapter_stores(:columns)
VALUES (:values)
ON CONFLICT (id) DO
UPDATE SET :update;
