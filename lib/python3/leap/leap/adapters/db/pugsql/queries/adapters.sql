-- :name get_adapter_by_id :one
SELECT  a.id AS id,
        brand_id,
        service_id,
        s.type AS service_type,
        a.created_at AS created_at,
        a.updated_at AS updated_at,
        internal,
        instance_id,
        source,
        sink,
        track_inventory,
        filters,
        data_state,
        a.config AS config
FROM    adapters AS a
JOIN    services AS s
ON      a.service_id = s.id
WHERE   a.id = :adapter_id
;

-- :name get_all_adapters_for_brand :many
SELECT  a.id AS id,
        brand_id,
        service_id,
        s.type AS service_type,
        a.created_at AS created_at,
        a.updated_at AS updated_at,
        internal,
        instance_id,
        source,
        sink,
        track_inventory,
        filters,
        data_state,
        a.config AS config
FROM    adapters AS a
JOIN    services AS s
ON      a.service_id = s.id
WHERE
    brand_id = :brand_id
    AND internal = :internal
;

-- :name get_adapter_for_brand_by_id :one
SELECT  a.id AS id,
        brand_id,
        service_id,
        s.type AS service_type,
        a.created_at AS created_at,
        a.updated_at AS updated_at,
        internal,
        instance_id,
        source,
        sink,
        track_inventory,
        filters,
        data_state,
        a.config AS config
FROM    adapters AS a
JOIN    services AS s
ON      a.service_id = s.id
WHERE
    brand_id = :brand_id
    AND a.id = :adapter_id
;

-- :name get_all_adapters :many
SELECT  a.id AS id,
        brand_id,
        service_id,
        s.type AS service_type,
        a.created_at AS created_at,
        a.updated_at AS updated_at,
        internal,
        instance_id,
        source,
        sink,
        track_inventory,
        filters,
        data_state,
        a.config AS config
FROM adapters AS a
JOIN services AS s
ON   a.service_id = s.id
:where
:order_by
LIMIT :limit OFFSET :offset
;

-- :name get_all_adapters_count :one
SELECT count(*)
FROM adapters
;

-- :name update_adapter_config :affected
UPDATE adapters  
SET config = :config
WHERE id = :adapter_id
;

-- :name upsert_adapter :affected
INSERT INTO adapters(:columns)
VALUES (:values)
ON CONFLICT (id) DO
UPDATE SET :update;

-- :name delete_adapter :affected
DELETE FROM adapters
where id = :adapter_id
CASCADE;
