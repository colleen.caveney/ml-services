-- :name get_all_actions_count :one
SELECT count(*)
FROM actions
;

-- :name get_all_actions :many
SELECT *
FROM actions
:order_by
LIMIT :limit OFFSET :offset
;

-- :name get_action_by_brand_id :one
SELECT *
FROM actions
WHERE brand_id = :brand_id;

-- :name insert_action :insert
INSERT INTO actions(id, brand_id, matched_subscription_ids, message, action_type)
VALUES
(
    :id,
    :brand_id,
    :matched_subscription_ids,
    :message,
    :action_type
);
