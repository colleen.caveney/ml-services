-- :name get_all_audit_logs :many
SELECT * FROM audit_logs;

-- :name get_audit_logs :many
SELECT * FROM audit_logs :where;

-- :name insert_audit_log :insert
INSERT INTO audit_logs(id, message_id, action_type, properties)
VALUES
(
    :id,
    :message_id,
    :action_type,
    :properties
);
