-- :name get_brand_assets_for_brand :many
SELECT id, brand_id, created_by_id, type, storage_service, created_at, updated_at, deleted, bucket, path, name
FROM brand_assets
WHERE
    brand_id = :brand_id
    AND deleted = :deleted
;

-- :name get_brand_asset_for_brand_by_id :one
SELECT id, brand_id, created_by_id, type, storage_service, created_at, updated_at, deleted, bucket, path, name
FROM brand_assets
WHERE
    brand_id = :brand_id
    AND id = :asset_id
;

-- :name save_brand_asset :insert
INSERT INTO brand_assets
(brand_id, created_by_id, bucket, path, name, type, id, storage_service)
VALUES
(:brand_id, :created_by_id, :bucket, :path, :name, :type, :id, :storage_service)
;

-- :name get_all_brand_assets_count :one
SELECT count(*)
FROM brand_assets
;

-- :name get_all_brand_assets :many
SELECT *
FROM brand_assets :order_by 
LIMIT :limit OFFSET :offset
;

-- :name set_deleted_brand_asset :affected
UPDATE brand_assets
SET deleted = true
WHERE id = :asset_id
;
