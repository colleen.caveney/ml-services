-- :name get_brand_profile_by_brand_id :one
SELECT *
FROM brand_profiles
WHERE
    brand_id = :brand_id
;

-- :name get_brand_profile_by_id :one
SELECT *
FROM brand_profiles
WHERE
    id = :id
;

-- :name insert_brand_profile :insert
INSERT INTO
    brand_profiles (
        brand_id,
        overview_updated_by,
        overview,
        visual_merchandising_updated_by,
        visual_merchandising,
        product_merchandising_updated_by,
        product_merchandising,
        store_operations_updated_by,
        store_operations,
        store_design_updated_by,
        store_design,
        marketing_updated_by,
        marketing,
        integrations_updated_by,
        integrations,
        social_updated_by,
        social
    )
VALUES
    (
        :brand_id,
        :overview_updated_by,
        :overview,
        :visual_merchandising_updated_by,
        :visual_merchandising,
        :product_merchandising_updated_by,
        :product_merchandising,
        :store_operations_updated_by,
        :store_operations,
        :store_design_updated_by,
        :store_design,
        :marketing_updated_by,
        :marketing,
        :integrations_updated_by,
        :integrations,
        :social_updated_by,
        :social
    );

-- :name get_latest_brand_profile :one
SELECT *
FROM brand_profiles
ORDER BY updated_at DESC LIMIT 1
;

-- :name update_brand_profile_by_brand_id :affected
UPDATE brand_profiles
SET updated_at                          = :updated_at,
    overview_updated_by                 = :overview_updated_by,
    overview                            = :overview,
    visual_merchandising_updated_by     = :visual_merchandising_updated_by,
    visual_merchandising                = :visual_merchandising,
    product_merchandising_updated_by    = :product_merchandising_updated_by,
    product_merchandising               = :product_merchandising,
    store_operations_updated_by         = :store_operations_updated_by,
    store_operations                    = :store_operations,
    store_design_updated_by             = :store_design_updated_by,
    store_design                        = :store_design,
    marketing_updated_by                = :marketing_updated_by,
    marketing                           = :marketing,
    integrations_updated_by             = :integrations_updated_by,
    integrations                        = :integrations,
    social_updated_by                   = :social_updated_by,
    social                              = :social
WHERE brand_id = :brand_id
;

-- :name get_brand_box_link_by_id :one
SELECT box_link
FROM brands
WHERE id = :brand_id
;

-- :name update_brand_box_link_by_id :affected
UPDATE brands
SET
    box_link = :box_link
WHERE id = :brand_id
;