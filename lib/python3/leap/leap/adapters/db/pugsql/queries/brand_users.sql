-- :name get_all_brand_users_count :one
SELECT count(*)
FROM brand_users
:where
;

-- :name get_all_brand_users :many
SELECT id, user_id, brand_id, enabled, created_at, updated_at
FROM brand_users
:where
:order_by
LIMIT :limit OFFSET :offset
;

-- :name insert_brand_user :insert
INSERT INTO brand_users(:columns)
VALUES (
    :values
);

-- :name delete_brand_user_by_id :affected
DELETE FROM brand_users
WHERE id = :brand_user_id;

-- :name get_brand_user_by_user_id_and_brand_id :one
SELECT * FROM brand_users
WHERE user_id = :user_id
AND brand_id = :brand_id;

-- :name get_brand_user_by_id :one
SELECT * FROM brand_users
WHERE id = :id;

-- :name update_brand_user :affected
UPDATE brand_users
SET :update
WHERE id = :brand_user_id;
