-- :name get_brand_by_ids :many
SELECT *
FROM brands
WHERE
    id in :brand_ids
;

-- :name get_all_brands :many
SELECT *
FROM brands
:where
:order_by
LIMIT :limit OFFSET :offset
;

-- :name get_all_brands_count :one
SELECT count(*)
FROM brands
:where
;

-- :name delete_brand_by_id :affected
DELETE
FROM brands
WHERE id = :brand_id;

-- :name insert_brand :insert
INSERT INTO brands(:columns)
VALUES (
    :values
);

-- :name update_brand :affected
UPDATE brands
SET
    :update
WHERE id = :brand_id;

-- :name get_brand_by_filter :many
SELECT *
FROM brands
:where;
