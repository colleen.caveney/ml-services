-- :name get_ephemeral_app_install :one
SELECT  *
FROM    ephemeral_shopify_installs
WHERE   instance_id = :instance_id
;

-- :name insert_ephemeral_shopify_install :insert
INSERT INTO
    ephemeral_shopify_installs (
        instance_id,
        access_token
    )
VALUES
    (
        :instance_id,
        :access_token
    )
ON CONFLICT (instance_id)
DO UPDATE
SET updated_at=NOW(), access_token=:access_token
;
