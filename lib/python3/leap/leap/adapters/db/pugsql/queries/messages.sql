-- :name get_all_messages :many
SELECT * FROM messages;

-- :name get_messages :many
SELECT * FROM messages :where

-- :name insert_message :insert
INSERT INTO messages(id, message_type, action_type, recipients, message_data)
VALUES
(
    :id,
    :message_type,
    :action_type,
    :recipients,
    :message_data
);
