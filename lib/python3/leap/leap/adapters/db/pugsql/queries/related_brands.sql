-- :name get_parent_brand :one
SELECT parent_id
FROM related_brands
WHERE
    child_id = :brand_id
;

-- :name get_children_brands :many
SELECT child_id
FROM related_brands
WHERE
    parent_id = :brand_id
;

-- :name get_related_group :many
SELECT *
FROM (select parent_id, array_agg(child_id) as children_ids
      from related_brands
      group by parent_id) as r
WHERE r.parent_id = :id
   OR :id = ANY (children_ids)
;

-- :name get_related_brand_by_id :one
SELECT *
FROM related_brands
WHERE id = :id
;

-- :name insert_related_brand :insert
INSERT INTO related_brands(id, parent_id, child_id)
VALUES (:id, :parent_id, :child_id)
;

-- :name update_related_brand :affected
UPDATE related_brands
SET parent_id = :parent_id,
    child_id = :child_id
WHERE id = :id
;

-- :name delete_related_brand_by_id :affected
DELETE
FROM related_brands
WHERE id = :id
;

-- :name get_related_brands :many
SELECT *
FROM related_brands
:where
;

-- :name get_all_related_brands :many
SELECT *
FROM related_brands
;
