-- :name get_select_space_by_id :one
SELECT *
FROM select_spaces
WHERE id = :id
;

-- :name get_select_space_by_salesforce_space_id :one
SELECT *
FROM select_spaces
WHERE salesforce_space_id = :salesforce_space_id
;

-- :name get_select_space_by_brand_id :many
SELECT *
FROM select_spaces
WHERE brand_id = :brand_id
;

-- :name insert_select_space :insert
INSERT INTO select_spaces(id, brand_id, user_id, salesforce_space_id)
VALUES (:id, :brand_id, :user_id, :salesforce_space_id)
;

-- :name upsert_salesforce_space_image_urls :insert
INSERT INTO salesforce_space_images (salesforce_space_id, image_urls)
VALUES (:salesforce_space_id, :image_urls)
ON CONFLICT (salesforce_space_id)
DO
    UPDATE SET image_urls = :image_urls
;

-- :name get_salesforce_space_image_urls_by_space_id :one
SELECT image_urls
FROM salesforce_space_images
WHERE salesforce_space_id = :salesforce_space_id;
;

-- :name delete_salesforce_space_images
DELETE FROM salesforce_space_images
;