-- :name get_service_by_id :one
SELECT
    id,
    handle,
    type,
    created_at,
    updated_at,
    config,
    api_key,
    api_secret
FROM services
WHERE
    id = :service_id;

-- :name get_all_services :many
SELECT * FROM services;

-- :name get_all_service_access_privileges :many
select
    p.id,
    s.handle as service_id,
    tier,
    p.privilege,
    p.created_at,
    p.updated_at
from
    service_access_privileges p
    inner join services s on s.id = p.service_id;

-- :name get_service_by_filter :many
SELECT *
FROM services
:where;

-- :name insert_service :insert
INSERT INTO services(:columns)
VALUES (:values);