-- :name get_all_spaces
SELECT * 
FROM spaces
:where;

-- :name insert_space :insert
INSERT INTO spaces(:columns)
VALUES (:values);

-- :name update_space :affected
UPDATE spaces
SET :update
WHERE id = :space_id;

-- :name delete_space :affected
DELETE FROM spaces
WHERE id = :space_id;

-- :name get_space_by_id :one
SELECT * FROM SPACES WHERE id = :space_id;
