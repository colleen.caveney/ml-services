-- :name get_all_stores_count :one
SELECT count(*)
FROM stores
:where
;

-- :name get_store_by_id :one
SELECT *
FROM stores
WHERE id = :store_id
;

-- :name get_store_by_code :one
SELECT *
FROM stores
WHERE code = :store_code
;

-- :name get_store_by_space_id :one
SELECT *
FROM stores
WHERE space_id = :space_id
;


-- :name get_all_stores :many
SELECT *
FROM stores
:where
:order_by
LIMIT :limit OFFSET :offset
;

-- :name insert_store :insert
INSERT INTO stores(:columns)
VALUES (:values);

-- :name update_store :affected
UPDATE stores
SET :update
WHERE code = :store_code;

-- :name delete_store :affected
UPDATE stores
SET deleted = true
WHERE code = :store_code;

-- :name get_all_admin_stores :many
SELECT
to_json(stores.*)::jsonb
||
json_build_object(
    'brand', to_json(brands.*),
    'space', to_json(spaces.*),
    'location', to_json(spaces.*),
    'merchant', to_json( CASE
    WHEN merchant.merchant_type is null THEN 'na'
    ELSE  merchant.merchant_type
    END)
    )::jsonb
AS store
FROM stores
JOIN brands ON brands.id = stores.brand_id
JOIN spaces ON spaces.id = stores.space_id
LEFT JOIN (
    SELECT adapter_stores.store_id as store_id,
       CASE
           WHEN adapters.internal = True THEN 'leap'
            ELSE 'brand'
            END as merchant_type
    FROM adapters
    LEFT JOIN adapter_stores on adapter_stores.adapter_id = adapters.id
    JOIN services on adapters.service_id = services.id AND services.type = 'shopify'
AND adapter_stores.is_primary = True
) as merchant on merchant.store_id = stores.id
:where
:order_by
LIMIT :limit
OFFSET :offset
;

-- :name get_all_admin_stores_count :one
SELECT count(1)
FROM stores
JOIN brands ON brands.id = stores.brand_id
JOIN spaces ON spaces.id = stores.space_id
LEFT JOIN (
    SELECT adapter_stores.store_id as store_id,
       CASE
           WHEN adapters.internal = True THEN 'leap'
            ELSE 'brand'
            END as merchant_type
    FROM adapters
    LEFT JOIN adapter_stores on adapter_stores.adapter_id = adapters.id
    JOIN services on adapters.service_id = services.id AND services.type = 'shopify'
AND adapter_stores.is_primary = True
) as merchant on merchant.store_id = stores.id
:where