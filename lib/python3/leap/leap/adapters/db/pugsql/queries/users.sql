-- :name get_all_users_count :one
SELECT count(*)
FROM users
;

-- :name get_all_users :many
SELECT *
FROM users
:where
:order_by
LIMIT :limit OFFSET :offset
;

-- :name create_test_user :insert
INSERT INTO users(id, identity_service, external_user_id)
VALUES (
    :id,
    :identity_service,
    :external_user_id)
;

-- :name insert_user :insert
INSERT INTO users(id, identity_service, external_user_id, email, display_name, full_name, title, phone_number, is_deleted, deleted_at)
VALUES
(
    :id,
    :identity_service,
    :external_user_id,
    :email,
    :display_name,
    :full_name,
    :title,
    :phone_number,
    :is_deleted,
    :deleted_at
);

-- :name delete_user_by_id :affected
DELETE FROM users where id = :user_id;

-- :name update_user :affected
UPDATE users
SET :update
WHERE id = :user_id;

-- :name get_user_by_id :one
SELECT *
FROM users
WHERe id = :id;
