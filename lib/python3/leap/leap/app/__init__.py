"""Leap app support.

The main concepts with this package are:

...

"""

import logging
from typing import Callable, List

import flask
from werkzeug.exceptions import HTTPException, default_exceptions

import leap.app.routing
import leap.errors

ALL_METHODS = ["TRACE", "OPTIONS", "HEAD",
               "GET", "POST", "PUT", "PATCH", "DELETE"]

CatchAllFunc = Callable[[flask.Request], flask.Response]


def catch_all(import_name: str,
              handler_fn: CatchAllFunc,
              methods: List[str] = ALL_METHODS,
              **kwargs: dict) -> flask.Flask:
    """Creates a catch-all Flask app.

    This app will accept all requests regardless of path or http-method. It will also define
    a keyword-arg named `path` which will contain the matched path sans the leading '/' char.

    Args:
        import_name: Needed by Flask.
        handler_fn: The function that will be executed for catch-all request handling.
        kwargs: Keyword args that are passed onto to Flask.

    Returns:
        A flask.Flask instance which can be used normally.
    """
    app = flask.Flask(import_name, **kwargs)
    app.url_map.converters["regex"] = leap.app.routing.RegexConverter
    app.add_url_rule('/<regex(".*"):path>', "trigger",
                     lambda path: handler_fn(flask.request),
                     provide_automatic_options=False,
                     methods=methods)

    # catch all HTTP exceptions and respond with json; implies we are a RESTful app
    json_exception_handler(app)

    return app


def _json_error_response(msg: dict, err: HTTPException) -> flask.Response:
    flask.g.message = msg
    res = flask.jsonify(msg)
    res.headers.extend(err.get_headers())
    res.status_code = err.code if isinstance(err, HTTPException) else 500
    return res


def _json_leap_http_error_response(err: leap.errors.http.HTTPError) -> flask.Response:
    resp = {
        "error": err.description,
        "class": err.__class__.__name__,
        "scopes": err.scopes,
    }
    if err.data:
        resp["data"] = err.data
    return _json_error_response(resp, err)


def _json_flask_error_response(err: HTTPException) -> flask.Response:
    return _json_error_response({
        "error": err.description,
        "class": err.__class__.__name__,
    }, err)


def json_exception_handler(app: flask.Flask):
    """Generates a JSON formatted error response for any HTTPException.

    See: https://coderwall.com/p/xq88zg/json-exception-handler-for-flask
    """
    app.register_error_handler(
        leap.errors.http.HTTPError, _json_leap_http_error_response)

    for code, _ in default_exceptions.items():
        app.register_error_handler(code, _json_flask_error_response)
    app.register_error_handler(HTTPException, _json_flask_error_response)
