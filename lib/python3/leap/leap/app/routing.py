"""Flask routing helpers.
"""

from typing import List

import flask
from werkzeug import routing


class RegexConverter(routing.BaseConverter):
    """Accepts first item as a string representation of a regex route.

    Using this converter, you can include regex matching in a route. For instance, to define a
    catch-all-paths route, you would specify the following:

        from leap.flask import routing
        app.url_map.converters['regex'] = routing.RegexConverter
        @app.route('/<regex(".*"):path>')
        def all(path):
            ...
    """
    def __init__(self, url_map: routing.Map, *items: List[str]):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


def add_catch_all_endpoint(app: flask.Flask, endpoint: str):
    """Adds a rule bound to an endpoint for catching all requests.

    This rule will accept all requests regardless of path or http-method. It will also define
    a keyword-arg named `path` which will contain the matched path sans the leading '/' char.
    """
    app.url_map.converters["regex"] = RegexConverter
    app.url_map.add(routing.Rule('/<regex(".*"):path>', endpoint=endpoint))
