from typing import Any, Optional

from leap.std import uuid

TRACE_ID = "x-leap-trace-id"


class ContextAttrs(dict):
    def __init__(self, attrs: dict = {}):
        self.update(attrs)

        if not self.trace_id:
            self[TRACE_ID] = next(uuid.uuid4_gen()).hex

    @property
    def trace_id(self) -> Optional[str]:
        """Get the trace-id from this context"""
        return self.get(TRACE_ID)

    def set(self, key: str, value: Optional[Any] = None) -> Optional[Any]:
        """Set value for an attribute in this context"""
        self[key] = value
        return value
