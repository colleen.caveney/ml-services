class BaseError(Exception):
    """This base of all Leap errors."""

    def __init__(self, *args):
        super().__init__(*args)

class UnresolvableError(BaseError):
    """This error represents a state which is unresolvable."""

    def __init__(self, *args):
        super().__init__(*args)

class ConflictError(BaseError):
    """This error represents a state conflict which is potentially resolvable.

    The conflict should be resolved before processing may be attempted again.
    """

    def __init__(self, *args):
        super().__init__(*args)

class RetryError(ConflictError):
    """This error represents a state conflict which is potentially resolvable with a Retry."""

    def __init__(self, retry_after_s: float, *args):
        super().__init__(*args)
        self._retry_after_s: float = retry_after_s

    @property
    def retry_after(self) -> float:
        return self._retry_after_s

class SkipAttemptUntilError(ConflictError):
    """Indicates that an attempt to do something should be skipped until some future time."""

    def __init__(self, retry_after_s: float):
        super().__init__("skipping processing until after {0}s".format(retry_after_s))

class MaxAttemptsReachedError(UnresolvableError):
    """Indicates that the maxiumum number of attempts has been reached."""

    def __init__(self, attempts: int, max: int):
        super().__init__("reached max attempts: {0}/{1}".format(attempts, max))

class InvalidEnvError(UnresolvableError):
    """Indicates that a provided environment name is invalid."""

    def __init__(self, env: str):
        super().__init__("invalid env name '{0}'".format(env))
