import logging as pylog
import os
import sys
import traceback
from typing import Dict, List, Union

import werkzeug.exceptions

class ErrorLogger(pylog.Logger):
    """Custom logger mainly used to log exceptions more concisely

    Code taken from https://stackoverflow.com/questions/49150486/custom-exception-default-logging
    """

    def makeRecord(self, name, level, fn, lno, msg, args, exc_info, func=None, extra=None, sinfo=None):
        """Override default logger to allow overridding internal attributes."""
        rv = pylog.LogRecord(name, level, fn, lno, msg, args, exc_info, func, sinfo)

        if extra is not None:
            for key in extra:
                rv.__dict__[key] = extra[key]
        return rv

pylog.setLoggerClass(ErrorLogger)
logger = pylog.getLogger("error")

class HTTPError(werkzeug.exceptions.HTTPException):
    scopes: List[Union[str, int, float]] = []
    data: Dict[str, Dict[str, Union[int, float, str]]] = {}

    def __init__(self, *args, **kwargs):
        if "scopes" in kwargs:
            self.scopes = list(kwargs.pop("scopes"))
        if "data" in kwargs:
            self.data = kwargs.pop("data")

        # https://stackoverflow.com/questions/49150486/custom-exception-default-logging
        try:
            raise ZeroDivisionError
        except ZeroDivisionError:
            # Find the traceback frame that raised this exception
            exception_frame = sys.exc_info()[2].tb_frame.f_back.f_back

        exception_stack = traceback.extract_stack(exception_frame, limit=1)[0]
        filename, lineno, funcName, tb_msg = exception_stack
        rel_path = os.path.relpath(filename, '/leap/platform/services')
        extra = {'pathname': rel_path, 'lineno': lineno, 'funcname': funcName}
        logger.error(f"ERROR: [{rel_path}:{funcName}:{lineno}] {list(args)[0]}", extra=extra)
        super().__init__(*args)
