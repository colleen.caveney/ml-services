from typing import Optional, Tuple

from ._base import RetryError, UnresolvableError

ServiceAdapterKey = Tuple[str, str]

class InvalidConfigError(UnresolvableError):
    """Indicates an error related to a Service's configuration."""

    def __init__(self, key: ServiceAdapterKey, msg: str):
        super().__init__("service:adapter '{}:{}' has invalid config: {}".format(*key, msg))

class TooManyRequests(RetryError):
    """Exception raised whenever an action is being rate-limited"""

    def __init__(self, key: ServiceAdapterKey, retry_after_s: float, endpoint: Optional[str], rate: Optional[str]):
        msg = "service:adapter '{}:{}' hit rate limit '{}' for '{}' with {}s retry wait".format(*key, rate, endpoint, retry_after_s)
        super().__init__(retry_after_s, msg)
