# @see https://github.com/GoogleCloudPlatform/python-docs-samples/blob/master/cdn/snippets.py

import base64
import hashlib
import hmac
from datetime import datetime
from urllib import parse


def sign_url(url: str, key_name: str, base64_key: str, expires_in_s: int):
    """Gets the Signed URL string for the specified URL and configuration.
    Args:
        url: URL to sign as a string.
        key_name: name of the signing key as a string.
        base64_key: signing key as a base64 encoded string.
        expires_in_s: number of seconds until expiration.
    Returns:
        Returns the Signed URL appended with the query parameters based on the
        specified configuration.
    """
    stripped_url = url.strip()
    parsed_url = parse.urlsplit(stripped_url)
    query_params = parse.parse_qs(parsed_url.query, keep_blank_values=True)

    exp_timestamp = int(datetime.now().timestamp() + expires_in_s)
    decoded_key = base64.urlsafe_b64decode(base64_key)

    url_pattern = u'{url}{separator}Expires={expires}&KeyName={key_name}'

    url_to_sign = url_pattern.format(
            url=stripped_url,
            separator='&' if query_params else '?',
            expires=exp_timestamp,
            key_name=key_name)

    digest = hmac.new(
        decoded_key, url_to_sign.encode('utf-8'), hashlib.sha1).digest()
    signature = base64.urlsafe_b64encode(digest).decode('utf-8')

    signed_url = u'{url}&Signature={signature}'.format(
            url=url_to_sign, signature=signature)

    return signed_url
