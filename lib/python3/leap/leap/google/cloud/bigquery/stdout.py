"""Leap BigQuery Stdout.

Basically, this module let's you "mock" a :class:`~google.cloud.bigquery.client.Client` with an
implementation that logs to stdout.

    import leap.google.cloud.bigquery.stdout as bq
    my_bq_client = bq.Client()
"""

from typing import Optional, Tuple

class Dataset:
    """Dataset

    ...
    """

    def __init__(self, name: str):
        self.name = name
        self.table_name: Optional[str] = None

    def table(self, name: str) -> Tuple[str, str]:
        """table ..."""
        self.table_name = name
        return (self.name, self.table_name)

class Client:
    """Client

    ...
    """

    def dataset(self, name: str) -> Dataset:
        """dataset ..."""
        return Dataset(name)

    def insert_rows_json(self, ds_tbl_tup: Tuple[str, str], **kwargs):
        """insert_rows_json ..."""
        print("DATASET+TABLE={}".format(ds_tbl_tup))
        print("ROWS={}".format(kwargs.get("json_rows")))
