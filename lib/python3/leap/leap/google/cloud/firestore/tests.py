"""Leap GCP Firestore test helpers."""

from datetime import datetime
from typing import Any, Dict, Iterator, List, Optional, Tuple

from google.cloud import firestore

from leap.std.collections import dict_key_filter

QueryClause = Tuple[str, str, Any]


class Query(firestore.Query):
    def __init__(self, docs: List[firestore.DocumentSnapshot], clause: Optional[QueryClause] = None):
        self._clauses = [clause] if clause else []
        self._docs = docs
        self._limit: Optional[int] = None
        self._order_by: Optional[tuple] = None
        self._start_after: Optional[dict] = None

    def __str__(self):
        return "{}(limit={}, order_by={}, start_after={}, clauses={})".format(
            self.__class__.__name__,
            self._limit,
            self._order_by,
            self._start_after,
            self._clauses
        )

    # overridden methods

    def stream(self, tx=None) -> Iterator[firestore.DocumentSnapshot]:
        """This where any filtering actually happens.

        Currently only applying "==" operator. Any other operator passes for the doc.
        """
        ctr: int = 0
        docs = self._docs

        if self._order_by:
            docs = sorted(docs, key=lambda d: d.to_dict()[self._order_by[0]],
                reverse=self._order_by[1] == 'DESCENDING')

        if self._start_after:
            keys = list(self._start_after.keys())
            i = 0
            # find index of first element matching condition
            for i, d in enumerate(docs):
                vals = dict_key_filter(d.to_dict(), keys)
                if vals == self._start_after:
                    break
            docs = docs[i+1:]

        for doc in docs:
            if ctr == self._limit:
                break

            matched = True
            for field, op, value in self._clauses:
                try:
                    if op == "==" and doc.get(field) != value:
                        matched = False
                        break
                    elif op == "<=" and doc.get(field) > value:
                        matched = False
                        break
                    elif op == ">=" and doc.get(field) < value:
                        matched = False
                        break
                    elif op == "in" and doc.get(field) not in value:
                        matched = False
                        break
                except KeyError:
                    matched = False
            if matched:
                yield(doc)
                ctr += 1

    def where(self, field: str, op: str, value: Any) -> firestore.Query:
        self._clauses.append((field, op, value))
        return self

    def limit(self, n: int) -> firestore.Query:
        self._limit = n
        return self

    def start_after(self, fields: dict) -> firestore.Query:
        self._start_after = fields
        return self

    def order_by(self, field: str, direction: str) -> firestore.Query:
        self._order_by = (field, direction)
        return self

    # helper methods

    @property
    def clauses(self) -> List[QueryClause]:
        return self._clauses

    @property
    def requested_limit(self) -> Optional[int]:
        return self._limit

    @property
    def requested_order_by(self) -> Optional[str]:
        return self._order_by


class DocumentReference(firestore.DocumentReference):
    def __init__(self, path: str, data: dict = None, coll=None):
        self._path = path.split("/")
        super().__init__(*self._path)
        self._data = data
        self._coll: Collection = coll

    # overridden methods

    def get(self) -> firestore.DocumentSnapshot:
        if not self._data:
            firestore.DocumentSnapshot(object(), None, False, 0, 0, 0)
        return firestore.DocumentSnapshot(object(), self._data, True, 0, 0, 0)

    def set(self, data: dict):
        self._data = data
        if self._coll:
            self._coll.store(self)

    def delete(self):
        if self._coll:
            self._coll.delete(self)

    # helper methods

    @property
    def collection_name(self) -> str:
        return "/".join(self._path[:-1])


class Collection(firestore.CollectionReference):
    def __init__(self, path: str, client):
        self._client = client
        self._path = path
        self.refs: Dict[str, DocumentReference] = {}
        self._queries: List[Query] = []

    # overridden methods

    def document(self, id: str) -> DocumentReference:
        ref = self.refs.get(id)
        if not ref:
            ref = DocumentReference(
                "/".join([self._path, id]), data=None, coll=self)
        return ref

    def where(self, field: str, op: str, value: Any) -> Query:
        query = Query(list(v.get()
                      for v in self.refs.values()), (field, op, value))
        self._queries.append(query)
        return query

    def limit(self, n: int) -> Query:
        query = Query(list(v.get() for v in self.refs.values()))
        self._queries.append(query)
        return query.limit(n)

    def order_by(self, field: str, direction: Optional[str] = "ASCENDING") -> Query:
        query = Query(list(v.get() for v in self.refs.values()))
        self._queries.append(query)
        return query.order_by(field, direction)

    def start_after(self, fields: dict) -> Query:
        query = Query(list(v.get() for v in self.refs.values()))
        self._queries.append(query)
        return query.start_after(fields)

    def stream(self, tx=None) -> Iterator[firestore.DocumentSnapshot]:
        query = Query(list(v.get() for v in self.refs.values()))
        self._queries.append(query)
        for d in query.stream():
            yield(d)

    # helper methods

    def store(self, ref: DocumentReference):
        ref._coll = self
        self.refs[ref.id] = ref

    def delete(self, ref: DocumentReference):
        self.refs.pop(ref.id)

    @property
    def queries(self) -> List[Query]:
        return self._queries


class Client(firestore.Client):
    def __init__(self):
        self._collections: Dict[str, Collection] = {}

    # overridden methods

    def collection(self, path: str) -> Collection:
        coll = self._collections.get(path)
        if not coll:
            coll = Collection(path, self)
            self._collections[path] = coll
        return coll

    def document(self, *doc_path) -> DocumentReference:
        path = "/".join(doc_path)
        tmp_ref = DocumentReference(path)
        return self.collection(tmp_ref.collection_name).document(tmp_ref.id)

    # helper methods

    def store_document(self, doc_path: str, data: dict):
        ref = DocumentReference(doc_path, data)
        if isinstance(data.get("created_at"), str):
            data["created_at"] = datetime.fromisoformat(data.get("created_at"))
        if isinstance(data.get("updated_at"), str):
            data["updated_at"] = datetime.fromisoformat(data.get("updated_at"))
        self.collection(ref.collection_name).store(ref)

    def clear(self):
        """Removes everything from this session."""
        self._collections = {}
