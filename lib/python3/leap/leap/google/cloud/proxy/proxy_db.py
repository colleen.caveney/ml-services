import logging
import os
import subprocess
from typing import Any, List

import google.oauth2.id_token
import requests
from google.auth.transport.requests import Request
from leap.adapters import db

SELECT_STORE_URL = "/proxy_db/public/select_store"


class ProxyDB(object):
    def __init__(self):
        data_proxy_url = os.environ.get("DATA_PROXY_PREST_URL", None)
        if data_proxy_url is None:
            raise ValueError("No DATA_PROXY_PREST_URL, ProxyDB can not run")
        self.base_url = data_proxy_url
        self.select_store_url = f"{self.base_url}{SELECT_STORE_URL}"
        self.headers = None
        # When running in GC you need to get a token from the google lib. If you are running locally however you need
        # a token you get from the command line hence the special PROXY_LOCAL_ENV_OVERRIDE override.
        # The long term plan is to be able to run a VPN locally to get around this silliness.
        if os.environ.get("SERVICE_ENV") == 'test':
            pass
        elif os.environ.get("PROXY_LOCAL_ENV_OVERRIDE", False) or os.environ.get("SERVICE_ENV", None) == "local":
            try:
                process = subprocess.Popen(["gcloud", "auth", "print-identity-token"],
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.PIPE)
                stdout, stderr = process.communicate()
                token = stdout.decode('utf-8').strip()
                self.headers = {
                    "Content-Type": "application/json",
                    "Authorization": f"Bearer {token}"
                }
            except Exception as e:
                logging.error(f"ProxyDB Failed gcloud auth with error: {e}")
        else:
            try:
                auth_req = Request()
                id_token = google.oauth2.id_token.fetch_id_token(auth_req, self.select_store_url)
                self.headers = {
                    "Content-Type": "application/json",
                    "Authorization": f"Bearer {id_token}"
                }
            except Exception as e:
                logging.error(f"ProxyDB Failed google oauth2 id_token with error: {e}")

    # TODO: we should investigate prestd custom queries, since this filtering should really happen at the sql level
    def filter_spaces(self, raw_spaces: List[dict[str, Any]]):
        # Only show spaces to the brand that fit the space_status and store_stage criteria
        ACCEPTED_SPACE_STATUS = [db.SpaceStatusEnum.ACTIVE_LEASE.value, db.SpaceStatusEnum.SPACE_PROPOSED.value]
        ACCEPTED_STORE_STAGE = [db.StoreStageEnum.POTENTIAL.value, db.StoreStageEnum.CONTRACTING.value]

        filtered_spaces = list(filter(lambda space: space['space_status'] in ACCEPTED_SPACE_STATUS and space['store_stage'] in ACCEPTED_STORE_STAGE, raw_spaces))
        return filtered_spaces

    def generate_query_params(self, page_num: int = 1, **query_params) -> dict[str, Any]:
        if page_num:
            query_params["_page"] = page_num
        return query_params

    def get_spaces(self, page_num: int = 1, filtered: bool = True, **kwargs) -> list[dict[str, str]]:
        spaces = None
        try:
            query_params = self.generate_query_params(page_num, **kwargs)
            r = requests.get(self.select_store_url, params=query_params, headers=self.headers)
            r.raise_for_status()
        except Exception as e:
            logging.error("Could not get_spaces for contact proxy db", exc_info=e)
            return []
        if r.ok:
            spaces = r.json()
            if filtered:
                spaces = self.filter_spaces(spaces)
        return spaces

    def get_spaces_count(self, brand_ids: List[str], filtered: bool = True) -> dict[str, int]:
        params = {"brand_guid": f"$in.{','.join(brand_ids)}"}
        try:
            r = requests.get(self.select_store_url, params=params, headers=self.headers)
            r.raise_for_status()
        except Exception as e:
            logging.error("Could not get_spaces_count for contact proxy db", exc_info=e)
            return {}
        ret_dict = {}
        if r.ok:
            spaces = r.json()
            if filtered:
                spaces = self.filter_spaces(spaces)
            for space in spaces:
                ret_dict[space.get("brand_guid")] = ret_dict.get(space.get("brand_guid"), 0) + 1
        return ret_dict
