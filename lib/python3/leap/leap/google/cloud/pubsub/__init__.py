from concurrent.futures import Future

from google.cloud import pubsub_v1


class TopicPublisher:
    def __init__(self, topic: str, client: pubsub_v1.PublisherClient):
        if not topic:
            raise ValueError("topic is required")
        if not client:
            raise ValueError("client is required")
        self.topic = topic
        self.client = client

    def publish(self, data: bytes, **attrs: dict) -> Future:
        return self.client.publish(self.topic, data, **attrs)
