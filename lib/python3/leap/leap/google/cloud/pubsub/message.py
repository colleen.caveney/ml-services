"""Leap GCP PubSub Message.

See: https://cloud.google.com/pubsub/docs/push#receiving_push_messages

The contract for a Message from a pubsub is:
    {
        "message": {
            "messageId":  str,
            "attributes": dict,
            "data":       str:base64
        },
        "subscription":   str
    }
"""

import base64
import json
from typing import Optional

class InvalidMessageError(Exception):
    def __init__(self, err_msg: str):
        super().__init__(err_msg)

class Message(dict):
    def __init__(self, msg: dict):
        self.update(msg)
        self._message = self.get("message", {})
        self._b64data = self._message.get("data", b'')
        self._data = base64.b64decode(self._b64data)

    @property
    def id(self) -> str:
        return self._message.get("messageId", None)

    @property
    def subscription(self) -> str:
        return self.get("subscription", None)

    @property
    def attrs(self) -> dict:
        # not using default arg because the value could be explicitly set to None
        d = self._message.get("attributes")
        if not d:
            return {}
        return d

    @property
    def b64data(self) -> str:
        return self._b64data

    @property
    def data(self) -> bytes:
        return self._data

    @property
    def json(self) -> Optional[dict]:
        if self.data:
            return json.loads(self.data.decode())
        return None

    def __str__(self) -> str:
        return str(dict(self))

    def __repr__(self) -> str:
        return "%s(%r)" % (self.__class__.__name__, dict(self))

def decode(msg_bytes: bytes) -> Message:
    try:
        return Message(json.loads(msg_bytes.decode()))
    except json.decoder.JSONDecodeError as json_err:
        raise InvalidMessageError("decoding message") from json_err

def encode_data(data: bytes) -> str:
    if data == None:
        return ""
    return base64.b64encode(data).decode("utf-8")

def encode_json_data(data: dict) -> str:
    if data == None:
        return ""
    return encode_data(bytes(json.dumps(data, separators=(',', ':')), "utf-8"))

class SubscriptionlessMessage(Message):
    """Useful in GCP functions when triggered via pubsub."""
    def __init__(self, msg: dict):
        if not msg:
            super().__init__(msg)
        else:
            super().__init__({"message": msg})
