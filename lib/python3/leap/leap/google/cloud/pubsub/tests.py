"""Leap GCP PubSub test helpers.

... conforms to pubsub_v1
"""

from concurrent import futures
from typing import List, Tuple

Message = Tuple[str, bytes, dict]

class PublisherClient:
    """PublisherClient

    ...
    """

    def __init__(self):
        self.executor = futures.ThreadPoolExecutor(max_workers=1)
        self.created_topic = ""
        self.messages: List[Message] = []

    def create_topic(self, topic: str):
        self.created_topic = topic

    def publish(self, topic: str, data: bytes, **attrs: dict):
        self.messages.append((topic, data, attrs))
        # api contract says we need to return a Future
        return self.executor.submit(lambda: "12345")
