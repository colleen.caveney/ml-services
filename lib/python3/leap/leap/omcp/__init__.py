from typing import Sequence, Tuple, Union

"""GRPCMetadata reflects a grpc metadata type."""
GRPCMetadata = Sequence[Tuple[str, Union[str, bytes]]]
