from enum import unique
from typing import List, Any

from leap.std.enum import TypedEnum

from . import GRPCMetadata

LEAP_ATTR_PREFIX = "x-leap-"


@unique
class Attr(str, TypedEnum):  # subclassing with str makes this type JSON serializable
    """The set of known, Leap-specific attributes which are supported by OMCP."""
    ADAPTER_ID = LEAP_ATTR_PREFIX + "adapter-id"
    BRAND_HANDLE = LEAP_ATTR_PREFIX + "brand"
    BRAND_ID = LEAP_ATTR_PREFIX + "brand-id"
    BRAND_NAME = LEAP_ATTR_PREFIX + "brand-name"
    BUILD = LEAP_ATTR_PREFIX + "build"
    EVENT_TYPE = LEAP_ATTR_PREFIX + "event-type"
    OMCP_VERSION = LEAP_ATTR_PREFIX + "omcp-version"
    SERVICE_HANDLE = LEAP_ATTR_PREFIX + "service-handle"
    SERVICE_NAME = LEAP_ATTR_PREFIX + "service-name"
    TRACE_ID = LEAP_ATTR_PREFIX + "trace-id"
    TRAIL = LEAP_ATTR_PREFIX + "trail"
    EVENT_SERVICE_TYPE = LEAP_ATTR_PREFIX + "event-service-type"
    ADAPTER_MERCHANT_TYPE = LEAP_ATTR_PREFIX + "adapter-merchant-type"


_allow_key_error = object()

class Attrs(dict):
    """Attrs is a Dict[str,str] type with support for specific, Leap supported attributes.

    Most of the dict methods have been overridden to ensure all keys and values are treated
    as strings when setting and querying.

    If `filter_unknown` is True, then Attrs will only honor setting attributes whose key has
    an "x-leap-" prefix. This is true when supplying a dict during construction and when
    assigning values post-construction.
    """
    def __init__(self, attrs: dict={}, filter_known=True):
        self._filter_known = filter_known
        self.update(attrs)

    @property
    def trail(self) -> List[str]:
        t = self.get(Attr.TRAIL, None)
        if not t:
            return []
        return t.split(",")

    def append_trail(self, *tail) -> "Attrs":
        trail = self.trail
        trail.extend([str(t) for t in tail])
        self[Attr.TRAIL] = ",".join(trail)
        return self

    def to_metadata(self) -> GRPCMetadata:
        return [(k, v) for (k,v) in self.items()]

    ## dict specific overrides

    def update(self, d: dict):
        for (k, v) in d.items():
            self[k] = v

    def get(self, k: Any, default: Any=None) -> Any:
        return super(Attrs, self).get(str(k), default)

    def setdefault(self, k: Any, v: Any) -> Any:
        strk = str(k)
        if not self._filter_known or strk.startswith(LEAP_ATTR_PREFIX):
            return super(Attrs, self).setdefault(strk, str(v))
        return v

    def pop(self, k: Any, default: Any=_allow_key_error):
        if default == _allow_key_error:
            return super(Attrs, self).pop(str(k))
        return super(Attrs, self).pop(str(k), default)

    def __getitem__(self, k: Any):
        super(Attrs, self).__getitem__(str(k))

    def __contains__(self, k: Any) -> bool:
        return super(Attrs, self).__contains__(str(k))

    def __delitem__(self, k: Any):
        super(Attrs, self).__delitem__(str(k))

    def __setitem__(self, k: Any, v: Any):
        strk = str(k)
        if not self._filter_known or strk.startswith(LEAP_ATTR_PREFIX):
            super(Attrs, self).__setitem__(str(k), str(v))
