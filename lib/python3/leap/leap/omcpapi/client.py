import logging
import os
from typing import Optional, Union, NamedTuple, Any, Callable

import grpc
import google.auth
import google.auth.credentials
import google.auth.transport.grpc
import google.auth.transport.requests
from google.oauth2 import id_token
from omcpapi.v1 import omcpapi_pb2_grpc

from leap.omcp.attrs import Attrs, GRPCMetadata

class _ClientCallDetailsFields(NamedTuple):
    """_ClientCallDetailsFields specifies the fields supported by grpc.ClientCallDetails.
    Unapologetically stolen from https://github.com/d5h-foss/grpc-interceptor/blob/master/src/grpc_interceptor/client.py
    """
    method: str
    timeout: Optional[float]
    metadata: Optional[GRPCMetadata]
    credentials: Optional[grpc.CallCredentials]
    wait_for_ready: Optional[bool]
    compression: Any  # Type added in grpcio 1.23.0

class ClientCallDetails(_ClientCallDetailsFields, grpc.ClientCallDetails):
    """ClientCallDetails is simply a respecification of grpc.ClientCallDetails. We have to do this
    since the grpc python library does not let us create new grpc.ClientCallDetails objects or
    modify the one passed into an interceptor.

    See https://grpc.github.io/grpc/python/grpc.html#grpc.ClientCallDetails
    """
    pass

GetAttrsArg = Union[Attrs, Callable[[], Attrs]]

class InjectAttrsClientInterceptor(grpc.UnaryUnaryClientInterceptor):
    """InjectAttrsClientInterceptor adds leap-specific attributes to the grpc call metadata."""

    def __init__(self, get_attrs: Optional[GetAttrsArg]=None):
        """Construct this interceptor.

        You may provide a value for `get_attrs` which is either a `leap.omcp.attrs.Attrs`
        object or a lambda which returns a `leap.omcp.attrs.Attrs` object when called; this
        lambda will be called for all grpc call intercepts; it will not be called in advance.
        Any `Attrs` object will have its key:value pairs added to the metadata header for
        that call.
        """
        self.attrs = Attrs()
        self.attrs_fn: Optional[GetAttrsArg] = None
        if get_attrs:
            if isinstance(get_attrs, Attrs):
                self.attrs = get_attrs
            else:
                self.attrs = None
                self.attrs_fn = get_attrs

    def intercept_unary_unary(self, continuation: grpc.Future, call_details: grpc.ClientCallDetails, request):
        attrs: Attrs = self.attrs
        if self.attrs_fn:
            attrs = self.attrs_fn()

        metadata = attrs.to_metadata()
        if call_details.metadata is not None:
            metadata = list(call_details.metadata) + metadata

        new_call_details = ClientCallDetails(
            call_details.method, call_details.timeout, metadata,
            call_details.credentials, call_details.wait_for_ready, call_details.compression)
        return continuation(new_call_details, request)

def _want_insecure() -> bool:
    # if the value of OMCPAPI_GRPC_INSECURE is anything other than [None, "0", "f", "false"],
    # then return True
    return os.environ.get("OMCPAPI_GRPC_INSECURE", "false").lower() not in [None, "0", "f", "false"]

def configure(addr: Optional[str]=None, get_attrs: Optional[GetAttrsArg]=None) -> omcpapi_pb2_grpc.OMCPAPIServiceStub:
    """Configure a omcpapi_pb2_grpc.OMCPAPIServiceStub using the provided address of an omcpapi, grpc server.

    If you do not provide an `addr` arg, configure will default to the value of the
    OMCPAPI_GRPC_ADDR environment variable.

    Set the environment variable `OMCPAPI_GRPC_INSECURE` to a truthy value if the
    connection should be made without any security. Otherwise, we assume we are
    running in a GCP environment and will need proper security handshaking to talk
    to omcpapi.

    See `InjectAttrsClientInterceptor` for more info on the `get_attrs` argument.
    """
    if not addr:
        addr = os.environ.get("OMCPAPI_GRPC_ADDR")
    insecure = _want_insecure()
    audience = "https://{}".format(addr.split(":")[0])
    logging.info("connecting to omcpapi: addr={}, insecure={}, audience={}".format(addr, insecure, audience))
    if insecure:
        grpc_chan = grpc.insecure_channel(addr)
    else:
        # gcp support; uses metadata server if available, otherwise looks for
        # service-account JSON file at path defined by GOOGLE_APPLICATION_CREDENTIALS
        # env var.
        creds = id_token.fetch_id_token_credentials(audience=audience)

        grpc_chan = google.auth.transport.grpc.secure_authorized_channel(
            credentials=creds,
            target=addr,
            # get an HTTP request function to refresh credentials.
            request=google.auth.transport.requests.Request(),
            ssl_credentials=grpc.ssl_channel_credentials())

    return omcpapi_pb2_grpc.OMCPAPIServiceStub(grpc.intercept_channel(grpc_chan, *[
        InjectAttrsClientInterceptor(get_attrs=get_attrs)
    ]))
