import abc

from leap.adapters import db


class Service(abc.ABC):
    """Abstract class defining all methods required for a Service."""

    @property
    @abc.abstractmethod
    def id(self) -> str:
        """Returns the unique id of this Service."""
        pass

    @property
    @abc.abstractmethod
    def adapter(self) -> db.AdapterConfig:
        """Returns the Adapter bound to this Service."""
        pass

    @property
    @abc.abstractmethod
    def instance_id(self) -> str:
        """Returns the instance-id of the Adapter.

        For example, this would be the Shopify shop name.
        """
        pass

    @property
    @abc.abstractmethod
    def config(self) -> dict:
        """Returns the service-specific configuration settings for the Adapter."""
        pass

    @abc.abstractmethod
    def request(self, method: str, uri: str, payload: dict={}, params: dict={}) -> db.EventAction:
        """Execute an HTTP request against the service.

        ...
        """
        pass

class BaseService(Service):
    def __init__(self, adapter: db.AdapterConfig):
        self._adapter = adapter

    @property
    def id(self) -> str:
        """Returns the unique id of this Service."""
        return self.adapter.service_id

    @property
    def adapter(self) -> db.AdapterConfig:
        """Returns the Adapter bound to this Service."""
        return self._adapter

    @property
    def instance_id(self) -> str:
        """Returns the instance-id of the Adapter.

        For example, this would be the Shopify shop name.
        """
        return self._adapter.instance_id

    @property
    def config(self) -> dict:
        """Returns the service-specific configuration settings for the Adapter."""
        return self._adapter.config
