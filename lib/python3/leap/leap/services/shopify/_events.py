from typing import Dict

from leap.adapters import EventType

# Shopify events/topics that our platform understands

# these are setup at the app level
AppLevelTopicMap: Dict[str, EventType] = {
    "customers/data_request": EventType.CUSTOMERS_DATA_REQUEST,
    "customers/redact": EventType.CUSTOMERS_REDACT,
    "shop/redact": EventType.SHOP_REDACT
}

# the rest are setup at the adapter level
RequiredTopicMap: Dict[str, EventType] = {
    "app/uninstalled": EventType.DECOM,
}

TopicMap: Dict[str, EventType] =  RequiredTopicMap | {
    "orders/create": EventType.ORDER_CREATE,
    "orders/updated": EventType.ORDER_UPDATE,
    "orders/edited": EventType.ORDER_UPDATE_LI,
    "orders/paid": EventType.ORDER_PAYMENT,
    "orders/cancelled": EventType.ORDER_CANCEL,
    "orders/delete": EventType.ORDER_DELETE,

    "refunds/create": EventType.REFUND_CREATE,

    "fulfillments/create": EventType.FULFILLMENT_CREATE,
    "fulfillments/update": EventType.FULFILLMENT_UPDATE,

    "products/create": EventType.PRODUCT_CREATE,
    "products/update": EventType.PRODUCT_UPDATE,
    "products/delete": EventType.PRODUCT_DELETE,

    "inventory_items/delete": EventType.SKU_DELETE,
    "inventory_levels/connect": EventType.INVENTORY_CONNECT,
    "inventory_levels/disconnect": EventType.INVENTORY_DISCONNECT,
    "inventory_levels/update": EventType.INVENTORY_ADJUST,
}

# reverse map of topics
EventTypeMap: Dict[EventType, str] = {v: k for k, v in TopicMap.items()}
RequiredEventTypeMap: Dict[EventType, str] = {v: k for k, v in RequiredTopicMap.items()}
