import re
from typing import List, Optional
from urllib.parse import parse_qs, urlparse

import requests

import leap.errors
from leap.adapters import db
from leap.services import BaseService

API_VERSION = "2022-04"

DEFAULT_RETRY_AFTER: float = 0.5
RETRY_AFTER_HEADER = "Retry-After"
API_CALL_LIMIT_HEADER = "X-Shopify-Shop-Api-Call-Limit"

_REQUIRED_PRIVATE_APP_ATTRS: List[str] = [
    "username", "password", "verification_key"]
_REQUIRED_PUBLIC_APP_ATTRS: List[str] = ["access_token"]


def link_header_next_page(link_header: Optional[str]) -> Optional[str]:
    # @see https://help.shopify.com/en/api/guides/paginated-rest-results
    if not link_header:
        return None

    next_page: Optional[str] = None
    link_match = re.search(r'<([^>]+)>;\s*rel="?next"?', link_header)
    if link_match and len(link_match.groups()) > 0:
        # grab only the first url matched
        next_url = urlparse(link_match.groups()[0])
        page_info_param = parse_qs(next_url.query).get("page_info")
        if page_info_param:
            # grab only the first page_info value
            next_page = page_info_param[0]

    return next_page


class ShopifyService(BaseService):
    def __init__(self, adapter: db.AdapterConfig):
        super().__init__(adapter)
        self._api_version = API_VERSION

        # allow override of default host
        self._host = self.config.get(
            "host", "{0}.myshopify.com".format(self.instance_id))
        self._base_url = "https://{0}/admin/api/{1}".format(
            self._host, self.api_version)

        # prefer public app via access-token, but also support private app with username:password
        if self.is_private_app:
            self._validate_app_config("private", _REQUIRED_PRIVATE_APP_ATTRS)
        else:
            self._validate_app_config("public", _REQUIRED_PUBLIC_APP_ATTRS)

    def _validate_app_config(self, scope: str, attrs: List[str]):
        missing: List[str] = []
        for name in attrs:
            if not self.config.get(name):
                missing.append(name)
        if missing:
            raise leap.errors.services.InvalidConfigError(
                (self.id, self.adapter.id), "{0} app missing: {1}".format(scope, ", ".join(missing)))

    @property
    def api_version(self) -> str:
        return self._api_version

    @property
    def host(self) -> str:
        return self._host

    @property
    def base_url(self) -> str:
        return self._base_url

    @property
    def username(self) -> Optional[str]:
        return self.config.get("username")

    @property
    def password(self) -> Optional[str]:
        return self.config.get("password")

    @property
    def access_token(self) -> Optional[str]:
        return self.config.get("access_token")

    @property
    def is_private_app(self) -> bool:
        return self.config.get("private_app", False)

    def request(self, method: str, uri: str, payload: dict = {}, params: dict = {}) -> db.EventAction:
        action = db.EventAction(self.adapter.id)
        url = self.base_url + uri + ".json"

        action.request.update({
            "method": method, "url": url, "params": params, "payload": payload,
        })

        headers: dict = {"Content-Type": "application/json"}
        req_kwargs: dict = {"headers": headers}

        if params:
            req_kwargs.update({"params": params})

        if payload:
            req_kwargs.update({"json": payload})

        if self.is_private_app:
            req_kwargs.update({"auth": (self.username, self.password)})
        else:
            headers.update({"X-Shopify-Access-Token": self.access_token})

        res = requests.request(method, url, **req_kwargs)

        # we're getting rate limited
        if res.status_code == 429:
            retry_after = DEFAULT_RETRY_AFTER
            if RETRY_AFTER_HEADER in res.headers:
                retry_after = float(res.headers[RETRY_AFTER_HEADER])
            rate = res.headers.get(API_CALL_LIMIT_HEADER)
            raise leap.errors.services.TooManyRequests(
                (self.id, self.adapter.id), retry_after, uri, rate)

        action.response.update({
            "status_code": res.status_code,
            "headers": dict(res.headers),
            "payload": res.json(),
            "page_info": link_header_next_page(res.headers.get("Link")),
        })

        return action
