import logging
from typing import List, Optional

import leap.errors
from leap.adapters import db
from leap.services.shopify import ShopifyService
from leap.services.shopify._events import EventType, EventTypeMap
from leap.std.retry import with_retry

WEBHOOK_URL_FMT = "https://{event_host}/v{omcp_version}?aid={adapter_id}&bid={brand_id}&bh={brand_handle}&est={service_type}&amt={merchant_type}"
WEBHOOK_URL_SIG_CHECK_FMT = WEBHOOK_URL_FMT + "&SIG={sig_check}"


def determine_sig_check(db: db.DB, adapter: db.AdapterConfig, bypass_override: bool) -> Optional[str]:
    service = db.query_service(adapter.service_id)
    if not service:
        raise leap.errors.UnresolvableError(f"service '{adapter.service_id}' not found")

    if service.has_sig_check_override:
        return service.config.get("sig_check")
    if adapter.has_sig_check_override:
        return adapter.config.get("sig_check")
    if bypass_override:
        return db.SigCheckType.BYPASS
    return None


def install_webhooks(db: db.DB,
                     srv: ShopifyService,
                     brand: db.Brand,
                     event_host: str,
                     evtypes: List[str],
                     reinstall_hooks: bool = True,
                     dry_run: bool = False,
                     bypass_sig_check: bool = False):

    sig_check_value = determine_sig_check(db, srv.adapter, bypass_sig_check)
    for evtype in evtypes:
        hook = db.adapter_webhook(adapter_id=srv.adapter.id, event_type=evtype)
        if hook:
            if reinstall_hooks:
                logging.info({"message": "deleting webhook", "data": hook.to_dict()})
                if not dry_run:
                    delete_shopify_webhook(srv, hook)
                    db.delete_adapter_webhook(hook)
            else:
                raise leap.errors.UnresolvableError(f"AdapterWebhook(id={hook.id}) already installed")

        logging.info({
            "message": "installing webhook",
            "data": {
                "adapter_id": srv.adapter.id,
                "brand_id": brand.id,
                "brand_handle": brand.handle,
                "event_type": evtype,
                "host": event_host,
                "omcp_version": brand.config.omcp_version,
                "event_service_type": srv.adapter.service_type,
                "merchant_type": srv.adapter.merchant_type,
                "sig_check": sig_check_value,
                "service_id": srv.adapter.service_id
            }
        })
        if not dry_run:
            db.save_adapter_webhook(install_shopify_webhook(srv, brand, event_host, evtype, sig_check_value))


def install_shopify_webhook(svc: ShopifyService,
                            brand: db.Brand,
                            event_host: str,
                            evtype: EventType,
                            sig_check: str = None) -> db.AdapterWebhook:
    topic = EventTypeMap[evtype]
    if sig_check:
        webhook_url = WEBHOOK_URL_SIG_CHECK_FMT.format(event_host=event_host,
                                                       omcp_version=brand.config.omcp_version,
                                                       adapter_id=svc.adapter.id,
                                                       brand_id=brand.id,
                                                       brand_handle=brand.handle,
                                                       service_type=svc.adapter.service_type,
                                                       merchant_type=svc.adapter.merchant_type,
                                                       sig_check=sig_check)
    else:
        webhook_url = WEBHOOK_URL_FMT.format(event_host=event_host,
                                             omcp_version=brand.config.omcp_version,
                                             adapter_id=svc.adapter.id,
                                             brand_id=brand.id,
                                             brand_handle=brand.handle,
                                             service_type=svc.adapter.service_type,
                                             merchant_type=svc.adapter.merchant_type)
    req_payload = {
        "address": webhook_url,
        "topic": topic,
        "format": "json",
    }

    action: db.EventAction = with_retry(lambda: svc.request("POST", "/webhooks", {"webhook": req_payload}))
    if not action.success:
        raise leap.errors.UnresolvableError(
            f"({action.status_code}) unable to install '{evtype}' webhook Adapter(id={svc.adapter.id}) {action.response}"
        )
    webhook_data = action.response.get("payload", {}).get("webhook")
    return db.AdapterWebhook(adapter_id=svc.adapter.id,
                             event_type=evtype,
                             adapter_event_type=topic,
                             adapter_webhook_id=str(webhook_data.get("id")),
                             omcp_version=brand.config.omcp_version)


def delete_shopify_webhook(svc: ShopifyService, hook: db.AdapterWebhook):
    action: db.EventAction = with_retry(lambda: svc.request("DELETE", f"/webhooks/{hook.adapter_webhook_id}"))
    if not (action.success or action.status_code == 404):
        raise leap.errors.UnresolvableError(
            f"({action.status_code}) unable to uninstall AdapterWebhook(id={hook.id}) {action.response}")
