"""argparse extensions
"""

import argparse

class ActionAddNoFlag(argparse.Action):
    """Allows a "no" prefix to disable store_true actions.

    For example, --debug will have an additional --no-debug to explicitly disable it.

    @see https://gist.github.com/thorsummoner/9850b5d6cd5e6bb5a3b9b7792b69b0a5
    """
    def __init__(self, opt_name: str, dest=None, default=True, required=False, help=None):
        options = ["--" + opt_name, "--no-" + opt_name]
        super().__init__(options,
            dest=(opt_name.replace("-", "_") if dest is None else dest),
            nargs=0, const=None, default=default, required=required, help=help)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, not option_string.startswith("--no-"))

class ActionAddNoFlagFormatter(argparse.HelpFormatter):
    """Format flags with a --no-... option.

    Assumes that the options are explicitly ordered as `["--longname", "--no-longname", ...].

    This changes the --help output from what is originally this:
        --file, --no-file, -f
    to this:
        --[no-]file, -f

    @see https://gist.github.com/thorsummoner/9850b5d6cd5e6bb5a3b9b7792b69b0a5
    """

    def _format_action_invocation(self, action):
        if len(action.option_strings) > 1 and action.option_strings[1].startswith("--no-"):
            options = ["--[no-]" + action.option_strings[0][2:]]
            options.extend(action.option_strings[2:])
            return ", ".join(options)
        return super()._format_action_invocation(action)

class ArgumentParser(argparse.ArgumentParser):
    """Standard argparse.ArgumentParser, but with support for ActionAddNoFlag."""

    def __init__(self, **kwargs):
        if not kwargs.get("formatter_class"):
            kwargs["formatter_class"]=ActionAddNoFlagFormatter
        super().__init__(**kwargs)

    def add_no_flag(self, opt_name: str, dest=None, default=True, required=False, help=None):
        super()._add_action(ActionAddNoFlag(opt_name, dest=dest, default=default, required=required, help=help))
