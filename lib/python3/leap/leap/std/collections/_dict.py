"""Dictionary helpers."""

from typing import Any, List, Tuple, Union

SimpleKey = Any
KeyFilterTuple = Tuple[SimpleKey, List[SimpleKey]]
KeyFilter = List[Union[KeyFilterTuple, SimpleKey]]


def _dict_key_subfilter(src: dict, filter: KeyFilter, fill=False) -> Any:
    if isinstance(src, dict):
        return dict_key_filter(src, filter, fill)
    elif isinstance(src, list):
        return [dict_key_filter(d, filter, fill) for d in src]
    return src


def dict_key_filter(src: dict, filter: KeyFilter, fill=False) -> dict:
    """Copy src dict, but perform a deep and inclusive filter on KV pairs.

    Create a copy of the provided src dict, but using a filter template, filter out
    KV pairs from the dst dict. This can even be applied to nested objects.

    The simplest filter uses the `filter` argument as a list of keys that should
    exist in the src dict. Those keys that do exist in the src dict will have
    their values copied (by reference) into the dst dict (including None values).
    The src dict remains unchanged. If a key does not exist in src, it will also
    not exist in the dst dict unless `fill` is True.

    Example:
        >>> d1 = {"a": 1, "b": 2, "c": 3}
        >>> dict_key_filter(d1, ["b", "c"])
        {"b": 2, "c": 3}

    dict_key_filter can also filter KV pairs in dicts which are values of selected
    filter; regardless of nesting. This does require understanding the "schema" of the
    src dict. To define a filter for a dict value assigned to a key, you add
    a tuple to the `filter` argument of the form `(key, list[sub-filter-to-filter])`.
    The `sub-filter-to-filter` list could also contain another tuple of the same;
    thus supporting deeply nested filtering.

    An additional note, sub-filtering will work against a dict and a list of dicts.

    Example:
        >>> key_filter = ["b", ("c", ["e", "f"])]
        >>> d1 = {"a": 1, "b": 2, "c": {"d": 4, "e": 5, "f": 6}}
        >>> dict_key_filter(d1, key_filter)
        {"b": 2, "c": {"e": 5, "f": 6}}
        >>> d2 = {"a": 1, "b": 2, "c": [
            {"d": 41, "e": 51, "f": 61}, {"d": 42, "e": 52, "f": 62}
        ]}
        >>> dict_key_filter(d1, key_filter)
        {"b": 2, "c": [{"e": 51, "f": 61}, {"e": 52, "f": 62}]}
    """
    dst = {}
    for k in filter:
        if isinstance(k, tuple):
            sub_k, sub_f = k
            if sub_k in src:
                dst[sub_k] = _dict_key_subfilter(src[sub_k], sub_f, fill)
        elif k in src:
            dst[k] = src[k]
        elif fill:
            dst[k] = None
    return dst


def dict_exclude_keys(d: dict, keys: List[Any]) -> dict:
    """Returns a new dict without k/v pairs whose key is in the provided `keys`."""
    return {k: v for k, v in d.items() if k not in keys}


def dict_diff(a: dict, b: dict) -> dict:
    """Returns any key/value pair of `b` that does NOT equal the same key/value pair in `a`"""
    # TODO figure out if it matters whether there are items in `a` that are not in `b` at all
    return {k: v for k, v in set(b.items())-set(a.items())}
