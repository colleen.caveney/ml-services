from typing import Iterator, Optional, TypeVar

T = TypeVar('T')


def iter_first(iter: Iterator[T]) -> Optional[T]:
    for el in iter:
        return el
    return None
