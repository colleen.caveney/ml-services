"""List helpers."""

def list_sub(a: list, b: list) -> list:
    """Substract elements in list(b) from list(a)."""
    return [e for e in a if e not in b]
