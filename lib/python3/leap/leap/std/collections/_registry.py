from typing import Dict, Generic, Optional, TypeVar

T = TypeVar('T')

class Registry(Generic[T]):
    def __init__(self):
        self._registry: Dict[str, T] = {}

    def register(self, name: str, value: T):
        self._registry[name] = value

    def unregister(self, name: str) -> Optional[T]:
        if name in self._registry:
            return self._registry.pop(name)
        return None

    def lookup(self, name: str) -> Optional[T]:
        return self._registry.get(name)
