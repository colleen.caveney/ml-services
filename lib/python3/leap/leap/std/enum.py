from enum import Enum
from typing import List, TypeVar, cast

T = TypeVar('T')


class TypedEnum(Enum):
    """TypedEnum represents an Enum whose elements are all of type T.

    The best way to use this class is to subclass it and extend the type you are interested
    in. Example:

        class FooEnum(str, TypedEnum):
            Bar = "bar"
            Baz = "boo"

    The main benefit of this class is the addition of helper methods such as: `names`,
    `values`, and `has_value`.
    """

    @classmethod
    def names(cls) -> List[str]:
        """names returns a List[str] of the names of each member in the enum."""
        return cast(List[str], cls._member_names_)

    @classmethod
    def values(cls) -> List[T]:
        """values returns a List[T] of the values (T) of each member."""
        return [e.value for e in cls]

    @classmethod
    def has_value(cls, val: T) -> bool:
        """has_value returns True if `val` is a value of one of the members."""
        return val in cls.values()

    def __str__(self) -> str:
        return self.value
