"""Leap Standard exceptions.

...
"""

class UndefinedEnvError(Exception):
    """Exception raised whenever an env-var is unexpectedly empty or undefined."""

    def __init__(self, env: str):
        super().__init__(env)
        self.env = env
