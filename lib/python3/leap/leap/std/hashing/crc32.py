import binascii

def hash_object(**kwargs) -> str:
    """Given a dict (`kwargs`) return a hex formatted string of the crc32 checksum of its bytes.

    K/V pairs are converted to a `K=V` format separated by commas. Both K and V should castable
    to str representations. The keys will first be sorted before generating the code.

    Example:

        from leap.std.hashing import crc32
        crc32.hash_object(state="il", city="chicago")
        => "a2c595e6"

    Under the hood, the string which gets encoded to bytes and then checksummed will look like:
    "city=chicago,state=il".

    There is no guarantee that nested dicts/objects will work correctly. You would typically use
    this with a flat dict.
    """
    sorted_keys = sorted(kwargs)
    key = ",".join(["{0}={1}".format(str(k), str(kwargs[k])) for k in sorted_keys])
    return hex(binascii.crc32(key.encode()))[2:] # removes the '0x' prefix
