import hashlib
from typing import Any, Optional


def _sortkey_to_str(k: Any) -> str:
    return str(k)

def hash_object(obj: Optional[Any]) -> str:
    """Creates an MD5 hash of a provided object.

    If object is a dict or a set, the collection will be sorted by the str cast of
    their keys or values respectively. This should ensure consistency.
    """
    tmp_s = str(obj)
    if isinstance(obj, dict):
        tmp_s = str(sorted(obj.items(), key=_sortkey_to_str))
    elif isinstance(obj, set):
        tmp_s = str(sorted(obj, key=_sortkey_to_str))

    m = hashlib.md5()
    m.update(tmp_s.encode("utf-8"))
    return m.hexdigest()
