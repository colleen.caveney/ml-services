import abc
import json
from datetime import datetime
from typing import cast


class Serializable(abc.ABC):

    @abc.abstractmethod
    def to_json(self) -> str:
        raise NotImplementedError

    @abc.abstractmethod
    def to_object(self) -> object:
        raise NotImplementedError


def _json_type_cast(o: object) -> object:
    if isinstance(o, Serializable):
        return cast(Serializable, o).to_object()
    elif isinstance(o, datetime):
        return cast(datetime, o).isoformat()
    return o


def dumps(obj: dict, **kwargs) -> str:
    """JSON-encode object to string using common Leap type-coercions.

    Relies entirely on the built-in `json.dumps`, but provides for common
    type-coercions. Currently supported coercions:

    - Serializable -> result of calling `to_object` on object; resulting object
                      should also be serializable
    - datetime.datetime -> ISO formatted datetime string

    Args:
        obj (dict): Any dict that can be transformed into json.

        **kwargs (dict): Any keyword argument json.dumps allows.
    Returns:
        A string of serialized JSON.
    """
    return json.dumps(obj, default=_json_type_cast, **kwargs)


def loads(obj: str, **kwargs):
    return json.loads(obj, **kwargs)


def dump_compact(obj: dict) -> str:
    """JSON-encode object to string and eliminate extraneous whitespace.

    Args:
        obj (dict): Any dict that can be transformed into json.
    Returns:
        A string of serialized JSON.
    """
    return dumps(obj, separators=(',', ':'))
