import logging as pylog
from datetime import datetime
from types import TracebackType
from typing import List, Optional

from pythonjsonlogger import jsonlogger

from leap.ctx import attrs


def epoch_ms() -> int:
    return int(datetime.now().timestamp() * 1000)


class LogEntry:
    def __init__(self, type: str, category: str, attrs: attrs.ContextAttrs, message: Optional[str] = None, data: Optional[dict] = {}):
        self.type = type
        self.category = category
        self.attrs = attrs
        self.ts = epoch_ms()
        self.latency_ms = 0
        self.message = message
        self.data = data or {}

    def capture_latency(self) -> int:
        """Record millis passed since the ts of this LogEntry"""
        millis = epoch_ms() - self.ts
        self.latency_ms = millis
        return millis

    def update(self, category: str, message: str, data: Optional[dict] = {}) -> "LogEntry":
        """Updates category, message, and data in one call.

        Note, data is updated (not replaced) using the standard dict.update method.
        """
        self.category = category
        self.message = message
        self.data.update(data)

    def to_dict(self) -> dict:
        return vars(self)


class JSONFormatter(jsonlogger.JsonFormatter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def process_log_record(self, log_record):
        # set severity to levelname; this is what gcp error-logging will look at
        log_record['severity'] = log_record.pop('levelname')
        log_record['ts'] = log_record.pop('asctime')
        return super().process_log_record(log_record)


def collect_tracebacks(tb: Optional[TracebackType]) -> List[dict]:
    tb_stack: List[dict] = []
    while tb:
        f_code = tb.tb_frame.f_code
        tb_stack.append({"file": f_code.co_filename,
                        "code": f_code.co_name, "line": tb.tb_lineno})
        tb = tb.tb_next
    return tb_stack


def submit(entry: LogEntry, capture_latency: bool = True):
    if capture_latency:
        entry.capture_latency()
    d = entry.to_dict()
    pylog.error(d) if entry.category == "error" else pylog.info(d)


def exception(err: Exception, tb: Optional[TracebackType], msg: dict = {}) -> dict:
    """Updates the provided message with a standardized 'error' field and logs as an ERROR.

    Collects tracebacks and notes the class-name for the exception along with the actual error message.

    Returns the logged message.
    """
    msg.update({
        "error": {
            "class": err.__class__.__name__,
            "message": str(err),
            "stack": collect_tracebacks(tb),
        },
    })
    pylog.error(msg)
    return msg
