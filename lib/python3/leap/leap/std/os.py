"""Leap Standard os extensions.

...
"""

import os
from typing import Optional

from . import exceptions


def require_env(name: str) -> str:
    """Raise an exception if env-var is undefined or empty, else return the value.

    Args:
        name: The name of the env-var to get from os.environ.
    Returns:
        A string containing the value of the env-var. The string will not be None
        and will not be zero-length.
    Raises:
        leap.std.exceptions.UndefinedEnvError
    """
    val = os.environ.get(name)
    if val:
        return val
    raise exceptions.UndefinedEnvError(name)

def pop_env(name: str, default: Optional[str]) -> Optional[str]:
    """Removes an item from the ENV gracefully and return the value.

    Args:
        name: The name of the env-var to get from os.environ.
    Returns:
        A string containing the value of the env-var or None if the env-var is
        not defined
    """
    if os.environ.get(name, default):
        return os.environ.pop(name)
    return None
