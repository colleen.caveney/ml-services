import time
from typing import Any, Callable, Optional

import leap.errors


def with_retry(fn: Callable[..., Any], n: int = 10) -> Any:
    """
    Execute a function until it returns without raising a leap.errors.RetryError, up to N times.

    When a leap.errors.RetryError is caught, this function will sleep for the value of
    :data:`retry_after` provided by the error.

    If the maximum number of attempts has been reached, the last seen leap.errors.RetryError
    will be raised. No other exceptions are caught; they pass through.

    If `n < 1`, no attempts will be made; i.e. `n == 0` does not imply infinite attempts. In this
    case, None is returned.

    Args:
        fn (Callable): the function to call

        n (int, default=10): max number of attempts
    Returns:
        Anything fn returns.
    """
    if n < 1:
        return None

    last_err: Optional[Exception] = None
    for _ in range(0, n):
        try:
            return fn()
        except leap.errors.RetryError as err:
            time.sleep(err.retry_after)
            last_err = err
    raise last_err
