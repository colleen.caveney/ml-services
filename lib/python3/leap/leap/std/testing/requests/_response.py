import json

class Response:
    """Response is meant to be used as a mock of requests.Response.

    It provides the bare minimum of properties and functions needed to replicate a
    requests.Response instance.
    """

    def __init__(self, status_code: int, json_data: dict={}, text_data=None, headers={}):
        self._url = None
        self._status_code = status_code
        self._json_data = json_data
        self._text_data = text_data
        self._headers = headers

    @property
    def status_code(self) -> int:
        return self._status_code

    @property
    def headers(self) -> dict:
        return self._headers

    def json(self) -> dict:
        return self._json_data

    @property
    def text(self) -> str:
        if self._text_data == None:
            return json.dumps(self._json_data)
        return self._text_data

    @property
    def content(self) -> str:
        return self.text.encode("utf-8")

    @property
    def ok(self) -> bool: # pylint: disable=invalid-name
        return self._status_code < 400

    def raise_for_status(self):
        if self._status_code >= 400:
            raise Exception(f"Status code: {self.status_code}")
