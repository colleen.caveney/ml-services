"""Leap time, date, clock, etc. support."""

import threading
from datetime import datetime, timedelta
from typing import Callable, Iterator

DatetimeGen = Iterator[datetime]
DatetimeFactory = Callable[[], datetime]
TimestampGen = Iterator[float]
TimestampFactory = Callable[[], float]

EPOCH = datetime(1970, 1, 1, 0, 0, 0)

def seq_gen(start: datetime=EPOCH, step: int=1) -> DatetimeGen:
    """Datetime generator that creates datetimes in increasing steps of seconds. Not thread safe.

    Given a starting datetime (defaults to epoch), this generator will create new datetimes by
    incrementing the starting datetime by `step` seconds (defaults to 1) for each call to
    `next()`. This is useful for testing as you can control what the next, generated datetime
    will be.

    Example:
        dtiter = seq_gen()
        str(next(dtiter))
        => '1970-01-01 00:00:00'
        str(next(dtiter))
        => '1970-01-01 00:00:01'

        dtiter = seq_gen(datetime(1984, 6, 15, 12, 30, 0), 10)
        str(next(dtiter))
        => '1984-06-15 12:30:00'
        str(next(dtiter))
        => '1984-06-15 12:30:10'
    """
    dtcur: datetime = start
    while True:
        yield dtcur
        dtcur += timedelta(seconds=step)

def seq_factory(start: datetime=EPOCH, step: int=1) -> DatetimeFactory:
    gen = seq_gen(start, step)
    lock = threading.Lock()
    def _factory_next() -> datetime:
        with lock:
            return next(gen)
    return _factory_next

def datetime_gen(utc: bool=False) -> DatetimeGen:
    """Datetime generator. Not thread safe.

    This generator will generate datetimes at the current time for each call to next(). This
    is equivalent to calling datetime.now() repeatedly.

    If the `utc` argument is set to True then datetime.utcnow() will be used instead.

    Example:
        dtiter = datetime_gen()
        next(dtiter)
        => '2019-08-22 13:03:23.860113'
        next(dtiter)
        => '2019-08-22 13:03:26.771900'
    """
    while True:
        yield datetime.utcnow() if utc else datetime.now()

def datetime_factory(utc: bool=False) -> DatetimeFactory:
    gen = datetime_gen(utc)
    lock = threading.Lock()
    def _factory_next() -> datetime:
        with lock:
            return next(gen)
    return _factory_next

def timestamp_gen(utc: bool=False) -> TimestampGen:
    """Timestamp generator. Not thread safe.

    This generator will generate timestamps at the current time for each call to next(). This
    is equivalent to calling datetime.now().timestamp() repeatedly.

    If the `utc` argument is set to True then datetime.utcnow() will be used instead.

    Example:
        tsiter = timestamp_gen()
        next(tsiter)
        => 1596733126.806253
        next(tsiter)
        => 1596733130.406376
    """
    dtgen = datetime_gen()
    while True:
        yield next(dtgen).timestamp()

def timestamp_factory(utc: bool=False) -> TimestampFactory:
    gen = timestamp_gen(utc)
    lock = threading.Lock()
    def _factory_next() -> float:
        with lock:
            return next(gen)
    return _factory_next
