"""Leap UUID support."""

import threading
import uuid
from typing import Callable, Iterator

UUIDGen = Iterator[uuid.UUID]
UUIDFactory = Callable[[], uuid.UUID]

def seq_gen(start_idx: int=0) -> UUIDGen:
    """UUID generator that creates UUIDs in sequence.

    Given a starting index (defaults to zero), this generator will create new UUIDs by
    incrementing the starting index value for each call to `next()`. This is useful for
    testing as you can control what the next, generated UUID will be.

    Example:
        uuiditer = seq_gen()
        next(uuiditer)
        => UUID(00000000-0000-0000-0000-000000000000)
        next(uuiditer)
        => UUID(00000000-0000-0000-0000-000000000001)

        uuiditer = seq_gen(int(1e38))
        next(uuiditer)
        => UUID(4b3b4ca8-5a86-c400-0000-000000000000)
        next(uuiditer)
        => UUID(4b3b4ca8-5a86-c400-0000-000000000001)
    """
    idx = start_idx
    while True:
        yield uuid.UUID(int=idx)
        idx = (idx+1) % int(1<<128)

def seq_factory(start_idx: int=0) -> UUIDFactory:
    gen = seq_gen(start_idx)
    lock = threading.Lock()
    def _seq_factory_next() -> uuid.UUID:
        with lock:
            return next(gen)
    return _seq_factory_next

def uuid4_gen() -> UUIDGen:
    """UUIDv4 generator.

    This generator will generate UUIDv4 uuids for each call to next().

    Example:
        uuiditer = uuid4_gen()
        next(uuiditer)
        => UUID('9a5553a4-26ad-4a85-90ff-a732c7da806c')
        next(uuiditer)
        => UUID('f2ade76c-be86-4ae0-8189-03810a7897cc')
    """
    while True:
        yield uuid.uuid4()

def uuid4_factory() -> UUIDFactory:
    gen = uuid4_gen()
    lock = threading.Lock()
    def _uuidv4_factory_next() -> uuid.UUID:
        with lock:
            return next(gen)
    return _uuidv4_factory_next
