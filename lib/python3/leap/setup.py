# pylint: disable=missing-docstring

import setuptools

# Only include packages under the 'leap' namespace. Do not include tests,
# benchmarks, etc.
PACKAGES = [
    package for package in setuptools.find_packages() if package.startswith("leap")
]

README = open('README.md').read()

setuptools.setup(
    name='leap',
    version='0.0.2',
    description='Python standard library for Leap.',
    long_description=README,
    author='Gus',
    author_email='dev@leapinc.co',
    url='https://gitlab.com/leapinc/data/tree/main/lib/python3/leap',
    packages=PACKAGES,
    install_requires=[
        "flask",
        "google-api-core>=1.16.0, < 3.0dev",
        "google-cloud-core>=1.3, < 3.0dev",
        "google-cloud-firestore>=1.7.0, < 3.0dev",
        "google-cloud-pubsub>=1.5.0, < 3.0dev",
        "psycopg2",
        "pugsql",
        "python-json-logger",
        "dacite",
        "faker",
        "python-dateutil",
        "pydantic",
        # hardcoding full commit ref as installers of our package will only see said
        # commit ref
        "omcpapi @ git+ssh://git@gitlab.com/leapinc/platform-omcp.git@b312d8b76fb2aeab104011d4787a62a35299bcbf#egg=omcpapi&subdirectory=gen/proto/python"
    ],
    classifiers=[
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.7',
        "Operating System :: OS Independent",
    ]
)
