import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreActions:
    @pytest.mark.parametrize("tc", [
        ({}, "act0"),
        ({"action_type": db.ActionType.NEW_BRAND}, "act0"),
        ({"action_type": "not a type"}, None),
    ])
    def test_single_action(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("actions/act0", {"id": "act0", "client_id": "abc123", "matched_subscription_ids": [],
            "action_type": db.ActionType.NEW_BRAND, "message": {}})
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.action(**filters)
        assert expected_id == (actual.id if expected_id else None)

    @pytest.mark.parametrize("tc", [
        (0, {}, ["act0", "act1", "act2"]),
        (2, {}, ["act0", "act1"]),
        (0, {"action_type": db.ActionType.NEW_BRAND}, ["act0", "act1", "act2"]),
        (0, {"id": "act1"}, ["act1"]),
    ])
    def test_actions(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("actions/act0", {"id": "act0", "client_id": "abc123", "matched_subscription_ids": [],
            "action_type": db.ActionType.NEW_BRAND, "message": {}})
        db_client.store_document("actions/act1", {"id": "act1", "client_id": "xyz456", "matched_subscription_ids": [],
            "action_type": db.ActionType.NEW_BRAND, "message": {}})
        db_client.store_document("actions/act2", {"id": "act2", "client_id": "999999", "matched_subscription_ids": [],
            "action_type": db.ActionType.NEW_BRAND, "message": {}})
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.actions(limit, **filters)]

    ## saving

    def test_save_action(self):
        db_client = fstests.Client()
        action = db.Action(db.ActionType.NEW_BRAND, [], "act0", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_action(action)

        ref = db_client.document("actions/{0}".format(action.id))
        assert dataclasses.asdict(action) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
