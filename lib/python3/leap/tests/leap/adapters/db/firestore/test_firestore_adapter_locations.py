import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreDB_AdapterLocations:
    @pytest.mark.parametrize("tc", [
        (0, {}, ["aloc0", "aloc1", "aloc2"]),
        (2, {}, ["aloc0", "aloc1"]),
        (0, {"enabled": True}, ["aloc0", "aloc2"]),
        (1, {"enabled": True}, ["aloc0"]),
        (0, {"adapter_id": "a0"}, ["aloc0", "aloc1"]),
        # testing 'in` operator
        (0, {"store_id": {"s0", "s2"}}, ["aloc0", "aloc2"]),
    ])
    def test_adapter_locations(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("adapter_locations/aloc0", {
            "id": "aloc0", "adapter_id": "a0", "store_id": "s0", "adapter_location_id": "12345-0", "enabled": True
        })
        db_client.store_document("adapter_locations/aloc1", {
            "id": "aloc1", "adapter_id": "a0", "store_id": "s1", "adapter_location_id": "12345-1", "enabled": False
        })
        db_client.store_document("adapter_locations/aloc2", {
            "id": "aloc2", "adapter_id": "a1", "store_id": "s2", "adapter_location_id": "54321-0", "enabled": True
        })
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [
            m.id for m in fsdb.adapter_locations(limit, **filters)]

    @pytest.mark.parametrize("tc", [
        ({}, "aloc0"),
        ({"enabled": False}, "aloc1"),
        ({"enabled": True}, "aloc0"),
        ({"adapter_id": "a0"}, "aloc0"),
        ({"adapter_id": "a3"}, None),
    ])
    def test_single_adapter_location(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("adapter_locations/aloc0", {
            "id": "aloc0", "adapter_id": "a0", "store_id": "s0", "adapter_location_id": "12345-0", "enabled": True
        })
        db_client.store_document("adapter_locations/aloc1", {
            "id": "aloc1", "adapter_id": "a0", "store_id": "s1", "adapter_location_id": "12345-1", "enabled": False
        })
        db_client.store_document("adapter_locations/aloc2", {
            "id": "aloc2", "adapter_id": "a1", "store_id": "s2", "adapter_location_id": "54321-0", "enabled": True
        })
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.adapter_location(**filters)
        assert expected_id == (actual.id if expected_id else None)

    def test_save_adapter_location(self):
        db_client = fstests.Client()
        aloc = db.AdapterLocation(
            "a0", "s0", "12345-0", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_adapter_location(aloc)

        ref = db_client.document("adapter_locations/{0}".format(aloc.id))
        assert dataclasses.asdict(aloc) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
