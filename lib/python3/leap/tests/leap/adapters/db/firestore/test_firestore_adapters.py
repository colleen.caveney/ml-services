import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreDB_Adapters:
    def test_query_adapter_requires_id(self):
        fsdb = db.FirestoreDB(fstests.Client())
        with pytest.raises(ValueError, match="id not provided"):
            fsdb.query_adapter("")

    def test_query_adapter(self):
        db_client = fstests.Client()
        db_client.store_document("adapters/001", {
            "id": "001",
            "client_id": "leap-test",
            "service_id": "echo-service",
            "service_type": "echo-service",
            "instance_id": "test-site-001"
        })

        fsdb = db.FirestoreDB(db_client)
        adapter = fsdb.query_adapter("001")

        assert "001" == adapter.id
        assert "leap-test" == adapter.client_id
        assert "leap-test" == adapter.brand_id
        assert "echo-service" == adapter.service_id
        assert "echo-service" == adapter.service_type
        assert "test-site-001" == adapter.instance_id

    def test_query_adapters_for_client_requires_client(self):
        fsdb = db.FirestoreDB(fstests.Client())
        with pytest.raises(ValueError, match="client_id not provided"):
            fsdb.query_adapters_for_client("")

    def test_query_adapters_for_client(self):
        db_client = fstests.Client()
        db_client.store_document("adapters/001", {
            "id": "001", "client_id": "test-client", "service_id": "echo-service", "service_type": "echo-service", "instance_id": "test-site-002"
        })
        db_client.store_document("adapters/002", {
            "id": "002", "client_id": "test-client", "service_id": "noop-service", "service_type": "noop-service", "instance_id": "test-site-003"
        })

        fsdb = db.FirestoreDB(db_client)
        adapters = fsdb.query_adapters_for_client("test-client")

        assert 2 == len(adapters)

        assert "001" == adapters[0].id
        assert "test-client" == adapters[0].client_id
        assert "test-client" == adapters[0].brand_id
        assert "echo-service" == adapters[0].service_id
        assert "echo-service" == adapters[0].service_type
        assert "test-site-002" == adapters[0].instance_id

        assert "002" == adapters[1].id
        assert "test-client" == adapters[1].client_id
        assert "test-client" == adapters[1].brand_id
        assert "noop-service" == adapters[1].service_id
        assert "noop-service" == adapters[1].service_type
        assert "test-site-003" == adapters[1].instance_id

        test_coll = db_client.collection("adapters")
        assert 1 == len(test_coll.queries)
        assert [("client_id", "==", "test-client")] == test_coll.queries[0].clauses

    def test_query_adapters_for_client_with_additional_conditions(self):
        db_client = fstests.Client()
        db_client.store_document("adapters/001", {
            "id": "001", "client_id": "test-client", "service_id": "echo-service", "service_type": "echo-service", "instance_id": "test-site-002"
        })
        db_client.store_document("adapters/002", {
            "id": "002", "client_id": "test-client", "service_id": "noop-service", "service_type": "noop-service", "instance_id": "test-site-003"
        })

        fsdb = db.FirestoreDB(db_client)
        adapters = fsdb.query_adapters_for_client("test-client", instance_id="test-site-003")

        # test observed query
        test_coll = db_client.collection("adapters")
        assert 1 == len(test_coll.queries)
        assert [
            ("client_id", "==", "test-client"),
            ("instance_id", "==", "test-site-003"),
        ] == test_coll.queries[0].clauses

        # test results
        assert 1 == len(adapters)

        assert "002" == adapters[0].id
        assert "test-client" == adapters[0].client_id
        assert "test-client" == adapters[0].brand_id
        assert "noop-service" == adapters[0].service_id
        assert "noop-service" == adapters[0].service_type
        assert "test-site-003" == adapters[0].instance_id

    def test_save_adapter(self):
        db_client = fstests.Client()
        adapter = db.AdapterConfig(client_id="leap-huge-client", service_id="shopify", service_type="shopify", instance_id="test-site-004", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_adapter(adapter)

        ref = db_client.document("adapters/{0}".format(adapter.id))
        assert dataclasses.asdict(adapter) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
        assert "leap-huge-client" == ref.get().get("client_id")
        assert "leap-huge-client" == ref.get().get("brand_id")

    def test_save_adapter_brand_id(self):
        db_client = fstests.Client()
        adapter = db.AdapterConfig(brand_id="leap-huge-client", service_id="shopify", service_type="shopify", instance_id="test-site-004", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_adapter(adapter)

        ref = db_client.document("adapters/{0}".format(adapter.id))
        assert dataclasses.asdict(adapter) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
        assert "leap-huge-client" == ref.get().get("client_id")
        assert "leap-huge-client" == ref.get().get("brand_id")

    def test_save_adapter_brand_id_and_client_id(self):
        db_client = fstests.Client()
        adapter = db.AdapterConfig(brand_id="leap-huge-client", client_id="leap-huge-client", service_id="shopify", service_type="shopify", instance_id="test-site-004", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_adapter(adapter)

        ref = db_client.document("adapters/{0}".format(adapter.id))
        assert dataclasses.asdict(adapter) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
        assert "leap-huge-client" == ref.get().get("client_id")
        assert "leap-huge-client" == ref.get().get("brand_id")

    def test_save_adapter_brand_id_and_different_client_id(self):
        with pytest.raises(TypeError):
            db.AdapterConfig(brand_id="leap-huge-client", client_id="leap-huge-client2", service_id="shopify", service_type="shopify", instance_id="test-site-004", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))

    def test_save_adapter_no_client_or_brand_id(self):
        with pytest.raises(TypeError):
            db.AdapterConfig(service_id="shopify", service_type="shopify", instance_id="test-site-004", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))


    @pytest.mark.parametrize("tc", [
        (0, {}, ["adapter0", "adapter1", "adapter2"]),
        (2, {}, ["adapter0", "adapter1"]),
        (0, {"internal": True}, ["adapter0", "adapter2"]),
        (1, {"internal": True}, ["adapter0"]),
        (0, {"client_id": "c0"}, ["adapter0", "adapter1"]),
    ])
    def test_adapters(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("adapters/adapter0", {
            "id": "adapter0", "client_id": "c0", "service_id": "s0", "service_type": "s0", "instance_id": "test-site-0", "internal": True
        })
        db_client.store_document("adapters/adapter1", {
            "id": "adapter1", "client_id": "c0", "service_id": "s1", "service_type": "s1", "instance_id": "test-site-1", "internal": False
        })
        db_client.store_document("adapters/adapter2", {
            "id": "adapter2", "client_id": "c1", "service_id": "s2", "service_type": "s2", "instance_id": "test-site-0", "internal": True
        })
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.adapters(limit, **filters)]

    @pytest.mark.parametrize("tc", [
        ({}, "adapter0"),
        ({"internal": True}, "adapter0"),
        ({"internal": False}, "adapter1"),
        ({"client_id": "c0", "service_id": "s1", "service_type": "s1"}, "adapter1"),
        ({"client_id": "c4"}, None),
    ])
    def test_adapter(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("adapters/adapter0", {
            "id": "adapter0", "client_id": "c0", "service_id": "s0", "service_type": "s0", "instance_id": "test-site-0", "internal": True
        })
        db_client.store_document("adapters/adapter1", {
            "id": "adapter1", "client_id": "c0", "service_id": "s1", "service_type": "s1", "instance_id": "test-site-1", "internal": False
        })
        db_client.store_document("adapters/adapter2", {
            "id": "adapter2", "client_id": "c1", "service_id": "s2", "service_type": "s2", "instance_id": "test-site-0", "internal": True
        })
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.adapter(**filters)
        assert expected_id == (actual.id if actual else actual)
