import dataclasses
from datetime import datetime
from typing import Tuple

import pytest

from leap.adapters import EventType, db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreDB_AdapterWebhooks:
    @pytest.mark.parametrize("tc", [
        (0, {}, ["hook0", "hook1", "hook2"]),
        (2, {}, ["hook0", "hook1"]),
        (0, {"adapter_id": "a0"}, ["hook0", "hook1"]),
        # testing 'in` operator
        (0, {"adapter_event_type": {"o/c", "i/c"}}, ["hook0", "hook2"]),
        (0, {"event_type": {EventType.ORDER_UPDATE,
         EventType.INVENTORY_CONNECT}}, ["hook1", "hook2"]),
    ])
    def test_adapter_webhooks(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("adapter_webhooks/hook0", {
            "id": "hook0",
            "adapter_id": "a0",
            "event_type": EventType.ORDER_CREATE,
            "adapter_event_type": "o/c",
            "adapter_webhook_id": "12345-0"
        })
        db_client.store_document("adapter_webhooks/hook1", {
            "id": "hook1",
            "adapter_id": "a0",
            "event_type": EventType.ORDER_UPDATE,
            "adapter_event_type": "o/u",
            "adapter_webhook_id": "12345-1"
        })
        db_client.store_document("adapter_webhooks/hook2", {
            "id": "hook2",
            "adapter_id": "a1",
            "event_type": EventType.INVENTORY_CONNECT,
            "adapter_event_type": "i/c",
            "adapter_webhook_id": "54321-0"
        })
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [
            m.id for m in fsdb.adapter_webhooks(limit, **filters)]

    @pytest.mark.parametrize("tc", [
        ({}, "hook0"),
        ({"adapter_id": "a0"}, "hook0"),
        ({"event_type": EventType.ORDER_UPDATE}, "hook1"),
        ({"event_type": "order:update"}, "hook1"),
        # testing 'in` operator
        ({"adapter_event_type": {"o/c", "i/c"}}, "hook0"),
    ])
    def test_adapter_webhook(self, tc: Tuple[int, dict, list]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("adapter_webhooks/hook0", {
            "id": "hook0",
            "adapter_id": "a0",
            "event_type": EventType.ORDER_CREATE,
            "adapter_event_type": "o/c",
            "adapter_webhook_id": "12345-0"
        })
        db_client.store_document("adapter_webhooks/hook1", {
            "id": "hook1",
            "adapter_id": "a0",
            "event_type": EventType.ORDER_UPDATE,
            "adapter_event_type": "o/u",
            "adapter_webhook_id": "12345-1"
        })
        db_client.store_document("adapter_webhooks/hook2", {
            "id": "hook2",
            "adapter_id": "a1",
            "event_type": EventType.INVENTORY_CONNECT,
            "adapter_event_type": "i/c",
            "adapter_webhook_id": "54321-0"
        })
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.adapter_webhook(**filters)
        assert expected_id == (actual.id if expected_id else None)

    def test_save_adapter_webhook(self):
        db_client = fstests.Client()
        hook = db.AdapterWebhook(
            "a0", EventType.NOOP, "noop", "12345-0", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_adapter_webhook(hook)

        ref = db_client.document("adapter_webhooks/{0}".format(hook.id))
        assert dataclasses.asdict(hook) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10

    def test_delete_adapter_webhook(self):
        db_client = fstests.Client()
        fs_client = db.FirestoreDB(db_client)

        hook = db.AdapterWebhook(
            "a1", EventType.NOOP, "noop", "12345-0", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        fs_client.save_adapter_webhook(hook)

        assert fs_client.adapter_webhook(id=hook.id)
        fs_client.delete_adapter_webhook(hook)
        assert None == fs_client.adapter_webhook(id=hook.id)
