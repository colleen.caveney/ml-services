import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreAuditLogs:
    @pytest.mark.parametrize("tc", [
        ({}, "al0"),
        ({"action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD}, "al0"),
        ({"action_type": "not a type"}, None),
    ])
    def test_single_audit_log(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("audit_logs/al0", {"id": "al0", "properties": {}, "message_id": "999",
            "action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD})
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.audit_log(**filters)
        assert expected_id == (actual.id if expected_id else None)

    @pytest.mark.parametrize("tc", [
        (0, {}, ["al0", "al1", "al2"]),
        (2, {}, ["al0", "al1"]),
        (0, {"action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD}, ["al0", "al1", "al2"]),
        (0, {"id": "al1"}, ["al1"]),
    ])
    def test_audit_logs(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("audit_logs/al0", {"id": "al0", "message_id": "123", "properties": {"client_id": "abc123"},
            "action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD})
        db_client.store_document("audit_logs/al1", {"id": "al1", "message_id": "234", "properties": {"client_id": "xyz456"},
            "action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD})
        db_client.store_document("audit_logs/al2", {"id": "al2", "message_id": "345", "properties": {"client_id": "999999"},
            "action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD})
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.audit_logs(limit, **filters)]

    ## saving

    def test_save_audit_log(self):
        db_client = fstests.Client()
        audit_log = db.AuditLog(db.ActionType.NEW_USER_CHANGE_PASSWORD, "123", {"client_id": "abc123"}, "al0", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_auditlog(audit_log)

        ref = db_client.document("audit_logs/{0}".format(audit_log.id))
        assert dataclasses.asdict(audit_log) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
