import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreDBBrands:
    def test_query_brand_requires_id(self):
        fsdb = db.FirestoreDB(fstests.Client())
        with pytest.raises(ValueError, match="id not provided"):
            fsdb.query_brand("")

    def test_query_brand_not_found(self):
        db_client = fstests.Client()
        fsdb = db.FirestoreDB(db_client)
        assert fsdb.query_brand("noop-svc") is None

    def test_query_brand(self):
        db_client = fstests.Client()
        db_client.store_document("clients/noop-client", {
            "id": "noop-client",
            "name": "Noop",
            "created_at": datetime(1970, 8, 3, 0, 0, 0, 0),
            "updated_at": datetime(2019, 8, 3, 0, 0, 0, 0),
        })

        fsdb = db.FirestoreDB(db_client)
        client = fsdb.query_brand("noop-client")

        assert "noop-client" == client.id
        assert "Noop" == client.name
        assert datetime(1970, 8, 3, 0, 0, 0, 0) == client.created_at
        assert datetime(2019, 8, 3, 0, 0, 0, 0) == client.updated_at

    def test_save_brand(self):
        db_client = fstests.Client()
        brand = db.Brand("noop-client-002",
                           updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_brand(brand)

        ref = db_client.document("clients/{0}".format(brand.id))
        assert dataclasses.asdict(brand) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10

    def test_delete_brand(self):
        db_client = fstests.Client()
        db_client.store_document("clients/huge-client", {
            "id": "huge-client",
            "name": "Huge",
            "created_at": datetime(1970, 8, 3, 0, 0, 0, 0),
            "updated_at": datetime(2019, 8, 3, 0, 0, 0, 0),
        })
        fsdb = db.FirestoreDB(db_client)

        client = fsdb.query_brand("huge-client")
        assert client

        fsdb.delete_brand(client)
        assert fsdb.query_brand("huge-client") == None

    @pytest.mark.parametrize("tc", [
        (0, {}, ["c2", "c0", "c1"]),
        (2, {}, ["c2", "c0"]),
        (2, {"order_by": {'id': "ASC"}}, ["c0", "c1"]),
        (0, {"deleted": False}, ["c2", "c0"]),
        (1, {"deleted": False}, ["c2"]),
        (1, {"deleted": False, "order_by": {'id': "ASC"}}, ["c0"]),
        (0, {"start_after": {"id": "c0"}, "order_by": {'id': "ASC"}}, ["c1", "c2"]),
    ])
    def test_brands(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        # notice: these are out of order on purpose
        db_client.store_document(
            "clients/c2", {"id": "c2", "name": "Brand 2", "handle": "brand-2", "deleted": False})
        db_client.store_document(
            "clients/c0", {"id": "c0", "name": "Brand 0", "handle": "brand-0", "deleted": False})
        db_client.store_document(
            "clients/c1", {"id": "c1", "name": "Brand 1", "handle": "brand-1", "deleted": True})
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.brands(limit, **filters)]

    @pytest.mark.parametrize("tc", [
        ({}, "c0"),
        ({"deleted": True}, "c1"),
        ({"handle": "brand-2"}, "c2"),
        ({"handle": "brand-3"}, None)
    ])
    def test_single_brand(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document(
            "clients/c0", {"id": "c0", "name": "Brand 0", "handle": "brand-0", "deleted": False})
        db_client.store_document(
            "clients/c1", {"id": "c1", "name": "Brand 1", "handle": "brand-1", "deleted": True})
        db_client.store_document(
            "clients/c2", {"id": "c2", "name": "Brand 2", "handle": "brand-2", "deleted": False})
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.brand(**filters)
        assert expected_id == (actual.id if expected_id else None)
