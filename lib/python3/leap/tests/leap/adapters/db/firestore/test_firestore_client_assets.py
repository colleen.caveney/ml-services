import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreDBBrandAssets:
    @pytest.mark.parametrize("tc", [
        (0, {}, ["asset0", "asset1", "asset2"]),
        (2, {}, ["asset0", "asset1"]),
        (0, {"bucket": "b1"}, ["asset0", "asset2"]),
        (1, {"bucket": "b1"}, ["asset0"]),
        (0, {"client_id": "client-1"}, ["asset1"]),
    ])
    def test_client_assets(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("client_assets/asset0", {
            "id": "asset0", "client_id": "client-0", "created_by_id": "user-0",
            "bucket": "b1", "path": "0.file", "name": "this0.file", "type": db.AssetType.FINANCIAL,
        })
        db_client.store_document("client_assets/asset1", {
            "id": "asset1", "client_id": "client-1", "created_by_id": "user-1",
            "bucket": "b2", "path": "1.file", "name": "this1.file", "type": db.AssetType.FINANCIAL,
        })
        db_client.store_document("client_assets/asset2", {
            "id": "asset2", "client_id": "client-2", "created_by_id": "user-2",
            "bucket": "b1", "path": "2.file", "name": "this2.file", "type": db.AssetType.FINANCIAL,
        })
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.client_assets(limit, **filters)]

    @pytest.mark.parametrize("tc", [
        ({}, "asset0"),
        ({"bucket": "b2"}, "asset1"),
        ({"client_id": "client-2"}, "asset2"),
        ({"client_id": "client-3"}, None),
    ])
    def test_single_client_asset(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("client_assets/asset0", {
            "id": "asset0", "client_id": "client-0", "created_by_id": "user-0",
            "bucket": "b1", "path": "0.file", "name": "this0.file", "type": db.AssetType.FINANCIAL,
        })
        db_client.store_document("client_assets/asset1", {
            "id": "asset1", "client_id": "client-1", "created_by_id": "user-1",
            "bucket": "b2", "path": "1.file", "name": "this1.file", "type": db.AssetType.FINANCIAL,
        })
        db_client.store_document("client_assets/asset2", {
            "id": "asset2", "client_id": "client-2", "created_by_id": "user-2",
            "bucket": "b1", "path": "2.file", "name": "this2.file", "type": db.AssetType.FINANCIAL,
        })
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.client_asset(**filters)
        assert expected_id == (actual.id if expected_id else None)

    def test_save_client_asset(self):
        db_client = fstests.Client()
        client_asset = db.BrandAsset("c0", "u0", "b0", "0.file", "this.file", db.AssetType.FINANCIAL, updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_client_asset(client_asset)

        ref = db_client.document("client_assets/{0}".format(client_asset.id))
        assert dataclasses.asdict(client_asset) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10

    def test_delete_client_asset(self):
        db_client = fstests.Client()
        db_client.store_document("client_assets/asset0", {
            "id": "asset0",
            "client_id": "c0",
            "created_by_id": "u0",
            "bucket": "b0",
            "path": "0.file",
            "name": "this.file",
            "type": db.AssetType.FINANCIAL,
            "created_at": datetime(1970, 8, 3, 0, 0, 0, 0),
            "updated_at": datetime(2019, 8, 3, 0, 0, 0, 0),
        })
        fsdb = db.FirestoreDB(db_client)

        client_asset = fsdb.client_asset(id="asset0")
        assert client_asset

        fsdb.delete_client_asset(client_asset)
        assert True == client_asset.deleted
