import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreDBBrandUsers:
    @pytest.mark.parametrize("tc", [
        (0, {}, ["cu0", "cu1", "cu2"]),
        (2, {}, ["cu0", "cu1"]),
        (0, {"enabled": True}, ["cu0", "cu2"]),
        (1, {"enabled": True}, ["cu0"]),
        (0, {"client_id": "client-1"}, ["cu1"]),
    ])
    def test_client_users(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("client_users/cu0", {"id": "cu0", "client_id": "client-0", "user_id": "user-0", "enabled": True})
        db_client.store_document("client_users/cu1", {"id": "cu1", "client_id": "client-1", "user_id": "user-1", "enabled": False})
        db_client.store_document("client_users/cu2", {"id": "cu2", "client_id": "client-2", "user_id": "user-2", "enabled": True})
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.client_users(limit, **filters)]

    @pytest.mark.parametrize("tc", [
        ({}, "cu0"),
        ({"enabled": False}, "cu1"),
        ({"client_id": "client-2"}, "cu2"),
        ({"client_id": "client-3"}, None),
    ])
    def test_single_client_user(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("client_users/cu0", {"id": "cu0", "client_id": "client-0", "user_id": "user-0", "enabled": True})
        db_client.store_document("client_users/cu1", {"id": "cu1", "client_id": "client-1", "user_id": "user-1", "enabled": False})
        db_client.store_document("client_users/cu2", {"id": "cu2", "client_id": "client-2", "user_id": "user-2", "enabled": True})
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.client_user(**filters)
        assert expected_id == (actual.id if actual else actual)

    def test_save_client_user(self):
        db_client = fstests.Client()
        client_user = db.BrandUser("c0", "u0", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_client_user(client_user)

        ref = db_client.document("client_users/{0}".format(client_user.id))
        assert dataclasses.asdict(client_user) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10

    def test_delete_client_user(self):
        db_client = fstests.Client()
        db_client.store_document("client_users/cu0", {
            "id": "cu0",
            "client_id": "c0",
            "user_id": "u0",
            "created_at": datetime(1970, 8, 3, 0, 0, 0, 0),
            "updated_at": datetime(2019, 8, 3, 0, 0, 0, 0),
        })
        fsdb = db.FirestoreDB(db_client)

        client_user = fsdb.client_user(id="cu0")
        assert client_user

        fsdb.delete_client_user(client_user)
        assert fsdb.client_user(id="cu0") == None
