from typing import Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreDB_Entities:
    @pytest.mark.parametrize("tc", [
        (0, {}, ["e0", "e1", "e2"]),
        (2, {}, ["e0", "e1"]),
        (0, {"type": "lamb"}, ["e0", "e2"]),
        (1, {"type": "lamb"}, ["e0"]),
        (0, {"type": "beef"}, ["e1"]),
        (0, {"source_adapter_id": "a0", "type": "beef"}, ["e1"]),
    ])
    def test_entities(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("entities/e0", {"id": "e0", "source_adapter_id": "a0", "type": "lamb"})
        db_client.store_document("entities/e1", {"id": "e1", "source_adapter_id": "a0", "type": "beef"})
        db_client.store_document("entities/e2", {"id": "e2", "source_adapter_id": "a1", "type": "lamb"})
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.entities(limit, **filters)]
