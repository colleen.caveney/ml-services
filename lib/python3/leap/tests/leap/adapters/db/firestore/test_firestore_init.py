import pytest

from leap.adapters import db


class TestFirestoreDB_Init:
    def test_init_requires_client_instance(self):
        with pytest.raises(ValueError, match="firestore client not provided"):
            db.FirestoreDB(None)
