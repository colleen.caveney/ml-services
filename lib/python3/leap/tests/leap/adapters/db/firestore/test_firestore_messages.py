import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreMessages:
    @pytest.mark.parametrize("tc", [
        ({}, "m0"),
        ({"action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD}, "m0"),
        ({"action_type": "not a type"}, None),
    ])
    def test_single_message(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("messages/m0", {"id": "m0", "recipients": [], "message_data": {}, "message_type": db.MessageType.EMAIL,
            "action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD})
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.message(**filters)
        assert expected_id == (actual.id if expected_id else None)

    @pytest.mark.parametrize("tc", [
        (0, {}, ["m0", "m1", "m2"]),
        (2, {}, ["m0", "m1"]),
        (0, {"action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD}, ["m0", "m1", "m2"]),
        (0, {"id": "m1"}, ["m1"]),
    ])
    def test_messages(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("messages/m0", {"id": "m0", "recipients": ["testingtester@leapinc.co"], "message_data": {}, "message_type": db.MessageType.EMAIL,
            "action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD})
        db_client.store_document("messages/m1", {"id": "m1", "recipients": ["anotheruser@leapinc.co"], "message_data": {}, "message_type": db.MessageType.EMAIL,
            "action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD})
        db_client.store_document("messages/m2", {"id": "m2", "recipients": ["testcase@leapinc.co"], "message_data": {}, "message_type": db.MessageType.EMAIL,
            "action_type": db.ActionType.NEW_USER_CHANGE_PASSWORD})
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.messages(limit, **filters)]

    ## saving

    def test_save_message(self):
        db_client = fstests.Client()
        message = db.Message(db.ActionType.NEW_USER_CHANGE_PASSWORD, db.MessageType.EMAIL, ["testcase@leapinc.co"], {}, "m0", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_message(message)

        ref = db_client.document("messages/{0}".format(message.id))
        assert dataclasses.asdict(message) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
