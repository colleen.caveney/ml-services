import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreDB_ServiceAccessPrivileges:
    @pytest.mark.parametrize("tc", [
        (0, {}, ["priv0", "priv1", "priv2"]),
        (2, {}, ["priv0", "priv1"]),
        (0, {"tier": db.ServiceAccessTier.READONLY}, ["priv0", "priv2"]),
        (1, {"tier": db.ServiceAccessTier.READONLY}, ["priv0"]),
        (0, {"service_id": "svc-1"}, ["priv1"]),
    ])
    def test_service_access_privileges(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("service_access_privileges/priv0", {
            "id": "priv0", "service_id": "svc-0", "privilege": "p0", "tier": db.ServiceAccessTier.READONLY
        })
        db_client.store_document("service_access_privileges/priv1", {
            "id": "priv1", "service_id": "svc-1", "privilege": "p1", "tier": db.ServiceAccessTier.FULL
        })
        db_client.store_document("service_access_privileges/priv2", {
            "id": "priv2", "service_id": "svc-2", "privilege": "p2", "tier": db.ServiceAccessTier.READONLY
        })
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.service_access_privileges(limit, **filters)]

    @pytest.mark.parametrize("tc", [
        ({}, "priv0"),
        ({"tier": db.ServiceAccessTier.FULL}, "priv1"),
        ({"service_id": "svc-2"}, "priv2"),
        ({"service_id": "svc-3"}, None),
    ])
    def test_single_service_access_privilege(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("service_access_privileges/priv0", {
            "id": "priv0", "service_id": "svc-0", "privilege": "p0", "tier": db.ServiceAccessTier.READONLY
        })
        db_client.store_document("service_access_privileges/priv1", {
            "id": "priv1", "service_id": "svc-1", "privilege": "p1", "tier": db.ServiceAccessTier.FULL
        })
        db_client.store_document("service_access_privileges/priv2", {
            "id": "priv2", "service_id": "svc-2", "privilege": "p2", "tier": db.ServiceAccessTier.READONLY
        })
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.service_access_privilege(**filters)
        assert expected_id == (actual.id if actual else actual)

    def test_save_service_access_privilege(self):
        db_client = fstests.Client()
        priv = db.BrandAsset("c0", "u0", "b0", "0.file", "this.file", db.AssetType.FINANCIAL, updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_service_access_privilege(priv)

        ref = db_client.document("service_access_privileges/{0}".format(priv.id))
        assert dataclasses.asdict(priv) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10

    def test_delete_service_access_privilege(self):
        db_client = fstests.Client()
        db_client.store_document("service_access_privileges/sap0", {
            "id": "sap0",
            "service_id": "sloppify",
            "privilege": "sloop",
            "tier": "ro",
            "created_at": datetime(1970, 8, 3, 0, 0, 0, 0),
            "updated_at": datetime(2019, 8, 3, 0, 0, 0, 0),
        })
        fsdb = db.FirestoreDB(db_client)

        sap = fsdb.service_access_privilege(id="sap0")
        assert sap

        fsdb.delete_service_access_privilege(sap)
        assert fsdb.service_access_privilege(id="sap0") == None
