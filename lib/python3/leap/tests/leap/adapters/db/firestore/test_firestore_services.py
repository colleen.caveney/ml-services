import dataclasses
from datetime import datetime

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreDB_Services:
    def test_query_service_requires_id(self):
        fsdb = db.FirestoreDB(fstests.Client())
        with pytest.raises(ValueError, match="id not provided"):
            fsdb.query_service("")

    def test_query_service_not_found(self):
        db_client = fstests.Client()
        fsdb = db.FirestoreDB(db_client)
        assert None == fsdb.query_service("noop-svc")

    def test_query_service(self):
        db_client = fstests.Client()
        db_client.store_document("services/noop-svc", {
            "id": "noop-svc",
            "api_key": "abc123",
            "api_secret": "123abc",
            "created_at": datetime(1970, 8, 3, 0, 0, 0, 0),
            "updated_at": datetime(2019, 8, 3, 0, 0, 0, 0),
        })

        fsdb = db.FirestoreDB(db_client)
        service = fsdb.query_service("noop-svc")

        assert "noop-svc" == service.id
        assert "abc123" == service.api_key
        assert "123abc" == service.api_secret
        assert datetime(1970, 8, 3, 0, 0, 0, 0) == service.created_at
        assert datetime(2019, 8, 3, 0, 0, 0, 0) == service.updated_at

    def test_save_service(self):
        db_client = fstests.Client()
        service = db.Service("leap-major-service", "some-key", "some-secret", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_service(service)

        ref = db_client.document("services/{0}".format(service.id))
        assert dataclasses.asdict(service) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
