from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests
from leap.std.hashing.md5 import hash_object

class TestFirestoreDB_SKUs:
    def test_keys_hash_updates_automatically(self):
        db_client = fstests.Client()

        asku = db.AdapterSKU("sku-1", "a-1", keys_hash="pintobean", keys={"a": "1", "b": "2"})
        db.FirestoreDB(db_client).save_adapter_sku(asku)
        ref = db_client.document("adapter_skus/{0}".format(asku.id))
        assert hash_object({"a": "1", "b": "2"}) == ref.get().get("keys_hash")

        asku.keys={"a": "2", "b": "3", "c": "4"}
        db.FirestoreDB(db_client).save_adapter_sku(asku)
        ref = db_client.document("adapter_skus/{0}".format(asku.id))
        assert hash_object({"a": "2", "b": "3", "c": "4"}) == ref.get().get("keys_hash")
