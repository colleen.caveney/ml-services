import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreSubscriptions:
    @pytest.mark.parametrize("tc", [
        ({}, "sub0"),
        ({"action_type": db.ActionType.NEW_BRAND}, "sub0"),
        ({"action_type": "not a type"}, None),
    ])
    def test_single_subscription(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("subscriptions/sub0", {"id": "sub0", "subscription_type": db.SubscriptionType.EMAIL, "action_type": db.ActionType.NEW_BRAND, "data": {}})
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.subscription(**filters)
        assert expected_id == (actual.id if expected_id else None)

    @pytest.mark.parametrize("tc", [
        (0, {}, ["sub0", "sub1", "sub2"]),
        (2, {}, ["sub0", "sub1"]),
        (0, {"subscription_type": db.SubscriptionType.EMAIL}, ["sub0", "sub1", "sub2"]),
        (0, {"id": "sub1"}, ["sub1"]),
    ])
    def test_subscriptions(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("subscriptions/sub0", {"id": "sub0", "subscription_type": db.SubscriptionType.EMAIL, "action_type": db.ActionType.NEW_BRAND, "data": {}})
        db_client.store_document("subscriptions/sub1", {"id": "sub1", "subscription_type": db.SubscriptionType.EMAIL, "action_type": db.ActionType.NEW_BRAND, "data": {}})
        db_client.store_document("subscriptions/sub2", {"id": "sub2", "subscription_type": db.SubscriptionType.EMAIL, "action_type": db.ActionType.NEW_BRAND, "data": {}})
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.subscriptions(limit, **filters)]

    ## saving

    def test_save_subscription(self):
        db_client = fstests.Client()
        subscription = db.Subscription(db.SubscriptionType.EMAIL, db.ActionType.NEW_BRAND, {}, "sub0", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_subscription(subscription)

        ref = db_client.document("subscriptions/{0}".format(subscription.id))
        assert dataclasses.asdict(subscription) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10

