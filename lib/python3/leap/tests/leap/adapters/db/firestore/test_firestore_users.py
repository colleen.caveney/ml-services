import dataclasses
from datetime import datetime
from typing import Optional, Tuple

import pytest

from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests


class TestFirestoreDB_Users:
    @pytest.mark.parametrize("tc", [
        (0, {}, ["cu0", "cu1", "cu2"]),
        (1, {"id": "cu0"}, ["cu0"]),
        (0, {"user_id": "user-2"}, ["cu2"]),
    ])
    def test_users(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("users/cu0", {"id": "cu0", "user_id": "user-0"})
        db_client.store_document("users/cu1", {"id": "cu1", "user_id": "user-1"})
        db_client.store_document("users/cu2", {"id": "cu2", "user_id": "user-2"})
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.users(limit, **filters)]

    @pytest.mark.parametrize("tc", [
        ({}, "cu0"),
        ({"id": "cu1"}, "cu1"),
        ({"user_id": "user-2"}, "cu2"),
        ({"id": "bad-id"}, None),
    ])
    def test_single_user(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("users/cu0", {"id": "cu0", "user_id": "user-0"})
        db_client.store_document("users/cu1", {"id": "cu1", "user_id": "user-1"})
        db_client.store_document("users/cu2", {"id": "cu2", "user_id": "user-2"})
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.user(**filters)
        assert expected_id == (actual.id if actual else actual)

    def test_save_user(self):
        db_client = fstests.Client()
        user = db.User("c0", "u0", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_user(user)

        ref = db_client.document("users/{0}".format(user.id))
        assert dataclasses.asdict(user) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10

    def test_delete_user(self):
        db_client = fstests.Client()
        db_client.store_document("users/cu0", {
            "id": "cu0",
            "user_id": "u0",
            "created_at": datetime(1970, 8, 3, 0, 0, 0, 0),
            "updated_at": datetime(2019, 8, 3, 0, 0, 0, 0),
        })
        fsdb = db.FirestoreDB(db_client)

        user = fsdb.user(id="cu0")
        assert user

        fsdb.delete_user(user)
        assert fsdb.user(id="cu0") == None
