import os
from typing import List

import psycopg2
import testing.postgresql

from leap.adapters.db import PlatformDB, OmniDBManager, FirestoreDB
from leap.google.cloud.firestore import tests as fstests

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")


def handler(postgresql):
    conn = psycopg2.connect(**postgresql.dsn())
    cursor = conn.cursor()
    with open(SCHEMA_FILE, 'r') as f:
        cursor.execute(f.read())
    cursor.close()
    conn.commit()
    conn.close()


Postgresql = testing.postgresql.PostgresqlFactory(cache_initialized_db=True, on_initialized=handler)


class BaseOmniDBSetup:
    
    databases = ['firestore', 'platformdb']

    @classmethod
    def setup_class(cls):
        cls.fsdb: FirestoreDB = cls.setup_firestore(cls)
        cls.platformdb: PlatformDB = cls.setup_platformdb(cls)
        cls.omnidb: OmniDBManager = cls.setup_omnidb(cls)

    @classmethod
    def teardown_class(cls):
        cls.postgresql.stop()

    def setup_firestore(self):
        fs_client = fstests.Client()
        return FirestoreDB(fs_client)

    def setup_platformdb(self):
        self.postgresql = Postgresql()
        self.conn = psycopg2.connect(**self.postgresql.dsn())
        self.cursor = self.conn.cursor()
        return PlatformDB(self.postgresql.url())

    def setup_omnidb(self):
        return OmniDBManager(firestore=self.fsdb, platformdb=self.platformdb)

    def insert_platformdb(self, dml_files: List):
        for file in dml_files:
            with open(file, 'r') as f:
                self.cursor.execute(f.read())
                self.conn.commit()
