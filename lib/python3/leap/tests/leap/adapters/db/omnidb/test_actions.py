import os
from omnidb_setup import BaseOmniDBSetup
from leap.adapters.db._models import Action, ActionType

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brands.sql"),
]


class TestOmniDBActions(BaseOmniDBSetup):
    """Test to insert actions into multiple databases

    Actions has the following constraints:
    actions_client_id_fkey
    """

    def setup_method(self, method):
        self.platformdb = self.setup_platformdb()
        self.fsdb = self.setup_firestore()
        self.omnidb = self.setup_omnidb()
        self.insert_platformdb(INSERT_FILES)
        self.brand_id = self.platformdb.get_all_brands_paging().raw_postgres_data[0].id

    def teardown_method(self, method):
        Action.platformdb = Action.firestore = True
        self.postgresql.stop()

    def test_insert_action_dual_write(self):
        expected_action = Action(ActionType.ADAPTER_ATTEMPT, ['test-id'], self.brand_id)
        self.omnidb.write(expected_action)

        actual_action_fs = self.omnidb.query('firestore', 'actions', brand_id=expected_action.brand_id)
        assert expected_action == actual_action_fs

        actual_action_platform = self.omnidb.query('platformdb', 'actions', brand_id=expected_action.brand_id)
        actual_action_platform.created_at = expected_action.created_at
        actual_action_platform.updated_at = expected_action.updated_at
        assert expected_action == actual_action_platform

    def test_insert_action_only_firestore(self):
        Action.platformdb = False
        expected_action = Action(ActionType.ADAPTER_ATTEMPT, ['test-id'], self.brand_id)

        self.omnidb.write(expected_action)

        actual_action_fs = self.omnidb.query('firestore', 'actions', brand_id=expected_action.brand_id)
        assert expected_action == actual_action_fs

        actual_action_platform = self.omnidb.query('platformdb', 'actions', brand_id=expected_action.brand_id)
        assert actual_action_platform is None

    def test_insert_action_only_platformdb(self):
        Action.firestore = False
        expected_action = Action(ActionType.ADAPTER_ATTEMPT, ['test-id'], self.brand_id)
        self.omnidb.write(expected_action)

        actual_action_fs = self.omnidb.query('firestore', 'actions', brand_id=expected_action.brand_id)
        assert actual_action_fs is None

        actual_action_platform = self.omnidb.query('platformdb', 'actions', brand_id=expected_action.brand_id)
        actual_action_platform.created_at = expected_action.created_at
        actual_action_platform.updated_at = expected_action.updated_at
        assert expected_action == actual_action_platform
