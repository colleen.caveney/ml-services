import pytest
from omnidb_setup import BaseOmniDBSetup
from leap.adapters.db._models import Brand
import leap.adapters.db as db

class TestOmniDBBrands(BaseOmniDBSetup):
    """Test to insert brands into multiple databases
    """

    def setup_method(self, method):
        self.platformdb = self.setup_platformdb()
        self.fsdb = self.setup_firestore()
        self.omnidb = self.setup_omnidb()

    def teardown_method(self, method):
        Brand.platformdb = Brand.firestore = True
        self.postgresql.stop()

    def test_write_brand(self):
        brand = Brand('test-brand')
        
        # write
        self.omnidb.write(brand)
        
        # firestore
        actual_brand = self.omnidb.query('firestore', 'brands', id=brand.id)
        assert actual_brand == brand
        
        # platformdb
        actual_brand = self.omnidb.query('platformdb', 'brands', id=brand.id)
        actual_brand.created_at = brand.created_at
        actual_brand.updated_at = brand.updated_at
        assert actual_brand == brand
        
    def test_update_brand(self):
        brand = Brand('test-brand', handle='orig-handle')

        # write
        self.omnidb.write(brand)
        orig_brand = self.omnidb.query('platformdb', 'brands', id=brand.id)

        # update
        brand.handle = "updated-handle"
        self.omnidb.update(brand)

        # firestore
        actual_brand = self.omnidb.query('firestore', 'brands', id=brand.id)
        assert actual_brand.handle == 'updated-handle'

        # platformdb
        actual_brand = self.omnidb.query('platformdb', 'brands', id=brand.id)
        assert actual_brand.handle == brand.handle
        assert actual_brand.created_at == orig_brand.created_at
        assert actual_brand.updated_at > orig_brand.updated_at

    def test_delete_brand(self):
        brand = Brand('test-brand')

        # write
        self.omnidb.write(brand)
        
        # delete
        self.omnidb.delete(brand)
        
        # firestore
        actual_brand = self.omnidb.query('firestore', 'brands', id=brand.id)
        assert actual_brand is None

        # platformdb
        actual_brand = self.omnidb.query('platformdb', 'brands', id=brand.id)
        assert actual_brand is None

    def test_query_multiple_brands(self):
        brands = [Brand(f'test-brand-{i}',handle=f'test-brand-{i}', status_key=db.BrandStatus.LIVE) for i in range(0, 5)]

        # write
        for brand in brands:
            self.omnidb.write(brand)

        # firestore
        actual_brand = self.omnidb.query('firestore', 'brands', status_key='LIVE')
        actual_brands = list(actual_brand)
        assert len(actual_brands) == 5
        assert sorted([brand.id for brand in actual_brands]) == sorted([brand.id for brand in actual_brands])

        # platformdb
        actual_brands = self.omnidb.query('platformdb', 'brands', status_key='LIVE')
        actual_brands = actual_brands.raw_postgres_data
        assert len(actual_brands) == 5
        assert sorted([brand.id for brand in actual_brands]) == sorted([brand.id for brand in actual_brands])
        
