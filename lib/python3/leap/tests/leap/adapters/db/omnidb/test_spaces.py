import pytest
from omnidb_setup import BaseOmniDBSetup
from leap.adapters.db._models import Brand
import leap.adapters.db as db


class TestOmniDBSpaces(BaseOmniDBSetup):
    """Test to check omnidb interface with spaces
    """

    def setup_method(self, method):
        self.platformdb = self.setup_platformdb()
        self.fsdb = self.setup_firestore()
        self.omnidb = self.setup_omnidb()
        self.space: db.Space = db.Space("123 mullberry st", "chicago", "il", "abc123", "60660", 1.0, -1.0, 650)


    def teardown_method(self, method):
        self.postgresql.stop()

    def test_spaces(self):
        self.omnidb.write(self.space)
        
        for database in self.databases:
            spaces = list(self.omnidb.query(database, 'spaces'))
            spaces[0].created_at = self.space.created_at
            spaces[0].updated_at = self.space.updated_at
            assert spaces[0].to_object() == self.space.to_object()

        self.space.state = 'oh'
        self.omnidb.update(self.space)

        for database in self.databases:
            spaces = list(self.omnidb.query(database, 'spaces'))
            assert spaces[0].state == 'oh'
        
        self.omnidb.delete(self.space)

        for database in self.databases:
            spaces = list(self.omnidb.query(database, 'spaces'))
            assert len(spaces) == 0
    