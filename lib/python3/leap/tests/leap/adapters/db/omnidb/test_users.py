import uuid
import pytest
from omnidb_setup import BaseOmniDBSetup
from leap.adapters.db._models import User
import leap.adapters.db as db

class TestOmniDBBrands(BaseOmniDBSetup):
    """Test to insert brands into multiple databases
    """

    def setup_method(self, method):
        self.platformdb = self.setup_platformdb()
        self.fsdb = self.setup_firestore()
        self.omnidb = self.setup_omnidb()

    def teardown_method(self, method):
        User.platformdb = User.firestore = True
        self.postgresql.stop()

    def test_write_user(self):
        user = User(user_id=uuid.uuid4().hex)
        
        # write
        self.omnidb.write(user)
        
        # firestore
        actual_user = self.omnidb.query('firestore', 'users', id=user.id)
        assert actual_user == user
    
        # platformdb
        actual_user = self.omnidb.query('platformdb', 'users', id=user.id)
        actual_user = actual_user
        actual_user.created_at = user.created_at
        actual_user.updated_at = user.updated_at
        assert actual_user == user
        
    def test_update_user(self):
        user = User(user_id=uuid.uuid4().hex, display_name="orig-display-name")

        # write
        self.omnidb.write(user)
        orig_user = self.omnidb.query('platformdb', 'users', id=user.id)
        assert orig_user.id == user.id
        assert orig_user.display_name == 'orig-display-name'

        # update
        user.display_name = "updated-display-name"
        self.omnidb.update(user)

        # firestore
        actual_user = self.omnidb.query('firestore', 'users', id=user.id)
        assert actual_user.display_name == 'updated-display-name'

        # platformdb
        actual_user = self.omnidb.query('platformdb', 'users', id=user.id)
        actual_user = actual_user
        assert actual_user.id == orig_user.id
        assert actual_user.display_name == 'updated-display-name'

    def test_delete_user(self):
        user = User(user_id=uuid.uuid4().hex)

        # write
        self.omnidb.write(user)
        
        # delete
        self.omnidb.delete(user)
        
        # firestore
        actual_user = self.omnidb.query('firestore', 'users', id=user.id)
        assert actual_user is None

        # platformdb
        actual_user = self.omnidb.query('platformdb', 'users', id=user.id)
        assert actual_user is None

    def test_query_multiple_users(self):
        users = [User(user_id=uuid.uuid4().hex) for i in range(0, 5)]

        # write
        for user in users:
            self.omnidb.write(user)

        # firestore
        actual_user = self.omnidb.query('firestore', 'users', identity_service='AUTH0')
        actual_users = list(actual_user)
        assert len(actual_users) == 5
        assert sorted([user.id for user in actual_users]) == sorted([user.id for user in actual_users])

        # platformdb
        actual_users = self.omnidb.query('platformdb', 'users', identity_service='AUTH0')
        actual_users = actual_users.raw_postgres_data
        assert len(actual_users) == 5
        assert sorted([user.id for user in actual_users]) == sorted([user.id for user in actual_users])
