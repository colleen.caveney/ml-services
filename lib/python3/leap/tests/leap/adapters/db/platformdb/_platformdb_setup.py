import os

import psycopg2
import testing.postgresql

from leap.adapters.db.platformdb import PlatformDB

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brands.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_actions.sql"),
]


def handler(postgresql):
    conn = psycopg2.connect(**postgresql.dsn())
    cursor = conn.cursor()
    with open(SCHEMA_FILE, 'r') as f:
        cursor.execute(f.read())
    cursor.close()
    conn.commit()
    conn.close()


Postgresql = testing.postgresql.PostgresqlFactory(cache_initialized_db=True,
                                                  on_initialized=handler)

class BaseAPITestPlatformDB:

    def load_data(self, dml_files):
        self.postgresql = Postgresql()
        conn = psycopg2.connect(**self.postgresql.dsn())
        self.cursor = conn.cursor()
        self.platformDB = PlatformDB(self.postgresql.url())
        for file in dml_files:
            with open(file, 'r') as f:
                self.cursor.execute(f.read())
                conn.commit()

    def teardown_method(self):
        self.postgresql.stop()
