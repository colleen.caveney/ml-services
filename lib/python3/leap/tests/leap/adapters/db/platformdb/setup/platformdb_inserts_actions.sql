INSERT INTO actions (id, brand_id, created_at, updated_at, matched_subscription_ids, message, action_type)
VALUES ('545f203e-7af1-44d5-b5db-62a8e6c7d38d', '965b3d10-8954-424e-914c-0b64b6d9649d', '2022-01-07 23:35:53.100000 +00:00', '2022-01-08 23:35:53.100000 +00:00',
        '["4340a783-7427-4e12-a561-150bbed241ba"]', '{"body": "body-a", "title": "title-a"}', 'NEW_BRAND');

INSERT INTO actions (id, brand_id, created_at, updated_at, matched_subscription_ids, message, action_type)
VALUES ('7b529c7d-9aef-4f18-90d1-1ffdc0a10744', '965b3d10-8954-424e-914c-0b64b6d9649d', '2022-01-09 23:35:53.100000 +00:00', '2022-01-10 23:35:53.100000 +00:00',
        '["ed725ca6-8366-4e0f-be11-b83e0474b75d"]', '{"body": "body-b", "title": "title-b"}', 'NEW_BRAND');

INSERT INTO actions (id, brand_id, created_at, updated_at, matched_subscription_ids, message, action_type)
VALUES ('d2745318-781f-4c9d-97b4-2818133a3b80', '965b3d10-8954-424e-914c-0b64b6d9649d', '2022-01-11 23:35:53.100000 +00:00', '2022-01-12 23:35:53.100000 +00:00',
       '["a846845a-2e14-43c2-ba96-305a5abae801"]', '{"body": "body-c", "title": "title-c"}', 'NEW_BRAND');
