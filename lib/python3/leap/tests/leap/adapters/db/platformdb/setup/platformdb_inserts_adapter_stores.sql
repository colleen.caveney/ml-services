INSERT INTO adapter_stores(id, adapter_id, store_id, created_at, updated_at, is_primary, enabled, external_id,
                           virtual_store_code)
VALUES ('82367fbf-5047-4067-af9d-97b6fe134fe8', 'a4d0fa97-3176-4b8e-9331-d6706b1e6598',
        'ee35fe1d-1a9d-4b23-bbe7-e87c212efe26', '2022-01-07 23:35:53.100000 +00:00',
        '2022-01-08 23:35:53.100000 +00:00', True, True, '123', 'vrt-A');

INSERT INTO adapter_stores(id, adapter_id, store_id, created_at, updated_at, is_primary, enabled, external_id,
                           virtual_store_code)
VALUES ('a1edef29-437d-4f3f-9186-854702b59f09', 'a4d0fa97-3176-4b8e-9331-d6706b1e6598',
        '46eecfe1-374b-4ed1-8a99-1094ef1c4b1b', '2022-01-09 23:35:53.100000 +00:00',
        '2022-01-10 23:35:53.100000 +00:00', True, True, '456', 'vrt-B');

INSERT INTO adapter_stores(id, adapter_id, store_id, created_at, updated_at, is_primary, enabled, external_id,
                           virtual_store_code)
VALUES ('28ef5dda-5a4e-4af0-9e22-7e678a5deff9', 'a4d0fa97-3176-4b8e-9331-d6706b1e6598',
        'b3095305-6b31-4c98-b3d4-49cfc1fe89ff', '2022-01-11 23:35:53.100000 +00:00',
        '2022-01-12 23:35:53.100000 +00:00', False, True, '789', Null);
