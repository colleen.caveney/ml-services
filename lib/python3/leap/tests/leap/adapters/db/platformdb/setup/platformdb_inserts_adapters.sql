INSERT INTO adapters (id, brand_id, service_id, created_at, updated_at, internal, instance_id,
                      source, sink, track_inventory, config, filters, data_state)
VALUES ('a4d0fa97-3176-4b8e-9331-d6706b1e6598', '965b3d10-8954-424e-914c-0b64b6d9649d',
        '5fdf32c7-e678-4955-9a06-72a1bb7bd88a', '2022-01-07 23:35:53.100000 +00:00',
        '2022-01-08 23:35:53.100000 +00:00', true, 'instance1', true, true, false, '{}', '{}', '{}');

INSERT INTO adapters (id, brand_id, service_id, created_at, updated_at, internal, instance_id,
                      source, sink, track_inventory, config, filters, data_state)
VALUES ('ca7a1145-7b71-4828-8108-9a7a08052eb9', '965b3d10-8954-424e-914c-0b64b6d9649d',
        '5fdf32c7-e678-4955-9a06-72a1bb7bd88a', '2022-01-09 23:35:53.100000 +00:00',
        '2022-01-10 23:35:53.100000 +00:00', true, 'instance2', true, true, false, '{}', '{}', '{}');

INSERT INTO adapters (id, brand_id, service_id, created_at, updated_at, internal, instance_id,
                      source, sink, track_inventory, config, filters, data_state)
VALUES ('f9963407-e405-4dbb-9aa1-08c1544c5207', '965b3d10-8954-424e-914c-0b64b6d9649d',
        '5fdf32c7-e678-4955-9a06-72a1bb7bd88a', '2022-01-11 23:35:53.100000 +00:00',
        '2022-01-12 23:35:53.100000 +00:00', true, 'instance3', true, true, false, '{}', '{}', '{}');

