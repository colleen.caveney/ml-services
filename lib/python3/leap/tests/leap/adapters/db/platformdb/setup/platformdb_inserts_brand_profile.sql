INSERT INTO brand_profiles(id, brand_id, created_at, updated_at, overview_updated_by, overview,
                           visual_merchandising_updated_by, visual_merchandising, product_merchandising_updated_by, product_merchandising,
                           store_operations_updated_by, store_operations, store_design_updated_by,
                           store_design, marketing_updated_by, marketing, integrations_updated_by,
                           integrations, social_updated_by, social)
VALUES ('ee8b5ff5-ccca-44fb-ab8a-bf8b2955470e',
        '965b3d10-8954-424e-914c-0b64b6d9649d',
        '2022-01-01 23:35:53.100000 +00:00',
        '2022-01-02 23:35:53.100000 +00:00',
        '89380209-52bd-446f-9c70-cffe25d34605',
        '{
          "brand_mission": "This is my brand mission",
          "brand_story": "This is my brand story",
          "core_values": [
            "core_value1",
            "core_value2",
            "core_value3"
          ],
          "customer_personas": [
            {
              "age_end": 25,
              "age_start": 18,
              "archetype": "an archetype of a younger person",
              "education": "HIGH_SCHOOL",
              "income_end": 100000,
              "income_start": 50000,
              "story": "Younger person story"
            },
            {
              "age_end": 70,
              "age_start": 55,
              "archetype": "an archetype of an older person",
              "education": "HIGHER_EDUCATION",
              "income_end": 150000,
              "income_start": 100000,
              "story": "Older person story"
            }
          ],
          "elevator_pitch": "This is my elevator pitch",
          "goals": [
            {
              "date": "2022-01-04 17:35:53.100000+00:00",
              "goal": "PROFITABILITY",
              "goal_percent": 30.0,
              "ranking": 2
            },
            {
              "date": "2022-01-03 17:35:53.100000+00:00",
              "goal": "CUSTOMER_BASE",
              "goal_percent": 30.0,
              "ranking": 1
            },
            {
              "date": "2022-01-05 17:35:53.100000+00:00",
              "goal": "BRAND_AWARENESS",
              "goal_percent": 30.0,
              "ranking": 3
            }
          ],
          "sales_channels": [
            {
              "channel": "ECOMMERCE",
              "details": "This is details about my ecommerce sales channel",
              "enabled": true
            },
            {
              "channel": "OTHER",
              "details": "This is details about my other sales channel",
              "enabled": true
            }
          ],
          "website": "mywebsite.com"
        }',
    '89380209-52bd-446f-9c70-cffe25d34605',
    '{
          "hanger_ownership": "LEAP",
          "material": {
            "color": "SILVER",
            "material_type": "METAL"
          },
          "product_placement_image": "COLLECTION_LIFESTYLE",
          "product_placement_note": "A note about product placement",
          "visual_merchandising_image": "GALLERY",
          "visual_merchandising_note": "A note about"
        }',
        '89380209-52bd-446f-9c70-cffe25d34605',
        '{
          "warehouses": [
            {
              "name": "warehouse1",
              "address": "123 fake street",
              "city": "nowhere",
              "state": "ST",
              "zip_code": "123456",
              "po_box": "12",
              "latitude": 10.0,
              "longitude": 11.1,
              "processing_time_start": 1,
              "processing_time_end": 4,
              "shipping_time_start": 2,
              "shipping_time_end": 5
            }
          ],
          "damages_choice": "EXCEPTION",
          "damages_address": {
            "attn": "bob",
            "address": "123 fake street",
            "city": "nowhere",
            "state": "ST",
            "zip_code": "123456",
            "po_box": "12",
            "latitude": 10.0,
            "longitude": 11.1
          },
          "rtv_choice": "EXCEPTION",
          "rtv_exception": "rtv exception",
          "labeling_confirmation": true,
          "return_choice": "EXCEPTION",
          "return_exception": "return exception",
          "exchange_choice": "EXCEPTION",
          "exchange_exception": "exchange exception",
          "connected_shopify": true,
          "connected_shopify_more": "fake.com",
          "core_assortment_percentage": 11,
          "seasonal_assortment_percentage": 12,
          "product_launch_quantity": 13,
          "product_launch_timeline": "MONTHLY",
          "product_launch_notes": "launch notes",
          "product_category_include": {
            "accessories": [
              "SUSPENDERS",
              "BELTS"
            ],
            "bags_backpacks_luggage": [
              "BACKPACKS",
              "GOLF_BAGS"
            ],
            "beauty_health_personal_care": [
              "HAIR_CARE",
              "SKIN_CARE"
            ],
            "bottoms": [
              "JEANS",
              "SHORTS"
            ],
            "home": [
              "BATHROOM_ACCESSORIES",
              "BEDDINGS"
            ],
            "intimates": [
              "SLEEPWEAR_LOUNGEWEAR",
              "BRAS"
            ],
            "jewelry_watches": [
              "WATCHES",
              "EARRINGS"
            ],
            "office_school_supplies": [
              "PAPER_PRODUCTS",
              "FILING_PRODUCTS"
            ],
            "other_clothing": [
              "CLOTHING_SETS",
              "DRESSES"
            ],
            "shoes": [
              "FLATS",
              "BOOTS"
            ],
            "swimsuits_cover_ups": [
              "COVER_UPS",
              "TANKINIS"
            ],
            "tops": [
              "SWEATERS",
              "COATS_JACKETS_VESTS"
            ],
            "other": "other"
          },
          "product_category_excluded": {
            "accessories": [
              "SUSPENDERS",
              "BELTS"
            ],
            "bags_backpacks_luggage": [
              "BACKPACKS",
              "GOLF_BAGS"
            ],
            "beauty_health_personal_care": [
              "HAIR_CARE",
              "SKIN_CARE"
            ],
            "bottoms": [
              "JEANS",
              "SHORTS"
            ],
            "home": [
              "BATHROOM_ACCESSORIES",
              "BEDDINGS"
            ],
            "intimates": [
              "SLEEPWEAR_LOUNGEWEAR",
              "BRAS"
            ],
            "jewelry_watches": [
              "WATCHES",
              "EARRINGS"
            ],
            "office_school_supplies": [
              "PAPER_PRODUCTS",
              "FILING_PRODUCTS"
            ],
            "other_clothing": [
              "CLOTHING_SETS",
              "DRESSES"
            ],
            "shoes": [
              "FLATS",
              "BOOTS"
            ],
            "swimsuits_cover_ups": [
              "COVER_UPS",
              "TANKINIS"
            ],
            "tops": [
              "SWEATERS",
              "COATS_JACKETS_VESTS"
            ],
            "other": "other"
          },
          "product_category_new": {
            "accessories": [
              "SUSPENDERS",
              "BELTS"
            ],
            "bags_backpacks_luggage": [
              "BACKPACKS",
              "GOLF_BAGS"
            ],
            "beauty_health_personal_care": [
              "HAIR_CARE",
              "SKIN_CARE"
            ],
            "bottoms": [
              "JEANS",
              "SHORTS"
            ],
            "home": [
              "BATHROOM_ACCESSORIES",
              "BEDDINGS"
            ],
            "intimates": [
              "SLEEPWEAR_LOUNGEWEAR",
              "BRAS"
            ],
            "jewelry_watches": [
              "WATCHES",
              "EARRINGS"
            ],
            "office_school_supplies": [
              "PAPER_PRODUCTS",
              "FILING_PRODUCTS"
            ],
            "other_clothing": [
              "CLOTHING_SETS",
              "DRESSES"
            ],
            "shoes": [
              "FLATS",
              "BOOTS"
            ],
            "swimsuits_cover_ups": [
              "COVER_UPS",
              "TANKINIS"
            ],
            "tops": [
              "SWEATERS",
              "COATS_JACKETS_VESTS"
            ],
            "other": "other"
          }
        }',
        '89380209-52bd-446f-9c70-cffe25d34605',
        '{
          "music_genre_selection": ["rock", "roll"],
          "music_artist_selection": ["ac", "dc"],
          "scent_selection": "BEACHY_TROPICAL",
          "store_discounts_policy_confirmation": "EXCEPTION",
          "store_discounts_policy_exception": "policy exception",
          "store_team_wardrobe_policy_confirmation": "EXCEPTION",
          "store_team_wardrobe_policy_exception": "wardrobe exception",
          "store_team_allowance_policy_confirmation": "EXCEPTION",
          "store_team_allowance_policy_exception": "allowance exception",
          "store_inventory_counts_policy_confirmation": "EXCEPTION",
          "store_inventory_counts_policy_exception": "inventory exception",
          "activation_fund_allowance_policy_confirmation": "EXCEPTION",
          "activation_fund_allowance_policy_exception": "allowance exception"
        }',
        '89380209-52bd-446f-9c70-cffe25d34605',
        '{
          "brand_colors": [
            "#FF5722",
            "#270801",
            "#A1A1A3"
          ],
          "mood_board_aesthetic_image": "MODERN_AND_MINIMAL",
          "mood_board_vibe_image": "MOODY_AND_DRAMATIC",
          "key_elements": "key elements to transfer over",
          "additional_comments": "additional comments"
        }',
        '89380209-52bd-446f-9c70-cffe25d34605',
        '{
          "ideal_event_details": "This is my ideal event details 1",
          "event_dos": "This is event dos",
          "event_donts": "This is event donts",
          "non_profit_partners": [
            {
              "details": "This is my non profit partners for reason 10"
            },
            {
              "details": "This is my non profit partners for reason 11"
            },
            {
              "details": "This is my non profit partners for reason 12"
            },
            {
              "details": "This is my non profit partners for reason 13"
            },
            {
              "details": "This is my non profit partners for reason 14"
            },
            {
              "details": "This is my non profit partners for reason 15"
            },
            {
              "details": "This is my non profit partners for reason 16"
            },
            {
              "details": "This is my non profit partners for reason 17"
            },
            {
              "details": "This is my non profit partners for reason 18"
            },
            {
              "details": "This is my non profit partners for reason 19"
            }
          ],
          "influencers": "These are the influencers that have influenced me"
        }',
        '89380209-52bd-446f-9c70-cffe25d34605',
        '{
          "software": [
            {"integration": "COMMERCE", "value": "commerce software 1"},
            {"integration": "ERP", "value": "erp software 1"},
            {"integration": "WMS", "value": "wms software 1"},
            {"integration": "IMS", "value": "ims software 1"},
            {"integration": "ACCOUNTING", "value": "accounting software 1"},
            {"integration": "THIRD_PARTY_LOGISTICS", "value": "3pl software 1"},
            {"integration": "RETURNS_PORTAL", "value": "returns portal 1"}
          ],
          "technical_returns_process": "This is my technical returns process",
          "affiliate_marketing_programs": "This is my affiliate marketing program",
          "upcoming_changes": "These are upcoming changes"
        }',
        '89380209-52bd-446f-9c70-cffe25d34605',
        '{
          "platform": [
            {"social": "FACEBOOK", "value": "www.facebook.com/"},
            {"social": "YOUTUBE", "value": "www.youtube.com/"},
            {"social": "INSTAGRAM", "value": "www.instagram.com/"},
            {"social": "TIKTOK", "value": "www.tiktok.com/"},
            {"social": "PINTEREST", "value": "www.pinterest.com/"},
            {"social": "SNAPCHAT", "value": "www.snapchat.com/"},
            {"social": "LINKEDIN", "value": "www.linkedin.com/"},
            {"social": "TWITTER", "value": "www.twitter.com/"}
          ]
        }')