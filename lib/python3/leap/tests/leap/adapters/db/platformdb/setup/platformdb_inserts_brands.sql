INSERT INTO brands(id, handle, deleted, status_key, created_at, updated_at, name,
                     timezone, website, contact_name, contact_email, contact_phone, config,
                     brand_details, brand_code, box_link, role)
VALUES ('965b3d10-8954-424e-914c-0b64b6d9649d', 'BOBBEL', false, 'LIVE',
        '2022-01-01 23:35:53.100000 +00:00', '2022-01-02 23:35:54.100000 +00:00', 'Bob Belcher',
        'Etc/UTC', 'www.burgers.com', 'contact1', 'bob@burgers.com', 'phone1', '{}', '{}', 'BOBBEL', 'foo.com', 'NEW');

INSERT INTO brands(id, handle, deleted, status_key, created_at, updated_at, name,
                     timezone, website, contact_name, contact_email, contact_phone, config,
                     brand_details, brand_code)
VALUES ('8ea2f941-0f9d-4570-b11d-e78809ae833b', 'LINDAB', false, 'NEW',
        '2022-01-03 23:35:53.100000 +00:00', '2022-01-04 23:35:53.100000 +00:00', 'Linda Belcher',
        'Etc/UTC', NULL, 'contact2', 'linda@burgers.com', 'phone2', '{}', '{}', 'LINDAB');

INSERT INTO brands(id, handle, deleted, status_key, created_at, updated_at, name,
                     timezone, website, contact_name, contact_email, contact_phone, config,
                     brand_details, brand_code)
VALUES ('8ff14ece-7bd6-4e5d-bfc4-6c1ef8df3cba', 'JOEYPE', true, 'EVAL',
        '2022-01-05 23:36:53.100000 +00:00', '2022-01-06 23:35:53.100000 +00:00', 'Joey Pesto',
        'Etc/UTC', NULL, 'contact3', 'joey@pesto.com', 'phone3', '{}', '{}', 'JOEYPE');

INSERT INTO brands(id, handle, deleted, status_key, created_at, updated_at, name,
                     timezone, website, contact_name, contact_email, contact_phone, config,
                     brand_details, brand_code)
VALUES ('9558fe1d-ea4b-4a34-b265-1dc8bc6aaaa9', 'JIMMYP', false, 'PROSPECTIVE',
        '2022-01-07 23:37:53.100000 +00:00', '2022-01-08 23:35:53.100000 +00:00', 'Jimmy Pesto',
        'Etc/UTC', NULL, 'contact4', 'jimmy@pesto.com', 'phone4', '{}', '{}', 'JIMMYP');

INSERT INTO brands(id, handle, deleted, status_key, created_at, updated_at, name,
                     timezone, website, contact_name, contact_email, contact_phone, config,
                     brand_details, brand_code)
VALUES ('85ce1ec7-e2ba-40ce-824c-6edb3bc6c315', 'TINABE', false, 'NEW',
        '2022-01-09 23:35:53.100000 +00:00', '2022-01-10 23:35:53.100000 +00:00', 'Tina Belcher',
        'Etc/UTC', NULL, 'contact5', 'tina@burgers.com', 'phone5', '{}', '{}', 'TINABE');
