INSERT INTO client_omni_channel_insights (id, created_at, client_handle, gross_profit_margin,
                                          ppv_score, fitness_score, orders_last_12_months,
                                          net_sales_last_12_months, buyers_last_12_months,
                                          margin_dollars_last_12_months,
                                          average_order_value_last_12_months,
                                          purchase_frequency_last_12_months,
                                          units_per_transaction_last_12_months,
                                          lifetime_value_last_12_months, refund_rate_last_12_months,
                                          average_order_value_omni_channel,
                                          purchase_frequency_omni_channel,
                                          units_per_transaction_omni_channel,
                                          lifetime_value_omni_channel, refund_rate_omni_channel,
                                          purchase_frequency_percent_change,
                                          average_order_value_percent_change,
                                          refund_rate_percent_change, lifetime_value_percent_change,
                                          top_5_net_sales_percentage, top_5_units_sold_percentage,
                                          total_product_count, top_5_product_concentration,
                                          top_market_city, top_market_state_abbreviation,
                                          total_leap_market_count)
VALUES ('9ea09898-f4e6-4b82-98e0-2cd1caa8efe9', '2022-05-11 21:01:49.549401 +00:00', 'redletter',
        40, 0.052000000000000005, 16.685714285714287, 60, 105334.7, 51, 702.2313333333334,
        1755.5783333333334, 1.1764705882352942, 1.1333333333333333, 1652.3090196078433, 0,
        2001.3592999999998, 1.4588235294117649, 1.156, 2335.704030117647, 0, 0.24000000000000007,
        0.13999999999999987, 0.0, 0.41359999999999986, 0.5792208880311722, 0.5806451612903226, 12,
        0.4166666666666667, 'Virginia Beach', 'VA', 3);

INSERT INTO client_omni_channel_insights (id, created_at, client_handle)
VALUES ('e458917b-02b3-44f5-b4d3-fa1cbb21f381', '2022-05-11 21:01:49.549401 +00:00', 'empty_insights');


INSERT INTO client_omni_channel_insights_markets (id, created_at, client_handle, market, cbsa_code,
                                                  net_sales_last_12_months, city,
                                                  state_abbreviation, market_net_sales_rank,
                                                  is_leap_market)
VALUES ('1fb12f8c-6e2f-46a3-8c2c-7a6dffd314e5', '2022-05-12 09:09:16.800453 +00:00', 'bad-birdie',
        'Phoenix-Mesa-Scottsdale, AZ', '38060', 453905.72000000055, 'Phoenix', 'AZ', 1, true);

INSERT INTO client_omni_channel_insights_markets (id, created_at, client_handle, market, cbsa_code,
                                                  net_sales_last_12_months, city,
                                                  state_abbreviation, market_net_sales_rank,
                                                  is_leap_market)
VALUES ('4b197afd-e359-4024-84c4-b0e722490e1c', '2022-05-12 10:10:16.800453 +00:00', 'bad-birdie',
        'Los Angeles-Long Beach-Anaheim, CA', '31080', 426499.80000000034, 'Los Angeles', 'CA', 2,
        true);

INSERT INTO client_omni_channel_insights_markets (id, created_at, client_handle)
VALUES ('817fb709-f4de-4a33-bcb4-128adbe8685a', '2022-05-12 10:10:16.800453 +00:00', 'empty_market');

INSERT INTO client_omni_channel_insights_top_products_units_sold (id, created_at, client_handle,
                                                                  product_id, product_title,
                                                                  product_units_sold,
                                                                  total_brand_units_sold,
                                                                  product_percent_of_units_sold,
                                                                  product_units_sold_rank,
                                                                  product_count)
VALUES ('48e1b12c-c017-4203-9385-96546dfbdd07', '2022-05-12 18:03:52.804210 +00:00',
        'vincero-collective', 4815581085786, 'Gift Bag', 7150, 202240, 0.03535403481012658, 1, 1006);

INSERT INTO client_omni_channel_insights_top_products_units_sold (id, created_at, client_handle,
                                                                  product_id, product_title,
                                                                  product_units_sold,
                                                                  total_brand_units_sold,
                                                                  product_percent_of_units_sold,
                                                                  product_units_sold_rank,
                                                                  product_count)
VALUES ('9017a0bf-42a3-4fef-88a9-bf2f17d56bfb', '2022-05-12 18:03:52.804210 +00:00',
        'vincero-collective', 4714500128858, 'Protection Plan', 4667, 202240, 0.02307654272151899, 2,
        1006);

INSERT INTO client_omni_channel_insights_top_products_units_sold (id, created_at, client_handle)
VALUES ('633060c8-5179-44cc-b5f2-16b8c4a0c52c', '2022-05-12 18:03:52.804210 +00:00', 'empty_units');

INSERT INTO omni_channel_active_leap_markets (id, created_at, cbsa_code, market, city,
                                              state_abbreviation, market_latitude, market_longitude)
VALUES ('e933aa44-6f76-4c1d-83de-b385b821c20f', '2022-05-12 18:03:44.013094 +00:00', '35620',
        'New York-Newark-Jersey City, NY-NJ-PA', 'New York', 'NY', 40.697403, -74.120106);

INSERT INTO omni_channel_active_leap_markets (id, created_at, cbsa_code, market, city,
                                              state_abbreviation, market_latitude, market_longitude)
VALUES ('01ac13b4-488d-4213-a663-c2098cf81645', '2022-05-12 18:03:44.013094 +00:00', '31080',
        'Los Angeles-Long Beach-Anaheim, CA', 'Los Angeles', 'CA', 34.0201598, -118.6925967);

INSERT INTO omni_channel_active_leap_markets (id, created_at)
VALUES ('8da46054-bf3a-4da9-9403-398f190b9635', '2022-05-12 18:03:44.013094 +00:00');

INSERT INTO client_omni_channel_insights_top_products_net_sales (id, created_at, client_handle,
                                                                 product_id, product_title,
                                                                 product_net_sales,
                                                                 total_brand_net_sales,
                                                                 product_percent_of_net_sales,
                                                                 product_net_sales_rank,
                                                                 product_count)
VALUES ('b8488c72-3320-4000-8a56-544b7a61fa33', '2022-05-12 19:07:02.043316 +00:00',
        'silk-laundry', 4326878773315, '90S SLIP DRESS HAZELNUT', 7161.08, 220732.81,
        0.03244229981034537, 4, 257);

INSERT INTO client_omni_channel_insights_top_products_net_sales (id, created_at, client_handle,
                                                                 product_id, product_title,
                                                                 product_net_sales,
                                                                 total_brand_net_sales,
                                                                 product_percent_of_net_sales,
                                                                 product_net_sales_rank,
                                                                 product_count)
VALUES ('4e70d6ed-eb61-46be-a33e-549b509c97eb', '2022-05-12 19:07:02.043316 +00:00', 'silk-laundry',
        4594344722499, '90S SLIP DRESS EMERALD', 5756.75, 220732.81, 0.026080173581806892, 5, 258);

INSERT INTO client_omni_channel_insights_top_products_net_sales (id, created_at, client_handle)
VALUES ('85ac203a-422f-4cb2-961e-8fc628a16e16', '2022-05-12 19:07:02.043316 +00:00', 'empty_net');
