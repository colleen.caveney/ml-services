INSERT INTO services (
    id, handle, type, created_at, updated_at, config, api_key, api_secret
) VALUES (
    '5fdf32c7-e678-4955-9a06-72a1bb7bd88a', 'shopify', 'shopify',
    '2022-01-05 23:35:53.100000 +00:00', '2022-01-06 23:35:53.100000 +00:00',
    '{"app_id":"shopify-ext-id", "landing_page":"http://leap-landing-page", "sig_check": "ok"}',
    'key', 'secret');

INSERT INTO services (
    id, handle, type, created_at, updated_at, config, api_key, api_secret
) VALUES (
    '330cc36974934030b5270fadbb22baab', 'shopify-legacy', 'shopify',
    '2023-01-11 23:35:53.100000 +00:00', '2023-01-11 23:35:53.100000 +00:00',
    '{"app_id":"shopify-legacy-id", "landing_page":"http://leap-landing-page"}',
    'key', 'secret');
