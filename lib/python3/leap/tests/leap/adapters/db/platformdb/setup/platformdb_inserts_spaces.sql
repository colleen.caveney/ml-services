INSERT INTO spaces(id, status, internal, created_at, updated_at, city_code, address, city, state, zip_code, latitude,
                   longitude, sqft)
VALUES ('82b83166-b387-44b2-80af-1ab4c20b0c50', 'OCCUPIED', false, '2022-01-07 23:35:53.100000 +00:00',
        '2022-01-08 23:35:53.100000 +00:00', '123', 'Fake St', 'nowhere', 'atlantis', '123', 0.0, 0.0, 1);

INSERT INTO spaces(id, status, internal, created_at, updated_at, city_code, address, city, state, zip_code, latitude,
                   longitude, sqft)
VALUES ('1f0d08aa-0956-43b7-9f78-f758d7cdfe94', 'OCCUPIED', false, '2022-01-08 23:35:53.100000 +00:00',
        '2022-01-09 23:35:53.100000 +00:00', '124', 'Fake Ave', 'somewhere', 'state name', '124', 0.0, 0.0, 1);

INSERT INTO spaces(id, status, internal, created_at, updated_at, city_code, address, city, state, zip_code, latitude,
                   longitude, sqft)
VALUES ('b1bb25b4-4a14-4e58-9caa-dd843d3e109b', 'OCCUPIED', false, '2022-01-10 23:35:53.100000 +00:00',
        '2022-01-11 23:35:53.100000 +00:00', '125', 'Fake Blv', 'anywhere', 'canyon', '125', 0.0, 0.0, 1);
