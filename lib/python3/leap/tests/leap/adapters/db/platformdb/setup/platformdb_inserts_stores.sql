INSERT INTO stores(id, space_id, brand_id, code, deleted, created_at, updated_at, slack_channel_id, type,
                   possession_date, open_date, close_date, margin, shopper_nps_survey_url)
VALUES ('ee35fe1d-1a9d-4b23-bbe7-e87c212efe26', '82b83166-b387-44b2-80af-1ab4c20b0c50', '965b3d10-8954-424e-914c-0b64b6d9649d',
        'BOBBEL-01', false, '2022-01-01 23:35:53.100000 +00:00',
        '2022-01-08 23:35:53.100000 +00:00', 'abc', Null, '2022-01-09 23:35:53.100000 +00:00', '2022-01-10 23:35:53.100000 +00:00',
        Null, Null, 'survey.com');

INSERT INTO stores(id, space_id, brand_id, code, deleted, created_at, updated_at, slack_channel_id, type,
                   possession_date, open_date, close_date, margin)
VALUES ('46eecfe1-374b-4ed1-8a99-1094ef1c4b1b', '1f0d08aa-0956-43b7-9f78-f758d7cdfe94', '965b3d10-8954-424e-914c-0b64b6d9649d',
        'BOBBEL-02', false, '2022-01-02 23:35:53.100000 +00:00',
        '2022-01-12 23:35:53.100000 +00:00', 'def', Null, '2022-01-13 23:35:53.100000 +00:00', '2022-01-15 23:35:53.100000 +00:00',
        Null, Null);

INSERT INTO stores(id, space_id, brand_id, code, deleted, created_at, updated_at, slack_channel_id, type,
                   possession_date, open_date, close_date, margin)
VALUES ('b3095305-6b31-4c98-b3d4-49cfc1fe89ff', 'b1bb25b4-4a14-4e58-9caa-dd843d3e109b', '965b3d10-8954-424e-914c-0b64b6d9649d',
        'BOBBEL-03', false, '2022-01-03 23:35:53.100000 +00:00',
        '2022-01-17 23:35:53.100000 +00:00', 'ghi', Null, '2022-01-18 23:35:53.100000 +00:00', '2022-01-19 23:35:53.100000 +00:00',
        Null, Null);

INSERT INTO stores (id, space_id, brand_id, code, deleted, created_at, updated_at, slack_channel_id, type,
                           possession_date, open_date, close_date, margin, shopper_nps_survey_url)
VALUES ('16b4d23a-511e-4790-83fb-bbc51d9ae844', '82b83166-b387-44b2-80af-1ab4c20b0c50',
        '8ea2f941-0f9d-4570-b11d-e78809ae833b', 'LINDAB-01', false, '2022-01-04 19:47:55.000000 +00:00',
        '2023-01-04 09:54:05.000000 +00:00', null, 'POS', '2020-01-26 19:22:15.000000 +00:00',
        '2020-02-26 19:22:15.000000 +00:00', null, null, null);

INSERT INTO stores (id, space_id, brand_id, code, deleted, created_at, updated_at, slack_channel_id, type,
                           possession_date, open_date, close_date, margin, shopper_nps_survey_url)
VALUES ('45fc6a90-d2ba-4e7b-a2cb-02d45c882de4', '82b83166-b387-44b2-80af-1ab4c20b0c50',
        '8ea2f941-0f9d-4570-b11d-e78809ae833b', 'LINDAB-02', false, '2022-01-05 19:47:55.000000 +00:00',
        '2023-01-04 09:54:05.000000 +00:00', null, 'POS', '2019-02-25 19:22:15.000000 +00:00',
        '2019-03-25 19:22:15.000000 +00:00', '2019-12-25 19:22:15.000000 +00:00', null, null);

INSERT INTO stores (id, space_id, brand_id, code, deleted, created_at, updated_at, slack_channel_id, type,
                           possession_date, open_date, close_date, margin, shopper_nps_survey_url)
VALUES ('bddbfbd1-be78-4852-ac66-bbc751e7403f', '82b83166-b387-44b2-80af-1ab4c20b0c50',
        '8ea2f941-0f9d-4570-b11d-e78809ae833b', 'LINDAB-03', false, '2022-01-06 19:47:55.000000 +00:00',
        '2023-01-04 09:54:05.000000 +00:00', null, 'POS', '2019-12-25 19:22:15.000000 +00:00',
        '2019-01-25 19:22:15.000000 +00:00', null, null, null);

INSERT INTO stores (id, space_id, brand_id, code, deleted, created_at, updated_at, slack_channel_id, type,
                           possession_date, open_date, close_date, margin, shopper_nps_survey_url)
VALUES ('60ab6b63-0e26-469c-aca8-fe9ae9eea5d7', '82b83166-b387-44b2-80af-1ab4c20b0c50',
        '85ce1ec7-e2ba-40ce-824c-6edb3bc6c315', 'TINABE-01', true, '2022-01-07 19:47:55.000000 +00:00',
        '2023-01-04 09:54:05.000000 +00:00', null, 'POS', '2019-03-25 19:22:15.000000 +00:00',
        '2019-04-25 19:22:15.000000 +00:00', '2019-11-25 19:22:15.000000 +00:00', null, null);

INSERT INTO stores (id, space_id, brand_id, code, deleted, created_at, updated_at, slack_channel_id, type,
                           possession_date, open_date, close_date, margin, shopper_nps_survey_url)
VALUES ('5d4f03f8-2e6f-4598-b39d-64c592b60d92', '82b83166-b387-44b2-80af-1ab4c20b0c50',
        '85ce1ec7-e2ba-40ce-824c-6edb3bc6c315', 'TINABE-02', false, '2022-01-08 19:47:55.000000 +00:00',
        '2023-01-04 09:54:05.000000 +00:00', null, 'POS', '2019-01-01 19:22:15.000000 +00:00',
        '2019-02-01 19:22:15.000000 +00:00', null, null, null);

INSERT INTO stores (id, space_id, brand_id, code, deleted, created_at, updated_at, slack_channel_id, type,
                           possession_date, open_date, close_date, margin, shopper_nps_survey_url)
VALUES ('08927e3b-55f5-4ec9-adcf-168c5028ba7c', '82b83166-b387-44b2-80af-1ab4c20b0c50',
        '85ce1ec7-e2ba-40ce-824c-6edb3bc6c315', 'TINABE-03', true, '2022-01-09 19:47:55.000000 +00:00',
        '2023-01-04 09:54:05.000000 +00:00', null, 'POS', '2019-02-25 19:22:15.000000 +00:00',
        '2019-03-25 19:22:15.000000 +00:00', '2019-12-25 19:22:15.000000 +00:00', null, null);
