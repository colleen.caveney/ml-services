INSERT INTO users (id, identity_service, created_at, updated_at, external_user_id, email,
                   display_name, full_name, is_deleted, deleted_at)
VALUES ('89380209-52bd-446f-9c70-cffe25d34605', 'AUTH0', '2022-01-09 23:35:53.100000 +00:00',
        '2022-01-10 23:35:53.100000 +00:00', 'external1', 'bob@burgers.com', 'Burgers4Life',
        'Bob Belcher', false, Null);

INSERT INTO users (id, identity_service, created_at, updated_at, external_user_id, email,
                   display_name, full_name, is_deleted, deleted_at)
VALUES ('cda48bff-dbed-456d-a700-55a4e5bc4072', 'AUTH0', '2022-01-11 23:35:53.100000 +00:00',
        '2022-01-12 23:35:53.100000 +00:00', 'external2', 'linda@burgers.com', 'WineGirl',
        'Linda Belcher', false, Null);

INSERT INTO users (id, identity_service, created_at, updated_at, external_user_id, email,
                   display_name, full_name, is_deleted, deleted_at)
VALUES ('cc423d89-8197-4bda-b3de-ad97585fed67', 'AUTH0', '2022-01-13 23:35:53.100000 +00:00',
        '2022-01-14 23:35:53.100000 +00:00', 'external3', 'gene@burgers.com', 'KeyKing',
        'Gene Belcher', false, Null);
