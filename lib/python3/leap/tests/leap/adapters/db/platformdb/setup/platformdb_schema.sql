CREATE EXTENSION IF NOT EXISTS pgcrypto; -- adds UUID support
CREATE TABLE client_omni_channel_insights (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    client_handle TEXT NOT NULL, -- currently called `brand` in the warehouse
    gross_profit_margin                     FLOAT,
    ppv_score                               FLOAT,
    fitness_score                           FLOAT,
    orders_last_12_months                   INT,
    net_sales_last_12_months                FLOAT,
    buyers_last_12_months                   INT,
    margin_dollars_last_12_months           FLOAT,
    average_order_value_last_12_months      FLOAT,
    purchase_frequency_last_12_months       FLOAT,
    units_per_transaction_last_12_months    FLOAT,
    lifetime_value_last_12_months           FLOAT,
    refund_rate_last_12_months              FLOAT,
    average_order_value_omni_channel        FLOAT,
    purchase_frequency_omni_channel         FLOAT,
    units_per_transaction_omni_channel      FLOAT,
    lifetime_value_omni_channel             FLOAT,
    refund_rate_omni_channel                FLOAT,
    purchase_frequency_percent_change       FLOAT,
    average_order_value_percent_change      FLOAT,
    refund_rate_percent_change              FLOAT,
    lifetime_value_percent_change           FLOAT,
    top_5_net_sales_percentage              FLOAT,
    top_5_units_sold_percentage             FLOAT,
    total_product_count                     INT,
    top_5_product_concentration             FLOAT,
    top_market_city                         TEXT,
    top_market_state_abbreviation           TEXT,
    total_leap_market_count                 INT
);
CREATE INDEX ON client_omni_channel_insights (client_handle);
CREATE TABLE client_omni_channel_insights_markets (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    client_handle TEXT NOT NULL, -- currently called `brand` in the warehouse
    market	                    TEXT,
    cbsa_code	                TEXT,
    net_sales_last_12_months	FLOAT,
    city	                    TEXT,
    state_abbreviation	        TEXT,
    market_net_sales_rank	    INT,
    is_leap_market	            BOOLEAN
);
CREATE INDEX ON client_omni_channel_insights_markets (client_handle);
CREATE TABLE client_omni_channel_insights_top_products_net_sales (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    client_handle TEXT NOT NULL, -- currently called `brand` in the warehouse
    product_id	                    BIGINT,
    product_title	                TEXT,
    product_net_sales	            FLOAT,
    total_brand_net_sales	        FLOAT,
    product_percent_of_net_sales    FLOAT,
    product_net_sales_rank	        INT,
    product_count	                INT
);
CREATE INDEX ON client_omni_channel_insights_top_products_net_sales (client_handle);
CREATE TABLE client_omni_channel_insights_top_products_units_sold (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    client_handle TEXT NOT NULL, -- currently called `brand` in the warehouse
    product_id	                    BIGINT,
    product_title	                TEXT,
    product_units_sold	            INT,
    total_brand_units_sold	        INT,
    product_percent_of_units_sold   FLOAT,
    product_units_sold_rank	        INT,
    product_count	                INT
);
CREATE INDEX ON client_omni_channel_insights_top_products_units_sold (client_handle);
CREATE TABLE omni_channel_active_leap_markets (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    cbsa_code           TEXT,
    market              TEXT,
    city                TEXT,
    state_abbreviation  TEXT,
    market_latitude     FLOAT,
    market_longitude    FLOAT
);
CREATE OR REPLACE FUNCTION set_updated_at_timestamp() RETURNS TRIGGER
AS $$
BEGIN
    IF (NEW != OLD) THEN
        NEW.updated_at = CURRENT_TIMESTAMP;
        RETURN NEW;
    END IF;
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;
CREATE TYPE client_status AS ENUM (
    'NEW',
    'EVAL',
    'PROSPECTIVE',
    'LIVE',
    'DEPRIORITIZED'
);
CREATE TABLE clients (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    handle TEXT NOT NULL UNIQUE,
    parent_handle TEXT NOT NULL,
    deleted BOOLEAN NOT NULL DEFAULT false,
    status_key client_status NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    name TEXT NOT NULL,
    timezone TEXT NOT NULL DEFAULT 'Etc/UTC',
    website TEXT,
    contact_name TEXT,
    contact_email TEXT,
    contact_phone TEXT,
    config JSONB NOT NULL DEFAULT '{}',
    client_details JSONB NOT NULL DEFAULT '{}'
);
CREATE TRIGGER clients_updated_at_timestamp BEFORE UPDATE ON clients
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TYPE service_type AS ENUM (
    'shopify',
    'klaviyo',
    'ga',
    'newstore'
);
CREATE TABLE services (
    id          UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    handle      TEXT NOT NULL UNIQUE,
    type        service_type NOT NULL,
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    config      JSONB NOT NULL DEFAULT '{}',
    api_key     TEXT,
    api_secret  TEXT
);
CREATE TRIGGER services_updated_at_timestamp BEFORE UPDATE ON services
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TABLE adapters (
    id              UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    client_id       UUID NOT NULL REFERENCES clients (id),
    service_id      UUID NOT NULL REFERENCES services (id),
    created_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    internal        BOOLEAN NOT NULL,
    instance_id     TEXT NOT NULL DEFAULT 'GLOBAL' CHECK (instance_id <> ''),
    source          BOOLEAN NOT NULL,
    sink            BOOLEAN NOT NULL,
    track_inventory BOOLEAN NOT NULL DEFAULT false,
    config          JSONB NOT NULL DEFAULT '{}',
    filters         JSONB NOT NULL DEFAULT '{}',
    data_state      JSONB NOT NULL DEFAULT '{}'
);
CREATE TRIGGER adapters_updated_at_timestamp BEFORE UPDATE ON adapters
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX adapters_client_idx ON adapters (client_id);
CREATE INDEX adapters_service_idx ON adapters (service_id);
CREATE TABLE fs2pgstream_bench (
    -- this should be defined upstream, never in platformdb
    id UUID PRIMARY KEY NOT NULL,
    run TEXT NOT NULL,
    -- defined by the host/script generating data updates
    host_created_at TIMESTAMPTZ NOT NULL,
    host_updated_at TIMESTAMPTZ NOT NULL,
    -- taken from the firestore event itself
    fs_created_at TIMESTAMPTZ NOT NULL,
    fs_updated_at TIMESTAMPTZ NOT NULL,
    -- DO let the db handle this, for consistency
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE TRIGGER fs2pgstream_bench_updated_at_timestamp BEFORE UPDATE ON fs2pgstream_bench
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX fs2pgstream_bench_run_idx ON fs2pgstream_bench (run);
ALTER TYPE service_type ADD VALUE IF NOT EXISTS 'looker';
create extension if not exists "uuid-ossp";
CREATE TABLE adapter_webhooks (
    id              UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    adapter_id      UUID NOT NULL REFERENCES adapters (id),
    created_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    -- leap's type/name for this event
    event_type TEXT NOT NULL,
    -- the type/name of the event as the adapter's service calls it
    adapter_event_type TEXT NOT NULL,
    -- the id of the event returned by the adapter's service
    adapter_webhook_id TEXT NOT NULL
);
CREATE TRIGGER adapter_webhooks_updated_at_timestamp BEFORE UPDATE ON adapter_webhooks
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX adapters_webhooks_adapter_idx ON adapter_webhooks (adapter_id);
CREATE TYPE identity_service_type AS ENUM (
    'AUTH0'
);
CREATE TABLE users (
    id                  UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    identity_service    identity_service_type NOT NULL,
    created_at          TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at          TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    -- the unique id of the User in the identity-service
    external_user_id TEXT NOT NULL
);
CREATE TRIGGER users_updated_at_timestamp BEFORE UPDATE ON users
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TABLE client_users (
    id          UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    user_id     UUID NOT NULL REFERENCES users (id),
    client_id   UUID NOT NULL REFERENCES clients (id),
    enabled     BOOLEAN NOT NULL DEFAULT true,
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at  TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE TRIGGER client_users_updated_at_timestamp BEFORE UPDATE ON client_users
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TYPE notification_type AS ENUM (
    'EMAIL'
);
CREATE TYPE action_type AS ENUM (
    'NEW_BRAND',
    'ADAPTER_ATTEMPT',
    'ADAPTER_CONNECTED',
    'INITIAL_IMPORT_COMPLETE',
    'ONBOARD_SURVEY_COMPLETE'
);
CREATE TABLE subscriptions (
    id                  UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    notification_type   notification_type NOT NULL,
    action_type         action_type NOT NULL,
    created_at          TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at          TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    data    JSONB NOT NULL DEFAULT '{}'
);
CREATE TRIGGER subscriptions_updated_at_timestamp BEFORE UPDATE ON subscriptions
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TABLE actions (
    id          UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    client_id   UUID NOT NULL REFERENCES clients (id),
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    -- TODO: we need to unroll/unnest/normalize this field such that we can create
    --   FKs and relationships between an action and a subscription
    matched_subscription_ids    JSONB NOT NULL DEFAULT '[]',
    message                     JSONB NOT NULL DEFAULT '{}'
);
CREATE TRIGGER actions_updated_at_timestamp BEFORE UPDATE ON actions
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TYPE storage_service_type AS ENUM (
    'gcs'
);
CREATE TYPE asset_type AS ENUM (
    'FINANCIAL',
    'COMMON',
    'STORE_DATA'
);
CREATE TABLE client_assets (
    id              UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    client_id       UUID NOT NULL REFERENCES clients (id),
    created_by_id   UUID NOT NULL REFERENCES users (id),
    type            asset_type NOT NULL,
    storage_service storage_service_type NOT NULL DEFAULT 'gcs',
    created_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted         BOOLEAN NOT NULL DEFAULT false,
    bucket          TEXT NOT NULL,
    path            TEXT NOT NULL,
    name            TEXT NOT NULL
);
CREATE TYPE location_status_type AS ENUM (
    'EVAL',
    'DEPRIORITIZED',
    'OCCUPIED',
    'UNOCCUPIED',
    'ARCHIVED'
);
CREATE TABLE locations ( -- aka spaces
    id          UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    status      location_status_type NOT NULL,
    internal    BOOLEAN NOT NULL DEFAULT false,
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    city_code   TEXT,
    address     TEXT,
    city        TEXT,
    state       TEXT,
    zip_code    TEXT,
    latitude    FLOAT NOT NULL DEFAULT 0.0,
    longitude   FLOAT NOT NULL DEFAULT 0.0,
    sqft        INT NOT NULL DEFAULT 0
);
CREATE TABLE stores (
    id          UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    location_id UUID NOT NULL REFERENCES locations (id),
    client_id   UUID NOT NULL REFERENCES clients (id),
    code        TEXT NOT NULL UNIQUE,
    deleted     BOOLEAN NOT NULL DEFAULT true,
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    slack_channel_id    TEXT,
    type                TEXT,
    possession_date     TIMESTAMPTZ,
    open_date           TIMESTAMPTZ,
    close_date          TIMESTAMPTZ
);
CREATE TABLE adapter_locations (
    id              UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    adapter_id      UUID NOT NULL REFERENCES adapters (id),
    store_id        UUID NOT NULL REFERENCES stores (id),
    created_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    is_primary          BOOLEAN NOT NULL, -- fka 'primary', which is not a valid column name
    enabled             BOOLEAN NOT NULL DEFAULT true,
    adapter_location_id TEXT NOT NULL,
    virtual_store_code  TEXT
);
CREATE TABLE service_access_tiers (
    id              UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    service_id      UUID NOT NULL REFERENCES services (id),
    handle          TEXT NOT NULL,
    created_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    UNIQUE (service_id, handle)
);
CREATE TABLE service_access_privileges (
    id                      UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    service_access_tier_id  UUID NOT NULL REFERENCES service_access_tiers (id),
    privilege               TEXT,
    created_at              TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at              TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    UNIQUE (service_access_tier_id, privilege)
);
ALTER TABLE users
    ADD COLUMN email TEXT UNIQUE,
    ADD COLUMN first_name TEXT,
    ADD COLUMN last_name TEXT;
ALTER TABLE users
    DROP COLUMN first_name,
    DROP COLUMN last_name,
    ADD COLUMN display_name TEXT,
    ADD COLUMN full_name TEXT;
ALTER TABLE locations RENAME TO spaces;
ALTER TYPE location_status_type RENAME TO space_status_type;
ALTER TABLE stores RENAME COLUMN location_id TO space_id;
ALTER TABLE stores RENAME CONSTRAINT "stores_location_id_fkey" TO "stores_space_id_fkey";
CREATE TRIGGER spaces_updated_at_timestamp BEFORE UPDATE ON spaces
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TRIGGER stores_updated_at_timestamp BEFORE UPDATE ON stores
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TRIGGER client_assets_updated_at_timestamp BEFORE UPDATE ON client_assets
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TRIGGER adapter_locations_updated_at_timestamp BEFORE UPDATE ON adapter_locations
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TRIGGER service_access_tiers_updated_at_timestamp BEFORE UPDATE ON service_access_tiers
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TRIGGER service_access_privileges_updated_at_timestamp BEFORE UPDATE ON service_access_privileges
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
ALTER TABLE adapter_locations RENAME COLUMN adapter_location_id TO external_id;
ALTER TABLE adapter_locations RENAME TO adapter_stores;
ALTER TABLE adapter_stores RENAME CONSTRAINT
    "adapter_locations_pkey" TO "adapter_stores_pkey";
ALTER TABLE adapter_stores RENAME CONSTRAINT
    "adapter_locations_adapter_id_fkey" TO "adapter_stores_adapter_id_fkey";
ALTER TABLE adapter_stores RENAME CONSTRAINT
    "adapter_locations_store_id_fkey" TO "adapter_stores_store_id_fkey";
ALTER TRIGGER adapter_locations_updated_at_timestamp ON adapter_stores
    RENAME TO adapter_stores_updated_at_timestamp;
ALTER TABLE spaces RENAME CONSTRAINT
    "locations_pkey" TO "spaces_pkey";
CREATE INDEX adapter_stores_external_lookup_idx ON adapter_stores (adapter_id, external_id);
ALTER TABLE stores ADD COLUMN IF NOT EXISTS margin NUMERIC(7,4);
CREATE TABLE products (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    client_id       UUID NOT NULL REFERENCES clients (id),
    created_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    title TEXT NOT NULL
);
CREATE TRIGGER products_updated_at_timestamp BEFORE UPDATE ON products
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX products_client_idx ON products (client_id);
CREATE TABLE adapter_products (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    adapter_id       UUID NOT NULL REFERENCES adapters (id),
    product_id       UUID NOT NULL REFERENCES products (id),
    created_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    external_id      TEXT NOT NULL,
    UNIQUE (adapter_id, product_id)
);
CREATE TRIGGER adapter_products_updated_at_timestamp BEFORE UPDATE ON adapter_products
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX adapter_products_lookup_idx ON adapter_products (adapter_id, external_id, product_id);
CREATE TABLE variants (
    id          UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    product_id  UUID NOT NULL REFERENCES products (id),
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    title   TEXT NOT NULL,
    sku     TEXT NOT NULL
);
CREATE TRIGGER variants_updated_at_timestamp BEFORE UPDATE ON variants
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX variants_product_idx ON variants (product_id);
CREATE TABLE adapter_variants (
    id          UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    adapter_id  UUID NOT NULL REFERENCES adapters (id),
    variant_id  UUID NOT NULL REFERENCES variants (id),
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    external_id             TEXT NOT NULL,
    external_inventory_id   TEXT NOT NULL,
    UNIQUE (adapter_id, variant_id)
);
CREATE TRIGGER adapter_variants_updated_at_timestamp BEFORE UPDATE ON adapter_variants
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX adapter_variants_lookup_idx ON adapter_variants (adapter_id, external_inventory_id, variant_id);
CREATE TABLE inventory_levels (
    id          UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    store_id    UUID NOT NULL REFERENCES stores (id),
    variant_id  UUID NOT NULL REFERENCES variants (id),
    created_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at  TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    level       INT NOT NULL DEFAULT 0
);
CREATE TRIGGER inventory_levels_updated_at_timestamp
    BEFORE UPDATE ON inventory_levels
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX inventory_levels_store_variant_idx ON inventory_levels (store_id, variant_id);
CREATE INDEX adapter_stores_lookup_idx ON adapter_stores (adapter_id, external_id, store_id);
ALTER TABLE clients RENAME TO brands;
ALTER TABLE brands RENAME COLUMN client_details TO brand_details;
ALTER TYPE action_type ADD VALUE IF NOT EXISTS 'NEW_USER_CHANGE_PASSWORD';
CREATE TYPE message_type AS ENUM (
    'EMAIL'
);
ALTER TYPE action_type ADD VALUE IF NOT EXISTS 'NEW_USER_CHANGE_PASSWORD';
CREATE TABLE messages (
    id                  UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    message_type        message_type NOT NULL,
    action_type         action_type NOT NULL,
    created_at          TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at          TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    recipients          JSONB NOT NULL DEFAULT '[]',
    message_data        JSONB NOT NULL DEFAULT '{}'
);
CREATE TRIGGER messages_updated_at_timestamp BEFORE UPDATE ON messages
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TABLE audit_logs (
    id                 UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    message_id         UUID NOT NULL REFERENCES messages (id),
    action_type        action_type NOT NULL,
    created_at         TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at         TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    properties         JSONB NOT NULL DEFAULT '{}'
);
CREATE TRIGGER audit_logs_updated_at_timestamp BEFORE UPDATE ON audit_logs
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
ALTER TABLE brands ADD COLUMN IF NOT EXISTS brand_code TEXT;
ALTER TABLE adapters RENAME COLUMN client_id TO brand_id;
ALTER TABLE client_assets RENAME TO brand_assets;
ALTER TABLE brand_assets RENAME COLUMN client_id TO brand_id;
ALTER TABLE products RENAME COLUMN client_id TO brand_id;
ALTER TABLE client_users RENAME TO brand_users;
ALTER TABLE brand_users RENAME COLUMN client_id TO brand_id;
ALTER TABLE stores RENAME COLUMN client_id TO brand_id;
ALTER TABLE actions RENAME COLUMN client_id TO brand_id;
ALTER TABLE adapter_webhooks ADD COLUMN IF NOT EXISTS omcp_version INTEGER;
ALTER TABLE users
    ADD COLUMN is_deleted BOOLEAN NOT NULL DEFAULT false,
    ADD COLUMN deleted_at TIMESTAMPTZ;
ALTER TYPE client_status RENAME TO brand_status;
CREATE TABLE brand_profiles (
    id uuid primary key not null default gen_random_uuid(),
    brand_id uuid not null constraint brand_id_brand_profile_fkey references brands,
    created_at timestamp with time zone default now()             not null,
    updated_at timestamp with time zone default now()             not null,
    files_url text,
    overview_updated_by uuid not null constraint user_id_brand_profile_overview_fkey references users,
    overview jsonb,
    vm_updated_by uuid not null constraint user_id_brand_profile_vm_fkey references users,
    visual_merchandising jsonb
);
CREATE INDEX brand_profiles_brand_id_idx ON brand_profiles (brand_id);
CREATE TRIGGER brand_profiles_updated_at_timestamp BEFORE UPDATE ON brand_profiles
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
ALTER TABLE brand_profiles ADD CONSTRAINT unique_brand_profiles_brand_id UNIQUE (brand_id);
ALTER TABLE brands ADD COLUMN IF NOT EXISTS box_link text;
ALTER TABLE brand_profiles DROP COLUMN IF EXISTS files_url;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS bm_updated_by uuid constraint user_id_brand_profile_bm_fkey references users;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS brand_merchandising jsonb;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS store_design_updated_by uuid constraint user_id_brand_profile_store_design_fkey references users;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS store_design jsonb;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS marketing_updated_by uuid constraint user_id_brand_profile_marketing_fkey references users;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS marketing jsonb;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS store_operations_updated_by uuid constraint user_id_brand_profile_store_operations_fkey references users;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS store_operations jsonb;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS integrations_updated_by uuid constraint user_id_brand_profile_integrations_fkey references users;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS integrations jsonb;
ALTER TABLE brand_profiles RENAME COLUMN brand_merchandising to product_merchandising;
ALTER TABLE brand_profiles RENAME COLUMN bm_updated_by to pm_updated_by;
ALTER TABLE brand_profiles RENAME COLUMN pm_updated_by to product_merchandising_updated_by;
ALTER TABLE brand_profiles RENAME COLUMN vm_updated_by to visual_merchandising_updated_by;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS social_updated_by uuid constraint user_id_brand_profile_social_fkey references users;
ALTER TABLE brand_profiles ADD COLUMN IF NOT EXISTS social jsonb;
ALTER TABLE stores ADD COLUMN IF NOT EXISTS typeform_url text;
ALTER TABLE stores RENAME COLUMN typeform_url to shopper_nps_survey_url;
ALTER TABLE users ADD COLUMN IF NOT EXISTS title TEXT;
ALTER TABLE users ADD COLUMN IF NOT EXISTS phone_number TEXT;
ALTER TYPE action_type
ADD VALUE IF NOT EXISTS 'BRAND_PROFILE_SUBMIT';
ALTER TYPE action_type
ADD
    VALUE IF NOT EXISTS 'BRAND_PROFILE_SAVE';
ALTER TYPE action_type
ADD
    VALUE IF NOT EXISTS 'ASSET_UPLOADED';
ALTER TYPE action_type
ADD
    VALUE IF NOT EXISTS 'ADMIN_ASSET_UPLOADED';
ALTER TYPE action_type
ADD
    VALUE IF NOT EXISTS 'ADAPTER_UNINSTALLED';
ALTER TABLE spaces ADD COLUMN IF NOT EXISTS unit TEXT;
ALTER TABLE spaces ADD COLUMN IF NOT EXISTS threshold_rssi TEXT;
ALTER TABLE services ADD COLUMN IF NOT EXISTS oauth_access_scopes TEXT;
CREATE TYPE service_access_tier AS ENUM (
    'full',
    'ro',
    'fuller'
);
ALTER TABLE service_access_privileges
ADD COLUMN IF NOT EXISTS tier service_access_tier; 
ALTER TABLE service_access_privileges
ADD COLUMN IF NOT EXISTS service_id UUID references services;
ALTER TABLE service_access_privileges
ADD CONSTRAINT service_access_privileges_service_id_tier_privilege_key UNIQUE (service_id, tier, privilege);
ALTER TABLE service_access_privileges
DROP COLUMN IF EXISTS service_access_tier_id; 
DROP TABLE IF EXISTS service_access_tiers;
ALTER TABLE services
    DROP COLUMN oauth_access_scopes,
    ADD COLUMN oauth_access_scopes jsonb DEFAULT '[]';
ALTER TYPE action_type
ADD VALUE IF NOT EXISTS 'ADMIN_ASSET_UPLOADED';
ALTER TABLE actions
ADD COLUMN IF NOT EXISTS action_type action_type;
ALTER TABLE brands DROP COLUMN parent_handle;
ALTER TABLE adapters
ADD CONSTRAINT instance_id_brand_id_service_id_unique UNIQUE(instance_id, brand_id, service_id);
CREATE TABLE ephemeral_shopify_installs (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    instance_id     TEXT NOT NULL CHECK (instance_id <> ''),
    created_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at      TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    UNIQUE(instance_id)
);
CREATE TRIGGER ephemeral_shopify_installs_updated_at_timestamp BEFORE UPDATE ON ephemeral_shopify_installs
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE TYPE brand_role AS ENUM (
    'PRODUCTION',
    'STAGE',
    'DEMO',
    'DUPLICATE',
    'NEW'
);
ALTER TABLE brands
    ADD COLUMN IF NOT EXISTS role brand_role;
CREATE TABLE related_brands
(
    id         UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    parent_id  UUID             NOT NULL CONSTRAINT parent_brand_id_fkey references brands,
    child_id   UUID             NOT NULL CONSTRAINT child_brand_id_fkey references brands,
    created_at TIMESTAMPTZ      NOT NULL DEFAULT NOW(),
    UNIQUE (child_id)
);
ALTER TABLE users
DROP CONSTRAINT users_email_key;
CREATE TABLE orders (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    brand_id UUID NOT NULL REFERENCES brands (id),
    name TEXT,
    payment_status TEXT,
    hashes JSONB NOT NULL DEFAULT '{}',
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE TRIGGER orders_updated_at_timestamp BEFORE UPDATE ON orders
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX orders_brand_idx ON orders (brand_id);
CREATE TABLE adapter_orders (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    order_id UUID NOT NULL REFERENCES orders (id),
    adapter_id UUID NOT NULL REFERENCES adapters (id),
    external_id TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE TRIGGER adapter_orders_updated_at_timestamp BEFORE UPDATE ON adapter_orders
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX adapter_orders_order_idx ON adapter_orders (order_id);
CREATE INDEX adapter_orders_adapter_idx ON adapter_orders (adapter_id);
CREATE TABLE fulfillments (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    order_id UUID NOT NULL REFERENCES orders (id),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE TRIGGER fulfillments_updated_at_timestamp BEFORE UPDATE ON fulfillments
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX fulfillments_order_idx ON fulfillments (order_id);
CREATE TABLE adapter_fulfillments (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    fulfillment_id UUID NOT NULL REFERENCES fulfillments (id),
    adapter_id UUID NOT NULL REFERENCES adapters (id),
    external_id TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE TRIGGER adapter_fulfillments_updated_at_timestamp BEFORE UPDATE ON adapter_fulfillments
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX adapter_fulfillments_fulfillment_idx ON adapter_fulfillments (fulfillment_id);
CREATE INDEX adapter_fulfillments_adapter_idx ON adapter_fulfillments (adapter_id);
CREATE TABLE line_items (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    order_id UUID NOT NULL REFERENCES orders (id),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE TRIGGER line_items_updated_at_timestamp BEFORE UPDATE ON line_items
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX line_items_order_idx ON line_items (order_id);
CREATE TABLE adapter_line_items (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    line_item_id UUID NOT NULL REFERENCES line_items (id),
    adapter_id UUID NOT NULL REFERENCES adapters (id),
    external_id TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE TRIGGER adapter_line_items_updated_at_timestamp BEFORE UPDATE ON adapter_line_items
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX adapter_line_items_line_item_idx ON adapter_line_items (line_item_id);
CREATE INDEX adapter_line_items_adapter_idx ON adapter_line_items (adapter_id);
CREATE TABLE refunds (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    order_id UUID NOT NULL REFERENCES orders (id),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE TRIGGER refunds_updated_at_timestamp BEFORE UPDATE ON refunds
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX refunds_order_idx ON refunds (order_id);
CREATE TABLE adapter_refunds (
    id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
    refund_id UUID NOT NULL REFERENCES refunds (id),
    adapter_id UUID NOT NULL REFERENCES adapters (id),
    external_id TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE TRIGGER adapter_refunds_updated_at_timestamp BEFORE UPDATE ON adapter_refunds
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
CREATE INDEX adapter_refunds_refund_idx ON adapter_refunds (refund_id);
CREATE INDEX adapter_refunds_adapter_idx ON adapter_refunds (adapter_id);
ALTER TABLE ephemeral_shopify_installs
ADD COLUMN access_token TEXT
;
CREATE TABLE select_spaces
(
    id                   uuid primary key not null default gen_random_uuid(),
    brand_id             uuid not null constraint brand_id_reserve_store_fkey references brands,
    user_id              uuid not null constraint user_id_reserve_store_fkey references users,
    salesforce_space_id  text not null,
    created_at           timestamp with time zone default now() not null,
    updated_at           timestamp with time zone default now() not null,
    UNIQUE (brand_id, salesforce_space_id)
);
CREATE INDEX select_spaces_brand_id_idx ON select_spaces (brand_id);
CREATE TRIGGER select_spaces_updated_at_timestamp BEFORE UPDATE ON select_spaces
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
ALTER TYPE action_type
ADD VALUE IF NOT EXISTS 'SELECT_SPACE_SAVE';
CREATE TABLE salesforce_space_images
(
    salesforce_space_id  text primary key not null,
    image_urls           JSONB DEFAULT '[]',
    created_at           timestamp with time zone default now() not null,
    updated_at           timestamp with time zone default now() not null
);
CREATE TRIGGER salesforce_space_images_updated_at_timestamp BEFORE UPDATE ON select_spaces
    FOR EACH ROW EXECUTE PROCEDURE set_updated_at_timestamp();
ALTER TABLE stores
ALTER COLUMN space_id
DROP NOT NULL;
ALTER TABLE fulfillments
ADD COLUMN hash text,
ADD COLUMN status text;
