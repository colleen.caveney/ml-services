import datetime
import os

from leap.adapters.db.models.model_helpers import check_url


class TestModelHelpers:
    def test_invalid_urls(self):
        assert check_url("aaa") is False
        assert check_url("aaa.c") is False
        assert check_url("aaa..com") is False

        assert check_url("www.aaa.c") is False
        assert check_url("www.aaa..com") is False

        assert check_url("http://www.aaa.c") is False
        assert check_url("http://www.aaa..com") is False
        assert check_url("https://www.aaa.c") is False
        assert check_url("https://www.aaa..com") is False
        assert check_url("     foo.com        baz") is False
        assert check_url("hello world") is False

    def test_valid_urls(self):
        assert check_url("foo.com")
        assert check_url("foo.com/bar")
        assert check_url("     foo.com/bar        ")
        assert check_url("www.foo.com")
        assert check_url("www.foo.com/bar")
        assert check_url("http://www.foo.com")
        assert check_url("http://www.foo.com/bar")
        assert check_url("https://www.foo.com")
        assert check_url("https://www.foo.com/bar")
