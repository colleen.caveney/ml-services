import datetime
import os

from leap.adapters.db._models import Action, ActionType
from leap.adapters.db.platformdb import PagerResultsInternal
from _platformdb_setup import BaseAPITestPlatformDB

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brands.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_actions.sql"),
]

class TestPlatformDBActions(BaseAPITestPlatformDB):
    def setup_method(self):
        self.load_data(INSERT_FILES)
        self._prepare_expected()

    def _prepare_expected(self):
        self.expected_action1 = Action(
            id="545f203e7af144d5b5db62a8e6c7d38d",
            brand_id="965b3d108954424e914c0b64b6d9649d",
            created_at=datetime.datetime(2022, 1, 7, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 8, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            matched_subscription_ids=["4340a78374274e12a561150bbed241ba"],
            message={"body": "body-a", "title": "title-a"},
            action_type=ActionType.NEW_BRAND)
        self.expected_action2 = Action(
            id="7b529c7d9aef4f1890d11ffdc0a10744",
            brand_id="965b3d108954424e914c0b64b6d9649d",
            created_at=datetime.datetime(2022, 1, 9, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 10, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            matched_subscription_ids=["ed725ca683664e0fbe11b83e0474b75d"],
            message={"body": "body-b", "title": "title-b"},
            action_type=ActionType.NEW_BRAND)
        self.expected_action3 = Action(
            id="d2745318781f4c9d97b42818133a3b80",
            brand_id="965b3d108954424e914c0b64b6d9649d",
            created_at=datetime.datetime(2022, 1, 11, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 12, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            matched_subscription_ids=["a846845a2e1443c2ba96305a5abae801"],
            message={"body": "body-c", "title": "title-c"},
            action_type=ActionType.NEW_BRAND)


    def test_get_action_paging_asc(self):
        expected_results = PagerResultsInternal(
            [self.expected_action1, self.expected_action2, self.expected_action3],
            3,
            10,
            0,
            1,
            1
        )
        actual = self.platformDB.get_all_actions_paging("created_at", "ASC", 10, 1)
        assert actual == expected_results

    def test_get_action_paging_desc(self):
        expected_results = PagerResultsInternal(
            [self.expected_action3, self.expected_action2, self.expected_action1],
            3,
            10,
            0,
            1,
            1
        )
        actual = self.platformDB.get_all_actions_paging("created_at", "DESC", 10, 1)
        assert actual == expected_results

    def test_get_action_paging_limit(self):
        expected_results = PagerResultsInternal(
            [self.expected_action3, self.expected_action2],
            3,
            2,
            0,
            2,
            1
        )
        actual = self.platformDB.get_all_actions_paging("created_at", "DESC", 2, 1)
        assert actual == expected_results

    def test_get_action_paging_limit_2nd_page(self):
        expected_results = PagerResultsInternal(
            [self.expected_action1],
            3,
            2,
            2,
            2,
            2
        )
        actual = self.platformDB.get_all_actions_paging("created_at", "DESC", 2, 2)
        assert actual == expected_results
