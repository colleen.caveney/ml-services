import datetime
import os


from leap.adapters.db._models import AdapterStore
from leap.adapters.db.platformdb import PagerResultsInternal
from _platformdb_setup import BaseAPITestPlatformDB

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brands.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_services.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_spaces.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_adapters.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_stores.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_adapter_stores.sql")
]

class TestPlatformDBAdapterStores(BaseAPITestPlatformDB):
    def setup_method(self):
        self.load_data(INSERT_FILES)
        self._prepare_expected()

    def _prepare_expected(self):
        self.expected_store1 = AdapterStore(id='82367fbf50474067af9d97b6fe134fe8',
                                               adapter_id='a4d0fa9731764b8e9331d6706b1e6598',
                                               store_id='ee35fe1d1a9d4b23bbe7e87c212efe26',
                                               created_at=datetime.datetime(2022, 1, 7, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               updated_at=datetime.datetime(2022, 1, 8, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               primary=True,
                                               is_primary=True,
                                               enabled=True,
                                               external_id='123',
                                               virtual_store_code="vrt-A",
                                               virtual_store_id="vrt-A")
        self.expected_store2 = AdapterStore(id='a1edef29437d4f3f9186854702b59f09',
                                               adapter_id='a4d0fa9731764b8e9331d6706b1e6598',
                                               store_id='46eecfe1374b4ed18a991094ef1c4b1b',
                                               created_at=datetime.datetime(2022, 1, 9, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               updated_at=datetime.datetime(2022, 1, 10, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               primary=True,
                                               is_primary=True,
                                               enabled=True,
                                               external_id='456',
                                               virtual_store_code="vrt-B",
                                               virtual_store_id="vrt-B")
        self.expected_store3 = AdapterStore(id='28ef5dda5a4e4af09e227e678a5deff9',
                                               adapter_id='a4d0fa9731764b8e9331d6706b1e6598',
                                               store_id='b30953056b314c98b3d449cfc1fe89ff',
                                               created_at=datetime.datetime(2022, 1, 11, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               updated_at=datetime.datetime(2022, 1, 12, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               primary=False,
                                               is_primary=False,
                                               enabled=True,
                                               external_id='789',
                                               virtual_store_code=None,
                                               virtual_store_id=None)

    def test_get_adapter_store_paging_asc(self):
        expected_results = PagerResultsInternal(
            [self.expected_store1, self.expected_store2, self.expected_store3],
            3,
            10,
            0,
            1,
            1
        )
        actual = self.platformDB.get_all_adapter_stores_paging("created_at", "ASC", 10, 1)
        assert actual == expected_results

    def test_get_adapter_store_paging_desc(self):
        expected_results = PagerResultsInternal(
            [self.expected_store3, self.expected_store2, self.expected_store1],
            3,
            10,
            0,
            1,
            1
        )
        actual = self.platformDB.get_all_adapter_stores_paging("created_at", "DESC", 10, 1)
        assert actual == expected_results

    def test_get_adapter_store_paging_limit(self):
        expected_results = PagerResultsInternal(
            [self.expected_store3, self.expected_store2],
            3,
            2,
            0,
            2,
            1
        )
        actual = self.platformDB.get_all_adapter_stores_paging("created_at", "DESC", 2, 1)
        assert actual == expected_results

    def test_get_adapter_store_paging_limit_2nd_page(self):
        expected_results = PagerResultsInternal(
            [self.expected_store1],
            3,
            2,
            2,
            2,
            2
        )
        actual = self.platformDB.get_all_adapter_stores_paging("created_at", "DESC", 2, 2)
        assert actual == expected_results
