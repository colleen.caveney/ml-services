import datetime
import os

from leap.adapters.db._models import AdapterConfig
from leap.adapters.db.platformdb import PagerResultsInternal
from _platformdb_setup import BaseAPITestPlatformDB

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brands.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_services.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_adapters.sql")
]

class TestPlatformDBAdapters(BaseAPITestPlatformDB):
    def setup_method(self):
        self.load_data(INSERT_FILES)
        self._prepare_expected()

    def _prepare_expected(self):
        self.expected_adapter1 = AdapterConfig(id='a4d0fa9731764b8e9331d6706b1e6598',
                                               brand_id='965b3d108954424e914c0b64b6d9649d',
                                               service_id='5fdf32c7e67849559a0672a1bb7bd88a',
                                               service_type="shopify",
                                               created_at=datetime.datetime(2022, 1, 7, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               updated_at=datetime.datetime(2022, 1, 8, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               internal=True,
                                               instance_id='instance1')
        self.expected_adapter2 = AdapterConfig(id='ca7a11457b71482881089a7a08052eb9',
                                               brand_id='965b3d108954424e914c0b64b6d9649d',
                                               service_id='5fdf32c7e67849559a0672a1bb7bd88a',
                                               service_type="shopify",
                                               created_at=datetime.datetime(2022, 1, 9, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               updated_at=datetime.datetime(2022, 1, 10, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               internal=True,
                                               instance_id='instance2')
        self.expected_adapter3 = AdapterConfig(id='f9963407e4054dbb9aa108c1544c5207',
                                               brand_id='965b3d108954424e914c0b64b6d9649d',
                                               service_id='5fdf32c7e67849559a0672a1bb7bd88a',
                                               service_type="shopify",
                                               created_at=datetime.datetime(2022, 1, 11, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               updated_at=datetime.datetime(2022, 1, 12, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               internal=True,
                                               instance_id='instance3')

    def test_get_adapter_paging_asc(self):
        expected_results = PagerResultsInternal(
            [self.expected_adapter1, self.expected_adapter2, self.expected_adapter3],
            3,
            10,
            0,
            1,
            1
        )
        actual = self.platformDB.get_all_adapters_paging("created_at", "ASC", 10, 1)
        assert actual == expected_results

    def test_get_adapter_paging_desc(self):
        expected_results = PagerResultsInternal(
            [self.expected_adapter3, self.expected_adapter2, self.expected_adapter1],
            3,
            10,
            0,
            1,
            1
        )
        actual = self.platformDB.get_all_adapters_paging("created_at", "DESC", 10, 1)
        assert actual == expected_results

    def test_get_adapter_paging_limit(self):
        expected_results = PagerResultsInternal(
            [self.expected_adapter3, self.expected_adapter2],
            3,
            2,
            0,
            2,
            1
        )
        actual = self.platformDB.get_all_adapters_paging("created_at", "DESC", 2, 1)
        assert actual == expected_results

    def test_get_adapter_paging_limit_2nd_page(self):
        expected_results = PagerResultsInternal(
            [self.expected_adapter1],
            3,
            2,
            2,
            2,
            2
        )
        actual = self.platformDB.get_all_adapters_paging("created_at", "DESC", 2, 2)
        assert actual == expected_results
