import datetime
import os
from uuid import UUID

import pytest

from _platformdb_setup import BaseAPITestPlatformDB
from leap.adapters.db.models import LONG_TEXT_VALUE, SHORT_TEXT_VALUE, NonApplicableEnum, SubmissionType
from leap.adapters.db.models.product_merchandising import AccessoriesSubCategoryEnum, BagsBackpacksLuggageSubCategoryEnum, \
    BeautyHealthPersonalSubCategoryEnum, BottomsSubCategoryEnum, ProductMerchandising, Categories, DamagesAddress, \
    HomeSubCategoryEnum, IntimatesSubCategoryEnum, JewelryWatchesSubCategoryEnum, OfficeSchoolSuppliesSubCategoryEnum, \
    OtherClothingSubCategoryEnum, PERCENTAGE_MAX, PRODUCT_LAUNCH_QUANTITY_MAX, ProductCategoryEnum, \
    ProductLaunchTimelineEnum, ShoesSubCategoryEnum, StandardExceptionEnum, SwimsuitsCoverUpsSubCategoryEnum, \
    TopsSubCategoryEnum, WAREHOUSE_LEN_MAX, Warehouse
from leap.adapters.db.models.brand_overview import BrandOverview, CORE_VALUES_LEN, CUSTOMER_PERSONAS_LEN, ChannelEnum, \
    CustomerPersona, EducationEnum, GOALS_LEN, GoalEnum, SALES_CHANNELS_LEN, SalesChannel, SalesGoal
from leap.adapters.db.models.brand_profile import BrandProfile
from leap.adapters.db.models.integrations import IntegrationTypeEnum, Integrations, Software, DeprecatedIntegrationTypeEnum
from leap.adapters.db.models.marketing import Marketing, Partner, RetailMarketingChannel, RetailMarketingChannelEnum
from leap.adapters.db.models.social import Social, Platform, SocialTypeEnum
from leap.adapters.db.models.store_design import AestheticImageEnum, StoreDesign, VibeImageEnum
from leap.adapters.db.models.store_operations import ScentSelectionEnum, StoreOperations
from leap.adapters.db.models.visual_merchandising import HangerMaterialEnum, HangerOwnershipEnum, Material, \
    MetalColorsEnum, ProductPlacementImageEnum, VisualMerchandising, VisualMerchandisingImageEnum, WoodColorsEnum, PlasticColorsEnum

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brands.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_users.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brand_profile.sql"),
]
BRAND_UUID = UUID("965b3d10-8954-424e-914c-0b64b6d9649d")
SHORT_TEXT_TOO_LONG = "a" * (SHORT_TEXT_VALUE + 1)
LONG_TEXT_TOO_LONG = "a" * (LONG_TEXT_VALUE + 1)


class TestPlatformDBBrandProfile(BaseAPITestPlatformDB):

    def setup_method(self):
        self.load_data(INSERT_FILES)
        self._prepare_expected()

    def _prepare_expected(self):
        self.expected_overview1 = BrandOverview(
            brand_mission="This is my brand mission",
            brand_story="This is my brand story",
            core_values=["core_value1", "core_value2", "core_value3"],
            customer_personas=[
                CustomerPersona(age_end=25,
                                age_start=18,
                                archetype="an archetype of a younger person",
                                education=EducationEnum.HIGH_SCHOOL,
                                income_start=50000,
                                income_end=100000,
                                story="Younger person story"),
                CustomerPersona(age_start=55,
                                age_end=70,
                                archetype="an archetype of an older person",
                                education=EducationEnum.HIGHER_EDUCATION,
                                income_start=100000,
                                income_end=150000,
                                story="Older person story")
            ],
            elevator_pitch="This is my elevator pitch",
            goals=[
                SalesGoal(date=datetime.datetime(2022, 1, 4, 17, 35, 53, 100000, tzinfo=datetime.timezone.utc),
                          goal=GoalEnum.PROFITABILITY,
                          goal_percent=30.0,
                          ranking=2),
                SalesGoal(date=datetime.datetime(2022, 1, 3, 17, 35, 53, 100000, tzinfo=datetime.timezone.utc),
                          goal=GoalEnum.CUSTOMER_BASE,
                          goal_percent=30.0,
                          ranking=1),
                SalesGoal(date=datetime.datetime(2022, 1, 5, 17, 35, 53, 100000, tzinfo=datetime.timezone.utc),
                          goal=GoalEnum.BRAND_AWARENESS,
                          goal_percent=30.0,
                          ranking=3)
            ],
            sales_channels=[
                SalesChannel(channel=ChannelEnum.ECOMMERCE,
                             details="This is details about my ecommerce sales channel",
                             enabled=True),
                SalesChannel(channel=ChannelEnum.OTHER,
                             details="This is details about my other sales channel",
                             enabled=True)
            ],
            website="mywebsite.com",
        )
        self.vm1 = VisualMerchandising(hanger_ownership=HangerOwnershipEnum.LEAP,
                                       material=Material(color=MetalColorsEnum.SILVER,
                                                         material_type=HangerMaterialEnum.METAL),
                                       product_placement_image=ProductPlacementImageEnum.COLLECTION_LIFESTYLE,
                                       product_placement_note="A note about product placement",
                                       visual_merchandising_image=VisualMerchandisingImageEnum.GALLERY,
                                       visual_merchandising_note="A note about")
        self.category1 = Categories(
            accessories=[AccessoriesSubCategoryEnum.SUSPENDERS, AccessoriesSubCategoryEnum.BELTS],
            bags_backpacks_luggage=[
                BagsBackpacksLuggageSubCategoryEnum.BACKPACKS, BagsBackpacksLuggageSubCategoryEnum.GOLF_BAGS
            ],
            beauty_health_personal_care=[
                BeautyHealthPersonalSubCategoryEnum.HAIR_CARE, BeautyHealthPersonalSubCategoryEnum.SKIN_CARE
            ],
            bottoms=[BottomsSubCategoryEnum.JEANS, BottomsSubCategoryEnum.SHORTS],
            home=[HomeSubCategoryEnum.BATHROOM_ACCESSORIES, HomeSubCategoryEnum.BEDDINGS],
            intimates=[IntimatesSubCategoryEnum.SLEEPWEAR_LOUNGEWEAR, IntimatesSubCategoryEnum.BRAS],
            jewelry_watches=[JewelryWatchesSubCategoryEnum.WATCHES, JewelryWatchesSubCategoryEnum.EARRINGS],
            office_school_supplies=[
                OfficeSchoolSuppliesSubCategoryEnum.PAPER_PRODUCTS, OfficeSchoolSuppliesSubCategoryEnum.FILING_PRODUCTS
            ],
            other_clothing=[OtherClothingSubCategoryEnum.CLOTHING_SETS, OtherClothingSubCategoryEnum.DRESSES],
            shoes=[ShoesSubCategoryEnum.FLATS, ShoesSubCategoryEnum.BOOTS],
            swimsuits_cover_ups=[SwimsuitsCoverUpsSubCategoryEnum.COVER_UPS, SwimsuitsCoverUpsSubCategoryEnum.TANKINIS],
            tops=[TopsSubCategoryEnum.SWEATERS, TopsSubCategoryEnum.COATS_JACKETS_VESTS],
            other="other",
        )
        self.pm1 = ProductMerchandising(
            warehouses=[
                Warehouse(name="warehouse1",
                          address="123 fake street",
                          city="nowhere",
                          state="ST",
                          zip_code="123456",
                          po_box="12",
                          latitude=10.0,
                          longitude=11.1,
                          processing_time_start=1,
                          processing_time_end=4,
                          shipping_time_start=2,
                          shipping_time_end=5)
            ],
            damages_choice=StandardExceptionEnum.EXCEPTION,
            damages_address=DamagesAddress(attn="bob",
                                           address="123 fake street",
                                           city="nowhere",
                                           state="ST",
                                           zip_code="123456",
                                           po_box="12",
                                           latitude=10.0,
                                           longitude=11.1),
            rtv_choice=StandardExceptionEnum.EXCEPTION,
            rtv_exception="rtv exception",
            labeling_confirmation=True,
            return_choice=StandardExceptionEnum.EXCEPTION,
            return_exception="return exception",
            exchange_choice=StandardExceptionEnum.EXCEPTION,
            exchange_exception="exchange exception",
            connected_shopify=True,
            connected_shopify_more="fake.com",
            core_assortment_percentage=11,
            seasonal_assortment_percentage=12,
            product_launch_quantity=13,
            product_launch_timeline=ProductLaunchTimelineEnum.MONTHLY,
            product_launch_notes="launch notes",
            product_category_include=self.category1,
            product_category_excluded=self.category1,
            product_category_new=self.category1,
        )
        self.store_operations1 = StoreOperations(
            music_artist_selection=["ac", "dc"],
            music_genre_selection=["rock", "roll"],
            scent_selection=ScentSelectionEnum.BEACHY_TROPICAL,
            store_discounts_policy_confirmation=StandardExceptionEnum.EXCEPTION,
            store_discounts_policy_exception="policy exception",
            store_team_wardrobe_policy_confirmation=StandardExceptionEnum.EXCEPTION,
            store_team_wardrobe_policy_exception="wardrobe exception",
            store_team_allowance_policy_confirmation=StandardExceptionEnum.EXCEPTION,
            store_team_allowance_policy_exception="allowance exception",
            store_inventory_counts_policy_confirmation=StandardExceptionEnum.EXCEPTION,
            store_inventory_counts_policy_exception="inventory exception",
            activation_fund_allowance_policy_confirmation=StandardExceptionEnum.EXCEPTION,
            activation_fund_allowance_policy_exception="allowance exception")
        self.store_design1 = StoreDesign(brand_colors=["#FF5722", "#270801", "#A1A1A3"],
                                         mood_board_aesthetic_image=AestheticImageEnum.MODERN_AND_MINIMAL,
                                         mood_board_vibe_image=VibeImageEnum.MOODY_AND_DRAMATIC,
                                         key_elements="key elements to transfer over",
                                         additional_comments="additional comments")
        non_profit_partners1 = [
            Partner(details=f"This is my non profit partners for reason {i}") for i in range(10, 20)
        ]
        influencers1 = "These are the influencers that have influenced me"
        self.marketing1 = Marketing(
            ideal_event_details="This is my ideal event details 1",
            event_dos="This is event dos",
            event_donts="This is event donts",
            non_profit_partners=non_profit_partners1,
            influencers=influencers1,
        )
        software = []
        software.append(Software(integration=IntegrationTypeEnum.COMMERCE, value="commerce software 1"))
        software.append(Software(integration=IntegrationTypeEnum.ERP, value="erp software 1"))
        software.append(Software(integration=IntegrationTypeEnum.WMS, value="wms software 1"))
        software.append(Software(integration=DeprecatedIntegrationTypeEnum.IMS, value="ims software 1"))
        software.append(Software(integration=DeprecatedIntegrationTypeEnum.ACCOUNTING, value="accounting software 1"))
        software.append(Software(integration=IntegrationTypeEnum.THIRD_PARTY_LOGISTICS, value="3pl software 1"))
        software.append(Software(integration=IntegrationTypeEnum.RETURNS_PORTAL, value="returns portal 1"))
        self.integrations1 = Integrations(
            software=software,
            technical_returns_process="This is my technical returns process",
            affiliate_marketing_programs="This is my affiliate marketing program",
            upcoming_changes="These are upcoming changes",
        )
        platform = []
        platform.append(Platform(social=SocialTypeEnum.FACEBOOK, value="www.facebook.com/"))
        platform.append(Platform(social=SocialTypeEnum.YOUTUBE, value="www.youtube.com/"))
        platform.append(Platform(social=SocialTypeEnum.INSTAGRAM, value="www.instagram.com/"))
        platform.append(Platform(social=SocialTypeEnum.TIKTOK, value="www.tiktok.com/"))
        platform.append(Platform(social=SocialTypeEnum.PINTEREST, value="www.pinterest.com/"))
        platform.append(Platform(social=SocialTypeEnum.SNAPCHAT, value="www.snapchat.com/"))
        platform.append(Platform(social=SocialTypeEnum.LINKEDIN, value="www.linkedin.com/"))
        platform.append(Platform(social=SocialTypeEnum.TWITTER, value="www.twitter.com/"))
        self.social1 = Social(platform=platform)
        self.expected_brand_profile1 = BrandProfile(
            id=UUID("ee8b5ff5-ccca-44fb-ab8a-bf8b2955470e"),
            brand_id=UUID("965b3d10-8954-424e-914c-0b64b6d9649d"),
            created_at=datetime.datetime(2022,
                                         1,
                                         1,
                                         17,
                                         35,
                                         53,
                                         100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022,
                                         1,
                                         2,
                                         17,
                                         35,
                                         53,
                                         100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            files_url="foo.com",
            overview_updated_by=UUID("89380209-52bd-446f-9c70-cffe25d34605"),
            overview=self.expected_overview1,
            visual_merchandising_updated_by=UUID("89380209-52bd-446f-9c70-cffe25d34605"),
            visual_merchandising=self.vm1,
            product_merchandising_updated_by=UUID("89380209-52bd-446f-9c70-cffe25d34605"),
            product_merchandising=self.pm1,
            store_operations_updated_by=UUID("89380209-52bd-446f-9c70-cffe25d34605"),
            store_operations=self.store_operations1,
            store_design_updated_by=UUID("89380209-52bd-446f-9c70-cffe25d34605"),
            store_design=self.store_design1,
            marketing_updated_by=UUID("89380209-52bd-446f-9c70-cffe25d34605"),
            marketing=self.marketing1,
            integrations_updated_by=UUID("89380209-52bd-446f-9c70-cffe25d34605"),
            integrations=self.integrations1,
            social_updated_by=UUID("89380209-52bd-446f-9c70-cffe25d34605"),
            social=self.social1,
        )

        self.expected_overview2 = BrandOverview(
            brand_mission="This is my brand mission",
            brand_story="This is my brand story",
            core_values=["core_value4", "core_value5", "core_value6"],
            customer_personas=[
                CustomerPersona(age_end=25,
                                age_start=18,
                                archetype="an archetype of a younger person 2",
                                education=EducationEnum.HIGH_SCHOOL,
                                income_start=50000,
                                income_end=100000,
                                story="Younger person story"),
                CustomerPersona(age_start=55,
                                age_end=70,
                                archetype="an archetype of an older person 2",
                                education=EducationEnum.HIGHER_EDUCATION,
                                income_start=100000,
                                income_end=150000,
                                story="Older person story")
            ],
            elevator_pitch="This is my elevator pitch 2",
            goals=[
                SalesGoal(date=datetime.datetime(2022, 1, 3, 17, 35, 53, 100000, tzinfo=datetime.timezone.utc),
                          goal=GoalEnum.CUSTOMER_BASE,
                          goal_percent=30.0,
                          ranking=1),
                SalesGoal(date=datetime.datetime(2022, 1, 4, 17, 35, 53, 100000, tzinfo=datetime.timezone.utc),
                          goal=GoalEnum.PROFITABILITY,
                          goal_percent=30.0,
                          ranking=2),
                SalesGoal(date=datetime.datetime(2022, 1, 5, 17, 35, 53, 100000, tzinfo=datetime.timezone.utc),
                          goal=GoalEnum.BRAND_AWARENESS,
                          goal_percent=30.0,
                          ranking=3)
            ],
            sales_channels=[
                SalesChannel(channel=ChannelEnum.ECOMMERCE,
                             details="This is details about my ecommerce sales channel 2",
                             enabled=True),
                SalesChannel(channel=ChannelEnum.OTHER,
                             details="This is details about my other sales channel 2",
                             enabled=True)
            ],
            website="mywebsite.com",
        )
        self.vm2 = VisualMerchandising(hanger_ownership=HangerOwnershipEnum.LEAP,
                                       material=Material(color=MetalColorsEnum.SILVER,
                                                         material_type=HangerMaterialEnum.METAL),
                                       product_placement_image=ProductPlacementImageEnum.COLLECTION_LIFESTYLE,
                                       product_placement_note="A note about product placement",
                                       visual_merchandising_image=VisualMerchandisingImageEnum.GALLERY,
                                       visual_merchandising_note="A note about")
        self.category2 = Categories(
            accessories=[AccessoriesSubCategoryEnum.GOLF_ACCESSORIES, AccessoriesSubCategoryEnum.BELTS],
            bags_backpacks_luggage=[
                BagsBackpacksLuggageSubCategoryEnum.CROSSBODY_BAGS, BagsBackpacksLuggageSubCategoryEnum.GOLF_BAGS
            ],
            beauty_health_personal_care=[
                BeautyHealthPersonalSubCategoryEnum.FRAGRANCES, BeautyHealthPersonalSubCategoryEnum.MAKEUP
            ],
            bottoms=[BottomsSubCategoryEnum.SKIRTS, BottomsSubCategoryEnum.PANTS],
            home=[HomeSubCategoryEnum.BEDDINGS, HomeSubCategoryEnum.TOWELS],
            intimates=[IntimatesSubCategoryEnum.UNDERWEAR, IntimatesSubCategoryEnum.SOCKS_HOSIERY],
            jewelry_watches=[JewelryWatchesSubCategoryEnum.JEWELRY_SETS, JewelryWatchesSubCategoryEnum.BRACELETS],
            office_school_supplies=[
                OfficeSchoolSuppliesSubCategoryEnum.CALENDARS_PLANNERS_PERSONAL_ORGANIZERS,
                OfficeSchoolSuppliesSubCategoryEnum.STORE_SIGNS_DISPLAY
            ],
            other_clothing=[
                OtherClothingSubCategoryEnum.JUMPSUITS_ROMPERS_OVERALLS, OtherClothingSubCategoryEnum.DRESSES
            ],
            shoes=[ShoesSubCategoryEnum.PUMPS, ShoesSubCategoryEnum.OXFORDS],
            swimsuits_cover_ups=[
                SwimsuitsCoverUpsSubCategoryEnum.OTHER_SWIMSUITS_COVER_UPS, SwimsuitsCoverUpsSubCategoryEnum.RASH_GUARDS
            ],
            tops=[TopsSubCategoryEnum.HOODIES_SWEATSHIRTS, TopsSubCategoryEnum.COATS_JACKETS_VESTS],
            other="other 2")
        self.pm2 = ProductMerchandising(warehouses=[
            Warehouse(name="warehouse2",
                      address="124 fake street",
                      city="nowhere 2",
                      state="ST 2",
                      zip_code="1234567",
                      latitude=12.0,
                      longitude=13.1,
                      processing_time_start=2,
                      processing_time_end=5,
                      shipping_time_start=3,
                      shipping_time_end=6)
        ],
                                        damages_choice=StandardExceptionEnum.EXCEPTION,
                                        damages_address=DamagesAddress(attn="boby",
                                                                       address="1234 fake street",
                                                                       city="nowhere 2",
                                                                       state="ST",
                                                                       zip_code="1234567",
                                                                       latitude=12.0,
                                                                       longitude=13.1),
                                        rtv_choice=StandardExceptionEnum.STANDARD,
                                        labeling_confirmation=True,
                                        return_choice=StandardExceptionEnum.STANDARD,
                                        exchange_choice=StandardExceptionEnum.STANDARD,
                                        connected_shopify=True,
                                        connected_shopify_more="fake.com",
                                        core_assortment_percentage=12,
                                        seasonal_assortment_percentage=13,
                                        product_launch_quantity=14,
                                        product_launch_timeline=ProductLaunchTimelineEnum.QUARTERLY,
                                        product_launch_notes="launch notes 2",
                                        product_category_include=self.category2,
                                        product_category_excluded=self.category2,
                                        product_category_new=self.category2)
        self.store_operations2 = StoreOperations(
            music_artist_selection=["snoop", "dog"],
            music_genre_selection=["hip", "hop"],
            scent_selection=ScentSelectionEnum.BEACHY_TROPICAL,
            store_discounts_policy_confirmation=StandardExceptionEnum.STANDARD,
            store_team_wardrobe_policy_confirmation=StandardExceptionEnum.STANDARD,
            store_team_allowance_policy_confirmation=StandardExceptionEnum.STANDARD,
            store_inventory_counts_policy_confirmation=StandardExceptionEnum.STANDARD,
            activation_fund_allowance_policy_confirmation=StandardExceptionEnum.STANDARD,
        )
        self.store_design2 = StoreDesign(brand_colors=["#FF5733", "#270802", "#A1A1A1"],
                                         mood_board_aesthetic_image=AestheticImageEnum.HOMEY_AND_COZY,
                                         mood_board_vibe_image=VibeImageEnum.LIGHT_AIRY_AND_BRIGHT,
                                         key_elements="key elements to transfer over",
                                         additional_comments="additional comments")
        non_profit_partners2 = [Partner(details=f"This is my non profit partners for reason {i}") for i in range(10)]
        influencers2 = "These are the influencers that have influenced me"
        self.marketing2 = Marketing(
            ideal_event_details="This is my ideal event details",
            event_dos="This is event dos",
            event_donts="This is event donts",
            non_profit_partners=non_profit_partners2,
            influencers=influencers2,
        )
        self.integrations2 = Integrations(
            software=software,
            technical_returns_process="This is my technical returns process",
            affiliate_marketing_programs="This is my affiliate marketing program",
            upcoming_changes="These are upcoming changes",
        )
        self.expected_brand_profile2 = BrandProfile(
            brand_id=UUID("8ea2f941-0f9d-4570-b11d-e78809ae833b"),
            overview_updated_by=UUID("cda48bff-dbed-456d-a700-55a4e5bc4072"),
            overview=self.expected_overview2,
            visual_merchandising_updated_by=UUID("cda48bff-dbed-456d-a700-55a4e5bc4072"),
            visual_merchandising=self.vm2,
            product_merchandising_updated_by=UUID("cda48bff-dbed-456d-a700-55a4e5bc4072"),
            product_merchandising=self.pm2,
            store_operations_updated_by=UUID("cda48bff-dbed-456d-a700-55a4e5bc4072"),
            store_operations=self.store_operations2,
            store_design_updated_by=UUID("cda48bff-dbed-456d-a700-55a4e5bc4072"),
            store_design=self.store_design2,
            marketing_updated_by=UUID("cda48bff-dbed-456d-a700-55a4e5bc4072"),
            marketing=self.marketing2,
            integrations=self.integrations2)

    def test_get_brand_profile_by_brand_id(self):
        actual_brand_profile = self.platformDB.get_brand_profile_by_brand_id(self.expected_brand_profile1.brand_id.hex)
        assert actual_brand_profile == self.expected_brand_profile1

    def test_post_brand_profile(self):
        self.platformDB.insert_brand_profile(self.expected_brand_profile2)
        actual_brand_profile = self.platformDB.get_brand_profile_by_brand_id(self.expected_brand_profile2.brand_id)
        # These 3 values are generated on write, but everything else should be the same
        self.expected_brand_profile2.created_at = actual_brand_profile.created_at
        self.expected_brand_profile2.updated_at = actual_brand_profile.updated_at
        self.expected_brand_profile2.id = actual_brand_profile.id
        assert actual_brand_profile == self.expected_brand_profile2

    def test_profile_files_len(self):
        with pytest.raises(ValueError):
            BrandProfile(brand_id=BRAND_UUID, files_url=SHORT_TEXT_TOO_LONG)

    def test_overview_website(self):
        with pytest.raises(ValueError):
            BrandOverview(website=SHORT_TEXT_TOO_LONG)

        with pytest.raises(ValueError):
            BrandOverview(website="not a url")

    def test_overview_elevator_pitch(self):
        with pytest.raises(ValueError):
            BrandOverview(website="foo.com", elevator_pitch=SHORT_TEXT_TOO_LONG)

    def test_overview_brand_story(self):
        with pytest.raises(ValueError):
            BrandOverview(website="foo.com", brand_story=LONG_TEXT_TOO_LONG)

    def test_overview_brand_mission(self):
        with pytest.raises(ValueError):
            BrandOverview(website="foo.com", brand_mission=LONG_TEXT_TOO_LONG)

    def test_overview_core_values(self):
        with pytest.raises(ValueError):
            BrandOverview(website="foo.com", core_values=list(range(CORE_VALUES_LEN + 1)))

    def test_overview_core_values_string(self):
        with pytest.raises(ValueError):
            BrandOverview(website="foo.com", core_values=[SHORT_TEXT_TOO_LONG])

        with pytest.raises(ValueError):
            BrandOverview(website="foo.com", core_values=["a", "b", SHORT_TEXT_TOO_LONG])

    def test_overview_cp_length(self):
        with pytest.raises(ValueError):
            BrandOverview(website="foo.com",
                          customer_personas=[CustomerPersona() for c in range(CUSTOMER_PERSONAS_LEN + 1)])

    def test_overview_cp_archetype(self):
        with pytest.raises(ValueError):
            CustomerPersona(archetype=SHORT_TEXT_TOO_LONG)

    def test_overview_cp_story(self):
        with pytest.raises(ValueError):
            CustomerPersona(story=LONG_TEXT_TOO_LONG)

    def test_overview_sales_channel_len(self):
        with pytest.raises(ValueError):
            BrandOverview(website="foo.com", sales_channels=[SalesChannel() for s in range(SALES_CHANNELS_LEN + 1)])

    def test_overview_sales_channel_details(self):
        with pytest.raises(ValueError):
            SalesChannel(details=LONG_TEXT_TOO_LONG)

    def test_overview_goals_len(self):
        with pytest.raises(ValueError):
            BrandOverview(website="foo.com", goals=[SalesGoal() for g in range(GOALS_LEN + 1)])

    def test_vm_vm_note(self):
        with pytest.raises(ValueError):
            VisualMerchandising(visual_merchandising_note=SHORT_TEXT_TOO_LONG)

    def test_vm_pp_note(self):
        with pytest.raises(ValueError):
            VisualMerchandising(product_placement_note=SHORT_TEXT_TOO_LONG)

    def test_goal_sorting(self):
        overview = BrandOverview(website="foo.com",
                                 goals=[SalesGoal(ranking=2),
                                        SalesGoal(ranking=3),
                                        SalesGoal(ranking=1)])
        assert overview.goals[0].ranking == 1
        assert overview.goals[1].ranking == 2
        assert overview.goals[2].ranking == 3

    def test_warehouse_name(self):
        with pytest.raises(ValueError):
            Warehouse(name=SHORT_TEXT_TOO_LONG,
                      address="124 fake street",
                      city="nowhere 2",
                      state="ST 2",
                      zip_code="1234567",
                      latitude=12.0,
                      longitude=13.1,
                      processing_time_start=2,
                      processing_time_end=5,
                      shipping_time_start=3,
                      shipping_time_end=6)

    def test_warehouse_address(self):
        with pytest.raises(ValueError):
            Warehouse(name="name",
                      address=SHORT_TEXT_TOO_LONG,
                      city="nowhere 2",
                      state="ST 2",
                      zip_code="1234567",
                      latitude=12.0,
                      longitude=13.1,
                      processing_time_start=2,
                      processing_time_end=5,
                      shipping_time_start=3,
                      shipping_time_end=6)

    def test_warehouse_city(self):
        with pytest.raises(ValueError):
            Warehouse(name="name",
                      address="a",
                      city=SHORT_TEXT_TOO_LONG,
                      state="ST 2",
                      zip_code="1234567",
                      latitude=12.0,
                      longitude=13.1,
                      processing_time_start=2,
                      processing_time_end=5,
                      shipping_time_start=3,
                      shipping_time_end=6)

    def test_warehouse_state(self):
        with pytest.raises(ValueError):
            Warehouse(name="name",
                      address="a",
                      city="a",
                      state=SHORT_TEXT_TOO_LONG,
                      zip_code="1234567",
                      latitude=12.0,
                      longitude=13.1,
                      processing_time_start=2,
                      processing_time_end=5,
                      shipping_time_start=3,
                      shipping_time_end=6)

    def test_warehouse_zip_code(self):
        with pytest.raises(ValueError):
            Warehouse(name="name",
                      address="a",
                      city="a",
                      state="a",
                      zip_code=SHORT_TEXT_TOO_LONG,
                      latitude=12.0,
                      longitude=13.1,
                      processing_time_start=2,
                      processing_time_end=5,
                      shipping_time_start=3,
                      shipping_time_end=6)

    def test_warehouse_po_box(self):
        with pytest.raises(ValueError):
            Warehouse(name="name",
                      address="a",
                      city="a",
                      state="a",
                      zip_code="a",
                      po_box=SHORT_TEXT_TOO_LONG,
                      latitude=12.0,
                      longitude=13.1,
                      processing_time_start=2,
                      processing_time_end=5,
                      shipping_time_start=3,
                      shipping_time_end=6)

    def test_warehouse_partial(self):
        w = Warehouse(name="name", shipping_time_end=6)
        assert w.name == "name"
        assert w.address is None
        assert w.shipping_time_end == 6

    def test_damage_address_attn(self):
        with pytest.raises(ValueError):
            DamagesAddress(attn=SHORT_TEXT_TOO_LONG,
                           address="a",
                           city="a",
                           state="a",
                           zip_code="a",
                           latitude=12.0,
                           longitude=13.1)

    def test_damage_address_address(self):
        with pytest.raises(ValueError):
            DamagesAddress(attn="a",
                           address=SHORT_TEXT_TOO_LONG,
                           city="a",
                           state="a",
                           zip_code="a",
                           latitude=12.0,
                           longitude=13.1)

    def test_damage_address_city(self):
        with pytest.raises(ValueError):
            DamagesAddress(attn="a",
                           address="a",
                           city=SHORT_TEXT_TOO_LONG,
                           state="a",
                           zip_code="a",
                           latitude=12.0,
                           longitude=13.1)

    def test_damage_address_state(self):
        with pytest.raises(ValueError):
            DamagesAddress(attn="a",
                           address="a",
                           city="a",
                           state=SHORT_TEXT_TOO_LONG,
                           zip_code="a",
                           latitude=12.0,
                           longitude=13.1)

    def test_damage_address_zip(self):
        with pytest.raises(ValueError):
            DamagesAddress(attn="a",
                           address="a",
                           city="a",
                           state="a",
                           zip_code=SHORT_TEXT_TOO_LONG,
                           latitude=12.0,
                           longitude=13.1)

    def test_damage_po_box(self):
        with pytest.raises(ValueError):
            DamagesAddress(attn="a",
                           address="a",
                           city="a",
                           state="a",
                           zip_code="a",
                           po_box=SHORT_TEXT_TOO_LONG,
                           latitude=12.0,
                           longitude=13.1)

    def test_damage_address_partial(self):
        d = DamagesAddress(attn="name", state="IL")
        assert d.attn == "name"
        assert d.address is None
        assert d.state == "IL"

    def test_merchandising_warehouse(self):
        with pytest.raises(ValueError):
            ProductMerchandising(warehouses=[
                Warehouse(name=SHORT_TEXT_TOO_LONG,
                          address="124 fake street",
                          city="nowhere 2",
                          state="ST 2",
                          zip_code="1234567",
                          latitude=12.0,
                          longitude=13.1,
                          processing_time_start=2,
                          processing_time_end=5,
                          shipping_time_start=3,
                          shipping_time_end=6) for x in range(WAREHOUSE_LEN_MAX + 1)
            ])

    def test_merchandising_max(self):
        with pytest.raises(ValueError):
            ProductMerchandising(None, None, None, None, None, core_assortment_percentage=(PERCENTAGE_MAX + 1))
        with pytest.raises(ValueError):
            ProductMerchandising(None, None, None, None, None, seasonal_assortment_percentage=(PERCENTAGE_MAX + 1))
        with pytest.raises(ValueError):
            ProductMerchandising(None,
                                 None,
                                 None,
                                 None,
                                 None,
                                 product_launch_quantity=(PRODUCT_LAUNCH_QUANTITY_MAX + 1))
        with pytest.raises(ValueError):
            ProductMerchandising(None, None, None, None, None, product_launch_notes=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            ProductMerchandising(None, None, None, None, None, product_category_new=SHORT_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            ProductMerchandising(None, None, None, None, None, rtv_exception=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            ProductMerchandising(None, None, None, None, None, rtv_exception=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            ProductMerchandising(None, None, None, None, None, return_exception=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            ProductMerchandising(None, None, None, None, None, exchange_exception=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            ProductMerchandising(None, None, None, None, None, connected_shopify_more=SHORT_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            Categories(other=SHORT_TEXT_TOO_LONG)

    def test_store_operations_max(self):
        with pytest.raises(ValueError):
            StoreOperations(None, None, None, None, None, music_genre_selection=["a" for m in range(11)])
        with pytest.raises(ValueError):
            StoreOperations(None, None, None, None, None, music_genre_selection=[SHORT_TEXT_TOO_LONG])
        with pytest.raises(ValueError):
            StoreOperations(None, None, None, None, None, music_genre_selection=["a" for m in range(11)])
        with pytest.raises(ValueError):
            StoreOperations(None, None, None, None, None, music_genre_selection=[SHORT_TEXT_TOO_LONG])
        with pytest.raises(ValueError):
            StoreOperations(None, None, None, None, None, store_discounts_policy_exception=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            StoreOperations(None, None, None, None, None, store_team_wardrobe_policy_exception=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            StoreOperations(None, None, None, None, None, store_team_allowance_policy_exception=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            StoreOperations(None, None, None, None, None, store_inventory_counts_policy_exception=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            StoreOperations(None, None, None, None, None, activation_fund_allowance_policy_exception=LONG_TEXT_TOO_LONG)

    def test_store_design_validation(self):
        with pytest.raises(ValueError):
            StoreDesign(key_elements=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError):
            StoreDesign(additional_comments=LONG_TEXT_TOO_LONG)
        with pytest.raises(ValueError) as e:
            hex_colors = ["notavalidhex", "alsonotvalid", "#000000"]
            StoreDesign(brand_colors=hex_colors)
        assert f"{sorted(set(hex_colors[:2]))} are not valid hex colors" == str(e.value)
        with pytest.raises(ValueError) as e:
            hex_colors = ["#000000", "notavalidhex", "alsonotvalid"]
            StoreDesign(brand_colors=hex_colors)
        assert f"{sorted(set(hex_colors[1:]))} are not valid hex colors" == str(e.value)
        with pytest.raises(ValueError) as e:
            hex_colors = ["too", "many", "values", "for", "colors"]
            StoreDesign(brand_colors=hex_colors)
        assert "Too many brand colors, Only 3 brand colors allowed" == str(e.value)

    def test_marketing_validation(self):
        with pytest.raises(ValueError) as e:
            Marketing(ideal_event_details=LONG_TEXT_TOO_LONG)
        assert f"ideal_event_details is too large. Must be less than {LONG_TEXT_VALUE} characters." == str(e.value)

        with pytest.raises(ValueError) as e:
            Marketing(event_dos=LONG_TEXT_TOO_LONG)
        assert f"event_dos is too large. Must be less than {LONG_TEXT_VALUE} characters." == str(e.value)

        with pytest.raises(ValueError) as e:
            Marketing(event_donts=LONG_TEXT_TOO_LONG)
        assert f"event_donts is too large. Must be less than {LONG_TEXT_VALUE} characters." == str(e.value)

        with pytest.raises(ValueError) as e:
            Partner(details=LONG_TEXT_TOO_LONG)
        assert f"Partner.details is too large (size={len(LONG_TEXT_TOO_LONG)}). Must be less than {LONG_TEXT_VALUE} characters." == str(
            e.value)

        with pytest.raises(ValueError) as e:
            non_profit_partners = [Partner(details="Details for a non profit partner") for i in range(11)]
            Marketing(non_profit_partners=non_profit_partners)
        assert f"List of non_profit_partners is too long (size={len(non_profit_partners)}). Up to 10 allowed" == str(e.value)

        with pytest.raises(ValueError) as e:
            Marketing(influencers=LONG_TEXT_TOO_LONG)
        assert f"influencers is too large. Must be less than {LONG_TEXT_VALUE} characters." == str(e.value)

        with pytest.raises(ValueError) as e:
            Marketing(retail_marketing_details=LONG_TEXT_TOO_LONG)
        assert f"retail_marketing_details is too large. Must be less than {LONG_TEXT_VALUE} characters." == str(e.value)

    def test_integrations_validation(self):
        with pytest.raises(ValueError) as e:
            Integrations(software=[Software(integration=IntegrationTypeEnum.COMMERCE, value=SHORT_TEXT_TOO_LONG)])
        assert f"commerce is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Integrations(software=[Software(integration=IntegrationTypeEnum.ERP, value=SHORT_TEXT_TOO_LONG)])
        assert f"erp is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Integrations(software=[Software(integration=IntegrationTypeEnum.WMS, value=SHORT_TEXT_TOO_LONG)])
        assert f"wms is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Integrations(software=[Software(integration=DeprecatedIntegrationTypeEnum.IMS, value=SHORT_TEXT_TOO_LONG)])
        assert f"ims is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Integrations(software=[Software(integration=DeprecatedIntegrationTypeEnum.ACCOUNTING, value=SHORT_TEXT_TOO_LONG)])
        assert f"accounting is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Integrations(
                software=[Software(integration=IntegrationTypeEnum.THIRD_PARTY_LOGISTICS, value=SHORT_TEXT_TOO_LONG)])
        assert f"third_party_logistics is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)

        with pytest.raises(ValueError) as e:
            Integrations(software=[Software(integration=IntegrationTypeEnum.RETURNS_PORTAL, value=LONG_TEXT_TOO_LONG)])
        assert f"returns_portal is too large (size={len(LONG_TEXT_TOO_LONG)}). Must be less than {LONG_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Integrations(technical_returns_process=LONG_TEXT_TOO_LONG)
        assert f"technical_returns_process is too large (size={len(LONG_TEXT_TOO_LONG)}). Must be less than {LONG_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Integrations(affiliate_marketing_programs=LONG_TEXT_TOO_LONG)
        assert f"affiliate_marketing_programs is too large (size={len(LONG_TEXT_TOO_LONG)}). Must be less than {LONG_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Integrations(upcoming_changes=LONG_TEXT_TOO_LONG)
        assert f"upcoming_changes is too large (size={len(LONG_TEXT_TOO_LONG)}). Must be less than {LONG_TEXT_VALUE} characters." == str(
            e.value)

    def test_social_validation_length(self):
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.FACEBOOK, value=SHORT_TEXT_TOO_LONG)])
        assert f"facebook url is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.YOUTUBE, value=SHORT_TEXT_TOO_LONG)])
        assert f"youtube url is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.INSTAGRAM, value=SHORT_TEXT_TOO_LONG)])
        assert f"instagram url is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.TIKTOK, value=SHORT_TEXT_TOO_LONG)])
        assert f"tiktok url is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.PINTEREST, value=SHORT_TEXT_TOO_LONG)])
        assert f"pinterest url is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.SNAPCHAT, value=SHORT_TEXT_TOO_LONG)])
        assert f"snapchat url is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.LINKEDIN, value=SHORT_TEXT_TOO_LONG)])
        assert f"linkedin url is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.TWITTER, value=SHORT_TEXT_TOO_LONG)])
        assert f"twitter url is too large (size={len(SHORT_TEXT_TOO_LONG)}). Must be less than {SHORT_TEXT_VALUE} characters." == str(
            e.value)

    def test_social_validation_url(self):
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.FACEBOOK, value="hello world")])
        assert "The value hello world is not a valid url" == str(e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.YOUTUBE, value="hello world")])
        assert "The value hello world is not a valid url" == str(e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.INSTAGRAM, value="hello world")])
        assert "The value hello world is not a valid url" == str(e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.TIKTOK, value="hello world")])
        assert "The value hello world is not a valid url" == str(e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.PINTEREST, value="hello world")])
        assert "The value hello world is not a valid url" == str(e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.SNAPCHAT, value="hello world")])
        assert "The value hello world is not a valid url" == str(e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.LINKEDIN, value="hello world")])
        assert "The value hello world is not a valid url" == str(e.value)
        with pytest.raises(ValueError) as e:
            Social(platform=[Platform(social=SocialTypeEnum.TWITTER, value="hello world")])
        assert "The value hello world is not a valid url" == str(e.value)

    def test_success_material_color(self):
        vm = VisualMerchandising(material=Material(HangerMaterialEnum.METAL))
        assert vm.material.material_type == HangerMaterialEnum.METAL

        vm = VisualMerchandising(material=Material(HangerMaterialEnum.METAL, MetalColorsEnum.SILVER))
        assert vm.material.material_type == HangerMaterialEnum.METAL
        assert vm.material.color == MetalColorsEnum.SILVER

        vm = VisualMerchandising(material=Material(HangerMaterialEnum.WOOD))
        assert vm.material.material_type == HangerMaterialEnum.WOOD

        vm = VisualMerchandising(material=Material(HangerMaterialEnum.WOOD, WoodColorsEnum.WALNUT))
        assert vm.material.material_type == HangerMaterialEnum.WOOD
        assert vm.material.color == WoodColorsEnum.WALNUT

        vm = VisualMerchandising(material=Material(HangerMaterialEnum.PLASTIC))
        assert vm.material.material_type == HangerMaterialEnum.PLASTIC

        vm = VisualMerchandising(material=Material(HangerMaterialEnum.PLASTIC, PlasticColorsEnum.CLEAR))
        assert vm.material.material_type == HangerMaterialEnum.PLASTIC
        assert vm.material.color == PlasticColorsEnum.CLEAR

    def test_failure_material_color(self):
        with pytest.raises(ValueError) as e:
            VisualMerchandising(material=Material(HangerMaterialEnum.METAL, "fake"))
        assert "Material type METAL and color fake are not a valid combination" == str(e.value)

        with pytest.raises(ValueError) as e:
            VisualMerchandising(material=Material(HangerMaterialEnum.METAL, WoodColorsEnum.WALNUT))
        assert "Material type METAL and color WALNUT are not a valid combination" == str(e.value)

        with pytest.raises(ValueError) as e:
            VisualMerchandising(material=Material(HangerMaterialEnum.WOOD, "fake"))
        assert "Material type WOOD and color fake are not a valid combination" == str(e.value)

        with pytest.raises(ValueError) as e:
            VisualMerchandising(material=Material(HangerMaterialEnum.WOOD, MetalColorsEnum.SILVER))
        assert "Material type WOOD and color SILVER are not a valid combination" == str(e.value)

        with pytest.raises(ValueError) as e:
            VisualMerchandising(material=Material(HangerMaterialEnum.PLASTIC, "fake"))
        assert "Material type PLASTIC and color fake are not a valid combination" == str(e.value)

        with pytest.raises(ValueError) as e:
            VisualMerchandising(material=Material(HangerMaterialEnum.PLASTIC, MetalColorsEnum.SILVER))
        assert "Material type PLASTIC and color SILVER are not a valid combination" == str(e.value)

    def test_success_submit_brand_overview(self):
        BrandOverview(website="test.com", submission_type=SubmissionType.SUBMIT.value)

    def test_failure_submit_brand_overview(self):
        with pytest.raises(ValueError):
            BrandOverview(website=None, submission_type=SubmissionType.SUBMIT.value)

    def test_success_submit_product_merchandising(self):
        ProductMerchandising(damages_choice=StandardExceptionEnum.STANDARD.value,
                             rtv_choice=StandardExceptionEnum.STANDARD.value,
                             return_choice=StandardExceptionEnum.STANDARD.value,
                             exchange_choice=StandardExceptionEnum.STANDARD.value,
                             warehouses=[Warehouse()],
                             submission_type=SubmissionType.SUBMIT.value)

    def test_failure_submit_product_merchandising(self):
        with pytest.raises(ValueError):
            ProductMerchandising(damages_choice=None,
                                 rtv_choice=None,
                                 return_choice=None,
                                 exchange_choice=None,
                                 warehouses=[],
                                 submission_type=SubmissionType.SUBMIT.value)

    def test_success_submit_store_operations(self):
        StoreOperations(store_discounts_policy_confirmation=StandardExceptionEnum.STANDARD.value,
                        store_team_wardrobe_policy_confirmation=StandardExceptionEnum.STANDARD.value,
                        store_team_allowance_policy_confirmation=StandardExceptionEnum.STANDARD.value,
                        store_inventory_counts_policy_confirmation=StandardExceptionEnum.STANDARD.value,
                        activation_fund_allowance_policy_confirmation=StandardExceptionEnum.STANDARD.value,
                        submission_type=SubmissionType.SUBMIT.value)

    def test_failure_submit_store_operations(self):
        with pytest.raises(ValueError):
            StoreOperations(store_discounts_policy_confirmation=None,
                            store_team_wardrobe_policy_confirmation=None,
                            store_team_allowance_policy_confirmation=None,
                            store_inventory_counts_policy_confirmation=None,
                            activation_fund_allowance_policy_confirmation=None,
                            submission_type=SubmissionType.SUBMIT.value)

    def test_type_non_applicable_for_integrations(self):
        # Check directly setting Enum
        s = Software(integration=IntegrationTypeEnum.COMMERCE, value=NonApplicableEnum.NON_APPLICABLE)
        assert s.value == "NON_APPLICABLE"

        # Check value is string if it is a string
        s = Software(integration=IntegrationTypeEnum.COMMERCE, value="validstr")
        assert s.value == "validstr"

        # Check value is string if it is a string
        s = Software(integration=IntegrationTypeEnum.COMMERCE, value="")
        assert s.value == ""

    def test_type_non_applicable_for_social(self):
        # Check directly setting Enum
        s = Platform(social=SocialTypeEnum.FACEBOOK, value=NonApplicableEnum.NON_APPLICABLE)
        assert s.value == "NON_APPLICABLE"

        # Check value is string if it is a string
        s = Platform(social=SocialTypeEnum.FACEBOOK, value="validurl.com")
        assert s.value == "validurl.com"

    def test_failure_retail_marketing_channel_block_null_values(self):
        with pytest.raises(Exception):
            # Block all null values
            RetailMarketingChannel(None, None, None)

        with pytest.raises(Exception):
            # block null success boolean
            RetailMarketingChannel(RetailMarketingChannelEnum.AFFILIATE, None, None)

        with pytest.raises(Exception):
            # block null channel enum
            RetailMarketingChannel(None, None, False)

        success: RetailMarketingChannel = RetailMarketingChannel(RetailMarketingChannelEnum.AFFILIATE, None, True)
        assert success.channel == RetailMarketingChannelEnum.AFFILIATE
        assert success.details is None
        assert success.success
