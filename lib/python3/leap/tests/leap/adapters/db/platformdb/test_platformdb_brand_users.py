import datetime
import os


from leap.adapters.db._models import BrandUser
from leap.adapters.db.platformdb import PagerResultsInternal
from _platformdb_setup import BaseAPITestPlatformDB

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_users.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brands.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brand_users.sql"),
]

class TestPlatformDBBrandUsers(BaseAPITestPlatformDB):
    def setup_method(self):
        self.load_data(INSERT_FILES)
        self._prepare_expected()

    def _prepare_expected(self):
        self.expected_brand_user1 = BrandUser(
            id='76d91c5dea354f36b3dfb8366f9c928f',
            user_id='8938020952bd446f9c70cffe25d34605',
            brand_id='965b3d108954424e914c0b64b6d9649d',
            enabled=True,
            created_at=datetime.datetime(2022, 1, 9, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 10, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(
                                             datetime.timedelta(days=-1, seconds=64800))),
        )
        self.expected_brand_user2 = BrandUser(
            id='9ef1872a5d4d46bda147d6f70c20727b',
            user_id='cda48bffdbed456da70055a4e5bc4072',
            brand_id='965b3d108954424e914c0b64b6d9649d',
            enabled=True,
            created_at=datetime.datetime(2022, 1, 11, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 12, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(
                                             datetime.timedelta(days=-1, seconds=64800))),
        )
        self.expected_brand_user3 = BrandUser(
            id='7e092ebafe5c4eafb48f6b61f73a1ac4',
            user_id='cc423d8981974bdab3dead97585fed67',
            brand_id='965b3d108954424e914c0b64b6d9649d',
            enabled=True,
            created_at=datetime.datetime(2022, 1, 13, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 14, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(
                                             datetime.timedelta(days=-1, seconds=64800))),
        )

    def test_get_brand_users_paging_asc(self):
        expected_results = PagerResultsInternal(
            [self.expected_brand_user1, self.expected_brand_user2, self.expected_brand_user3],
            3,
            10,
            0,
            1,
            1
        )
        actual = self.platformDB.get_all_brand_users_paging("created_at", "ASC", 10, 1)
        assert actual == expected_results

    def test_get_brand_users_paging_desc(self):
        expected_results = PagerResultsInternal(
            [self.expected_brand_user3, self.expected_brand_user2, self.expected_brand_user1],
            3,
            10,
            0,
            1,
            1
        )
        actual = self.platformDB.get_all_brand_users_paging("created_at", "DESC", 10, 1)
        assert actual == expected_results

    def test_get_brand_users_paging_limit(self):
        expected_results = PagerResultsInternal(
            [self.expected_brand_user3, self.expected_brand_user2],
            3,
            2,
            0,
            2,
            1
        )
        actual = self.platformDB.get_all_brand_users_paging("created_at", "DESC", 2, 1)
        assert actual == expected_results

    def test_get_brand_users_paging_limit_2nd_page(self):
        expected_results = PagerResultsInternal(
            [self.expected_brand_user1],
            3,
            2,
            2,
            2,
            2
        )
        actual = self.platformDB.get_all_brand_users_paging("created_at", "DESC", 2, 2)
        assert actual == expected_results
