import datetime
import os

import pytest
from _platformdb_setup import BaseAPITestPlatformDB
from leap.adapters.db._models import (AdapterConfig, AssetType, Brand, BrandAsset, BrandLite,
                                      BrandRole, BrandStatus, IdentityService, Service,
                                      StorageService, User)
from leap.adapters.db.platformdb import PagerResultsInternal

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brands.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_users.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_services.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brand_assets.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_adapters.sql")
]


class TestPlatformDBBrands(BaseAPITestPlatformDB):
    def setup_method(self):
        self.load_data(INSERT_FILES)
        self._prepare_expected()

    def _prepare_expected(self):
        self.expected_brand1 = Brand(id='965b3d108954424e914c0b64b6d9649d',
                                     handle='BOBBEL',
                                     deleted=False,
                                     status_key=BrandStatus.LIVE,
                                     created_at=datetime.datetime(2022, 1, 1, 17, 35, 53, 100000,
                                                                  tzinfo=datetime.timezone(
                                                                      datetime.timedelta(days=-1, seconds=64800))),
                                     updated_at=datetime.datetime(2022, 1, 2, 17, 35, 54, 100000,
                                                                  tzinfo=datetime.timezone(
                                                                      datetime.timedelta(days=-1, seconds=64800))),
                                     name='Bob Belcher',
                                     timezone='Etc/UTC',
                                     contact_name='contact1',
                                     contact_email='bob@burgers.com',
                                     contact_phone='phone1',
                                     role=BrandRole.NEW.value,
                                     brand_code='BOBBEL',
                                     website="www.burgers.com",
                                     box_link='foo.com')
        self.expected_brand_lite1 = BrandLite(id='965b3d108954424e914c0b64b6d9649d',
                                              name='Bob Belcher',
                                              role=BrandRole.NEW.value,
                                              brand_code='BOBBEL')
        self.expected_brand2 = Brand(id='8ea2f9410f9d4570b11de78809ae833b',
                                     handle='LINDAB',
                                     deleted=False,
                                     status_key=BrandStatus.NEW,
                                     created_at=datetime.datetime(2022, 1, 3, 17, 35, 53, 100000,
                                                                  tzinfo=datetime.timezone(
                                                                      datetime.timedelta(days=-1, seconds=64800))),
                                     updated_at=datetime.datetime(2022, 1, 4, 17, 35, 53, 100000,
                                                                  tzinfo=datetime.timezone(
                                                                      datetime.timedelta(days=-1, seconds=64800))),
                                     name='Linda Belcher',
                                     timezone='Etc/UTC',
                                     contact_name='contact2',
                                     contact_email='linda@burgers.com',
                                     role=None,
                                     brand_code='LINDAB',
                                     contact_phone='phone2')
        self.expected_brand_lite2 = BrandLite(id='8ea2f9410f9d4570b11de78809ae833b',
                                              name='Linda Belcher',
                                              role=None,
                                              brand_code='LINDAB')
        self.expected_brand3 = Brand(id='8ff14ece7bd64e5dbfc46c1ef8df3cba',
                                     handle='JOEYPE',
                                     deleted=True,
                                     status_key=BrandStatus.EVAL,
                                     created_at=datetime.datetime(2022, 1, 5, 17, 36, 53, 100000,
                                                                  tzinfo=datetime.timezone(
                                                                      datetime.timedelta(days=-1, seconds=64800))),
                                     updated_at=datetime.datetime(2022, 1, 6, 17, 35, 53, 100000,
                                                                  tzinfo=datetime.timezone(
                                                                      datetime.timedelta(days=-1, seconds=64800))),
                                     name='Joey Pesto',
                                     timezone='Etc/UTC',
                                     contact_name='contact3',
                                     contact_email='joey@pesto.com',
                                     role=None,
                                     brand_code='JOEYPE',
                                     contact_phone='phone3')
        self.expected_brand_lite3 = BrandLite(id='8ff14ece7bd64e5dbfc46c1ef8df3cba',
                                              name='Joey Pesto',
                                              role=None,
                                              brand_code='JOEYPE')
        self.expected_brand4 = Brand(id='9558fe1dea4b4a34b2651dc8bc6aaaa9',
                                     handle='JIMMYP',
                                     deleted=False,
                                     status_key=BrandStatus.PROSPECTIVE,
                                     created_at=datetime.datetime(2022, 1, 7, 17, 37, 53, 100000,
                                                                  tzinfo=datetime.timezone(
                                                                      datetime.timedelta(days=-1, seconds=64800))),
                                     updated_at=datetime.datetime(2022, 1, 8, 17, 35, 53, 100000,
                                                                  tzinfo=datetime.timezone(
                                                                      datetime.timedelta(days=-1, seconds=64800))),
                                     name='Jimmy Pesto',
                                     timezone='Etc/UTC',
                                     contact_name='contact4',
                                     contact_email='jimmy@pesto.com',
                                     role=None,
                                     brand_code='JIMMYP',
                                     contact_phone='phone4')
        self.expected_brand_lite4 = BrandLite(id='9558fe1dea4b4a34b2651dc8bc6aaaa9',
                                              name='Jimmy Pesto',
                                              role=None,
                                              brand_code='JIMMYP')
        self.expected_brand5 = Brand(id='85ce1ec7e2ba40ce824c6edb3bc6c315',
                                     handle='TINABE',
                                     deleted=False,
                                     status_key=BrandStatus.NEW,
                                     created_at=datetime.datetime(2022, 1, 9, 17, 35, 53, 100000,
                                                                  tzinfo=datetime.timezone(
                                                                      datetime.timedelta(days=-1, seconds=64800))),
                                     updated_at=datetime.datetime(2022, 1, 10, 17, 35, 53, 100000,
                                                                  tzinfo=datetime.timezone(
                                                                      datetime.timedelta(days=-1, seconds=64800))),
                                     name='Tina Belcher',
                                     timezone='Etc/UTC',
                                     contact_name='contact5',
                                     contact_email='tina@burgers.com',
                                     contact_phone='phone5',
                                     role=None,
                                     brand_code='TINABE')
        self.expected_brand_lite5 = BrandLite(id='85ce1ec7e2ba40ce824c6edb3bc6c315',
                                              name='Tina Belcher',
                                              role=None,
                                              brand_code='TINABE')

        self.expected_service1 = Service(id='5fdf32c7e67849559a0672a1bb7bd88a',
                                         type='shopify',
                                         handle='shopify',
                                         created_at=datetime.datetime(2022, 1, 5, 17, 35, 53, 100000,
                                                                      tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                         updated_at=datetime.datetime(2022, 1, 6, 17, 35, 53, 100000,
                                                                      tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                         api_key='key',
                                         api_secret='secret',
                                         config={"app_id": "shopify-ext-id", "landing_page": "http://leap-landing-page", "sig_check": "ok"})

        self.expected_adapter1 = AdapterConfig(id='a4d0fa9731764b8e9331d6706b1e6598',
                                               brand_id=self.expected_brand1.id,
                                               service_id=self.expected_service1.id,
                                               service_type=self.expected_service1.type,
                                               created_at=datetime.datetime(2022, 1, 7, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               updated_at=datetime.datetime(2022, 1, 8, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               internal=True,
                                               instance_id='instance1')
        self.expected_adapter2 = AdapterConfig(id='ca7a11457b71482881089a7a08052eb9',
                                               brand_id=self.expected_brand1.id,
                                               service_id=self.expected_service1.id,
                                               service_type=self.expected_service1.type,
                                               created_at=datetime.datetime(2022, 1, 9, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               updated_at=datetime.datetime(2022, 1, 10, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               internal=True,
                                               instance_id='instance2')
        self.expected_adapter3 = AdapterConfig(id='f9963407e4054dbb9aa108c1544c5207',
                                               brand_id=self.expected_brand1.id,
                                               service_id=self.expected_service1.id,
                                               service_type=self.expected_service1.type,
                                               created_at=datetime.datetime(2022, 1, 11, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               updated_at=datetime.datetime(2022, 1, 12, 17, 35, 53, 100000,
                                                                            tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                               internal=True,
                                               instance_id='instance3')

        self.expected_user1 = User(id='8938020952bd446f9c70cffe25d34605',
                                   user_id='external1',
                                   identity_service=IdentityService.AUTH0,
                                   created_at=datetime.datetime(2022, 1, 9, 17, 35, 53, 100000,
                                                                  tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                   updated_at=datetime.datetime(2022, 1, 10, 17, 35, 53, 100000,
                                                                  tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))))

        self.expected_asset1 = BrandAsset(id='a1eb5183cf4b4128b0ceca74bcc8aa79',
                                          brand_id=self.expected_brand1.id,
                                          created_by_id=self.expected_user1.id,
                                          type=AssetType.STORE_DATA,
                                          storage_service=StorageService.GCS,
                                          created_at=datetime.datetime(2022, 1, 11, 17, 35, 53, 100000,
                                                                       tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                          updated_at=datetime.datetime(2022, 1, 12, 17, 35, 53, 100000,
                                                                       tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
                                          deleted=False,
                                          bucket='bucket1',
                                          path='path1',
                                          name='asset1')

    def test_get_brand_by_id(self):
        actual_brand1 = self.platformDB.get_brand_by_id(self.expected_brand1.id)
        assert actual_brand1 == self.expected_brand1

        actual_brand2 = self.platformDB.get_brand_by_id(self.expected_brand2.id)
        assert actual_brand2 == self.expected_brand2

    def test_get_adapter_for_brand(self):
        actual_adapters = self.platformDB.get_adapters_for_brand(self.expected_brand1.id, True)
        assert len(actual_adapters) == 3
        assert actual_adapters[0] == self.expected_adapter1
        assert actual_adapters[0].client_id == self.expected_brand1.id
        assert actual_adapters[0].brand_id == self.expected_brand1.id
        assert actual_adapters[1] == self.expected_adapter2
        assert actual_adapters[1].client_id == self.expected_brand1.id
        assert actual_adapters[1].brand_id == self.expected_brand1.id
        assert actual_adapters[2] == self.expected_adapter3
        assert actual_adapters[2].client_id == self.expected_brand1.id
        assert actual_adapters[2].brand_id == self.expected_brand1.id

        actual_adapters = self.platformDB.get_adapters_for_brand(self.expected_brand2.id, True)
        assert len(actual_adapters) == 0

    def test_save_adapter_brand_id_and_client_id(self):
        actual_adapter = AdapterConfig(id='a4d0fa9731764b8e9331d6706b1e6598',
                                       brand_id=self.expected_brand1.id,
                                       client_id=self.expected_brand1.id,
                                       service_id=self.expected_service1.id,
                                       service_type=self.expected_service1.type,
                                       created_at=datetime.datetime(2022, 1, 7, 17, 35, 53, 100000,
                                                                    tzinfo=datetime.timezone(
                                                                        datetime.timedelta(days=-1, seconds=64800))),
                                       updated_at=datetime.datetime(2022, 1, 8, 17, 35, 53, 100000,
                                                                    tzinfo=datetime.timezone(
                                                                        datetime.timedelta(days=-1, seconds=64800))),
                                       internal=True,
                                       instance_id='instance1')
        assert actual_adapter == self.expected_adapter1

    def test_save_adapter_brand_id_and_different_client_id(self):
        with pytest.raises(TypeError):
            AdapterConfig(id='a4d0fa9731764b8e9331d6706b1e6598',
                          brand_id="one",
                          client_id="two",
                          service_id=self.expected_service1.id,
                          service_type=self.expected_service1.type,
                          created_at=datetime.datetime(2022, 1, 7, 17, 35, 53, 100000,
                                                       tzinfo=datetime.timezone(
                                                           datetime.timedelta(days=-1, seconds=64800))),
                          updated_at=datetime.datetime(2022, 1, 8, 17, 35, 53, 100000,
                                                       tzinfo=datetime.timezone(
                                                           datetime.timedelta(days=-1, seconds=64800))),
                          internal=True,
                          instance_id='instance1')

    def test_save_adapter_no_client_or_brand_id(self):
        with pytest.raises(TypeError):
            AdapterConfig(id='a4d0fa9731764b8e9331d6706b1e6598',
                          service_id=self.expected_service1.id,
                          service_type=self.expected_service1.type,
                          created_at=datetime.datetime(2022, 1, 7, 17, 35, 53, 100000,
                                                       tzinfo=datetime.timezone(
                                                           datetime.timedelta(days=-1, seconds=64800))),
                          updated_at=datetime.datetime(2022, 1, 8, 17, 35, 53, 100000,
                                                       tzinfo=datetime.timezone(
                                                           datetime.timedelta(days=-1, seconds=64800))),
                          internal=True,
                          instance_id='instance1')

    def test_save_adapter_brand_id(self):
        actual_adapter = AdapterConfig(id='a4d0fa9731764b8e9331d6706b1e6598',
                                       client_id=self.expected_brand1.id,
                                       service_id=self.expected_service1.id,
                                       service_type=self.expected_service1.type,
                                       created_at=datetime.datetime(2022, 1, 7, 17, 35, 53, 100000,
                                                                    tzinfo=datetime.timezone(
                                                                        datetime.timedelta(days=-1, seconds=64800))),
                                       updated_at=datetime.datetime(2022, 1, 8, 17, 35, 53, 100000,
                                                                    tzinfo=datetime.timezone(
                                                                        datetime.timedelta(days=-1, seconds=64800))),
                                       internal=True,
                                       instance_id='instance1')
        assert actual_adapter == self.expected_adapter1

    def test_get_brand_assets_for_brand(self):
        actual_assets = self.platformDB.get_brand_assets_for_brand(self.expected_brand1.id, False)
        assert len(actual_assets) == 1
        assert actual_assets[0] == self.expected_asset1

        actual_assets = self.platformDB.get_brand_assets_for_brand(self.expected_brand2.id, False)
        assert len(actual_assets) == 0

    def test_get_service_by_id(self):
        actual_service = self.platformDB.get_service_by_id(self.expected_service1.id)
        assert actual_service == self.expected_service1

    def test_where_generator(self):
        actual_str = self.platformDB._generate_where_clause("bIll", None, None)
        assert actual_str == "WHERE LOWER(name) SIMILAR TO '%bill%'"

        actual_str = self.platformDB._generate_where_clause("ben", True, None)
        assert actual_str == "WHERE deleted = true AND LOWER(name) SIMILAR TO '%ben%'"

        actual_str = self.platformDB._generate_where_clause("BoB", True, "LIVE")
        assert actual_str == "WHERE deleted = true AND status_key = 'LIVE' AND LOWER(name) SIMILAR TO '%bob%'"

        actual_str = self.platformDB._generate_where_clause(None, None, None)
        assert actual_str == ""

        actual_str = self.platformDB._generate_where_clause(None, True, None)
        assert actual_str == "WHERE deleted = true"

        actual_str = self.platformDB._generate_where_clause(None, False, None)
        assert actual_str == "WHERE deleted = false"

        actual_str = self.platformDB._generate_where_clause(None, None, "LIVE")
        assert actual_str == "WHERE status_key = 'LIVE'"

        actual_str = self.platformDB._generate_where_clause(None, True, "LIVE")
        assert actual_str == "WHERE deleted = true AND status_key = 'LIVE'"


    @pytest.mark.parametrize("tc", [
        'Bob', 'bob', 'BOB', 'bOb', 'BoB', 'bo', 'ob'])
    def test_get_brand_paging_find_single(self, tc: str):
        expected_result = PagerResultsInternal(
            [self.expected_brand1],
            1,
            10,
            0,
            1,
            1)

        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, tc)
        assert actual_result == expected_result

    @pytest.mark.parametrize("tc", [
        'Bob', 'bob', 'BOB', 'bOb', 'BoB', 'bo', 'ob'])
    def test_get_brand_paging_find_single_lite(self, tc: str):
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite1],
            1,
            10,
            0,
            1,
            1)

        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, tc, lite=True)
        assert actual_result == expected_result

    @pytest.mark.parametrize("tc", [
        'Belcher', 'belcher', 'bElChEr', 'bel', 'che', 'er', 'ElCh'
    ])
    def test_get_brand_paging_find_many(self, tc: str):
        expected_result = PagerResultsInternal(
            [self.expected_brand1, self.expected_brand2, self.expected_brand5],
            3,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, tc)
        assert actual_result == expected_result

    @pytest.mark.parametrize("tc", [
        'Belcher', 'belcher', 'bElChEr', 'bel', 'che', 'er', 'ElCh'
    ])
    def test_get_brand_paging_find_many_lite(self, tc: str):
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite1, self.expected_brand_lite2, self.expected_brand_lite5],
            3,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, tc, lite=True)
        assert actual_result == expected_result

    def test_get_brand_paging_deleted(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand3],
            1,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, "pesto", True)
        assert actual_result == expected_result

    def test_get_brand_paging_deleted_lite(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite3],
            1,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, "pesto", True, lite=True)
        assert actual_result == expected_result

    def test_get_brand_paging_status(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand2, self.expected_brand5],
            2,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, "Belcher", False, BrandStatus.NEW.value)
        assert actual_result == expected_result

        expected_result = PagerResultsInternal(
            [],
            0,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, "Belcher", False, BrandStatus.EVAL.value)
        assert actual_result == expected_result

    def test_get_brand_paging_status_lite(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite2, self.expected_brand_lite5],
            2,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, "Belcher", False, BrandStatus.NEW.value, lite=True)
        assert actual_result == expected_result

        expected_result = PagerResultsInternal(
            [],
            0,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, "Belcher", False, BrandStatus.EVAL.value, lite=True)
        assert actual_result == expected_result

    def test_get_brand_paging_status_deleted(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand3],
            1,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, "Pesto", True, BrandStatus.EVAL.value)
        assert actual_result == expected_result

        expected_result = PagerResultsInternal(
            [],
            0,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, "Pesto", True, BrandStatus.PROSPECTIVE.value)
        assert actual_result == expected_result

    def test_get_brand_paging_status_deleted_lite(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite3],
            1,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, "Pesto", True, BrandStatus.EVAL.value, lite=True)
        assert actual_result == expected_result

        expected_result = PagerResultsInternal(
            [],
            0,
            10,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, "Pesto", True, BrandStatus.PROSPECTIVE.value, lite=True)
        assert actual_result == expected_result

    def test_get_brand_paging_limit(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand1, self.expected_brand2],
            3,
            2,
            0,
            2,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 2, 1, "Belcher")
        assert actual_result == expected_result

        # Reverse order
        expected_result = PagerResultsInternal(
            [self.expected_brand5, self.expected_brand2],
            3,
            2,
            0,
            2,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "DESC", 2, 1, "Belcher")
        assert actual_result == expected_result

    def test_get_brand_paging_limit_lite(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite1, self.expected_brand_lite2],
            3,
            2,
            0,
            2,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 2, 1, "Belcher", lite=True)
        assert actual_result == expected_result

        # Reverse order
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite5, self.expected_brand_lite2],
            3,
            2,
            0,
            2,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "DESC", 2, 1, "Belcher", lite=True)
        assert actual_result == expected_result

    def test_get_brand_paging_limit_second_page(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand5],
            3,
            2,
            2,
            2,
            2)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 2, 2, "Belcher")
        assert actual_result == expected_result

        # Reverse order
        expected_result = PagerResultsInternal(
            [self.expected_brand1],
            3,
            2,
            2,
            2,
            2)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "DESC", 2, 2, "Belcher")
        assert actual_result == expected_result

    def test_get_brand_paging_limit_second_page_lite(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite5],
            3,
            2,
            2,
            2,
            2)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 2, 2, "Belcher", lite=True)
        assert actual_result == expected_result

        # Reverse order
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite1],
            3,
            2,
            2,
            2,
            2)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "DESC", 2, 2, "Belcher", lite=True)
        assert actual_result == expected_result

    def test_get_brand_paging_status_deleted_limit(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand2, self.expected_brand5],
            2,
            2,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 2, 1, "Belcher", False, BrandStatus.NEW.value)
        assert actual_result == expected_result

        # Reverse order
        expected_result = PagerResultsInternal(
            [self.expected_brand5, self.expected_brand2],
            2,
            2,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "DESC", 2, 1, "Belcher", False, BrandStatus.NEW.value)
        assert actual_result == expected_result

    def test_get_brand_paging_status_deleted_limit_lite(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite2, self.expected_brand_lite5],
            2,
            2,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 2, 1, "Belcher", False, BrandStatus.NEW.value, lite=True)
        assert actual_result == expected_result

        # Reverse order
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite5, self.expected_brand_lite2],
            2,
            2,
            0,
            1,
            1)
        actual_result = self.platformDB.get_all_brands_paging("created_at", "DESC", 2, 1, "Belcher", False, BrandStatus.NEW.value, lite=True)
        assert actual_result == expected_result

    def test_get_brand_paging_limit_diff_sort(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand1, self.expected_brand2],
            3,
            2,
            0,
            2,
            1)
        actual_result = self.platformDB.get_all_brands_paging("name", "ASC", 2, 1, "Belcher")
        assert actual_result == expected_result

        # Reverse order
        expected_result = PagerResultsInternal(
            [self.expected_brand5, self.expected_brand2],
            3,
            2,
            0,
            2,
            1)
        actual_result = self.platformDB.get_all_brands_paging("name", "DESC", 2, 1, "Belcher")
        assert actual_result == expected_result

    def test_get_brand_paging_limit_diff_sort_lite(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite1, self.expected_brand_lite2],
            3,
            2,
            0,
            2,
            1)
        actual_result = self.platformDB.get_all_brands_paging("name", "ASC", 2, 1, "Belcher", lite=True)
        assert actual_result == expected_result

        # Reverse order
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite5, self.expected_brand_lite2],
            3,
            2,
            0,
            2,
            1)
        actual_result = self.platformDB.get_all_brands_paging("name", "DESC", 2, 1, "Belcher", lite=True)
        assert actual_result == expected_result

    def test_get_brand_paging_limit_diff_sort_second_page(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand5],
            3,
            2,
            2,
            2,
            2)
        actual_result = self.platformDB.get_all_brands_paging("name", "ASC", 2, 2, "Belcher")
        assert actual_result == expected_result

        # Reverse order
        expected_result = PagerResultsInternal(
            [self.expected_brand1],
            3,
            2,
            2,
            2,
            2)
        actual_result = self.platformDB.get_all_brands_paging("name", "DESC", 2, 2, "Belcher")
        assert actual_result == expected_result

    def test_get_brand_paging_limit_diff_sort_second_page_lite(self):
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite5],
            3,
            2,
            2,
            2,
            2)
        actual_result = self.platformDB.get_all_brands_paging("name", "ASC", 2, 2, "Belcher", lite=True)
        assert actual_result == expected_result

        # Reverse order
        expected_result = PagerResultsInternal(
            [self.expected_brand_lite1],
            3,
            2,
            2,
            2,
            2)
        actual_result = self.platformDB.get_all_brands_paging("name", "DESC", 2, 2, "Belcher", lite=True)
        assert actual_result == expected_result

    @pytest.mark.parametrize("tc", ['bob2', 'BOB2', 'weird'])
    def test_get_brand_paging_find_none(self, tc: str):
        expected_result = PagerResultsInternal(
            [],
            0,
            10,
            0,
            1,
            1)

        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, tc)
        assert actual_result == expected_result

    @pytest.mark.parametrize("tc", ['bob2', 'BOB2', 'weird'])
    def test_get_brand_paging_find_none_lite(self, tc: str):
        expected_result = PagerResultsInternal(
            [],
            0,
            10,
            0,
            1,
            1)

        actual_result = self.platformDB.get_all_brands_paging("created_at", "ASC", 10, 1, tc, lite=True)
        assert actual_result == expected_result

    def test_get_brand_box_link(self):
        brand_id = self.expected_brand1.id
        box_link = "box.link.com"
        affected = self.platformDB.update_brand_box_link_by_id(brand_id, box_link)
        assert affected == 1

        actual_result = self.platformDB.get_brand_box_link_by_id(brand_id)
        expected_result = box_link
        assert actual_result == expected_result

        actual_brand = self.platformDB.get_brand_by_id(brand_id)
        assert actual_brand.box_link == box_link
        assert actual_brand.updated_at != self.expected_brand1.updated_at
