import os
import pytest

from leap.adapters.db.models.ephemeral_shopify_install import EphemeralShopifyInstall
from _platformdb_setup import BaseAPITestPlatformDB
from typing import Tuple

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_ephemeral_shopify_install.sql"),
]

class TestPlatformDBBrandUsers(BaseAPITestPlatformDB):
    def setup_method(self):
        self.load_data(INSERT_FILES)

    @pytest.mark.parametrize("test_cases", [
        ("bs-app-tester", True),
        ("bobbleheads", False),
    ])
    def test_is_ephemeral_app_installed(self, test_cases: Tuple[str, bool]):
        instance_id, expected = test_cases

        actual = self.platformDB.is_ephemeral_app_installed(instance_id)

        assert actual == expected

    def test_insert_ephemeral_shopify_install(self):
        assert False == self.platformDB.is_ephemeral_app_installed("bobbleheads")

        self.platformDB.insert_ephemeral_shopify_install(
            EphemeralShopifyInstall.build(
                {
                    "instance_id": "bobbleheads",
                    "access_token": None
                }
            )
        )
        
        assert True == self.platformDB.is_ephemeral_app_installed("bobbleheads")

    def test_insert_ephemeral_shopify_install_with_access_token(self):
        assert False == self.platformDB.is_ephemeral_app_installed("bobbleheads")

        self.platformDB.insert_ephemeral_shopify_install(
            EphemeralShopifyInstall.build(
                {
                    "instance_id": "bobbleheads",
                    "access_token": "bobbleheads-token"
                }
            )
        )
        
        assert True == self.platformDB.is_ephemeral_app_installed("bobbleheads")
