import datetime
import os

from leap.adapters.db._models import ClientOmniChannelInsights, ClientOmniChannelInsightsMarkets, \
    ClientOmniChannelInsightsTopProductsNetSales, ClientOmniChannelInsightsTopProductsUnitsSold, \
    OmniChannelActiveLeapMarkets
from _platformdb_setup import BaseAPITestPlatformDB

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILE = os.path.join(CUR_DIR, "setup/platformdb_inserts_omni.sql")


class TestPlatformDB(BaseAPITestPlatformDB):
    def setup_method(self):
        self.load_data([INSERT_FILE])
        self._prepare_expected()

    def _prepare_expected(self):
        self.expected_omni_insights1 = ClientOmniChannelInsights(
            client_handle='redletter',
            gross_profit_margin=40.0,
            ppv_score=0.052000000000000005,
            fitness_score=16.685714285714287,
            orders_last_12_months=60,
            net_sales_last_12_months=105334.7,
            buyers_last_12_months=51,
            margin_dollars_last_12_months=702.2313333333334,
            average_order_value_last_12_months=1755.5783333333334,
            purchase_frequency_last_12_months=1.1764705882352942,
            units_per_transaction_last_12_months=1.1333333333333333,
            lifetime_value_last_12_months=1652.3090196078433,
            refund_rate_last_12_months=0.0,
            average_order_value_omni_channel=2001.3592999999998,
            purchase_frequency_omni_channel=1.4588235294117649,
            units_per_transaction_omni_channel=1.156,
            lifetime_value_omni_channel=2335.704030117647,
            refund_rate_omni_channel=0.0,
            purchase_frequency_percent_change=0.24000000000000007,
            average_order_value_percent_change=0.13999999999999987,
            refund_rate_percent_change=0.0,
            lifetime_value_percent_change=0.41359999999999986,
            top_5_net_sales_percentage=0.5792208880311722,
            top_5_units_sold_percentage=0.5806451612903226,
            total_product_count=12,
            top_5_product_concentration=0.4166666666666667,
            top_market_city='Virginia Beach',
            top_market_state_abbreviation='VA',
            total_leap_market_count=3,
            id='9ea09898f4e64b8298e02cd1caa8efe9',
            created_at=datetime.datetime(2022, 5, 11, 16, 1, 49, 549401,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=68400))))
        self.expected_omni_insights_empty = ClientOmniChannelInsights(
            client_handle='empty_insights',
            gross_profit_margin=None,
            ppv_score=None,
            fitness_score=None,
            orders_last_12_months=None,
            net_sales_last_12_months=None,
            buyers_last_12_months=None,
            margin_dollars_last_12_months=None,
            average_order_value_last_12_months=None,
            purchase_frequency_last_12_months=None,
            units_per_transaction_last_12_months=None,
            lifetime_value_last_12_months=None,
            refund_rate_last_12_months=None,
            average_order_value_omni_channel=None,
            purchase_frequency_omni_channel=None,
            units_per_transaction_omni_channel=None,
            lifetime_value_omni_channel=None,
            refund_rate_omni_channel=None,
            purchase_frequency_percent_change=None,
            average_order_value_percent_change=None,
            refund_rate_percent_change=None,
            lifetime_value_percent_change=None,
            top_5_net_sales_percentage=None,
            top_5_units_sold_percentage=None,
            total_product_count=None,
            top_5_product_concentration=None,
            top_market_city=None,
            top_market_state_abbreviation=None,
            total_leap_market_count=None,
            id='e458917b02b344f5b4d3fa1cbb21f381',
            created_at=datetime.datetime(2022, 5, 11, 16, 1, 49, 549401,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=68400))))

        self.expected_omni_market1 = ClientOmniChannelInsightsMarkets(
            client_handle='bad-birdie',
            market='Phoenix-Mesa-Scottsdale, AZ',
            cbsa_code='38060',
            net_sales_last_12_months=453905.72000000055,
            city='Phoenix',
            state_abbreviation='AZ',
            market_net_sales_rank=1,
            is_leap_market=True,
            id='1fb12f8c6e2f46a38c2c7a6dffd314e5',
            created_at=datetime.datetime(2022, 5, 12, 4, 9, 16, 800453,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=68400))))
        self.expected_omni_market2 = ClientOmniChannelInsightsMarkets(
            client_handle='bad-birdie',
            market='Los Angeles-Long Beach-Anaheim, CA',
            cbsa_code='31080',
            net_sales_last_12_months=426499.80000000034,
            city='Los Angeles',
            state_abbreviation='CA',
            market_net_sales_rank=2,
            is_leap_market=True,
            id='4b197afde359402484c4b0e722490e1c',
            created_at=datetime.datetime(2022, 5, 12, 5, 10, 16, 800453,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=68400))))
        self.expected_omni_market_empty = ClientOmniChannelInsightsMarkets(
            client_handle='empty_market',
            market=None,
            cbsa_code=None,
            net_sales_last_12_months=None,
            city=None,
            state_abbreviation=None,
            market_net_sales_rank=None,
            is_leap_market=None,
            id='817fb709f4de4a33bcb4128adbe8685a',
            created_at=datetime.datetime(2022, 5, 12, 5, 10, 16, 800453,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=68400))))

        self.expected_omni_units1 = ClientOmniChannelInsightsTopProductsUnitsSold(
            client_handle='vincero-collective',
            product_id=4815581085786,
            product_title='Gift Bag',
            product_units_sold=7150,
            total_brand_units_sold=202240,
            product_percent_of_units_sold=0.03535403481012658,
            product_units_sold_rank=1,
            product_count=1006,
            id='48e1b12cc0174203938596546dfbdd07',
            created_at=datetime.datetime(2022, 5, 12, 13, 3, 52, 804210, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=68400))))
        self.expected_omni_units2 = ClientOmniChannelInsightsTopProductsUnitsSold(
            client_handle='vincero-collective',
            product_id=4714500128858,
            product_title='Protection Plan',
            product_units_sold=4667,
            total_brand_units_sold=202240,
            product_percent_of_units_sold=0.02307654272151899,
            product_units_sold_rank=2,
            product_count=1006,
            id='9017a0bf42a34fef88a9bf2f17d56bfb',
            created_at=datetime.datetime(2022, 5, 12, 13, 3, 52, 804210, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=68400))))
        self.expected_omni_units_empty = ClientOmniChannelInsightsTopProductsUnitsSold(
            client_handle='empty_units',
            product_id=None,
            product_title=None,
            product_units_sold=None,
            total_brand_units_sold=None,
            product_percent_of_units_sold=None,
            product_units_sold_rank=None,
            product_count=None,
            id='633060c8517944ccb5f216b8c4a0c52c',
            created_at=datetime.datetime(2022, 5, 12, 13, 3, 52, 804210, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=68400))))

        self.expected_omni_active_markets1 = OmniChannelActiveLeapMarkets(
            cbsa_code='35620',
            market='New York-Newark-Jersey City, NY-NJ-PA',
            city='New York',
            state_abbreviation='NY',
            market_latitude=40.697403,
            market_longitude=-74.120106,
            id='e933aa446f764c1d83deb385b821c20f',
            created_at=datetime.datetime(2022, 5, 12, 13, 3, 44, 13094,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=68400))))
        self.expected_omni_active_markets2 = OmniChannelActiveLeapMarkets(
            cbsa_code='31080',
            market='Los Angeles-Long Beach-Anaheim, CA',
            city='Los Angeles',
            state_abbreviation='CA',
            market_latitude=34.0201598,
            market_longitude=-118.6925967,
            id='01ac13b4488d4213a663c2098cf81645',
            created_at=datetime.datetime(2022, 5, 12, 13, 3, 44, 13094,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=68400))))
        self.expected_omni_active_markets3 = OmniChannelActiveLeapMarkets(
            cbsa_code=None,
            market=None,
            city=None,
            state_abbreviation=None,
            market_latitude=None,
            market_longitude=None,
            id='8da46054bf3a4da99403398f190b9635',
            created_at=datetime.datetime(2022, 5, 12, 13, 3, 44, 13094,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=68400))))

        self.expected_omni_net_sales1 = ClientOmniChannelInsightsTopProductsNetSales(
            client_handle='silk-laundry',
            product_id=4326878773315,
            product_title='90S SLIP DRESS HAZELNUT',
            product_net_sales=7161.08,
            total_brand_net_sales=220732.81,
            product_percent_of_net_sales=0.03244229981034537,
            product_net_sales_rank=4,
            product_count=257,
            id='b8488c72332040008a56544b7a61fa33',
            created_at=datetime.datetime(2022, 5, 12, 14, 7, 2, 43316, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=68400))))
        self.expected_omni_net_sales2 = ClientOmniChannelInsightsTopProductsNetSales(
            client_handle='silk-laundry',
            product_id=4594344722499,
            product_title='90S SLIP DRESS EMERALD',
            product_net_sales=5756.75,
            total_brand_net_sales=220732.81,
            product_percent_of_net_sales=0.026080173581806892,
            product_net_sales_rank=5,
            product_count=258,
            id='4e70d6edeb6146bea33e549b509c97eb',
            created_at=datetime.datetime(2022, 5, 12, 14, 7, 2, 43316, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=68400))))
        self.expected_omni_net_sales_empty = ClientOmniChannelInsightsTopProductsNetSales(
            client_handle='empty_net',
            product_id=None,
            product_title=None,
            product_net_sales=None,
            total_brand_net_sales=None,
            product_percent_of_net_sales=None,
            product_net_sales_rank=None,
            product_count=None,
            id='85ac203a422f4cb2961e8fc628a16e16',
            created_at=datetime.datetime(2022, 5, 12, 14, 7, 2, 43316, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=68400))))

    def test_get_client_omni_channel_insights(self):
        actual_insight1 = self.platformDB.get_client_omni_channel_insights('redletter')
        assert actual_insight1 == self.expected_omni_insights1

        actual_insight2 = self.platformDB.get_client_omni_channel_insights('bad_id')
        assert actual_insight2 is None

    def test_get_client_omni_channel_insights_empty(self):
        actual_insight1 = self.platformDB.get_client_omni_channel_insights('empty_insights')
        assert actual_insight1 == self.expected_omni_insights_empty

    def test_get_client_omni_channel_insights_markets(self):
        actual_markets_list = self.platformDB.get_client_omni_channel_insights_markets('bad-birdie', 10)
        assert len(actual_markets_list) == 2
        assert actual_markets_list[0] == self.expected_omni_market1
        assert actual_markets_list[1] == self.expected_omni_market2

        actual_markets_list = self.platformDB.get_client_omni_channel_insights_markets('NA', 10)
        assert len(actual_markets_list) == 0

    def test_get_client_omni_channel_insights_markets_empty(self):
        actual_markets_list = self.platformDB.get_client_omni_channel_insights_markets('empty_market', 10)
        assert len(actual_markets_list) == 1
        assert actual_markets_list[0] == self.expected_omni_market_empty

    def test_get_client_omni_channel_insights_top_products_net_sales(self):
        actual_net_sales_list = self.platformDB.get_client_omni_channel_insights_top_products_net_sales('silk-laundry', 10)
        assert len(actual_net_sales_list) == 2
        assert actual_net_sales_list[0] == self.expected_omni_net_sales1
        assert actual_net_sales_list[1] == self.expected_omni_net_sales2

        actual_net_sales_list = self.platformDB.get_client_omni_channel_insights_top_products_net_sales('NA', 10)
        assert len(actual_net_sales_list) == 0

    def test_get_client_omni_channel_insights_top_products_net_sales_empty(self):
        actual_net_sales_list = self.platformDB.get_client_omni_channel_insights_top_products_net_sales('empty_net', 10)
        assert len(actual_net_sales_list) == 1
        assert actual_net_sales_list[0] == self.expected_omni_net_sales_empty

    def test_get_client_omnichannel_insights_products_units_sold(self):
        actual_units_list = self.platformDB.get_client_omnichannel_insights_products_units_sold('vincero-collective', 10)
        assert len(actual_units_list) == 2
        assert actual_units_list[0] == self.expected_omni_units1
        assert actual_units_list[1] == self.expected_omni_units2

        actual_units_list = self.platformDB.get_client_omnichannel_insights_products_units_sold('NA', 10)
        assert len(actual_units_list) == 0

    def test_get_client_omnichannel_insights_products_units_sold_empty(self):
        actual_units_list = self.platformDB.get_client_omnichannel_insights_products_units_sold('empty_units', 10)
        assert len(actual_units_list) == 1
        assert actual_units_list[0] == self.expected_omni_units_empty

    def test_get_omni_channel_active_leap_markets(self):
        actual_markets_list = self.platformDB.get_omni_channel_active_leap_markets()
        assert len(actual_markets_list) == 3
        assert actual_markets_list[0] == self.expected_omni_active_markets1
        assert actual_markets_list[1] == self.expected_omni_active_markets2
        assert actual_markets_list[2] == self.expected_omni_active_markets3
