import datetime
import os
import uuid
import pytest
import sqlalchemy.exc

from _platformdb_setup import BaseAPITestPlatformDB
from leap.adapters.db._models import RelatedBrand, RelatedBrandsGroup

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brands.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_related_brands.sql"),
]


class TestPlatformDBBrands(BaseAPITestPlatformDB):
    def setup_method(self):
        self.load_data(INSERT_FILES)
        self._prepare_expected()

    def _prepare_expected(self):
        self.expected_brand1 = '965b3d108954424e914c0b64b6d9649d'
        self.expected_brand2 = '8ea2f9410f9d4570b11de78809ae833b'
        self.expected_brand3 = '8ff14ece7bd64e5dbfc46c1ef8df3cba'
        self.expected_brand4 = '9558fe1dea4b4a34b2651dc8bc6aaaa9'
        self.expected_brand5 = '85ce1ec7e2ba40ce824c6edb3bc6c315'

        self.expected_related_brand1 = RelatedBrand(
            id='cafa1f3c90e9459c91a943523a1c1ed8',
            parent_id=self.expected_brand1,
            child_id=self.expected_brand2,
            created_at=datetime.datetime(2022, 1, 2, 17, 35, 54, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))))

        self.expected_related_brand2 = RelatedBrand(
            id='71c430a60ee4408691fb5388667f0695',
            parent_id=self.expected_brand1,
            child_id=self.expected_brand3,
            created_at=datetime.datetime(2022, 1, 2, 17, 35, 54, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))))

    def test_get_related_brands_group(self):
        related_brands_group1 = self.platformDB.get_related_brands_group(self.expected_brand1)
        assert len(related_brands_group1) == 1
        assert self.expected_brand1 == related_brands_group1[0].parent.id
        assert self.expected_brand2 in [x.id for x in related_brands_group1[0].children]
        assert self.expected_brand3 in [x.id for x in related_brands_group1[0].children]
        assert self.expected_brand1 == related_brands_group1[0].brand_id

        related_brands_group2 = self.platformDB.get_related_brands_group(self.expected_brand2)
        assert len(related_brands_group2) == 1
        assert self.expected_brand1 == related_brands_group2[0].parent.id
        assert self.expected_brand2 in [x.id for x in related_brands_group2[0].children]
        assert self.expected_brand3 in [x.id for x in related_brands_group2[0].children]
        assert self.expected_brand2 == related_brands_group2[0].brand_id

        related_brands_group3 = self.platformDB.get_related_brands_group(self.expected_brand3)
        assert len(related_brands_group3) == 1
        assert self.expected_brand1 == related_brands_group3[0].parent.id
        assert self.expected_brand2 in [x.id for x in related_brands_group3[0].children]
        assert self.expected_brand3 in [x.id for x in related_brands_group3[0].children]
        assert self.expected_brand3 == related_brands_group3[0].brand_id

        related_brands_group4 = self.platformDB.get_related_brands_group(self.expected_brand4)
        assert len(related_brands_group4) == 0

        related_brands_group5 = self.platformDB.get_related_brands_group(self.expected_brand5)
        assert len(related_brands_group5) == 0

    def test_get_related_brands(self):
        actual_related_brands1 = self.platformDB.query_related_brands(id=self.expected_related_brand1.id)
        assert self.expected_related_brand1 == actual_related_brands1

        actual_related_brands2 = self.platformDB.query_related_brands(id=self.expected_related_brand2.id)
        assert self.expected_related_brand2 == actual_related_brands2

        actual_related_brands3 = self.platformDB.query_related_brands(parent_id=self.expected_related_brand1.parent_id)
        assert 2 == len(actual_related_brands3)
        assert self.expected_related_brand1 == actual_related_brands3[0]
        assert self.expected_related_brand2 == actual_related_brands3[1]

        actual_related_brands4 = self.platformDB.query_related_brands(child_id=self.expected_related_brand1.child_id)
        assert 1 == len(actual_related_brands4)
        assert self.expected_related_brand1 == actual_related_brands4[0]

    def test_insert_failure(self):
        # Insert will not work because this is not a valid brand id
        with pytest.raises(sqlalchemy.exc.IntegrityError):
            self.platformDB.save_relatedbrand(RelatedBrand(
                id=uuid.uuid4().hex,
                parent_id="7c7405a7-2364-438d-95d4-3c927a6840ef",
                child_id=self.expected_brand5))
        actual_related_brands1 = self.platformDB.get_related_brands_group(self.expected_brand5)
        assert len(actual_related_brands1) == 0

        # Insert will not work because a child can only have one parent
        with pytest.raises(sqlalchemy.exc.IntegrityError):
            self.platformDB.save_relatedbrand(RelatedBrand(
                id=uuid.uuid4().hex,
                parent_id=self.expected_brand4,
                child_id=self.expected_brand2))
        actual_related_brands2 = self.platformDB.get_related_brands_group(self.expected_brand2)
        assert len(actual_related_brands2) == 1
        assert self.expected_brand1 == actual_related_brands2[0].parent.id
        assert self.expected_brand2 in [x.id for x in actual_related_brands2[0].children]
        assert self.expected_brand3 in [x.id for x in actual_related_brands2[0].children]
        assert self.expected_brand2 == actual_related_brands2[0].brand_id

    def test_save_brand_multiple_relationships(self):
        self.platformDB.save_relatedbrand(RelatedBrand(
            id=uuid.uuid4().hex,
            parent_id=self.expected_brand2,
            child_id=self.expected_brand4))
        actual_related_brands = self.platformDB.get_related_brands_group(self.expected_brand2)
        assert len(actual_related_brands) == 2

        assert self.expected_brand1 == actual_related_brands[0].parent.id
        assert self.expected_brand2 in [x.id for x in actual_related_brands[0].children]
        assert self.expected_brand3 in [x.id for x in actual_related_brands[0].children]
        assert self.expected_brand2 == actual_related_brands[0].brand_id

        assert self.expected_brand2 == actual_related_brands[1].parent.id
        assert self.expected_brand4 in [x.id for x in actual_related_brands[1].children]
        assert self.expected_brand2 == actual_related_brands[1].brand_id

    def test_save_brand(self):
        self.platformDB.save_relatedbrand(RelatedBrand(
            id=uuid.uuid4().hex,
            parent_id=self.expected_brand4,
            child_id=self.expected_brand5))
        actual_related_brands1 = self.platformDB.get_related_brands_group(self.expected_brand5)
        assert len(actual_related_brands1) == 1
        assert self.expected_brand4 == actual_related_brands1[0].parent.id
        assert self.expected_brand5 in [x.id for x in actual_related_brands1[0].children]
        assert self.expected_brand5 == actual_related_brands1[0].brand_id

        actual_related_brands2 = self.platformDB.get_related_brands_group(self.expected_brand4)
        assert self.expected_brand4 == actual_related_brands2[0].parent.id
        assert self.expected_brand5 in [x.id for x in actual_related_brands2[0].children]
        assert self.expected_brand4 == actual_related_brands2[0].brand_id
