from _platformdb_setup import BaseAPITestPlatformDB

class TestPlatformDBSalesforceSpaceImages(BaseAPITestPlatformDB):
    
    def setup_method(self):
        self.load_data([])

    def test_insert_space_image_urls(self):
        self.platformDB.upsert_salesforce_space_image_urls('salesforce_space_id_test', ['image_url_1', 'image_url_2'])
        actual = self.platformDB.get_salesforce_space_image_urls_by_space_id('salesforce_space_id_test')
        assert actual == ['image_url_1', 'image_url_2']
        
    def test_update_space_image_urls(self):
        self.platformDB.upsert_salesforce_space_image_urls('salesforce_space_id_test', [
                                                'image_url_1', 'image_url_2'])
        self.platformDB.upsert_salesforce_space_image_urls('salesforce_space_id_test', [
                                                'image_url_1_updated', 'image_url_2'])
        actual = self.platformDB.get_salesforce_space_image_urls_by_space_id(
            'salesforce_space_id_test')
        assert actual == ['image_url_1_updated', 'image_url_2']
