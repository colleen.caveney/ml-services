import datetime
import os

from leap.adapters.db._models import Store
from leap.adapters.db.platformdb import PagerResultsInternal
from _platformdb_setup import BaseAPITestPlatformDB

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_brands.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_spaces.sql"),
    os.path.join(CUR_DIR, "setup/platformdb_inserts_stores.sql")
]


class TestPlatformDBAdapters(BaseAPITestPlatformDB):
    def setup_method(self):
        self.load_data(INSERT_FILES)
        self._prepare_expected()

    def _prepare_expected(self):
        self.expected_store1 = Store(
            id="ee35fe1d1a9d4b23bbe7e87c212efe26",
            space_id="82b83166b38744b280af1ab4c20b0c50",
            brand_id="965b3d108954424e914c0b64b6d9649d",
            code="BOBBEL-01",
            deleted=False,
            created_at=datetime.datetime(2022, 1, 1, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 8, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            slack_channel_id='abc',
            possession_date=datetime.datetime(2022, 1, 9, 17, 35, 53, 100000,
                                              tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            open_date=datetime.datetime(2022, 1, 10, 17, 35, 53, 100000,
                                        tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            close_date=None,
            shopper_nps_survey_url='survey.com')

        self.expected_store2 = Store(
            id="46eecfe1374b4ed18a991094ef1c4b1b",
            space_id="1f0d08aa095643b79f78f758d7cdfe94",
            brand_id="965b3d108954424e914c0b64b6d9649d",
            code="BOBBEL-02",
            deleted=False,
            created_at=datetime.datetime(2022, 1, 2, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 12, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            slack_channel_id='def',
            possession_date=datetime.datetime(2022, 1, 13, 17, 35, 53, 100000,
                                              tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            open_date=datetime.datetime(2022, 1, 15, 17, 35, 53, 100000,
                                        tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            close_date=None)
        self.expected_store3 = Store(
            id="b30953056b314c98b3d449cfc1fe89ff",
            space_id="b1bb25b44a144e589caadd843d3e109b",
            brand_id="965b3d108954424e914c0b64b6d9649d",
            code="BOBBEL-03",
            deleted=False,
            created_at=datetime.datetime(2022, 1, 3, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 17, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            slack_channel_id='ghi',
            possession_date=datetime.datetime(2022, 1, 18, 17, 35, 53, 100000,
                                              tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            open_date=datetime.datetime(2022, 1, 19, 17, 35, 53, 100000,
                                        tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            close_date=None)
        self.expected_store4 = Store(
            id='LINDAB-01',
            client_id='8ea2f9410f9d4570b11de78809ae833b',
            brand_id='8ea2f9410f9d4570b11de78809ae833b',
            location_id='82b83166b38744b280af1ab4c20b0c50',
            space_id='82b83166b38744b280af1ab4c20b0c50',
            deleted=False,
            slack_channel_id=None,
            possession_date=datetime.datetime(2020, 1, 26, 13, 22, 15,
                                              tzinfo=datetime.timezone(
                                                  datetime.timedelta(days=-1, seconds=64800))),
            open_date=datetime.datetime(2020, 2, 26, 13, 22, 15, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=64800))),
            shopper_nps_survey_url=None,
            margin=None,
            code='LINDAB-01',
            created_at=datetime.datetime(
                2022, 1, 4, 13, 47, 55,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(
                2023, 1, 4, 3, 54, 5,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            close_date=None)
        self.expected_store5 = Store(
            id='LINDAB-02',
            client_id='8ea2f9410f9d4570b11de78809ae833b',
            brand_id='8ea2f9410f9d4570b11de78809ae833b',
            location_id='82b83166b38744b280af1ab4c20b0c50',
            space_id='82b83166b38744b280af1ab4c20b0c50',
            deleted=False,
            slack_channel_id=None,
            possession_date=datetime.datetime(2019, 2, 25, 13, 22, 15,
                                              tzinfo=datetime.timezone(
                                                  datetime.timedelta(days=-1, seconds=64800))),
            open_date=datetime.datetime(2019, 3, 25, 14, 22, 15, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=68400))),
            close_date=datetime.datetime(2019, 12, 25, 13, 22, 15, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=64800))),
            shopper_nps_survey_url=None,
            margin=None,
            code='LINDAB-02',
            created_at=datetime.datetime(
                2022, 1, 5, 13, 47, 55,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(
                2023, 1, 4, 3, 54, 5,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))))
        self.expected_store6 = Store(
            id='LINDAB-03',
            client_id='8ea2f9410f9d4570b11de78809ae833b',
            brand_id='8ea2f9410f9d4570b11de78809ae833b',
            location_id='82b83166b38744b280af1ab4c20b0c50',
            space_id='82b83166b38744b280af1ab4c20b0c50',
            deleted=False,
            slack_channel_id=None,
            possession_date=datetime.datetime(2019, 12, 25, 13, 22, 15,
                                              tzinfo=datetime.timezone(
                                                  datetime.timedelta(days=-1, seconds=64800))),
            open_date=datetime.datetime(2019, 1, 25, 13, 22, 15, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=64800))),
            close_date=None,
            shopper_nps_survey_url=None,
            margin=None,
            code='LINDAB-03',
            created_at=datetime.datetime(
                2022, 1, 6, 13, 47, 55,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(
                2023, 1, 4, 3, 54, 5,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))))
        self.expected_store7 = Store(
            id='TINABE-01',
            client_id='85ce1ec7e2ba40ce824c6edb3bc6c315',
            brand_id='85ce1ec7e2ba40ce824c6edb3bc6c315',
            location_id='82b83166b38744b280af1ab4c20b0c50',
            space_id='82b83166b38744b280af1ab4c20b0c50',
            deleted=True,
            slack_channel_id=None,
            possession_date=datetime.datetime(2019, 3, 25, 14, 22, 15,
                                              tzinfo=datetime.timezone(
                                                  datetime.timedelta(days=-1, seconds=68400))),
            open_date=datetime.datetime(2019, 4, 25, 14, 22, 15, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=68400))),
            close_date=datetime.datetime(2019, 11, 25, 13, 22, 15, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=64800))),
            shopper_nps_survey_url=None,
            margin=None,
            code='TINABE-01',
            created_at=datetime.datetime(
                2022, 1, 7, 13, 47, 55,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(
                2023, 1, 4, 3, 54, 5,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))))
        self.expected_store8 = Store(
            id='TINABE-02',
            client_id='85ce1ec7e2ba40ce824c6edb3bc6c315',
            brand_id='85ce1ec7e2ba40ce824c6edb3bc6c315',
            location_id='82b83166b38744b280af1ab4c20b0c50',
            space_id='82b83166b38744b280af1ab4c20b0c50',
            deleted=False,
            slack_channel_id=None,
            possession_date=datetime.datetime(2019, 1, 1, 13, 22, 15,
                                              tzinfo=datetime.timezone(
                                                  datetime.timedelta(days=-1, seconds=64800))),
            open_date=datetime.datetime(2019, 2, 1, 13, 22, 15, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=64800))),
            close_date=None,
            shopper_nps_survey_url=None,
            margin=None,
            code='TINABE-02',
            created_at=datetime.datetime(
                2022, 1, 8, 13, 47, 55,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(
                2023, 1, 4, 3, 54, 5,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))))
        self.expected_store9 = Store(
            id='TINABE-03',
            client_id='85ce1ec7e2ba40ce824c6edb3bc6c315',
            brand_id='85ce1ec7e2ba40ce824c6edb3bc6c315',
            location_id='82b83166b38744b280af1ab4c20b0c50',
            space_id='82b83166b38744b280af1ab4c20b0c50',
            deleted=True,
            slack_channel_id=None,
            possession_date=datetime.datetime(2019, 2, 25, 13, 22, 15,
                                              tzinfo=datetime.timezone(
                                                  datetime.timedelta(days=-1, seconds=64800))),
            open_date=datetime.datetime(2019, 3, 25, 14, 22, 15, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=68400))),
            close_date=datetime.datetime(2019, 12, 25, 13, 22, 15, tzinfo=datetime.timezone(
                datetime.timedelta(days=-1, seconds=64800))),
            shopper_nps_survey_url=None,
            margin=None,
            code='TINABE-03',
            created_at=datetime.datetime(
                2022, 1, 9, 13, 47, 55,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(
                2023, 1, 4, 3, 54, 5,
                tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))))

        # TODO remove these lines when frontend has updated to use code instead of id
        self.expected_store1_id = self.expected_store1.id
        self.expected_store2_id = self.expected_store2.id
        self.expected_store3_id = self.expected_store3.id
        self.expected_store1.id = self.expected_store1.code
        self.expected_store2.id = self.expected_store2.code
        self.expected_store3.id = self.expected_store3.code

    def test_get_store_by_id(self):
        actual = self.platformDB.get_store_by_id(self.expected_store1_id)
        assert actual == self.expected_store1

        actual = self.platformDB.get_store_by_id(self.expected_store2_id)
        assert actual == self.expected_store2

        actual = self.platformDB.get_store_by_id(self.expected_store3_id)
        assert actual == self.expected_store3

    def test_get_stores_paging_asc(self):
        expected_results = PagerResultsInternal(
            [self.expected_store1, self.expected_store2, self.expected_store3, self.expected_store4, self.expected_store5],
            9,
            5,
            0,
            2,
            1
        )
        actual = self.platformDB.get_all_stores_paging("created_at", "ASC", 5, 1)
        assert actual == expected_results

    def test_get_stores_paging_desc(self):
        expected_results = PagerResultsInternal(
            [self.expected_store9, self.expected_store8, self.expected_store7, self.expected_store6, self.expected_store5],
            9,
            5,
            0,
            2,
            1
        )
        actual = self.platformDB.get_all_stores_paging("created_at", "DESC", 5, 1)
        assert actual == expected_results

    def test_get_stores_paging_limit(self):
        expected_results = PagerResultsInternal(
            [self.expected_store9, self.expected_store8],
            9,
            2,
            0,
            5,
            1
        )
        actual = self.platformDB.get_all_stores_paging("created_at", "DESC", 2, 1)
        assert actual == expected_results

    def test_get_stores_paging_limit_2nd_page(self):
        expected_results = PagerResultsInternal(
            [self.expected_store7, self.expected_store6],
            9,
            2,
            2,
            5,
            2
        )
        actual = self.platformDB.get_all_stores_paging("created_at", "DESC", 2, 2)
        assert actual == expected_results
