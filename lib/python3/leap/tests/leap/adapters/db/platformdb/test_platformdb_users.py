import datetime
import os

from leap.adapters.db._models import User, IdentityService
from leap.adapters.db.platformdb import PagerResultsInternal
from _platformdb_setup import BaseAPITestPlatformDB

CUR_DIR = os.path.dirname(os.path.realpath(__file__))
SCHEMA_FILE = os.path.join(CUR_DIR, "setup/platformdb_schema.sql")
INSERT_FILES = [
    os.path.join(CUR_DIR, "setup/platformdb_inserts_users.sql"),
]


class TestPlatformDBUsers(BaseAPITestPlatformDB):
    def setup_method(self):
        self.load_data(INSERT_FILES)
        self._prepare_expected()

    def _prepare_expected(self):
        self.expected_user1 = User(
            id="8938020952bd446f9c70cffe25d34605",
            identity_service=IdentityService.AUTH0,
            created_at=datetime.datetime(2022, 1, 9, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 10, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            external_user_id="external1",
            is_deleted=False,
            email="bob@burgers.com",
            display_name="Burgers4Life",
            full_name="Bob Belcher"
        )
        self.expected_user2 = User(
            id="cda48bffdbed456da70055a4e5bc4072",
            identity_service=IdentityService.AUTH0,
            created_at=datetime.datetime(2022, 1, 11, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 12, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            external_user_id="external2",
            is_deleted=False,
            email="linda@burgers.com",
            display_name="WineGirl",
            full_name="Linda Belcher"
        )
        self.expected_user3 = User(
            id="cc423d8981974bdab3dead97585fed67",
            identity_service=IdentityService.AUTH0,
            created_at=datetime.datetime(2022, 1, 13, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            updated_at=datetime.datetime(2022, 1, 14, 17, 35, 53, 100000,
                                         tzinfo=datetime.timezone(datetime.timedelta(days=-1, seconds=64800))),
            external_user_id="external3",
            is_deleted=False,
            email="gene@burgers.com",
            display_name="KeyKing",
            full_name="Gene Belcher"
        )

    def test_get_users_paging_asc(self):
        expected_results = PagerResultsInternal(
            [self.expected_user1, self.expected_user2, self.expected_user3],
            3,
            10,
            0,
            1,
            1
        )
        actual = self.platformDB.get_all_users_paging("created_at", "ASC", 10, 1)
        assert actual == expected_results

    def test_get_users_paging_desc(self):
        expected_results = PagerResultsInternal(
            [self.expected_user3, self.expected_user2, self.expected_user1],
            3,
            10,
            0,
            1,
            1
        )
        actual = self.platformDB.get_all_users_paging("created_at", "DESC", 10, 1)
        assert actual == expected_results

    def test_get_users_paging_limit(self):
        expected_results = PagerResultsInternal(
            [self.expected_user3, self.expected_user2],
            3,
            2,
            0,
            2,
            1
        )
        actual = self.platformDB.get_all_users_paging("created_at", "DESC", 2, 1)
        assert actual == expected_results

    def test_get_users_paging_limit_2nd_page(self):
        expected_results = PagerResultsInternal(
            [self.expected_user1],
            3,
            2,
            2,
            2,
            2
        )
        actual = self.platformDB.get_all_users_paging("created_at", "DESC", 2, 2)
        assert actual == expected_results
