import dataclasses
import re
import uuid
from datetime import datetime
from typing import cast

import pytest

import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models
from leap.google.cloud.firestore import tests as fstests


class TestAdapterSKUModel:
    def setup_method(self):
        self.preserved_uuidfactory = _models._uuidfactory

    def teardown_method(self):
        _models._uuidfactory = self.preserved_uuidfactory

    # tests

    def test_required_fields(self):
        asku = db.AdapterSKU("sku1", "a1")
        dataclasses.is_dataclass(asku)
        assert "sku1" == asku.sku_id
        assert "a1" == asku.adapter_id

    def test_default_fields(self):
        asku = db.AdapterSKU("sku1", "a1")
        dataclasses.is_dataclass(asku)
        assert asku.id
        assert (datetime.now() - asku.created_at).seconds < 10
        assert asku.updated_at
        assert (datetime.now() - asku.updated_at).seconds < 10

    def test_generate_default_id(self):
        asku1 = db.AdapterSKU("sku1", "a1")
        asku2 = db.AdapterSKU("v2", "a1")
        assert re.match(r"^[0-9a-f]{32}$", cast(str, asku1.id))
        assert re.match(r"^[0-9a-f]{32}$", cast(str, asku2.id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        asku1 = db.AdapterSKU("sku1", "a1")
        asku2 = db.AdapterSKU("v2", "a1")
        assert uuid.UUID(int=0).hex == asku1.id
        assert uuid.UUID(int=1).hex == asku2.id


class TestFirestoreAdapterSKU:
    # lookup with id

    def test_adapter_sku_not_found(self):
        db_client = fstests.Client()
        fsdb = db.FirestoreDB(db_client)
        assert None == fsdb.adapter_sku(id="noop-asku")

    def test_adapter_sku_success(self):
        db_client = fstests.Client()
        db_client.store_document("adapter_skus/noop-asku", {
            "id": "noop-asku",
            "adapter_id": "a1",
            "sku_id": "sku1",
            "created_at": datetime(1970, 8, 3, 0, 0, 0, 0),
            "updated_at": datetime(2019, 8, 3, 0, 0, 0, 0),
        })

        fsdb = db.FirestoreDB(db_client)
        asku = fsdb.adapter_sku(id="noop-asku")

        assert asku
        assert "noop-asku" == asku.id
        assert "a1" == asku.adapter_id
        assert "sku1" == asku.sku_id
        assert datetime(1970, 8, 3, 0, 0, 0, 0) == asku.created_at
        assert datetime(2019, 8, 3, 0, 0, 0, 0) == asku.updated_at

    # lookup with filters

    def test_stream_no_limit_no_filters(self):
        db_client = fstests.Client()
        db_client.store_document(
            "adapter_skus/1", {"id": "1", "adapter_id": "a1", "sku_id": "sku1"})
        db_client.store_document(
            "adapter_skus/2", {"id": "2", "adapter_id": "a2", "sku_id": "v2"})
        db_client.store_document(
            "adapter_skus/3", {"id": "3", "adapter_id": "a1", "sku_id": "v3"})
        fsdb = db.FirestoreDB(db_client)

        askus = [asku for asku in fsdb.adapter_skus()]
        assert 3 == len(askus)
        assert ["1", "2", "3"] == [asku.id for asku in askus]

    def test_stream_with_limit_no_filters(self):
        db_client = fstests.Client()
        db_client.store_document(
            "adapter_skus/1", {"id": "1", "adapter_id": "a1", "sku_id": "sku1"})
        db_client.store_document(
            "adapter_skus/2", {"id": "2", "adapter_id": "a2", "sku_id": "v2"})
        db_client.store_document(
            "adapter_skus/3", {"id": "3", "adapter_id": "a1", "sku_id": "v3"})
        fsdb = db.FirestoreDB(db_client)

        askus = [asku for asku in fsdb.adapter_skus(2)]
        assert 2 == len(askus)
        assert ["1", "2"] == [asku.id for asku in askus]

    def test_stream_no_limit_with_filters(self):
        db_client = fstests.Client()
        db_client.store_document(
            "adapter_skus/1", {"id": "1", "adapter_id": "a1", "sku_id": "sku1"})
        db_client.store_document(
            "adapter_skus/2", {"id": "2", "adapter_id": "a2", "sku_id": "v2"})
        db_client.store_document(
            "adapter_skus/3", {"id": "3", "adapter_id": "a1", "sku_id": "v3"})
        fsdb = db.FirestoreDB(db_client)

        askus = [asku for asku in fsdb.adapter_skus(adapter_id="a1")]
        assert 2 == len(askus)
        assert ["1", "3"] == [asku.id for asku in askus]

    def test_stream_with_limit_with_filters(self):
        db_client = fstests.Client()
        db_client.store_document(
            "adapter_skus/1", {"id": "1", "adapter_id": "a1", "sku_id": "sku1"})
        db_client.store_document(
            "adapter_skus/2", {"id": "2", "adapter_id": "a2", "sku_id": "v2"})
        db_client.store_document(
            "adapter_skus/3", {"id": "3", "adapter_id": "a1", "sku_id": "v2"})
        fsdb = db.FirestoreDB(db_client)

        askus = [asku for asku in fsdb.adapter_skus(limit=1, sku_id="v2")]
        assert 1 == len(askus)
        assert ["2"] == [asku.id for asku in askus]

    def test_stream_with_no_limit_with_nested_keys_filter(self):
        db_client = fstests.Client()
        db_client.store_document("adapter_skus/1", {
            "id": "1", "adapter_id": "a1", "sku_id": "sku1",
            "keys": {"a": "1", "b": "2"}
        })
        db_client.store_document("adapter_skus/2", {
            "id": "2", "adapter_id": "a2", "sku_id": "v2",
            "keys": {"a": "1", "b": "3"}
        })
        fsdb = db.FirestoreDB(db_client)

        askus = [asku for asku in fsdb.adapter_skus(**{"keys.a": "1"})]
        assert 2 == len(askus)
        assert ["1", "2"] == [asku.id for asku in askus]

        askus = [asku for asku in fsdb.adapter_skus(**{"keys.b": "3"})]
        assert 1 == len(askus)
        assert ["2"] == [asku.id for asku in askus]

    # lookup with adapter-key

    def test_asku_via_adapter_requires_key(self):
        fsdb = db.FirestoreDB(fstests.Client())
        with pytest.raises(ValueError, match="lookup key not provided"):
            fsdb.adapter_sku_via_adapter(None)

    def test_asku_via_adapter_not_found(self):
        fsdb = db.FirestoreDB(fstests.Client())
        assert None == fsdb.adapter_sku_via_adapter(("a-noop", "0"))

    def test_asku_via_adapter_success(self):
        db_client = fstests.Client()
        db_client.store_document("adapter_skus/noop-asku", {
            "id": "noop-asku",
            "adapter_id": "a1",
            "sku_id": "sku1",
            "keys": {"inventory_item_id": "asku1"},
            "created_at": datetime(1970, 8, 3, 0, 0, 0, 0),
            "updated_at": datetime(2019, 8, 3, 0, 0, 0, 0),
        })

        fsdb = db.FirestoreDB(db_client)
        asku = fsdb.adapter_sku_via_adapter(("a1", "asku1"))
        assert asku
        assert "noop-asku" == asku.id
        assert "a1" == asku.adapter_id
        assert "sku1" == asku.sku_id
        assert {"inventory_item_id": "asku1"} == asku.keys
        assert datetime(1970, 8, 3, 0, 0, 0, 0) == asku.created_at
        assert datetime(2019, 8, 3, 0, 0, 0, 0) == asku.updated_at

        queries = db_client.collection("adapter_skus").queries
        assert 1 == len(queries)
        assert 1 == queries[0].requested_limit

    # saving

    def test_save_adapter_sku(self):
        db_client = fstests.Client()
        asku = db.AdapterSKU(
            "sku123", "a1", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_adapter_sku(asku)

        ref = db_client.document("adapter_skus/{0}".format(asku.id))
        assert dataclasses.asdict(asku) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
