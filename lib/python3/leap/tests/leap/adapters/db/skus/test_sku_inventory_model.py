import dataclasses
import re
import uuid
from datetime import datetime
from typing import Optional, Tuple, cast

import pytest

import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models
from leap.google.cloud.firestore import tests as fstests


class TestSKUInventoryModel:
    def setup_method(self):
        self.preserved_uuidfactory = _models._uuidfactory

    def teardown_method(self):
        _models._uuidfactory = self.preserved_uuidfactory

    # tests

    def test_required_fields(self):
        inv = db.SKUInventory("sku1", "ll-test1", 123)
        dataclasses.is_dataclass(inv)
        assert "sku1" == inv.sku_id
        assert "ll-test1" == inv.store_id
        assert 123 == inv.absolute_stock_level

    def test_default_fields(self):
        inv = db.SKUInventory("sku2", "ll-test2", 456)
        dataclasses.is_dataclass(inv)
        assert inv.id
        assert (datetime.now() - inv.created_at).seconds < 10
        assert inv.updated_at
        assert (datetime.now() - inv.updated_at).seconds < 10

    def test_generate_default_id(self):
        inv1 = db.SKUInventory("sku3", "ll-test3", 3456)
        inv2 = db.SKUInventory("sku4", "ll-test4", 4456)
        assert re.match(r"^[0-9a-f]{32}$", cast(str, inv1.id))
        assert re.match(r"^[0-9a-f]{32}$", cast(str, inv2.id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        inv1 = db.SKUInventory("sku3", "ll-test3", 3456)
        inv2 = db.SKUInventory("sku4", "ll-test4", 4456)
        assert uuid.UUID(int=0).hex == inv1.id
        assert uuid.UUID(int=1).hex == inv2.id

class TestFirestoreSKUInventory:
    @pytest.mark.parametrize("tc", [
        ({}, "si0"),
        ({"absolute_stock_level": 20}, "si1"),
        ({"store_id": "store-2"}, "si2"),
        ({"store_id": "store-3"}, None),
    ])
    def test_single_sku_inventory(self, tc: Tuple[dict, Optional[str]]):
        filters, expected_id = tc

        db_client = fstests.Client()
        db_client.store_document("sku_inventories/si0", {"id": "si0", "sku_id": "sku0", "store_id": "store-0", "absolute_stock_level": 10})
        db_client.store_document("sku_inventories/si1", {"id": "si1", "sku_id": "sku1", "store_id": "store-1", "absolute_stock_level": 20})
        db_client.store_document("sku_inventories/si2", {"id": "si2", "sku_id": "sku2", "store_id": "store-2", "absolute_stock_level": 10})
        fsdb = db.FirestoreDB(db_client)

        actual = fsdb.sku_inventory(**filters)
        assert expected_id == (actual.id if expected_id else None)

    @pytest.mark.parametrize("tc", [
        (0, {}, ["si0", "si1", "si2"]),
        (2, {}, ["si0", "si1"]),
        (0, {"absolute_stock_level": 10}, ["si0", "si2"]),
        (1, {"absolute_stock_level": 10}, ["si0"]),
        (0, {"id": "si1"}, ["si1"]),
    ])
    def test_sku_inventories(self, tc: Tuple[int, dict, list]):
        limit, filters, expected_ids = tc

        db_client = fstests.Client()
        db_client.store_document("sku_inventories/si0", {"id": "si0", "sku_id": "sku0", "store_id": "store-0", "absolute_stock_level": 10})
        db_client.store_document("sku_inventories/si1", {"id": "si1", "sku_id": "sku1", "store_id": "store-1", "absolute_stock_level": 20})
        db_client.store_document("sku_inventories/si2", {"id": "si2", "sku_id": "sku2", "store_id": "store-2", "absolute_stock_level": 10})
        fsdb = db.FirestoreDB(db_client)

        assert expected_ids == [m.id for m in fsdb.sku_inventories(limit, **filters)]

    ## saving

    def test_save_sku_inventory(self):
        db_client = fstests.Client()
        inv = db.SKUInventory("sku123", "ll-test1", 10, updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_sku_inventory(inv)

        ref = db_client.document("sku_inventories/{0}".format(inv.id))
        assert dataclasses.asdict(inv) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
