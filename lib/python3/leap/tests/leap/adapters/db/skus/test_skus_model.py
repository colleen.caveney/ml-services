import dataclasses
import re
import uuid
from datetime import datetime
from typing import Any, List, Tuple, cast

import pytest

import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models
from leap.google.cloud.firestore import tests as fstests


class TestSKUModel:
    def setup_method(self):
        self.preserved_uuidfactory = _models._uuidfactory

    def teardown_method(self):
        _models._uuidfactory = self.preserved_uuidfactory

    # tests

    def test_required_fields(self):
        sku = db.SKU("test-client", "sku-123")
        dataclasses.is_dataclass(sku)
        assert "test-client" == sku.client_id
        assert "sku-123" == sku.sku

    def test_default_fields(self):
        sku = db.SKU("test-client", "sku-123")
        dataclasses.is_dataclass(sku)
        assert sku.id
        assert (datetime.now() - sku.created_at).seconds < 10
        assert sku.updated_at
        assert (datetime.now() - sku.updated_at).seconds < 10

    def test_generate_default_id(self):
        sku1 = db.SKU("test-client-1", "sku-1230")
        sku2 = db.SKU("test-client-2", "sku-1231")
        assert re.match(r"^[0-9a-f]{32}$", cast(str, sku1.id))
        assert re.match(r"^[0-9a-f]{32}$", cast(str, sku2.id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        sku1 = db.SKU("test-client-1", "sku-1230")
        sku2 = db.SKU("test-client-2", "sku-1231")
        assert uuid.UUID(int=0).hex == sku1.id
        assert uuid.UUID(int=1).hex == sku2.id

    @pytest.mark.parametrize("data_expected", [
        ([], ""),
        (["a", "b"], "A|B"),
        (["a", None, "b"], "A|NONE|B"),
        ([1, {}, "b", 3], "1|{}|B|3"),
    ])
    def test_format_ssku(self, data_expected: Tuple[List[Any], str]):
        data, expected = data_expected
        assert expected == db.SKU.format_ssku(*data)

class TestFirestoreSKUs:
    ## lookup

    def test_sku_requires_id(self):
        fsdb = db.FirestoreDB(fstests.Client())
        with pytest.raises(ValueError, match="id not provided"):
            fsdb.sku("")

    def test_sku_not_found(self):
        db_client = fstests.Client()
        fsdb = db.FirestoreDB(db_client)
        assert None == fsdb.sku("noop-sku")

    def test_sku_success(self):
        db_client = fstests.Client()
        db_client.store_document("skus/noop-sku", {
            "id": "noop-sku",
            "client_id": "client-001",
            "sku": "123abc",
            "created_at": datetime(1970, 8, 3, 0, 0, 0, 0),
            "updated_at": datetime(2019, 8, 3, 0, 0, 0, 0),
        })

        fsdb = db.FirestoreDB(db_client)
        sku = fsdb.sku("noop-sku")

        assert sku
        assert "noop-sku" == sku.id
        assert "client-001" == sku.client_id
        assert "123abc" == sku.sku
        assert datetime(1970, 8, 3, 0, 0, 0, 0) == sku.created_at
        assert datetime(2019, 8, 3, 0, 0, 0, 0) == sku.updated_at

    ## lookup with filters

    def test_stream_no_limit_no_filters(self):
        db_client = fstests.Client()
        db_client.store_document("skus/1", {"id": "1", "client_id": "c1", "sku": "sku1"})
        db_client.store_document("skus/2", {"id": "2", "client_id": "c2", "sku": "sku2"})
        db_client.store_document("skus/3", {"id": "3", "client_id": "c1", "sku": "sku2"})
        fsdb = db.FirestoreDB(db_client)

        avs = [av for av in fsdb.skus()]
        assert 3 == len(avs)
        assert ["1", "2", "3"] == [av.id for av in avs]

    def test_stream_with_limit_no_filters(self):
        db_client = fstests.Client()
        db_client.store_document("skus/1", {"id": "1", "client_id": "c1", "sku": "sku1"})
        db_client.store_document("skus/2", {"id": "2", "client_id": "c2", "sku": "sku2"})
        db_client.store_document("skus/3", {"id": "3", "client_id": "c1", "sku": "sku2"})
        fsdb = db.FirestoreDB(db_client)

        avs = [av for av in fsdb.skus(2)]
        assert 2 == len(avs)
        assert ["1", "2"] == [av.id for av in avs]

    def test_stream_no_limit_with_filters(self):
        db_client = fstests.Client()
        db_client.store_document("skus/1", {"id": "1", "client_id": "c1", "sku": "sku1"})
        db_client.store_document("skus/2", {"id": "2", "client_id": "c2", "sku": "sku2"})
        db_client.store_document("skus/3", {"id": "3", "client_id": "c1", "sku": "sku2"})
        fsdb = db.FirestoreDB(db_client)

        avs = [av for av in fsdb.skus(client_id="c1")]
        assert 2 == len(avs)
        assert ["1", "3"] == [av.id for av in avs]

    def test_stream_with_limit_with_filters(self):
        db_client = fstests.Client()
        db_client.store_document("skus/1", {"id": "1", "client_id": "c1", "sku": "sku1"})
        db_client.store_document("skus/2", {"id": "2", "client_id": "c2", "sku": "sku2"})
        db_client.store_document("skus/3", {"id": "3", "client_id": "c1", "sku": "sku2"})
        fsdb = db.FirestoreDB(db_client)

        avs = [av for av in fsdb.skus(limit=1, sku="sku2")]
        assert 1 == len(avs)
        assert ["2"] == [av.id for av in avs]

    ## saving

    def test_save_sku(self):
        db_client = fstests.Client()
        sku = db.SKU("client-001", "sku123", updated_at=datetime(1970, 1, 1, 0, 0, 0, 0))
        db.FirestoreDB(db_client).save_sku(sku)

        ref = db_client.document("skus/{0}".format(sku.id))
        assert dataclasses.asdict(sku) == ref.get().to_dict()
        assert (datetime.now() - ref.get().get("updated_at")).seconds < 10
