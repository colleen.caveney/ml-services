import dataclasses
from datetime import datetime

import pytest

from leap.adapters import db


class TestActionModel:
    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    # tests

    def test_required_fields(self):
        message = {"title": "a title",
                   "body": "hello"}
        action = db.Action(action_type=db.ActionType.NEW_BRAND, client_id="abc123",
                           matched_subscription_ids=["123", "456"], message=message)
        dataclasses.is_dataclass(action)
        assert db.ActionType.NEW_BRAND == action.action_type
        assert "abc123" == action.client_id
        assert "abc123" == action.brand_id
        assert ["123", "456"] == action.matched_subscription_ids
        assert message == action.message

    def test_default_fields(self):
        message = {"title": "a title",
                   "body": "hello"}
        action = db.Action(action_type=db.ActionType.NEW_BRAND, client_id="abc123",
                           matched_subscription_ids=["123", "456"], message=message)
        assert (datetime.now() - action.created_at).seconds < 10
        assert action.updated_at
        assert (datetime.now() - action.updated_at).seconds < 10
        assert "abc123" == action.client_id
        assert "abc123" == action.brand_id

    def test_brand_id(self):
        message = {"title": "a title",
                   "body": "hello"}
        action = db.Action(action_type=db.ActionType.NEW_BRAND, brand_id="abc123",
                           matched_subscription_ids=["123", "456"], message=message)
        dataclasses.is_dataclass(action)
        assert db.ActionType.NEW_BRAND == action.action_type
        assert "abc123" == action.client_id
        assert "abc123" == action.brand_id
        assert ["123", "456"] == action.matched_subscription_ids
        assert message == action.message

    def test_client_and_brand(self):
        message = {"title": "a title",
                   "body": "hello"}
        action = db.Action(action_type=db.ActionType.NEW_BRAND, brand_id="abc123", client_id="abc123",
                           matched_subscription_ids=["123", "456"], message=message)
        dataclasses.is_dataclass(action)
        assert db.ActionType.NEW_BRAND == action.action_type
        assert "abc123" == action.client_id
        assert "abc123" == action.brand_id
        assert ["123", "456"] == action.matched_subscription_ids
        assert message == action.message

    def test_client_different_brand(self):
        message = {"title": "a title",
                   "body": "hello"}
        with pytest.raises(TypeError):
            db.Action(action_type=db.ActionType.NEW_BRAND, brand_id="abc123", client_id="abc1234",
                      matched_subscription_ids=["123", "456"], message=message)

    def test_no_client_or_brand(self):
        message = {"title": "a title",
                   "body": "hello"}
        with pytest.raises(TypeError):
            db.Action(action_type=db.ActionType.NEW_BRAND, matched_subscription_ids=["123", "456"], message=message)
