import dataclasses
import re
import uuid
from datetime import datetime
from typing import cast

import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models


class TestAdapterConfigModel:

    def setup_method(self):
        self.preserved_uuidfactory = _models._uuidfactory

    def teardown_method(self):
        _models._uuidfactory = self.preserved_uuidfactory

    def test_required_fields(self):
        adapter = db.AdapterConfig(client_id="test-client",
                                   service_id="echo-service",
                                   service_type="echo-service",
                                   instance_id="test-site-001")
        dataclasses.is_dataclass(adapter)
        assert "test-client" == adapter.client_id
        assert "echo-service" == adapter.service_id
        assert "echo-service" == adapter.service_type
        assert "test-site-001" == adapter.instance_id

    def test_default_fields(self):
        adapter = db.AdapterConfig(client_id="test-client",
                                   service_id="echo-service",
                                   service_type="echo-service",
                                   instance_id="test-site-002")
        assert False == adapter.internal
        assert adapter.source
        assert adapter.sink
        assert isinstance(adapter.config, dict)
        assert adapter.bq_dataset is None
        assert isinstance(adapter.data_state, dict)
        assert False == adapter.track_inventory
        assert db.InventorySyncStrategy.ONE == adapter.inventory_sync_strategy
        assert (datetime.now() - adapter.created_at).seconds < 10
        assert adapter.updated_at
        assert (datetime.now() - adapter.updated_at).seconds < 10

    def test_generate_default_id(self):
        adapter1 = db.AdapterConfig(client_id="test-client-1",
                                    service_id="echo-service",
                                    service_type="echo-service",
                                    instance_id="test-site-003")
        adapter2 = db.AdapterConfig(client_id="test-client-2",
                                    service_id="echo-service",
                                    service_type="echo-service",
                                    instance_id="test-site-004")
        assert re.match(r"^[0-9a-f]{32}$", cast(str, adapter1.id))
        assert re.match(r"^[0-9a-f]{32}$", cast(str, adapter2.id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        adapter1 = db.AdapterConfig(client_id="test-client-1",
                                    service_id="echo-service",
                                    service_type="echo-service",
                                    instance_id="test-site-005")
        adapter2 = db.AdapterConfig(client_id="test-client-2",
                                    service_id="echo-service",
                                    service_type="echo-service",
                                    instance_id="test-site-006")
        assert uuid.UUID(int=0).hex == adapter1.id
        assert uuid.UUID(int=1).hex == adapter2.id

    def test_is_connected(self):
        adapter = db.AdapterConfig(client_id="client0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0")
        assert not adapter.is_connected
        adapter.config = {"access_token": "not-an-actual-access-token"}
        assert adapter.is_connected
        adapter = db.AdapterConfig(client_id="client0",
                                   service_id="echo-service",
                                   service_type="echo-service",
                                   instance_id="shop0")
        assert adapter.is_connected

    def test_is_collecting(self):
        adapter = db.AdapterConfig(client_id="client0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0")
        assert not adapter.is_collecting
        adapter.config = {"access_token": "not-an-actual-access-token"}
        assert adapter.is_collecting
        adapter.data_state = {"job_id": "some-job-id", "imported": True}
        assert not adapter.is_collecting
        adapter.data_state = {"job_id": "some-job-id", "imported": False}
        assert adapter.is_collecting
        adapter.data_state = {"job_id": "some-job-id"}
        assert adapter.is_collecting
        adapter = db.AdapterConfig(client_id="client0",
                                   service_id="echo-service",
                                   service_type="echo-service",
                                   instance_id="shop0")
        assert not adapter.is_collecting

    def test_is_imported(self):
        adapter = db.AdapterConfig(client_id="client0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0")
        assert not adapter.is_imported
        adapter.data_state = {"job_id": "some-job-id"}
        assert not adapter.is_imported
        adapter.data_state = {"job_id": "some-job-id", "imported": False}
        assert not adapter.is_imported
        adapter.data_state = {"job_id": "some-job-id", "imported": True}
        assert adapter.is_imported
        adapter = db.AdapterConfig(client_id="client0",
                                   service_id="echo-service",
                                   service_type="echo-service",
                                   instance_id="shop0")
        assert not adapter.is_imported

    def test_tier(self):
        adapter = db.AdapterConfig(client_id="client0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0")
        assert "n/a" == adapter.tier
        adapter.config = {"access_token": "not-an-actual-access-token"}
        assert "unknown" == adapter.tier
        adapter.config = {"access_token": "not-an-actual-access-token", "tier": "full"}
        assert "full" == adapter.tier
        adapter.config = {"access_token": "not-an-actual-access-token", "tier": "ro"}
        assert "ro" == adapter.tier
        adapter = db.AdapterConfig(client_id="client0",
                                   service_id="echo-service",
                                   service_type="echo-service",
                                   instance_id="shop0")
        assert "n/a" == adapter.tier

    def test_inventory_sync_strategy(self):
        adapter = db.AdapterConfig(client_id="client0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0")
        assert db.InventorySyncStrategy.ONE == adapter.inventory_sync_strategy
        adapter.config["inventory_sync_strategy"] = db.InventorySyncStrategy.ALL
        assert db.InventorySyncStrategy.ALL == adapter.inventory_sync_strategy
        adapter.config["inventory_sync_strategy"] = "WHOOPS"
        assert db.InventorySyncStrategy.ONE == adapter.inventory_sync_strategy

    def test_merchant_type(self):
        adapter = db.AdapterConfig(client_id="client0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   internal=True)
        assert db.MerchantType.LEAP == adapter.merchant_type
        adapter.internal = False
        assert db.MerchantType.BRAND == adapter.merchant_type
        adapter.service_id = "klaviyo"
        adapter.service_type = "klaviyo"
        assert db.MerchantType.NA == adapter.merchant_type

    def test_sig_check_override(self):
        adapter = db.AdapterConfig(client_id="test-client",
                                   service_id="echo-service",
                                   service_type="echo-service",
                                   instance_id="test-site-002")
        assert False == adapter.has_sig_check_override

        adapter = db.AdapterConfig(client_id="test-client",
                                   service_id="echo-service",
                                   service_type="echo-service",
                                   instance_id="test-site-002",
                                   config={"sig_check": "not-bypass"})
        assert False == adapter.has_sig_check_override

        adapter = db.AdapterConfig(client_id="test-client",
                                   service_id="echo-service",
                                   service_type="echo-service",
                                   instance_id="test-site-002",
                                   config={"sig_check": "bypass"})
        assert True == adapter.has_sig_check_override

        adapter = db.AdapterConfig(client_id="test-client",
                                   service_id="echo-service",
                                   service_type="echo-service",
                                   instance_id="test-site-002",
                                   config={"sig_check": "ok"})
        assert True == adapter.has_sig_check_override
