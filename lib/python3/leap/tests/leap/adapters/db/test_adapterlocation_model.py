import dataclasses
import re
import uuid
from datetime import datetime
from typing import Any, Tuple, cast

import pytest

import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models


class TestAdapterLocationModel:
    def setup_method(self):
        self.preserved_uuidfactory = _models._uuidfactory

    def teardown_method(self):
        _models._uuidfactory = self.preserved_uuidfactory

    def test_required_fields(self):
        aloc = db.AdapterLocation("a0", "LA0-store0", "12345")
        dataclasses.is_dataclass(aloc)
        assert "a0" == aloc.adapter_id
        assert "LA0-store0" == aloc.store_id
        assert "12345" == aloc.adapter_location_id

    def test_default_fields(self):
        aloc = db.AdapterLocation("a0", "LA0-store0", "12345")
        assert True == aloc.primary
        assert True == aloc.enabled
        assert None == aloc.virtual_store_id
        assert (datetime.now() - aloc.created_at).seconds < 10
        assert aloc.updated_at
        assert (datetime.now() - aloc.updated_at).seconds < 10

    @pytest.mark.parametrize("tc", [
        (None, False),
        ("", False),
        (" \n\t", False),
        ("abc", True),
    ])
    def test_is_virtual(self, tc: Tuple[Any, bool]):
        vs_id, expected_is_virtual = tc
        aloc = db.AdapterLocation("a0", "LA0-store0", "12345", virtual_store_id=vs_id)
        assert expected_is_virtual == aloc.is_virtual

    def test_generate_default_id(self):
        aloc1 = db.AdapterLocation("a0-1", "LA0-store0", "12345")
        aloc2 = db.AdapterLocation("a0-2", "LA0-store0", "12345")
        assert re.match(r"^[0-9a-f]{32}$", cast(str, aloc1.id))
        assert re.match(r"^[0-9a-f]{32}$", cast(str, aloc2.id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        aloc1 = db.AdapterLocation("a0-1", "LA0-store0", "12345")
        aloc2 = db.AdapterLocation("a0-2", "LA0-store0", "12345")
        assert uuid.UUID(int=0).hex == aloc1.id
        assert uuid.UUID(int=1).hex == aloc2.id
