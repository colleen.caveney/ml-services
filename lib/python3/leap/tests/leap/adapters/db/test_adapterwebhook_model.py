import dataclasses
import re
import uuid
from datetime import datetime
from typing import cast

import leap.std.uuid
from leap.adapters import EventType, db
from leap.adapters.db import _models


class TestAdapterWebhookModel:
    def setup_method(self):
        self.preserved_uuidfactory = _models._uuidfactory

    def teardown_method(self):
        _models._uuidfactory = self.preserved_uuidfactory

    def test_required_fields(self):
        hook = db.AdapterWebhook(
            "a0", EventType.INVENTORY_CONNECT, "inventory_levels/connect", "12345")
        dataclasses.is_dataclass(hook)
        assert "a0" == hook.adapter_id
        assert EventType.INVENTORY_CONNECT == hook.event_type
        assert "inventory_levels/connect" == hook.adapter_event_type
        assert "12345" == hook.adapter_webhook_id

    def test_default_fields(self):
        hook = db.AdapterWebhook(
            "a1", EventType.INVENTORY_CONNECT, "inventory_levels/connect", "12345")
        assert (datetime.now() - hook.created_at).seconds < 10
        assert hook.updated_at
        assert (datetime.now() - hook.updated_at).seconds < 10
        assert 0 == hook.omcp_version

    def test_generate_default_id(self):
        hook1 = db.AdapterWebhook(
            "a3", EventType.INVENTORY_CONNECT, "inventory_levels/connect", "12345")
        hook2 = db.AdapterWebhook(
            "a4", EventType.INVENTORY_CONNECT, "inventory_levels/connect", "12345")
        assert re.match(r"^[0-9a-f]{32}$", cast(str, hook1.id))
        assert re.match(r"^[0-9a-f]{32}$", cast(str, hook2.id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        hook1 = db.AdapterWebhook(
            "a5", EventType.INVENTORY_CONNECT, "inventory_levels/connect", "12345")
        hook2 = db.AdapterWebhook(
            "a6", EventType.INVENTORY_CONNECT, "inventory_levels/connect", "12345")
        assert uuid.UUID(int=0).hex == hook1.id
        assert uuid.UUID(int=1).hex == hook2.id

    def test_with_omcp_version(self):
        hook = db.AdapterWebhook(adapter_id="a0",
                                 event_type=EventType.INVENTORY_CONNECT,
                                 adapter_event_type="inventory_levels/connect",
                                 adapter_webhook_id="12345",
                                 omcp_version=1)
        dataclasses.is_dataclass(hook)
        assert "a0" == hook.adapter_id
        assert EventType.INVENTORY_CONNECT == hook.event_type
        assert "inventory_levels/connect" == hook.adapter_event_type
        assert "12345" == hook.adapter_webhook_id
        assert 1 == hook.omcp_version
