import dataclasses
import re
import uuid
from datetime import datetime
from typing import cast

import pytest

import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models


class TestbrandModel:
    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    # tests

    def test_required_fields(self):
        brand = db.Brand("test-brand")
        dataclasses.is_dataclass(brand)
        assert "test-brand" == brand.name

    def test_default_fields(self):
        brand = db.Brand("test-brand")
        
        assert not brand.deleted
        assert "Etc/UTC" == brand.timezone
        assert db.ClientConfig() == brand.config
        assert db.BrandStatus.PROSPECTIVE == brand.status_key

        assert (datetime.now() - brand.created_at).seconds < 10
        assert brand.updated_at
        assert (datetime.now() - brand.updated_at).seconds < 10

    @pytest.mark.parametrize("status", list(db.BrandStatus))
    def test_status_field_casting(self, status: db.BrandStatus):
        brand = db.Brand.build({"name": "test-brand", "status_key": status.value})
        assert brand
        assert status == brand.status_key

    @pytest.mark.parametrize("status_key", [
        "LIVES", "FOO"
    ])
    def test_status_field_casting_fails(self, status_key: str):
        with pytest.raises(ValueError, match="'{}' is not a valid BrandStatus".format(status_key)):
            db.Brand.build({"name": "test-brand", "status_key": status_key})

    def test_generate_default_id(self):
        hex_uuid_re = r"^[0-9a-f]{32}$"
        assert re.match(hex_uuid_re, cast(str, db.Brand("brand-001").id))
        assert re.match(hex_uuid_re, cast(str, db.Brand("brand-002").id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        assert uuid.UUID(int=0).hex == db.Brand("brand-003").id
        assert uuid.UUID(int=1).hex == db.Brand("brand-004").id
