import dataclasses
import re
import uuid
from datetime import datetime
from typing import cast

import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models


class TestBrandUserModel:
    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    # tests

    def test_required_fields(self):
        client_user = db.BrandUser("client001", "user001")
        dataclasses.is_dataclass(client_user)
        assert "user001" == client_user.user_id
        assert "client001" == client_user.client_id

    def test_default_fields(self):
        client_user = db.BrandUser("client001", "user001")
        assert client_user.enabled
        assert (datetime.now() - client_user.created_at).seconds < 10
        assert client_user.updated_at
        assert (datetime.now() - client_user.updated_at).seconds < 10

    def test_generate_default_id(self):
        hex_uuid_re = r"^[0-9a-f]{32}$"
        assert re.match(hex_uuid_re, cast(str, db.BrandUser("client001", "user001").id))
        assert re.match(hex_uuid_re, cast(str, db.BrandUser("client002", "user002").id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        assert uuid.UUID(int=0).hex == db.BrandUser("client003", "user003").id
        assert uuid.UUID(int=1).hex == db.BrandUser("client004", "user004").id

