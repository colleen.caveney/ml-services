import dataclasses
import re
import uuid
from datetime import datetime
from typing import cast

import pytest

import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models


class TestBrandAssetModel:
    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    # tests

    def test_required_fields(self):
        asset = db.BrandAsset("client001", "user001", "my-bucket", "/some/path/to.file", "big.file", db.AssetType.FINANCIAL)
        dataclasses.is_dataclass(asset)
        assert "client001" == asset.client_id
        assert "user001" == asset.created_by_id
        assert "my-bucket" == asset.bucket
        assert "/some/path/to.file" == asset.path
        assert "big.file" == asset.name
        assert db.AssetType.FINANCIAL == asset.type

    def test_default_fields(self):
        asset = db.BrandAsset("client001", "user001", "my-bucket", "/some/path/to.file", "a.file", db.AssetType.FINANCIAL)
        assert db.StorageService.GCS == asset.storage_service
        assert (datetime.now() - asset.created_at).seconds < 10
        assert asset.updated_at
        assert (datetime.now() - asset.updated_at).seconds < 10

    def test_generate_default_id(self):
        hex_uuid_re = r"^[0-9a-f]{32}$"
        assert re.match(hex_uuid_re, cast(str, db.BrandAsset("client001", "user001", "b001", "/1.file", "a.file", db.AssetType.FINANCIAL).id))
        assert re.match(hex_uuid_re, cast(str, db.BrandAsset("client002", "user002", "b002", "/2.file", "a.file", db.AssetType.FINANCIAL).id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        assert uuid.UUID(int=0).hex == db.BrandAsset("client003", "user003", "b003", "/3.file", "a.file", db.AssetType.FINANCIAL).id
        assert uuid.UUID(int=1).hex == db.BrandAsset("client004", "user004", "b004", "/4.file", "a.file", db.AssetType.FINANCIAL).id

    @pytest.mark.parametrize("asset_type", list(db.AssetType))
    def test_asset_type_field_casting(self, asset_type: db.AssetType):
        asset = db.BrandAsset.build({
            "client_id": "client001",
            "created_by_id": "user001",
            "bucket": "my-bucket",
            "path": "/to/a.file",
            "name": "this.file",
            "type": asset_type.value,
        })
        assert asset
        assert asset_type == asset.type

    @pytest.mark.parametrize("storage_svc", list(db.StorageService))
    def test_identity_service_field_casting(self, storage_svc: db.StorageService):
        asset = db.BrandAsset.build({
            "client_id": "client001",
            "created_by_id": "user001",
            "bucket": "my-bucket",
            "path": "/to/a.file",
            "name": "this.file",
            "type": db.AssetType.FINANCIAL,
            "storage_service": storage_svc.value,
        })
        assert asset
        assert storage_svc == asset.storage_service
