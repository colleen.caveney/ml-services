import dataclasses
import re
import uuid
from datetime import datetime
from typing import List, Optional, Tuple, cast

import pytest

import leap.std.hashing.md5 as md5
import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models


class TestEntityModel:
    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    # tests

    def test_required_fields(self):
        entity = db.Entity("order", "a-1")
        dataclasses.is_dataclass(entity)
        assert "order" == entity.type
        assert "a-1" == entity.source_adapter_id

    def test_default_fields(self):
        entity = db.Entity("order", "a-1")
        assert isinstance(entity.adapter_entity_map, dict)
        assert isinstance(entity.line_item_maps, list)
        assert isinstance(entity.fulfillments, list)

        assert (datetime.now() - entity.created_at).seconds < 10
        assert (datetime.now() - entity.updated_at).seconds < 10

    def test_generate_default_id(self):
        hex_uuid_re = r"^[0-9a-f]{32}$"
        assert re.match(hex_uuid_re, cast(str, db.Entity("order", "a-1").id))
        assert re.match(hex_uuid_re, cast(str, db.Entity("order", "a-2").id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        assert uuid.UUID(int=0).hex == db.Entity("order", "a-1").id
        assert uuid.UUID(int=1).hex == db.Entity("order", "a-2").id

    def test_entity_id(self):
        entity = db.Entity("order", "a-1", adapter_entity_map={
            "a-1": "123",
            "a-2": "234",
        })
        assert "123" == entity.entity_id("a-1")
        assert "234" == entity.entity_id("a-2")
        assert None == entity.entity_id("a-3")

    # line-items

    def test_line_item(self):
        li1 = {"a-1": "li-1", "a-2": "li-3"}
        li2 = {"a-1": "li-2", "a-2": "li-4"}
        entity = db.Entity("order", "a-1", line_item_maps=[li1, li2])

        assert li1 == entity.line_item(("a-2", "li-3"))
        assert li1 == entity.line_item(("a-1", "li-1"))
        assert li2 == entity.line_item(("a-1", "li-2"))
        assert li2 == entity.line_item(("a-2", "li-4"))
        assert None == entity.line_item(("a-2", "nope"))

    def test_add_line_item_with_empty_key(self):
        entity = db.Entity("order", "a-1")
        li = entity.add_line_item((), ("a-2", "li-2"))
        assert None == li
        assert 0 == len(entity.line_item_maps)

    def test_add_line_item_with_empty_new_key(self):
        entity = db.Entity("order", "a-1")
        li = entity.add_line_item(("a-1", "li-1"), ())
        assert None == li
        assert 0 == len(entity.line_item_maps)

    def test_add_line_item_when_key_not_found(self):
        rec1 = {"a-1": "li-1"}
        entity = db.Entity("order", "a-1", line_item_maps=[rec1])

        li = entity.add_line_item(("a-1", "li-nope"), ("a-2", "li-2"))
        assert None == li
        assert 1 == len(entity.line_item_maps)

    def test_add_line_item_adds_new_key(self):
        rec1 = {"a-1": "li-1", "a-2": "li-4"}
        rec2 = {"a-1": "li-2"}
        rec3 = {"a-1": "li-3", "a-2": "li-6"}
        expected = {"a-1": "li-2", "a-2": "li-5"}
        entity = db.Entity("order", "a-1", line_item_maps=[rec1, rec2, rec3])

        li = entity.add_line_item(("a-1", "li-2"), ("a-2", "li-5"))
        assert 3 == len(entity.line_item_maps)
        assert expected == li
        assert expected == entity.line_item(("a-1", "li-2"))

    def test_add_line_item_updates_with_new_key(self):
        rec1 = {"a-1": "li-1", "a-2": "li-4"}
        rec2 = {"a-1": "li-2", "a-2": "li-nope"}
        rec3 = {"a-1": "li-3", "a-2": "li-6"}
        expected = {"a-1": "li-2", "a-2": "li-5"}
        entity = db.Entity("order", "a-1", line_item_maps=[rec1, rec2, rec3])

        li = entity.add_line_item(("a-1", "li-2"), ("a-2", "li-5"))
        assert 3 == len(entity.line_item_maps)
        assert expected == li
        assert expected == entity.line_item(("a-1", "li-2"))

    def test_line_item_id(self):
        entity = db.Entity("order", "a-1", line_item_maps=[
            {"a-1": "123", "a-2": "234"},
            {"a-1": "345", "a-2": "456"},
        ])
        assert "123" == entity.line_item_id("a-1", ("a-2", "234"))
        assert "234" == entity.line_item_id("a-2", ("a-1", "123"))
        assert "345" == entity.line_item_id("a-1", ("a-2", "456"))
        assert "456" == entity.line_item_id("a-2", ("a-1", "345"))
        assert None == entity.line_item_id("a-1", ("a-2", "nope"))

    # fulfillments

    def test_fulfillment(self):
        rec1 = {"a-1": "f-1", "a-2": "f-2", "status": "success"}
        rec2 = {"a-1": "f-3", "a-2": "f-4", "status": "cancelled"}
        entity = db.Entity("order", "a-1", fulfillments=[rec1, rec2])

        assert None == entity.fulfillment(("a-1", "f-nope"))
        assert None == entity.fulfillment(("a-1", "f-2"))
        assert rec1 == entity.fulfillment(("a-1", "f-1"))
        assert None == entity.fulfillment(("a-2", "f-1"))
        assert rec2 == entity.fulfillment(("a-2", "f-4"))

    def test_add_fulfillment_with_empty_key(self):
        entity = db.Entity("order", "a-1")
        fulfillment = entity.add_fulfillment((), ("a-2", "f-2"))
        assert None == fulfillment
        assert 0 == len(entity.fulfillments)

    def test_add_fulfillment_with_empty_new_key(self):
        entity = db.Entity("order", "a-1")
        fulfillment = entity.add_fulfillment(("a-1", "f-1"), ())
        assert None == fulfillment
        assert 0 == len(entity.fulfillments)

    def test_add_fulfillment_when_key_not_found(self):
        rec1 = {"a-1": "f-1", "status": "success"}
        entity = db.Entity("order", "a-1", fulfillments=[rec1])

        fulfillment = entity.add_fulfillment(("a-1", "f-nope"), ("a-2", "f-2"))
        assert None == fulfillment
        assert 1 == len(entity.fulfillments)

    def test_add_fulfillment_adds_new_key(self):
        rec1 = {"a-1": "f-1", "a-2": "f-4", "status": "success"}
        rec2 = {"a-1": "f-2", "status": "success"}
        rec3 = {"a-1": "f-3", "a-2": "f-6", "status": "success"}
        expected = {"a-1": "f-2", "a-2": "f-5", "status": "success"}
        entity = db.Entity("order", "a-1", fulfillments=[rec1, rec2, rec3])

        fulfillment = entity.add_fulfillment(("a-1", "f-2"), ("a-2", "f-5"))
        assert 3 == len(entity.fulfillments)
        assert expected == fulfillment
        assert expected == entity.fulfillment(("a-1", "f-2"))

    def test_add_fulfillment_updates_with_new_key(self):
        rec1 = {"a-1": "f-1", "a-2": "f-4", "status": "success"}
        rec2 = {"a-1": "f-2", "a-2": "f-nope", "status": "success"}
        rec3 = {"a-1": "f-3", "a-2": "f-6", "status": "success"}
        expected = {"a-1": "f-2", "a-2": "f-5", "status": "success"}
        entity = db.Entity("order", "a-1", fulfillments=[rec1, rec2, rec3])

        fulfillment = entity.add_fulfillment(("a-1", "f-2"), ("a-2", "f-5"))
        assert 3 == len(entity.fulfillments)
        assert expected == fulfillment
        assert expected == entity.fulfillment(("a-1", "f-2"))

    def test_update_fulfillment_status_with_empty_key(self):
        rec1 = {"a-1": "f-1", "a-2": "f-4", "status": "success"}
        entity = db.Entity("order", "a-1", fulfillments=[rec1.copy()])

        fulfillment = entity.update_fulfillment_status((), "changed")
        assert 1 == len(entity.fulfillments)
        assert None == fulfillment
        assert rec1 == entity.fulfillment(("a-1", "f-1"))

    def test_update_fulfillment_status_not_found(self):
        rec1 = {"a-1": "f-1", "a-2": "f-4", "status": "success"}
        entity = db.Entity("order", "a-1", fulfillments=[rec1.copy()])

        fulfillment = entity.update_fulfillment_status(
            ("a-1", "f-nope"), "changed")
        assert 1 == len(entity.fulfillments)
        assert None == fulfillment
        assert rec1 == entity.fulfillment(("a-1", "f-1"))

    def test_update_fulfillment_status(self):
        rec1 = {"a-1": "f-1", "a-2": "f-4", "status": "success"}
        expected = {"a-1": "f-1", "a-2": "f-4", "status": "changed"}
        entity = db.Entity("order", "a-1", fulfillments=[rec1.copy()])

        fulfillment = entity.update_fulfillment_status(
            ("a-1", "f-1"), "changed")
        assert 1 == len(entity.fulfillments)
        assert expected == fulfillment
        assert expected == entity.fulfillment(("a-2", "f-4"))

    # refunds

    @pytest.mark.parametrize("tc", [
        ("a-1", "r-nope", None),
        ("a-1", "r-2", None),
        ("a-1", "r-1", {"a-1": "r-1", "a-2": "r-2"}),
        ("a-2", "r-1", None),
        ("a-2", "r-4", {"a-1": "r-3", "a-2": "r-4"}),
    ])
    def test_refund(self, tc: Tuple[str, str, Optional[dict]]):
        entity = db.Entity("order", "a-1", refunds=[
            {"a-1": "r-1", "a-2": "r-2"},
            {"a-1": "r-3", "a-2": "r-4"},
        ])
        adapter_id, refund_id, expected = tc
        assert expected == entity.refund((adapter_id, refund_id))

    @pytest.mark.parametrize("tc", [
        ([], (), ("a-2", "r-2")),  # with empty lookup-key
        ([], ("a-1", "r-1"), ()),  # with empty new-key
        ([{"a-1": "r-1"}], ("a-1", "r-nope"),
         ("a-2", "r-2")),  # lookup key not found
    ])
    def test_add_refund_fails(self, tc: Tuple[List[dict], _models.AdapterKV, _models.AdapterKV]):
        current_refunds, lookup_key, new_key = tc
        entity = db.Entity("order", "a-1", refunds=current_refunds)
        refund = entity.add_refund(lookup_key, new_key)
        assert None == refund
        assert current_refunds == entity.refunds

    def test_add_refund_adds_new_key(self):
        rec1 = {"a-1": "r-1", "a-2": "r-4"}
        rec2 = {"a-1": "r-2"}  # refund to be updated
        rec3 = {"a-1": "r-3", "a-2": "r-6"}
        expected = {"a-1": "r-2", "a-2": "r-5"}
        entity = db.Entity("order", "a-1", refunds=[rec1, rec2, rec3])

        refund = entity.add_refund(("a-1", "r-2"), ("a-2", "r-5"))
        assert 3 == len(entity.refunds)
        assert expected == refund
        assert expected == entity.refund(("a-1", "r-2"))

    def test_add_refund_updates_with_new_key(self):
        rec1 = {"a-1": "r-1", "a-2": "r-4"}
        rec2 = {"a-1": "r-2", "a-2": "r-nope"}  # refund to be updated
        rec3 = {"a-1": "r-3", "a-2": "r-6"}
        expected = {"a-1": "r-2", "a-2": "r-5"}
        entity = db.Entity("order", "a-1", refunds=[rec1, rec2, rec3])

        refund = entity.add_refund(("a-1", "r-2"), ("a-2", "r-5"))
        assert 3 == len(entity.refunds)
        assert expected == refund
        assert expected == entity.refund(("a-1", "r-2"))
