import dataclasses
import re
import uuid
from datetime import datetime
from typing import cast, Tuple

import pytest

import leap.std.uuid
from leap.adapters import EventType, db
from leap.adapters.db import _models


class TestEventModel:
    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    # tests

    def test_required_fields(self):
        ev = db.Event(EventType.NOOP)
        dataclasses.is_dataclass(ev)
        assert EventType.NOOP == ev.type

    def test_default_fields(self):
        ev = db.Event(EventType.NOOP)
        assert isinstance(ev.data, dict)

        assert (datetime.now() - ev.timestamp).seconds < 10

    def test_generate_default_id(self):
        hex_uuid_re = r"^[0-9a-f]{32}$"
        assert re.match(hex_uuid_re, cast(str, db.Event(EventType.NOOP).id))
        assert re.match(hex_uuid_re, cast(str, db.Event(EventType.NOOP).id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        assert uuid.UUID(int=0).hex == db.Event(EventType.NOOP).id
        assert uuid.UUID(int=1).hex == db.Event(EventType.NOOP).id

    @pytest.mark.parametrize("tt", [
        (False, {}), # empty attrs
        (False, {"x-leap-omcp-version": "0"}),
        (False, {"x-leap-omcp-version": "0"}),
        (True, {"x-leap-omcp-version": "1"}),
        (False, {"x-leap-omcp-version": 2}),
        (True, {"x-leap-omcp-version": 1}),
    ])
    def test_omcpv1(self, tt: Tuple[bool, dict]):
        expected, attrs = tt
        assert expected == db.Event(EventType.NOOP, attrs=attrs).omcpv1
