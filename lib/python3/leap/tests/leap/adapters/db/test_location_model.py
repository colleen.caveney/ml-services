import dataclasses
import re
import uuid
from datetime import datetime
from typing import cast

import pytest

import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models


class TestLocationModel:
    def setup_method(self):
        self.preserved_uuidfactory = _models._uuidfactory

    def teardown_method(self):
        _models._uuidfactory = self.preserved_uuidfactory

    # tests

    def test_required_fields(self):
        location = db.Location("123 mullberry st", "chicago", "il", "abc123", "60660", 1.0, -1.0, 650)
        dataclasses.is_dataclass(location)
        assert "123 mullberry st" == location.address
        assert "chicago" == location.city
        assert "il" == location.state
        assert "abc123" == location.city_code
        assert "60660" == location.zip_code
        assert 1.0 == location.latitude
        assert -1.0 == location.longitude
        assert 650 == location.sqft

    def test_default_fields(self):
        location = db.Location("123 mullberry st", "chicago", "il", "123abc", "60660", 1.0, -1.0, 650)
        assert location.id
        assert db.LocationStatus.EVAL == location.status_key
        assert location.created_at
        assert (datetime.now() - location.created_at).seconds < 10
        assert location.updated_at
        assert (datetime.now() - location.updated_at).seconds < 10

    @pytest.mark.parametrize("status", list(db.LocationStatus))
    def test_status_field_casting(self, status: db.LocationStatus):
        location = db.Location.build({
            "address": "123 mullberry st",
            "city": "chicago", "state": "il",
            "city_code": "123abc",
            "zip_code": "60660",
            "latitude": 1.0,
            "longitude": -1.0,
            "sqft": 650,
            "status_key": status.value
        })
        assert location
        assert status == location.status_key

    @pytest.mark.parametrize("status_key", [
        "LIVES", "FOO"
    ])
    def test_status_field_casting_fails(self, status_key: str):
        with pytest.raises(ValueError, match="'{}' is not a valid LocationStatus".format(status_key)):
            db.Location.build({
                "address": "123 mullberry st",
                "city": "chicago", "state": "il",
                "city_code": "123abc",
                "zip_code": "60660",
                "latitude": 1.0,
                "longitude": -1.0,
                "sqft": 650,
                "status_key": status_key
            })

    def test_generate_default_id(self):
        location1 = db.Location("456 mullberry st", "chicago", "il", "123abc", "60661", 2.0, -1.0, 650)
        location2 = db.Location("123 mullberry st", "chicago", "il", "123abc", "60660", 1.0, -2.0, 540)
        assert re.match(r"^[0-9a-f]{32}$", cast(str, location1.id))
        assert re.match(r"^[0-9a-f]{32}$", cast(str, location2.id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        location1 = db.Location("789 mullberry st", "chicago", "il", "123abc", "60662", 4.0, -4.0, 440)
        location2 = db.Location("321 mullberry st", "chicago", "il", "123abc", "60666", 5.0, -5.0, 900)
        assert uuid.UUID(int=0).hex == location1.id
        assert uuid.UUID(int=1).hex == location2.id
