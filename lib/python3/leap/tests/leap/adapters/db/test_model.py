from dataclasses import dataclass
from typing import Optional

import pytest

from leap.adapters.db import Model


@dataclass
class ModelA(Model):
    id: str
    foo: Optional[str] = None
    bar: Optional[str] = None

    def __post_init__(self):
        self.copy_attributes("foo", "bar")


@dataclass()
class ModelB(Model):
    id: str
    foo: Optional[str] = None
    bar: Optional[str] = None

    def __post_init__(self):
        self.copy_attributes("foo", "bar", required=False)


@dataclass()
class ModelC(Model):
    id: str
    foo: Optional[str] = None
    bar: Optional[str] = None

    def __post_init__(self):
        self.copy_attributes("foo", "bar", required=False, default="baz")


class TestModel:
    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    # tests
    def test_fields_same(self):
        m = ModelA("one", foo="foo", bar="foo")
        assert m.id == "one"
        assert m.foo == "foo"
        assert m.bar == "foo"

    def test_fields_foo(self):
        m = ModelA("one", foo="foo")
        assert m.id == "one"
        assert m.foo == "foo"
        assert m.bar == "foo"

    def test_fields_bar(self):
        m = ModelA("one", bar="foo")
        assert m.id == "one"
        assert m.foo == "foo"
        assert m.bar == "foo"

    def test_fields_diff(self):
        with pytest.raises(TypeError):
            ModelA("one", foo="foo", bar="bar")

    def test_fields_neither(self):
        with pytest.raises(TypeError):
            ModelA("one")

    def test_fields_not_required(self):
        m = ModelB("one")
        assert m.id == "one"
        assert m.foo is None
        assert m.bar is None

    def test_fields_not_required_foo(self):
        m = ModelB("one", foo="foo")
        assert m.id == "one"
        assert m.foo == "foo"
        assert m.bar == "foo"

    def test_fields_not_required_bar(self):
        m = ModelB("one", bar="foo")
        assert m.id == "one"
        assert m.foo == "foo"
        assert m.bar == "foo"

    def test_fields_not_required_bar(self):
        m = ModelB("one", foo="foo", bar="foo")
        assert m.id == "one"
        assert m.foo == "foo"
        assert m.bar == "foo"

    def test_fields_not_required_diff(self):
        with pytest.raises(TypeError):
            ModelB("one", foo="foo", bar="bar")

    def test_fields_default(self):
        m = ModelC("one")
        assert m.id == "one"
        assert m.foo == "baz"
        assert m.bar == "baz"

    def test_fields_one_default(self):
        m = ModelC("one", foo="foo")
        assert m.id == "one"
        assert m.foo == "foo"
        assert m.bar == "foo"

    def test_fields_one_default(self):
        m = ModelC("one", bar="foo")
        assert m.id == "one"
        assert m.foo == "foo"
        assert m.bar == "foo"
