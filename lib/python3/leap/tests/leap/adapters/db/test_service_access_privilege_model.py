import dataclasses
import re
import uuid
from typing import cast

import pytest

import leap.std.uuid
from leap.adapters import db
from leap.adapters.db import _models


class TestServiceAccessPrivilegeModel:
    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    # tests

    def test_required_fields(self):
        priv = db.ServiceAccessPrivilege("s01", "read-things", db.ServiceAccessTier.READONLY)
        dataclasses.is_dataclass(priv)
        assert "s01" == priv.service_id
        assert "read-things" == priv.privilege
        assert db.ServiceAccessTier.READONLY == priv.tier

    def test_generate_default_id(self):
        hex_uuid_re = r"^[0-9a-f]{32}$"
        assert re.match(hex_uuid_re, cast(str, db.ServiceAccessPrivilege("s02", "write-stuff", db.ServiceAccessTier.FULL).id))
        assert re.match(hex_uuid_re, cast(str, db.ServiceAccessPrivilege("s03", "write-stuff", db.ServiceAccessTier.FULL).id))

    def test_generate_default_id_uses_uuidfactory(self):
        _models._uuidfactory = leap.std.uuid.seq_factory()
        assert uuid.UUID(int=0).hex == db.ServiceAccessPrivilege("s04", "read-all-things", db.ServiceAccessTier.READONLY).id
        assert uuid.UUID(int=1).hex == db.ServiceAccessPrivilege("s05", "write-all-things", db.ServiceAccessTier.FULL).id

    @pytest.mark.parametrize("tier", list(db.ServiceAccessTier))
    def test_tier_field_casting(self, tier: db.ServiceAccessTier):
        priv = db.ServiceAccessPrivilege.build({
            "service_id": "s06",
            "privilege": "no-can-do",
            "tier": tier.value,
        })
        assert priv
        assert tier == priv.tier
