import dataclasses
from datetime import datetime
from distutils.command.config import config
from typing import Optional, Tuple

import pytest
from leap.adapters import db


class TestServiceModel:

    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    # tests

    def test_required_fields(self):
        service = db.Service("test-service", "abc123", "efg456")
        dataclasses.is_dataclass(service)
        assert "test-service" == service.id
        assert "abc123" == service.api_key
        assert "efg456" == service.api_secret

    def test_default_fields(self):
        service = db.Service("test-service", "abc123", "efg456")
        assert (datetime.now() - service.created_at).seconds < 10
        assert service.updated_at
        assert (datetime.now() - service.updated_at).seconds < 10

    @pytest.mark.parametrize(
        "tc",
        [
            (None, None),  # nil config
            (None, {}),  # empty config
            (None, {
                "app_id": "1234"
            }),  # non-empty config, but no landing page
            ("a.b.c", {
                "landing_page": "a.b.c"
            }),  # success
        ])
    def test_landing_page(self, tc: Tuple[Optional[str], Optional[dict]]):
        want, cfg = tc
        service = db.Service("test-service", "abc123", "efg456")
        if not cfg is None:
            service = db.Service("test-service", "abc123", "efg456", config=cfg)
        assert want == service.landing_page

    def test_sig_check_override(self):
        service = db.Service("test-service", "abc123", "efg456")

        assert False == service.has_sig_check_override

        service = db.Service("test-service", "abc123", "efg456", config={"sig_check": "not-bypass"})
        assert False == service.has_sig_check_override

        service = db.Service("test-service", "abc123", "efg456", config={"sig_check": "bypass"})
        assert True == service.has_sig_check_override

        service = db.Service("test-service", "abc123", "efg456", config={"sig_check": "ok"})
        assert True == service.has_sig_check_override
