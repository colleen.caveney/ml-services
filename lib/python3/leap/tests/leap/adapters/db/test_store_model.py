import dataclasses
from datetime import datetime

import pytest

from leap.adapters import db
from leap.adapters.db import _models


class TestStoreModel:
    def setup_method(self):
        self.preserved_uuidfactory = _models._uuidfactory

    def teardown_method(self):
        _models._uuidfactory = self.preserved_uuidfactory

    # tests

    def test_required_fields(self):
        store = db.Store(id="store-code-1", client_id="client-1", location_id="location-1")
        dataclasses.is_dataclass(store)
        assert "store-code-1" == store.id
        assert "client-1" == store.client_id
        assert "location-1" == store.location_id

    def test_build(self):
        store = db.Store.build({
            "id": "store-code-1",
            "client_id": "client-1",
            "location_id": "loc-1",
            "deleted": False,
            "slack_channel_id": "slack_channel_1",
            "store_type": db.StoreType.POS,
            "possession_date": datetime(2020, 1, 1).isoformat(),
            "open_date": datetime(2020, 2, 1).isoformat(),
            "close_date": datetime(2020, 3, 1).isoformat(),
        })
        dataclasses.is_dataclass(store)
        assert "store-code-1" == store.id
        assert "client-1" == store.client_id
        assert "loc-1" == store.location_id
        assert False == store.deleted
        assert "slack_channel_1" == store.slack_channel_id
        assert db.StoreType.POS == store.store_type
        assert datetime(2020, 1, 1) == store.possession_date
        assert datetime(2020, 2, 1) == store.open_date
        assert datetime(2020, 3, 1) == store.close_date

    def test_default_fields(self):
        store = db.Store(id="store-code-1", client_id="client-1", location_id="location-1")
        assert store.deleted == False
        assert store.slack_channel_id is None
        assert store.store_type == db.StoreType.POS
        assert store.possession_date is None
        assert store.open_date is None
        assert store.close_date is None
        assert store.created_at
        assert (datetime.now() - store.created_at).seconds < 10
        assert store.updated_at
        assert (datetime.now() - store.updated_at).seconds < 10

    def test_brand_id(self):
        store = db.Store(id="store-code-1", brand_id="client-1", location_id="location-1")
        assert store.client_id == "client-1"

    def test_brand_id_and_client_id(self):
        store = db.Store(id="store-code-1", client_id="client-1", brand_id="client-1", location_id="location-1")
        assert store.client_id == "client-1"

    def test_no_brand_id_or_client_id(self):
        with pytest.raises(TypeError):
            db.Store(id="store-code-1", location_id="location-1")

    def test_brand_id_different_client_id(self):
        with pytest.raises(TypeError):
            db.Store(id="store-code-1", client_id="client-1", brand_id="brand-1", location_id="location-1")
