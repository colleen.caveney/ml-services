import dataclasses
from datetime import datetime

from leap.adapters import db


class TestSubscriptionModel:
    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    # tests

    def test_required_fields(self):
        data_dict = {}
        subscription = db.Subscription(db.SubscriptionType.EMAIL, db.ActionType.NEW_BRAND, data_dict)
        dataclasses.is_dataclass(subscription)
        assert data_dict == subscription.data
        assert db.SubscriptionType.EMAIL == subscription.subscription_type
        assert db.ActionType.NEW_BRAND == subscription.action_type

    def test_default_fields(self):
        data_dict = {}
        subscription = db.Subscription(db.SubscriptionType.EMAIL, db.ActionType.NEW_BRAND, data_dict)
        assert (datetime.now() - subscription.created_at).seconds < 10
        assert subscription.updated_at
        assert (datetime.now() - subscription.updated_at).seconds < 10
