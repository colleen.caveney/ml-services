from enum import Enum

from leap.adapters import EventType


class TestEventTypes:
    def test_is_enum(self):
        assert isinstance(EventType.NOOP, Enum)
