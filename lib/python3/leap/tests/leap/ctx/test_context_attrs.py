import re

from leap.ctx import attrs


class TestContextAttrs:
    def test_default_constructor(self):
        ctx_attrs = attrs.ContextAttrs()
        assert [attrs.TRACE_ID] == list(ctx_attrs.keys())
        assert re.match("^[a-f0-9]{32}$", ctx_attrs.trace_id)

    def test_construct_with_attr_overrides(self):
        ctx_attrs = attrs.ContextAttrs({
            attrs.TRACE_ID: "abc123",
        })

        assert "abc123" == ctx_attrs.trace_id

    def test_construct_with_custom_attrs(self):
        ctx_attrs = attrs.ContextAttrs({
            "foo": "bar",
            "baz": 2,
        })

        assert re.match("^[a-f0-9]{32}$", ctx_attrs.trace_id)

        assert "bar" == ctx_attrs.get("foo")
        assert 2 == ctx_attrs.get("baz")
        assert None == ctx_attrs.get("does-not-exist")

    def test_edit_custom_attr(self):
        ctx_attrs = attrs.ContextAttrs()

        assert None == ctx_attrs.get("foo")
        assert "baz" == ctx_attrs.set("foo", "baz")
        assert "baz" == ctx_attrs.get("foo")

        assert None == ctx_attrs.get("bar")
        assert 123 == ctx_attrs.set("bar", 123)
        assert 123 == ctx_attrs.get("bar")
