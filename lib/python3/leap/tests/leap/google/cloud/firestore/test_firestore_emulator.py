import leap.google.cloud.firestore.tests as fsemulate

class TestFirestoreEmulator:
    def setup_method(self):
        self.fsclient = fsemulate.Client()

    def test_query_nested_objects(self):
        self.fsclient.store_document("objs/foo", {
            "id": "foo",
            "obj": { "x": 1, "y": 2}
        })
        self.fsclient.store_document("objs/bar", {
            "id": "bar",
            "obj": { "x": 1, "y": 3}
        })

        query = self.fsclient.collection("objs").where("obj.x", "==", 1)
        assert ["foo", "bar"] == [o.get("id") for o in query.stream()]

        query = self.fsclient.collection("objs").where("obj.y", "==", 2)
        assert ["foo"] == [o.get("id") for o in query.stream()]

        query = self.fsclient.collection("objs").where("obj.y", "==", 3)
        assert ["bar"] == [o.get("id") for o in query.stream()]
