import os
import unittest.mock as mock
from leap.google.cloud.proxy.proxy_db import ProxyDB


class TestProxyDB:
    def setup_method(self):
        os.environ["DATA_PROXY_PREST_URL"] = "foo.com"
        self.proxy_db = ProxyDB()

    def teardown_method(self):
        pass

    def test_default_headers(self):
        # by default there are no headers
        assert self.proxy_db.headers is None

    @mock.patch("leap.google.cloud.proxy.proxy_db.subprocess.Popen")
    def test_service_env_headers(self, mock_subprocess: mock.MagicMock):
        process_mock = mock.MagicMock()
        attrs = {"communicate.return_value": ("token".encode('utf-8'), "error")}
        process_mock.configure_mock(**attrs)
        mock_subprocess.return_value = process_mock

        os.environ["SERVICE_ENV"] = "local"
        self.proxy_db = ProxyDB()
        assert {'Content-Type': 'application/json', 'Authorization': 'Bearer token'} == self.proxy_db.headers

    @mock.patch("leap.google.cloud.proxy.proxy_db.subprocess.Popen")
    def test_local_override_headers(self, mock_subprocess: mock.MagicMock):
        process_mock = mock.MagicMock()
        attrs = {"communicate.return_value": ("token".encode('utf-8'), "error")}
        process_mock.configure_mock(**attrs)
        mock_subprocess.return_value = process_mock

        os.environ["PROXY_LOCAL_ENV_OVERRIDE"] = "true"
        self.proxy_db = ProxyDB()
        assert {'Content-Type': 'application/json', 'Authorization': 'Bearer token'} == self.proxy_db.headers

    def test_urls(self):
        params = self.proxy_db.generate_query_params(brand_handle="burgers")
        assert {'brand_handle': 'burgers', '_page': 1} == params

        params = self.proxy_db.generate_query_params(brand_handle="burgers", page_num=0)
        assert {'brand_handle': 'burgers'} == params

        params = self.proxy_db.generate_query_params(brand_handle="burgers", page_num=45)
        assert {'brand_handle': 'burgers', '_page': 45} == params

        params = self.proxy_db.generate_query_params(page_num=45)
        assert {'_page': 45} == params

        params = self.proxy_db.generate_query_params()
        assert {'_page': 1} == params

        params = self.proxy_db.generate_query_params(page_num=0)
        assert {} == params
