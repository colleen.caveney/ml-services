import json

import pytest

import leap.google.cloud.pubsub.message as message


class TestSubscriptionlessMessage:
    def test_empty_message(self):
        msg = message.SubscriptionlessMessage({})
        assert {} == msg
        assert not msg.id
        assert {} == msg.attrs
        assert not msg.b64data
        assert not msg.data
        assert not msg.json

    def test_data_as_str(self):
        decoded_bytes = b"this is a str"
        encoded_str = "dGhpcyBpcyBhIHN0cg=="
        msg = message.SubscriptionlessMessage({
            "data": encoded_str
        })
        assert encoded_str == msg.b64data
        assert decoded_bytes == msg.data
        with pytest.raises(json.decoder.JSONDecodeError):
            msg.json

    def test_data_as_json(self):
        expected_json = {"a": "b"}
        decoded_bytes = b'{"a":"b"}'
        encoded_str = "eyJhIjoiYiJ9"
        msg = message.SubscriptionlessMessage({
            "data": encoded_str
        })
        assert encoded_str == msg.b64data
        assert decoded_bytes == msg.data
        assert expected_json == msg.json

class TestMessage:
    def test_empty_message(self):
        msg = message.Message({})
        assert {} == msg
        assert not msg.id
        assert not msg.subscription
        assert {} == msg.attrs
        assert not msg.b64data
        assert not msg.data
        assert not msg.json

    def test_data_as_str(self):
        decoded_bytes = b"this is a str"
        encoded_str = "dGhpcyBpcyBhIHN0cg=="
        msg = message.Message({
            "message": {"data": encoded_str}
        })
        assert encoded_str == msg.b64data
        assert decoded_bytes == msg.data
        with pytest.raises(json.decoder.JSONDecodeError):
            msg.json

    def test_data_as_json(self):
        expected_json = {"a": "b"}
        decoded_bytes = b'{"a":"b"}'
        encoded_str = "eyJhIjoiYiJ9"
        msg = message.Message({
            "message": {"data": encoded_str}
        })
        assert encoded_str == msg.b64data
        assert decoded_bytes == msg.data
        assert expected_json == msg.json

    ## decode Message tests

    def test_decode_empty_message(self):
        with pytest.raises(message.InvalidMessageError, match=r"decoding message"):
            message.decode(b'')

    def test_decode_invalid_json_message(self):
        with pytest.raises(message.InvalidMessageError, match=r"decoding message"):
            message.decode(b'{{{')

    def test_decode(self):
        expected_json = {"a":"b"}
        decoded_bytes = b'{"a":"b"}'

        msg = message.decode(b'{"message":{"messageId":"abc123","attrs":{},"data":"eyJhIjoiYiJ9"}, "subscription":"foo"}')
        assert "foo" == msg.subscription
        assert "abc123" == msg.id
        assert {} == msg.attrs
        assert decoded_bytes == msg.data
        assert expected_json == msg.json

    ## encoding tests

    def test_encode_json_data(self):
        assert "eyJhIjoiYiJ9" == message.encode_json_data({"a": "b"})

    def test_encode_empty_json_data(self):
        assert "e30=" == message.encode_json_data({})

    def test_encode_data(self):
        assert "eyJhIjoiYiJ9" == message.encode_data(b'{"a":"b"}')

    def test_encode_empty_data(self):
        assert "" == message.encode_data(b'')

