from typing import Tuple
from unittest import mock

import pytest
from google.cloud import pubsub_v1

from leap.google.cloud.pubsub import TopicPublisher


class TestTopicPublisher:
    @pytest.mark.parametrize("topic_client_error", [
        (None, None, "topic is required"),
        ("", None, "topic is required"),
        ("cool-topic", None, "client is required"),
    ])
    def test_fail_init(self, topic_client_error: Tuple[str, pubsub_v1.PublisherClient, str]):
        topic, client, error = topic_client_error
        with pytest.raises(ValueError, match=error):
            TopicPublisher(topic, client)

    def test_publish(self):
        topic = "test-topic"
        client = mock.MagicMock()
        pub = TopicPublisher(topic, client)

        pub.publish(b"this is a test", foo="bar", bar="baz")
        client.publish.assert_called_with(
            topic, b"this is a test", foo="bar", bar="baz")

        pub.publish(b"this is another test", eating="glue", healthy=True)
        client.publish.assert_called_with(
            topic, b"this is another test", eating="glue", healthy=True)

        assert 2 == client.publish.call_count
