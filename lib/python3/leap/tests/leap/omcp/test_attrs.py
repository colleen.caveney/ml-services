from enum import Enum
from typing import Any, List

import pytest

from leap.omcp.attrs import Attr, Attrs, GRPCMetadata


class TestAttrs:
    def test_Attr_is_enum(self):
        assert isinstance(Attr.TRAIL, Enum)

    @pytest.mark.parametrize("data,expected", [
        ({}, []),
        ({Attr.TRAIL: "foo"}, ["foo"]),
        ({Attr.TRAIL: "foo,bar"}, ["foo", "bar"]),
    ])
    def test_trail(self, data: dict, expected: Any):
        assert expected == Attrs(data).trail

    @pytest.mark.parametrize("data,trail,expected", [
        ({}, [], {Attr.TRAIL: ""}),
        ({Attr.TRAIL: ""}, ["foo"], {Attr.TRAIL: "foo"}),
        ({Attr.TRAIL: ""}, ["foo", "bar"], {Attr.TRAIL: "foo,bar"}),
        ({Attr.TRAIL: "foo"}, ["bar"], {Attr.TRAIL: "foo,bar"}),
        ({Attr.TRAIL: "foo,bar"}, ["baz", "x"], {Attr.TRAIL: "foo,bar,baz,x"}),
        ({Attr.TRAIL: "foo,bar"}, [1,2], {Attr.TRAIL: "foo,bar,1,2"}),
    ])
    def test_append_trail(self, data: dict, trail: List[str], expected: Any):
        assert expected == dict(Attrs(data).append_trail(*trail))

    @pytest.mark.parametrize("data,expected", [
        ({}, []),
        ({Attr.TRAIL: "foo"}, [(str(Attr.TRAIL), "foo")]),
        ({Attr.TRAIL: "foo", "x-foo":123}, [(str(Attr.TRAIL), "foo"), ("x-foo", "123")]),
    ])
    def test_to_metadata(self, data: dict, expected: GRPCMetadata):
        assert expected == Attrs(data, False).to_metadata()

    ## dict specific tests

    @pytest.mark.parametrize("data,expected,optargs", [
        ({}, {}, {}),
        ({"x": 1}, {"x": "1"}, {"filter_known": False}), # allow any attributes, values convert to strings
        ({"x": 1, Attr.TRAIL: "foo,bar"}, {str(Attr.TRAIL): "foo,bar"}, {"filter_known": True}),
        ({"x": 1, Attr.TRAIL: "foo,bar"}, {str(Attr.TRAIL): "foo,bar"}, {}), # filter by default
    ])
    def test_construct(self, data: dict, expected: dict, optargs: dict):
        assert expected == dict(Attrs(data, **optargs))

    @pytest.mark.parametrize("data,expected,optargs", [
        ({}, {}, {}),
        ({"x": 1}, {"x": "1"}, {"filter_known": False}), # allow any attributes, values convert to strings
        ({"x": 1, Attr.TRAIL: "foo,bar"}, {str(Attr.TRAIL): "foo,bar"}, {"filter_known": True}),
        ({"x": 1, Attr.TRAIL: "foo,bar"}, {str(Attr.TRAIL): "foo,bar"}, {}), # filter by default
    ])
    def test_update(self, data: dict, expected: dict, optargs: dict):
        a = Attrs(data, **optargs)
        a.update(data)
        assert expected == dict(a)

    @pytest.mark.parametrize("data,key,expected,optargs", [
        ({}, "foo", None, {}),
        ({"x": 1}, "x", "1", {"filter_known": False}),
        ({1: 2}, "1", "2", {"filter_known": False}),
        ({"1": 3}, 1, "3", {"filter_known": False}),
        ({"x": 1, Attr.TRAIL: "foo,bar"}, "x", None, {"filter_known": True}),
        ({"x": 1, Attr.TRAIL: "foo,bar"}, Attr.TRAIL, "foo,bar", {}),
    ])
    def test_get(self, data: dict, key: Any, expected: Any, optargs: dict):
        a = Attrs(data, **optargs)
        assert expected == a.get(key)

    @pytest.mark.parametrize("data,key,expected,optargs", [
        ({}, "foo", KeyError, {}),
        ({"x": 1}, "x", "1", {"filter_known": False}),
        ({1: 2}, "1", "2", {"filter_known": False}),
        ({"1": 3}, 1, "3", {"filter_known": False}),
        ({"x": 1, Attr.TRAIL: "foo,bar"}, "x", KeyError, {"filter_known": True}),
        ({"x": 1, Attr.TRAIL: "foo,bar"}, Attr.TRAIL, "foo,bar", {}),
    ])
    def test_get_direct(self, data: dict, key: Any, expected: Any, optargs: dict):
        a = Attrs(data, **optargs)
        if expected == KeyError:
            try:
                a[key]
                assert False # should not have gotten here
            except KeyError:
                assert True
        else:
            expected == a[key]

    @pytest.mark.parametrize("key,value,expected,optargs", [
        ("foo", "bar", {}, {}),
        ("foo", 1, {"foo": "1"}, {"filter_known": False}),
        (123, "bar", {"123": "bar"}, {"filter_known": False}),
        ("test", "val", {}, {"filter_known": True}),
        (Attr.TRAIL, "foo,bar", {Attr.TRAIL: "foo,bar"}, {}),
    ])
    def test_setdefault(self, key: dict, value: Any, expected: dict, optargs: dict):
        a = Attrs({}, **optargs)
        a.setdefault(key, value)
        assert expected == dict(a)

    @pytest.mark.parametrize("key,value,expected,optargs", [
        ("foo", "bar", {}, {}),
        ("foo", 1, {"foo": "1"}, {"filter_known": False}),
        (123, "bar", {"123": "bar"}, {"filter_known": False}),
        ("test", "val", {}, {"filter_known": True}),
        (Attr.TRAIL, "foo,bar", {Attr.TRAIL: "foo,bar"}, {}),
    ])
    def test_set_direct(self, key: dict, value: Any, expected: dict, optargs: dict):
        a = Attrs({}, **optargs)
        a[key] = value
        assert expected == dict(a)

    @pytest.mark.parametrize("data,key,expected,optargs", [
        ({}, "foo", KeyError, {}),
        ({"x": 1}, "x", "1", {"filter_known": False}),
        ({1: 2}, "1", "2", {"filter_known": False}),
        ({"1": 3}, 1, "3", {"filter_known": False}),
        ({"x": 1, Attr.TRAIL: "foo,bar"}, "x", KeyError, {"filter_known": True}),
        ({"x": 1, Attr.TRAIL: "foo,bar"}, Attr.TRAIL, "foo,bar", {}),
    ])
    def test_pop(self, data: dict, key: Any, expected: Any, optargs: dict):
        a = Attrs(data, **optargs)
        if expected == KeyError:
            try:
                a.pop(key)
                assert False # should not have gotten here
            except KeyError:
                assert True
        else:
            expected == a.pop(key)

    @pytest.mark.parametrize("data,key,expected,optargs", [
        ({}, "foo", False, {}),
        ({"x": 1}, "x", True, {"filter_known": False}),
        ({1: 2}, "1", True, {"filter_known": False}),
        ({"1": 3}, 1, True, {"filter_known": False}),
        ({"x": 1, Attr.TRAIL: "foo,bar"}, "x", False, {"filter_known": True}),
        ({"x": 1, Attr.TRAIL: "foo,bar"}, Attr.TRAIL, True, {}),
    ])
    def test_contains(self, data: dict, key: Any, expected: Any, optargs: dict):
        assert expected == (key in Attrs(data, **optargs))

