from typing import Any, List

import grpc
import pytest

from leap.omcp.attrs import Attr, Attrs, GRPCMetadata
from leap.omcpapi.client import InjectAttrsClientInterceptor, ClientCallDetails

class TestInjectAttrsClientInterceptor:
    def test_is_unary_unary_interceptor(self):
        # sanity test
        assert isinstance(InjectAttrsClientInterceptor(), grpc.UnaryUnaryClientInterceptor)

    # construction

    def test_constructor_no_attrs_provided(self):
        interceptor = InjectAttrsClientInterceptor()
        assert interceptor.attrs == Attrs()
        assert interceptor.attrs_fn is None

    def test_constructor_provided_custom_Attrs(self):
        interceptor = InjectAttrsClientInterceptor(Attrs({"x":1}, False))
        assert interceptor.attrs == Attrs({"x":1}, False)
        assert interceptor.attrs_fn is None

    def test_constructor_provided_get_attrs_func(self):
        fn = lambda: None
        interceptor = InjectAttrsClientInterceptor(fn)
        assert interceptor.attrs is None
        assert interceptor.attrs_fn == fn

    # interception

    @pytest.mark.parametrize("attrs,metadata,expected", [
        (None, None, []),
        # static attrs
        (Attrs({Attr.TRAIL: "a-service"}), None, [(Attr.TRAIL, "a-service")]),
        (
            Attrs({Attr.TRAIL: "a-service"}),
            [("x-foo", "123")],
            [("x-foo", "123"), (Attr.TRAIL, "a-service")]
        ),
        (
            Attrs({Attr.TRAIL: "a-service", Attr.ADAPTER_ID: "aid"}),
            [("x-foo", "123"), ("x-bar", "321")],
            [("x-foo", "123"), ("x-bar", "321"), (Attr.TRAIL, "a-service"), (Attr.ADAPTER_ID, "aid")]
        ),
        # lambda attrs
        (
            lambda: Attrs({Attr.TRAIL: "a-service"}),
            None,
            [(Attr.TRAIL, "a-service")]
        ),
        (
            lambda: Attrs({Attr.TRAIL: "a-service"}),
            [("x-foo", "123")],
            [("x-foo", "123"), (Attr.TRAIL, "a-service")]
        ),
    ])
    def test_intercept_metadata(self, attrs: Attrs, metadata: GRPCMetadata, expected: GRPCMetadata):
        def continuation(call_details: grpc.ClientCallDetails, req):
            # support for closures in python sucks, so we do this hack
            continuation.got_call_details = call_details
            continuation.got_req = req
            return None

        interceptor = InjectAttrsClientInterceptor(attrs)

        arg_call_details = ClientCallDetails(
            method="/foo",
            timeout=10,
            metadata=metadata,
            credentials="some-credentials",
            wait_for_ready=False,
            compression="compressed")
        interceptor.intercept_unary_unary(continuation, arg_call_details, "a-request")

        assert continuation.got_req == "a-request"
        # notice change to metadata arg
        assert continuation.got_call_details == ClientCallDetails(
            method="/foo",
            timeout=10,
            metadata=expected,
            credentials="some-credentials",
            wait_for_ready=False,
            compression="compressed")
