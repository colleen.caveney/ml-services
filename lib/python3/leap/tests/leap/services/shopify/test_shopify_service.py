import unittest.mock as mock
from typing import Callable, Tuple, Type

import pytest

import leap.errors
from leap.adapters import db
from leap.services.shopify import ShopifyService
from leap.std.testing.requests import Response as MockResponse

MockResponder = Callable[..., MockResponse]


def mock_api_request(status_code: int = 200, json_data: dict = {}, headers: dict = {}) -> MockResponder:
    return lambda *args, **kwargs: MockResponse(status_code, json_data, headers=headers)


class TestShopifyService:
    @pytest.mark.parametrize("tc", [
        ({
            "client_id": "c0",
            "service_id": "shopify",
            "service_type": "shopify",
            "instance_id": "shop0"
        }, leap.errors.services.InvalidConfigError, "missing: access_token"),
        ({
            "client_id": "c0",
            "service_id": "shopify",
            "service_type": "shopify",
            "instance_id": "shop0",
            "config": {"private_app": True, "password": "p", "verification_key": "v"},
        }, leap.errors.services.InvalidConfigError, "missing: username"),
        ({
            "client_id": "c0",
            "service_id": "shopify",
            "service_type": "shopify",
            "instance_id": "shop0",
            "config": {"private_app": True, "username": "u", "verification_key": "v"},
        }, leap.errors.services.InvalidConfigError, "missing: password"),
        ({
            "client_id": "c0",
            "service_id": "shopify",
            "service_type": "shopify",
            "instance_id": "shop0",
            "config": {"private_app": True, "username": "u", "password": "p"},
        }, leap.errors.services.InvalidConfigError, "missing: verification_key"),
    ])
    def test_fail_init(self, tc: Tuple[dict, Type[Exception], str]):
        adapter_data, err_cls, err_msg = tc
        adapter = db.AdapterConfig.build(adapter_data)
        with pytest.raises(err_cls, match=err_msg):
            ShopifyService(adapter)

    def test_init_public_app(self):
        adapter = db.AdapterConfig(
            client_id="c0", service_id="shopify", service_type="shopify", instance_id="shop0", config={"access_token": "t"})
        assert None == adapter.config.get("host")

        svc = ShopifyService(adapter)

        assert "shop0.myshopify.com" == svc.host
        assert "https://shop0.myshopify.com/admin/api/2022-04" == svc.base_url

        assert not svc.is_private_app
        assert "t" == svc.access_token

    def test_init_with_host_override(self):
        adapter = db.AdapterConfig(
            client_id="c0", service_id="shopify", service_type="shopify", instance_id="shop0", config={"access_token": "t", "host": "shopX.examp.le"})

        svc = ShopifyService(adapter)

        assert "shopX.examp.le" == svc.host
        assert "https://shopX.examp.le/admin/api/2022-04" == svc.base_url

        assert not svc.is_private_app
        assert "t" == svc.access_token

    def test_init_private_app(self):
        adapter = db.AdapterConfig(
            client_id="c1", service_id="shopify", service_type="shopify", instance_id="shop1",
            config={"private_app": True, "username": "u", "password": "p", "verification_key": "v"})
        assert None == adapter.config.get("host")

        svc = ShopifyService(adapter)

        assert "shop1.myshopify.com" == svc.host
        assert "https://shop1.myshopify.com/admin/api/2022-04" == svc.base_url

        assert svc.is_private_app
        assert "u" == svc.username
        assert "p" == svc.password

    # request testing

    @mock.patch('requests.request',
                side_effect=mock_api_request(200, {"id": 12345}))
    def test_GET_request_public_app(self, mock_req: mock.MagicMock):
        adapter = db.AdapterConfig(
            client_id="c0", service_id="shopify", service_type="shopify", instance_id="shop0", config={"access_token": "t0"})
        svc = ShopifyService(adapter)

        action = svc.request("GET", "/resource/x", params={"limit": 2})

        expected_url = "{}/resource/x.json".format(svc.base_url)
        mock_req.assert_called_once_with(
            "GET", expected_url, **{
                "params": {"limit": 2},
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
            })

        assert 200 == action.status_code
        assert 0 < action.timestamp.timestamp()
        assert {
            "method": "GET",
            "url": expected_url,
            "params": {"limit": 2},
            "payload": {},
        } == action.request
        assert {
            "status_code": 200,
            "headers": {},
            "payload": {"id": 12345},
            "page_info": None,
        } == action.response

    @mock.patch('requests.request',
                side_effect=mock_api_request(201, {"id": 12345}))
    def test_POST_request_public_app(self, mock_req: mock.MagicMock):
        adapter = db.AdapterConfig(
            client_id="c0", service_id="shopify", service_type="shopify", instance_id="shop0", config={"access_token": "t0"})
        svc = ShopifyService(adapter)

        action = svc.request("POST", "/resource/x", payload={"name": "x"})

        expected_url = "{}/resource/x.json".format(svc.base_url)
        mock_req.assert_called_once_with(
            "POST", expected_url, **{
                "json": {"name": "x"},  # the payload
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
            })

        assert 201 == action.status_code
        assert 0 < action.timestamp.timestamp()
        assert {
            "method": "POST",
            "url": expected_url,
            "params": {},
            "payload": {"name": "x"},
        } == action.request
        assert {
            "status_code": 201,
            "headers": {},
            "payload": {"id": 12345},
            "page_info": None,
        } == action.response

    @mock.patch('requests.request',
                side_effect=mock_api_request(200, {"id": 12345}))
    def test_GET_request_private_app(self, mock_req: mock.MagicMock):
        adapter = db.AdapterConfig(
            client_id="c0", service_id="shopify", service_type="shopify", instance_id="shop0", config={
                "private_app": True,
                "username": "u0",
                "password": "p0",
                "verification_key": "v0",
            })
        svc = ShopifyService(adapter)

        action = svc.request("GET", "/resource/x", params={"limit": 2})

        expected_url = "{}/resource/x.json".format(svc.base_url)
        mock_req.assert_called_once_with(
            "GET", expected_url, **{
                "params": {"limit": 2},
                "auth": ("u0", "p0"),
                "headers": {
                    "Content-Type": "application/json",
                },
            })

        assert 200 == action.status_code
        assert 0 < action.timestamp.timestamp()
        assert {
            "method": "GET",
            "url": expected_url,
            "params": {"limit": 2},
            "payload": {},
        } == action.request
        assert {
            "status_code": 200,
            "headers": {},
            "payload": {"id": 12345},
            "page_info": None,
        } == action.response

    @mock.patch('requests.request', side_effect=mock_api_request(
        200,
        {"id": 12345},
        {"Link": "<https://x.y.z?page_info=abc123&limit=5>; rel=next, <https://x.y.z?page_info=321cba&limit=5>; rel=previous"}))
    def test_GET_request_public_app_with_page_info(self, mock_req: mock.MagicMock):
        adapter = db.AdapterConfig(
            client_id="c0", service_id="shopify", service_type="shopify", instance_id="shop0", config={"access_token": "t0"})
        svc = ShopifyService(adapter)

        action = svc.request("GET", "/resource/x", params={"limit": 2})

        expected_url = "{}/resource/x.json".format(svc.base_url)
        mock_req.assert_called_once_with(
            "GET", expected_url, **{
                "params": {"limit": 2},
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
            })

        assert 200 == action.status_code
        assert 0 < action.timestamp.timestamp()
        assert {
            "method": "GET",
            "url": expected_url,
            "params": {"limit": 2},
            "payload": {},
        } == action.request
        assert {
            "status_code": 200,
            "headers": {
                "Link": "<https://x.y.z?page_info=abc123&limit=5>; rel=next, <https://x.y.z?page_info=321cba&limit=5>; rel=previous"
            },
            "payload": {"id": 12345},
            "page_info": "abc123",
        } == action.response
