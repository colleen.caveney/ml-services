import unittest.mock as mock

import leap.errors
import pytest
from leap.adapters import db
from leap.google.cloud.firestore import tests as fstests
from leap.services.shopify import ShopifyService
from leap.services.shopify._events import EventType, RequiredEventTypeMap
from leap.services.shopify._webhook import (WEBHOOK_URL_FMT, WEBHOOK_URL_SIG_CHECK_FMT, delete_shopify_webhook,
                                            install_shopify_webhook, install_webhooks)
from leap.std.testing.requests import Response as MockResponse


def mock_shopify_request(status_code: int = 200, json_data: dict = {}, headers: dict = {}) -> MockResponse:
    return lambda *args, **kwargs: MockResponse(status_code, json_data, headers=headers)


def mock_shopify_request_failure(*args, **kwargs) -> MockResponse:
    return MockResponse(500)


class TestWebhook:

    @mock.patch('requests.request', side_effect=mock_shopify_request(200, {"id": 12345}))
    def test_delete_shopify_webhook_success(self, mock_req: mock.MagicMock):
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        svc = ShopifyService(adapter)
        adapter_webhook = db.AdapterWebhook(adapter_id=adapter.id,
                                            event_type=EventType.DECOM,
                                            adapter_event_type="app/uninstalled",
                                            adapter_webhook_id="123455")

        delete_shopify_webhook(svc, adapter_webhook)
        expected_url = f"{svc.base_url}/webhooks/{adapter_webhook.adapter_webhook_id}.json"
        mock_req.assert_called_once_with(
            "DELETE", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
            })

    @mock.patch('requests.request', side_effect=mock_shopify_request_failure)
    def test_delete_shopify_webhook_failure(self, mock_req: mock.MagicMock):
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        svc = ShopifyService(adapter)
        adapter_webhook = db.AdapterWebhook(adapter_id=adapter.id,
                                            event_type=EventType.DECOM,
                                            adapter_event_type="app/uninstalled",
                                            adapter_webhook_id="123455")

        with pytest.raises(leap.errors.UnresolvableError):
            delete_shopify_webhook(svc, adapter_webhook)
        expected_url = f"{svc.base_url}/webhooks/{adapter_webhook.adapter_webhook_id}.json"
        mock_req.assert_called_once_with(
            "DELETE", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
            })

    @mock.patch('requests.request',
                side_effect=mock_shopify_request(200, {"webhook": {
                    "id": "the adapter webhook id"
                }}))
    def test_install_shopify_webhook_success(self, mock_req: mock.MagicMock):
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 0})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"
        event_topic = "app/uninstalled"
        expected_webhook = db.AdapterWebhook(adapter_id=adapter.id,
                                             event_type=EventType.DECOM,
                                             adapter_event_type=event_topic,
                                             adapter_webhook_id="the adapter webhook id")

        req_payload = {
            "webhook": {
                "address":
                    WEBHOOK_URL_FMT.format(event_host=event_host,
                                           omcp_version=brand.config.omcp_version,
                                           adapter_id=svc.adapter.id,
                                           brand_id=brand.id,
                                           brand_handle=brand.handle,
                                           service_type=svc.adapter.service_type,
                                           merchant_type=svc.adapter.merchant_type),
                "topic":
                    event_topic,
                "format":
                    "json",
            }
        }
        adapter_webhook = install_shopify_webhook(svc, brand, event_host, "decom")
        expected_url = f"{svc.base_url}/webhooks.json"
        mock_req.assert_called_once_with(
            "POST", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
                "json": req_payload,
            })

        assert expected_webhook.adapter_id == adapter_webhook.adapter_id
        assert expected_webhook.adapter_webhook_id == adapter_webhook.adapter_webhook_id
        assert expected_webhook.event_type == adapter_webhook.event_type
        assert expected_webhook.adapter_event_type == adapter_webhook.adapter_event_type

    @mock.patch('requests.request',
                side_effect=mock_shopify_request(200, {"webhook": {
                    "id": "the adapter webhook id"
                }}))
    def test_install_shopify_webhook_bypass_success(self, mock_req: mock.MagicMock):
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={
                                       "access_token": "t0",
                                       "sig_check": db.SigCheckType.BYPASS
                                   })
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 0})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"
        event_topic = "app/uninstalled"
        expected_webhook = db.AdapterWebhook(adapter_id=adapter.id,
                                             event_type=EventType.DECOM,
                                             adapter_event_type=event_topic,
                                             adapter_webhook_id="the adapter webhook id")

        req_payload = {
            "webhook": {
                "address":
                    WEBHOOK_URL_SIG_CHECK_FMT.format(event_host=event_host,
                                                     omcp_version=brand.config.omcp_version,
                                                     adapter_id=svc.adapter.id,
                                                     brand_id=brand.id,
                                                     brand_handle=brand.handle,
                                                     service_type=svc.adapter.service_type,
                                                     merchant_type=svc.adapter.merchant_type,
                                                     sig_check=db.SigCheckType.BYPASS),
                "topic":
                    event_topic,
                "format":
                    "json",
            }
        }
        adapter_webhook = install_shopify_webhook(svc, brand, event_host, "decom", db.SigCheckType.BYPASS)
        expected_url = f"{svc.base_url}/webhooks.json"
        mock_req.assert_called_once_with(
            "POST", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
                "json": req_payload,
            })

        assert expected_webhook.adapter_id == adapter_webhook.adapter_id
        assert expected_webhook.adapter_webhook_id == adapter_webhook.adapter_webhook_id
        assert expected_webhook.event_type == adapter_webhook.event_type
        assert expected_webhook.adapter_event_type == adapter_webhook.adapter_event_type

    @mock.patch('requests.request', side_effect=mock_shopify_request_failure)
    def test_install_shopify_webhook_failure(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(db.Service("shopify", "some-key", "some-secret"))
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 0})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"
        event_topic = "app/uninstalled"

        req_payload = {
            "webhook": {
                "address":
                    WEBHOOK_URL_FMT.format(event_host=event_host,
                                           omcp_version=brand.config.omcp_version,
                                           adapter_id=svc.adapter.id,
                                           brand_id=brand.id,
                                           brand_handle=brand.handle,
                                           service_type=svc.adapter.service_type,
                                           merchant_type=svc.adapter.merchant_type),
                "topic":
                    event_topic,
                "format":
                    "json",
            }
        }

        with pytest.raises(leap.errors.UnresolvableError):
            install_shopify_webhook(svc, brand, event_host, "decom")

        expected_url = f"{svc.base_url}/webhooks.json"
        mock_req.assert_called_once_with(
            "POST", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
                "json": req_payload,
            })

    @mock.patch('requests.request',
                side_effect=mock_shopify_request(200, {"webhook": {
                    "id": "the adapter webhook id"
                }}))
    def test_install_webhooks_success_v0(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(db.Service("shopify", "some-key", "some-secret"))

        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 0})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"
        event_topic = "app/uninstalled"
        expected_webhook = db.AdapterWebhook(adapter_id=adapter.id,
                                             event_type=EventType.DECOM,
                                             adapter_event_type="app/uninstalled",
                                             adapter_webhook_id="the adapter webhook id")

        req_payload = {
            "webhook": {
                "address":
                    WEBHOOK_URL_FMT.format(event_host=event_host,
                                           omcp_version=brand.config.omcp_version,
                                           adapter_id=svc.adapter.id,
                                           brand_id=brand.id,
                                           brand_handle=brand.handle,
                                           service_type=svc.adapter.service_type,
                                           merchant_type=svc.adapter.merchant_type),
                "topic":
                    event_topic,
                "format":
                    "json",
            }
        }
        install_webhooks(db_client, svc, brand, event_host, [EventType.DECOM])
        expected_url = f"{svc.base_url}/webhooks.json"
        mock_req.assert_called_once_with(
            "POST", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
                "json": req_payload,
            })

        # load adapter webhook from the db
        actual_webhook = db_client.adapter_webhook(adapter_id=adapter.id, event_type=EventType.DECOM)

        assert expected_webhook.adapter_id == actual_webhook.adapter_id
        assert expected_webhook.adapter_webhook_id == actual_webhook.adapter_webhook_id
        assert expected_webhook.event_type == actual_webhook.event_type
        assert expected_webhook.adapter_event_type == actual_webhook.adapter_event_type

    @mock.patch('requests.request',
                side_effect=mock_shopify_request(200, {"webhook": {
                    "id": "the adapter webhook id"
                }}))
    def test_install_webhooks_success_v1(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(db.Service("shopify", "some-key", "some-secret"))

        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 1})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"
        event_topic = "app/uninstalled"
        expected_webhook = db.AdapterWebhook(adapter_id=adapter.id,
                                             event_type=EventType.DECOM,
                                             adapter_event_type="app/uninstalled",
                                             adapter_webhook_id="the adapter webhook id")

        req_payload = {
            "webhook": {
                "address":
                    WEBHOOK_URL_FMT.format(event_host=event_host,
                                           omcp_version=brand.config.omcp_version,
                                           adapter_id=svc.adapter.id,
                                           brand_id=brand.id,
                                           brand_handle=brand.handle,
                                           service_type=svc.adapter.service_type,
                                           merchant_type=svc.adapter.merchant_type),
                "topic":
                    event_topic,
                "format":
                    "json",
            }
        }
        install_webhooks(db_client, svc, brand, event_host, [EventType.DECOM])
        expected_url = f"{svc.base_url}/webhooks.json"
        mock_req.assert_called_once_with(
            "POST", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
                "json": req_payload,
            })

        # load adapter webhook from the db
        actual_webhook = db_client.adapter_webhook(adapter_id=adapter.id, event_type=EventType.DECOM)

        assert expected_webhook.adapter_id == actual_webhook.adapter_id
        assert expected_webhook.adapter_webhook_id == actual_webhook.adapter_webhook_id
        assert expected_webhook.event_type == actual_webhook.event_type
        assert expected_webhook.adapter_event_type == actual_webhook.adapter_event_type

    @mock.patch('requests.request',
                side_effect=mock_shopify_request(200, {"webhook": {
                    "id": "the adapter webhook id"
                }}))
    def test_install_webhooks_success_with_bypass(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(db.Service("shopify", "some-key", "some-secret"))

        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={
                                       "access_token": "t0",
                                       "sig_check": db.SigCheckType.BYPASS
                                   })
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 1})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"
        event_topic = "app/uninstalled"
        expected_webhook = db.AdapterWebhook(adapter_id=adapter.id,
                                             event_type=EventType.DECOM,
                                             adapter_event_type="app/uninstalled",
                                             adapter_webhook_id="the adapter webhook id")

        req_payload = {
            "webhook": {
                "address":
                    WEBHOOK_URL_SIG_CHECK_FMT.format(event_host=event_host,
                                                     omcp_version=brand.config.omcp_version,
                                                     adapter_id=svc.adapter.id,
                                                     brand_id=brand.id,
                                                     brand_handle=brand.handle,
                                                     service_type=svc.adapter.service_type,
                                                     merchant_type=svc.adapter.merchant_type,
                                                     sig_check=db.SigCheckType.BYPASS),
                "topic":
                    event_topic,
                "format":
                    "json",
            }
        }
        install_webhooks(db_client, svc, brand, event_host, [EventType.DECOM])
        expected_url = f"{svc.base_url}/webhooks.json"
        mock_req.assert_called_once_with(
            "POST", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
                "json": req_payload,
            })

        # load adapter webhook from the db
        actual_webhook = db_client.adapter_webhook(adapter_id=adapter.id, event_type=EventType.DECOM)

        assert expected_webhook.adapter_id == actual_webhook.adapter_id
        assert expected_webhook.adapter_webhook_id == actual_webhook.adapter_webhook_id
        assert expected_webhook.event_type == actual_webhook.event_type
        assert expected_webhook.adapter_event_type == actual_webhook.adapter_event_type

    @mock.patch('requests.request',
                side_effect=mock_shopify_request(200, {"webhook": {
                    "id": "the adapter webhook id"
                }}))
    def test_install_webhooks_success_dry_run(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(db.Service("shopify", "some-key", "some-secret"))

        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 0})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"
        install_webhooks(db_client, svc, brand, event_host, [EventType.DECOM], dry_run=True)
        mock_req.assert_not_called()

        # load adapter webhook from the db
        actual_webhook = db_client.adapter_webhook(adapter_id=adapter.id, event_type=EventType.DECOM)
        # make sure nothing was found
        assert None == actual_webhook

    @mock.patch('requests.request',
                side_effect=mock_shopify_request(200, {"webhook": {
                    "id": "the adapter webhook id"
                }}))
    def test_install_webhooks_success_dont_reinstall_hooks(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(db.Service("shopify", "some-key", "some-secret"))
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 1})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"

        existing_hook = db.AdapterWebhook(adapter_id=adapter.id,
                                          event_type=EventType.DECOM,
                                          adapter_event_type="app/uninstall",
                                          adapter_webhook_id="the adapter old webhook id")

        db_client.save_adapter_webhook(existing_hook)

        with pytest.raises(leap.errors.UnresolvableError):
            install_webhooks(db_client, svc, brand, event_host, [EventType.DECOM], reinstall_hooks=False)
        mock_req.assert_not_called()

        # old hook is still here
        old_webhook = db_client.adapter_webhook(adapter_id=adapter.id,
                                                adapter_webhook_id=existing_hook.adapter_webhook_id,
                                                event_type=EventType.DECOM)

        assert existing_hook.adapter_id == old_webhook.adapter_id
        assert existing_hook.adapter_webhook_id == old_webhook.adapter_webhook_id
        assert existing_hook.event_type == old_webhook.event_type
        assert existing_hook.adapter_event_type == old_webhook.adapter_event_type

    @mock.patch('requests.request',
                side_effect=mock_shopify_request(200, {"webhook": {
                    "id": "the adapter webhook id"
                }}))
    def test_install_webhooks_success_reinstall(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(db.Service("shopify", "some-key", "some-secret"))
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 1})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"
        event_topic = "app/uninstalled"

        existing_hook = db.AdapterWebhook(adapter_id=adapter.id,
                                          event_type=EventType.DECOM,
                                          adapter_event_type="app/uninstalled",
                                          adapter_webhook_id="the-old-adapter-webhook-id")
        db_client.save_adapter_webhook(existing_hook)

        expected_webhook = db.AdapterWebhook(adapter_id=adapter.id,
                                             event_type=EventType.DECOM,
                                             adapter_event_type="app/uninstalled",
                                             adapter_webhook_id="the adapter webhook id")

        req_payload = {
            "webhook": {
                "address":
                    WEBHOOK_URL_FMT.format(event_host=event_host,
                                           omcp_version=brand.config.omcp_version,
                                           adapter_id=svc.adapter.id,
                                           brand_id=brand.id,
                                           brand_handle=brand.handle,
                                           service_type=svc.adapter.service_type,
                                           merchant_type=svc.adapter.merchant_type),
                "topic":
                    event_topic,
                "format":
                    "json",
            }
        }
        expected_delete_url = f"{svc.base_url}/webhooks/{existing_hook.adapter_webhook_id}.json"
        expected_install_url = f"{svc.base_url}/webhooks.json"

        install_webhooks(db_client, svc, brand, event_host, [EventType.DECOM], reinstall_hooks=True)

        mock_req.assert_any_call("DELETE", expected_delete_url, **{
            "headers": {
                "Content-Type": "application/json",
                "X-Shopify-Access-Token": "t0",
            },
        })

        mock_req.assert_any_call(
            "POST", expected_install_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
                "json": req_payload,
            })

        # old hook is gone
        old_webhook = db_client.adapter_webhook(adapter_id=adapter.id,
                                                adapter_webhook_id=existing_hook.adapter_webhook_id,
                                                event_type=EventType.DECOM)
        assert None == old_webhook

        # load adapter webhook from the db
        actual_webhook = db_client.adapter_webhook(adapter_id=adapter.id, event_type=EventType.DECOM)

        assert expected_webhook.adapter_id == actual_webhook.adapter_id
        assert expected_webhook.adapter_webhook_id == actual_webhook.adapter_webhook_id
        assert expected_webhook.event_type == actual_webhook.event_type
        assert expected_webhook.adapter_event_type == actual_webhook.adapter_event_type

    @mock.patch('requests.request', side_effect=mock_shopify_request_failure)
    def test_install_webhooks_success_reinstall_delete_failure(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(db.Service("shopify", "some-key", "some-secret"))
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 1})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"

        existing_hook = db.AdapterWebhook(adapter_id=adapter.id,
                                          event_type=EventType.DECOM,
                                          adapter_event_type="app/uninstall",
                                          adapter_webhook_id="the-old-adapter-webhook-id")
        db_client.save_adapter_webhook(existing_hook)
        expected_delete_url = f"{svc.base_url}/webhooks/{existing_hook.adapter_webhook_id}.json"

        with pytest.raises(leap.errors.UnresolvableError):
            install_webhooks(db_client, svc, brand, event_host, [EventType.DECOM], reinstall_hooks=True)

        mock_req.assert_any_call("DELETE", expected_delete_url, **{
            "headers": {
                "Content-Type": "application/json",
                "X-Shopify-Access-Token": "t0",
            },
        })

        # old hook is still here
        old_webhook = db_client.adapter_webhook(adapter_id=adapter.id,
                                                adapter_webhook_id=existing_hook.adapter_webhook_id,
                                                event_type=EventType.DECOM)

        assert existing_hook.adapter_id == old_webhook.adapter_id
        assert existing_hook.adapter_webhook_id == old_webhook.adapter_webhook_id
        assert existing_hook.event_type == old_webhook.event_type
        assert existing_hook.adapter_event_type == old_webhook.adapter_event_type

    @mock.patch('requests.request', side_effect=mock_shopify_request_failure)
    def test_install_webhooks_success_install_failure(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(db.Service("shopify", "some-key", "some-secret"))
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 1})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"
        event_topic = "app/uninstalled"

        req_payload = {
            "webhook": {
                "address":
                    WEBHOOK_URL_FMT.format(event_host=event_host,
                                           omcp_version=brand.config.omcp_version,
                                           adapter_id=svc.adapter.id,
                                           brand_id=brand.id,
                                           brand_handle=brand.handle,
                                           service_type=svc.adapter.service_type,
                                           merchant_type=svc.adapter.merchant_type),
                "topic":
                    event_topic,
                "format":
                    "json",
            }
        }
        expected_url = f"{svc.base_url}/webhooks.json"

        with pytest.raises(leap.errors.UnresolvableError):
            install_webhooks(db_client, svc, brand, event_host, [EventType.DECOM])

        mock_req.assert_called_once_with(
            "POST", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
                "json": req_payload,
            })

        # load adapter webhook from the db
        actual_webhook = db_client.adapter_webhook(adapter_id=adapter.id, event_type=EventType.DECOM)
        assert None == actual_webhook

    @mock.patch('requests.request',
                side_effect=mock_shopify_request(200, {"webhook": {
                    "id": "the adapter webhook id"
                }}))
    def test_install_webhooks_success_v1_many(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(db.Service("shopify", "some-key", "some-secret"))
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 1})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"

        req_payload = {
            "webhook": {
                "address":
                    WEBHOOK_URL_FMT.format(event_host=event_host,
                                           omcp_version=brand.config.omcp_version,
                                           adapter_id=svc.adapter.id,
                                           brand_id=brand.id,
                                           brand_handle=brand.handle,
                                           service_type=svc.adapter.service_type,
                                           merchant_type=svc.adapter.merchant_type),
                "topic":
                    "app/uninstalled",
                "format":
                    "json",
            }
        }
        expected_url = f"{svc.base_url}/webhooks.json"

        install_webhooks(db_client, svc, brand, event_host, RequiredEventTypeMap.keys())
        assert 1 == mock_req.call_count
        # verify we called with each topic
        mock_req.assert_any_call(
            "POST", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
                "json": req_payload,
            })

    def test_install_shopify_webhook_failure_service_doesnt_exist(self):
        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={"access_token": "t0"})
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 0})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"

        with pytest.raises(leap.errors.UnresolvableError):
            install_shopify_webhook(svc, brand, event_host, "decom")

    @mock.patch('requests.request',
                side_effect=mock_shopify_request(200, {"webhook": {
                    "id": "the adapter webhook id"
                }}))
    def test_install_webhooks_success_with_service_sig_check(self, mock_req: mock.MagicMock):
        db_client = db.FirestoreDB(fstests.Client())
        db_client.save_service(
            db.Service("shopify", "some-key", "some-secret", config={"sig_check": db.SigCheckType.OK}))

        adapter = db.AdapterConfig(client_id="c0",
                                   service_id="shopify",
                                   service_type="shopify",
                                   instance_id="shop0",
                                   config={
                                       "access_token": "t0",
                                       "sig_check": db.SigCheckType.BYPASS
                                   })
        brand = db.Brand(id="123124", name="brandy", handle="brand-handle", config={"omcp_version": 1})
        svc = ShopifyService(adapter)
        event_host = "eventhost.com"
        event_topic = "app/uninstalled"
        expected_webhook = db.AdapterWebhook(adapter_id=adapter.id,
                                             event_type=EventType.DECOM,
                                             adapter_event_type="app/uninstalled",
                                             adapter_webhook_id="the adapter webhook id")

        req_payload = {
            "webhook": {
                "address":
                    WEBHOOK_URL_SIG_CHECK_FMT.format(event_host=event_host,
                                                     omcp_version=brand.config.omcp_version,
                                                     adapter_id=svc.adapter.id,
                                                     brand_id=brand.id,
                                                     brand_handle=brand.handle,
                                                     service_type=svc.adapter.service_type,
                                                     merchant_type=svc.adapter.merchant_type,
                                                     sig_check=db.SigCheckType.OK),
                "topic":
                    event_topic,
                "format":
                    "json",
            }
        }
        install_webhooks(db_client, svc, brand, event_host, [EventType.DECOM])
        expected_url = f"{svc.base_url}/webhooks.json"
        mock_req.assert_called_once_with(
            "POST", expected_url, **{
                "headers": {
                    "Content-Type": "application/json",
                    "X-Shopify-Access-Token": "t0",
                },
                "json": req_payload,
            })

        # load adapter webhook from the db
        actual_webhook = db_client.adapter_webhook(adapter_id=adapter.id, event_type=EventType.DECOM)

        assert expected_webhook.adapter_id == actual_webhook.adapter_id
        assert expected_webhook.adapter_webhook_id == actual_webhook.adapter_webhook_id
        assert expected_webhook.event_type == actual_webhook.event_type
        assert expected_webhook.adapter_event_type == actual_webhook.adapter_event_type
