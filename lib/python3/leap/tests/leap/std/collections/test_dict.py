import leap.std.collections as c


class TestDictKeyFilter:
    def test_no_keys_in_filter(self):
        d1 = {"a": 1}
        d2 = c.dict_key_filter(d1, [])

        assert d1 != d2
        assert d1 is not d2
        assert {"a": 1} == d1
        assert {} == d2

    def test_nothing_to_filter(self):
        d1 = {"a": 1}
        d2 = c.dict_key_filter(d1, ["b"])

        assert d1 != d2
        assert d1 is not d2
        assert {"a": 1} == d1
        assert {} == d2

    def test_on_root_level(self):
        d1 = {"a": 1, "b": 2, "c": {}}
        d2 = c.dict_key_filter(d1, ["b", "c"])

        assert d1 is not d2
        assert d1 != d2
        assert {"a": 1, "b": 2, "c": {}} == d1
        assert {"b": 2, "c": {}} == d2

    def test_with_fill_true(self):
        d1 = {"a": 1, "b": 2, "d": 4, "f": 6, "G": {"g1": 1, "g3": 3}}

        assert {"b": 2, "c": None, "d": 4, "e": None} == c.dict_key_filter(
            d1, ["b", "c", "d", "e"], True)
        assert {"a": 1, "c": None, "G": {"g1": 1, "g2": None}} == c.dict_key_filter(
            d1, ["a", "c", ("G", ["g1", "g2"])], True)

    def test_object_on_first_branch(self):
        d1 = {"a": 1, "b": 2, "c": {"d": 4, "e": 5, "f": 6}}
        d2 = c.dict_key_filter(d1, ["a", ("c", ["e", "f"])])

        assert d1 != d2
        assert {"a": 1, "b": 2, "c": {"d": 4, "e": 5, "f": 6}} == d1
        # notice that 'b' and 'd' are missing
        assert {"a": 1, "c": {"e": 5, "f": 6}} == d2
        assert d1["c"] is not d2["c"]

    def test_list_of_objects_on_first_branch(self):
        d1 = {
            "a": 1,
            "b": 2,
            "c": [
                {"d": 41, "e": 51, "f": 61},
                {"d": 42, "e": 52, "f": 62}
            ]
        }
        d2 = c.dict_key_filter(d1, ["a", ("c", ["e", "f"])])

        assert d1 != d2

        assert {
            "a": 1,
            "b": 2,
            "c": [
                {"d": 41, "e": 51, "f": 61},
                {"d": 42, "e": 52, "f": 62}
            ]
        } == d1

        # notice that 'b' and 'd's are missing
        assert {
            "a": 1,
            "c": [
                {"e": 51, "f": 61},
                {"e": 52, "f": 62}
            ]
        } == d2

    def test_object_on_first_branch_which_cannot_be_filtered(self):
        d1 = {"a": 1, "b": 2, "c": 3}
        # notice we say we want to filter 'c' as if it's an object or list of objects
        d2 = c.dict_key_filter(d1, ["a", ("c", ["e", "f"])])

        assert d1 != d2
        assert {"a": 1, "b": 2, "c": 3} == d1
        assert {"a": 1, "c": 3} == d2

    def test_with_deep_nesting(self):
        d1 = {
            "a": 1,
            "b": [
                {"b.1": [
                    {"b.1.1": 2, "b.1.2": 3},
                ]},
            ],
            "c": [
                {"c.1": 41, "c.2": {"c.2.1": 511, "c.2.2": 512}, "c.3": 61},
                {"c.1": 42, "c.2": 52, "c.3": {"c.3.1": 621, "c.3.2": 622}},
                {"c.1": 42, "c.2": 52, "c.3": 63},
            ]
        }
        d2 = c.dict_key_filter(d1, [
            ("b", [("b.1", ["b.1.2"])]),
            ("c", [("c.2", ["c.2.2"]), ("c.3", ["c.3.1"])])
        ])

        assert d1 != d2

        assert {
            "a": 1,
            "b": [
                {"b.1": [
                    {"b.1.1": 2, "b.1.2": 3},
                ]},
            ],
            "c": [
                {"c.1": 41, "c.2": {"c.2.1": 511, "c.2.2": 512}, "c.3": 61},
                {"c.1": 42, "c.2": 52, "c.3": {"c.3.1": 621, "c.3.2": 622}},
                {"c.1": 42, "c.2": 52, "c.3": 63},
            ]
        } == d1

        assert {
            "b": [
                {"b.1": [{"b.1.2": 3}]},
            ],
            "c": [
                {"c.2": {"c.2.2": 512}, "c.3": 61},
                {"c.2": 52, "c.3": {"c.3.1": 621}},
                {"c.2": 52, "c.3": 63},
            ]
        } == d2


class TestDictExcludeKeys:
    def test_empty_keys(self):
        data = {"a": 1, "b": 2}
        assert data == c.dict_exclude_keys(data, [])

    def test_data_is_empty(self):
        data = {}
        assert data == c.dict_exclude_keys(data, ["c", "d"])

    def test_no_matching_keys(self):
        data = {"a": 1, "b": 2}
        assert data == c.dict_exclude_keys(data, ["c", "d"])

    def test_with_matching_keys(self):
        data = {"a": 1, "b": 2, "c": 3, "d": 4}
        assert {"a": 1, "b": 2} == c.dict_exclude_keys(data, ["c", "d"])


class TestDictDiff:
    def test_both_inputs_empty(self):
        a = {}
        b = {}
        assert {} == c.dict_diff(a, b)

    def test_right_side_empty(self):
        a = {"a": 1, "b": 2}
        b = {}
        assert {} == c.dict_diff(a, b)

    def test_left_side_empty(self):
        a = {}
        b = {"a": 1, "b": 2}
        assert b == c.dict_diff(a, b)

    def test_no_differences(self):
        a = {"a": 1, "b": 2}
        b = a
        assert {} == c.dict_diff(a, b)

    def test_all_values_differ(self):
        a = {"a": 1, "b": 2}
        b = {"a": 3, "b": 4}
        assert b == c.dict_diff(a, b)

    def test_some_values_differ(self):
        a = {"a": 1, "b": 2}
        b = {"a": 1, "b": 4}
        assert {"b": 4} == c.dict_diff(a, b)
