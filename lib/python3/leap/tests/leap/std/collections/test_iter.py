from typing import Any, Iterator, Tuple

import pytest

import leap.std.collections as c


class TestIterHelpers:
    @pytest.mark.parametrize("tc", [
        ([], None),
        ([1, 2, 3, 4, 5, 6], 1),
        ([None, "a", "b", 3], None),
    ])
    def test_iter(self, tc: Tuple[Iterator[Any], Any]):
        data, expected = tc
        assert expected == c.iter_first(data)
