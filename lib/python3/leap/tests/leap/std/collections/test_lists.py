import leap.std.collections as c


class TestListComprehensions:
    def test_list_sub(self):
        assert [4, 1, 1] == c.list_sub([4, 3, 2, 1, 2, 3, 1], [2, 3])
