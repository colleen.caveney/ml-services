import leap.std.collections as c


class TestRegistry:
    def test_lookup_and_register(self):
        reg: c.Registry[int] = c.Registry()
        assert None == reg.lookup("a")
        reg.register("a", 1)
        assert 1 == reg.lookup("a")
        reg.register("abcd", 1234)
        assert 1234 == reg.lookup("abcd")
        assert 1 == reg.lookup("a")

    def test_unregister(self):
        reg: c.Registry[int] = c.Registry()
        assert None == reg.lookup("a")
        reg.register("a", 1)
        assert 1 == reg.lookup("a")
        assert 1 == reg.unregister("a")
        assert None == reg.lookup("a")
        assert None == reg.unregister("a")
