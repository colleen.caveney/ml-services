import time
from datetime import datetime

from leap.ctx import attrs
from leap.std import logging as leaplog


def now_ts() -> int:
    return int(datetime.now().timestamp() * 1000)


class TestLeapLogEntry:
    def test_default_constructor(self):
        log_entry = leaplog.LogEntry("event", "success", attrs.ContextAttrs())

        assert "event" == log_entry.type
        assert "success" == log_entry.category

        assert log_entry.ts > 0
        assert now_ts() - log_entry.ts < 1000

        assert 0 == log_entry.latency_ms

        assert None == log_entry.message
        assert {} == log_entry.data

        assert [
            attrs.TRACE_ID,
        ] == list(log_entry.attrs.keys())

    def test_construct_with_message(self):
        log_entry = leaplog.LogEntry(
            "batch", "complete", attrs.ContextAttrs(), message="yassss")

        assert "batch" == log_entry.type
        assert "complete" == log_entry.category
        assert "yassss" == log_entry.message

    def test_update_message(self):
        log_entry = leaplog.LogEntry("t", "c", attrs.ContextAttrs())
        log_entry.message = "new message"
        assert "new message" == log_entry.message

    def test_capture_latency(self):
        log_entry = leaplog.LogEntry("t", "c", attrs.ContextAttrs())
        time.sleep(0.5)
        log_entry.capture_latency()
        assert log_entry.latency_ms >= 500 and log_entry.latency_ms < 1000

    def test_update_without_additional_data(self):
        log_entry = leaplog.LogEntry("t", "c", attrs.ContextAttrs())
        log_entry.update("pending", "waiting for stuff")

        assert "pending" == log_entry.category
        assert "waiting for stuff" == log_entry.message
        assert {} == log_entry.data

    def test_update_with_additional_data(self):
        log_entry = leaplog.LogEntry("t", "c", attrs.ContextAttrs(), data={
            "foo": "bar",
            "baz": 3,
        })

        assert {
            "foo": "bar",
            "baz": 3,
        } == log_entry.data

        log_entry.update("pending", "waiting for stuff", {
            "baz": 5,
            "car": [1, 2, 3],
        })

        assert "pending" == log_entry.category
        assert "waiting for stuff" == log_entry.message

        assert {
            "foo": "bar",
            "baz": 5,
            "car": [1, 2, 3],
        } == log_entry.data
