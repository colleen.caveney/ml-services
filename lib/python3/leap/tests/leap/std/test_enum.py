from leap.std.enum import TypedEnum


class StrEnum(str, TypedEnum):
    A = "a"
    B = "b"
    C = 3


class IntEnum(int, TypedEnum):
    A = 1
    B = 2
    C = 3


class TestEnumModule:
    def test_StrEnum_honors_type(self):
        assert ["A", "B", "C"] == StrEnum.names()
        assert ["a", "b", "3"] == StrEnum.values()

        assert "3" == StrEnum.C
        assert True == StrEnum.has_value("3")
        assert False == StrEnum.has_value(3)

    def test_IntEnum_honors_type(self):
        assert ["A", "B", "C"] == IntEnum.names()
        assert [1, 2, 3] == IntEnum.values()

        assert 3 == IntEnum.C
        assert True == IntEnum.has_value(3)
        assert False == IntEnum.has_value("3")
