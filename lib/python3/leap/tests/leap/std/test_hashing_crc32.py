from typing import Tuple

import pytest

from leap.std.hashing import crc32


class TestHashingCRC32:
    @pytest.mark.parametrize("test_data", [
        ({}, "0"),
        ({"city": "chicago", "state": "il"}, "a2c595e6"),
        ({"state": "il", "city": "chicago"}, "a2c595e6"),
        ({"int-a": 1, "float-b": 0.123}, "3df210a9"),
        ({"list-a": ["a", "b", 3]}, "a7bbde91"),
        ({"dict-a": {"int-b": 1, "str-c": "abc"}}, "716c2f7c"),
    ])
    def test_hash_object(self, test_data: Tuple[dict, str]):
        data, expected = test_data
        assert expected == crc32.hash_object(**data)
