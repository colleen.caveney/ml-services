from typing import Optional, Tuple

import pytest

from leap.std.hashing import md5


class TestHashingMD5:
    @pytest.mark.parametrize("data_expected", [
        (None, "6adf97f83acf6453d4a6a4b1070f3754"),
        ({}, "d751713988987e9331980363e24189ce"),
        ({"a": 1, "b": {"c": 3}}, "e8095db5465ed8f225eb5b36f52690ce"),
        ({"a": 1, "b": 2, "c": 3, "d": 4}, "9361a1077b978f35350f0572dee73f95"),
        ({"b": 2, "d": 4, "c": 3, "a": 1}, "9361a1077b978f35350f0572dee73f95"),
        ({"a": 1, "b": {"c": 3}, 1: 4}, "ea3174fa49c2b6fef72567f536bf63fb"),
        ("", "d41d8cd98f00b204e9800998ecf8427e"),
        ("hello, world", "e4d7f1b4ed2e42d15898f4b27b019da4"),
        (["a", 4, "d", 2, "c", 1], "d1f141f94a06e8c6d1945d722941c4d6"),
        ([{"a": 1}, {"b": 2}], "d89a613b007187841728304d81f56761"),
        ({1, 2, "a", "d"}, "86197b63f89949c17018048bad4994c4"),
        (12345, "827ccb0eea8a706c4c34a16891f84e7b"),
    ])
    def test_hash_object(self, data_expected: Tuple[Optional[dict], str]):
        data, expected = data_expected
        assert expected == md5.hash_object(data)
