# pylint: disable=missing-docstring,redefined-outer-name
from datetime import datetime

import pytest

import leap.std.json


class Foo(leap.std.json.Serializable):
    def __init__(self, s: str):
        self.s = s

    def to_json(self) -> str:
        return self.s

    def to_object(self) -> object:
        return self.s

class TestJSON:
    @pytest.mark.parametrize("tc", [
        ({}, '{}'),
        ({"a": 1}, '{"a": 1}'),
        ({"a": 1, "ts": datetime(2020, 1, 1)}, '{"a": 1, "ts": "2020-01-01T00:00:00"}'),
        ({"what": Foo("now")}, '{"what": "now"}')
    ])
    def test_dumps(self, tc):
        data, expected = tc
        assert expected == leap.std.json.dumps(data)

    def test_dump_compact(self):
        obj: dict = {
            "foo": "bar",
            "baz": [1, 2, 3, 4],
            "bum": { "a": "b" },
        }
        expected = '{"foo":"bar","baz":[1,2,3,4],"bum":{"a":"b"}}'
        assert expected == leap.std.json.dump_compact(obj)
