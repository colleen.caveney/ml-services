from datetime import datetime

import pytest

import leap.errors
from leap.std.retry import with_retry


class TestRetryModule:
    def test_with_retry_N_too_low(self):
        ctr = 0

        def fn():
            nonlocal ctr
            ctr += 1
            return ctr

        assert None == with_retry(fn, 0)

    def test_with_retry_success_first_try(self):
        ctr = 0

        def fn():
            nonlocal ctr
            ctr += 1
            return ctr

        assert 1 == with_retry(fn)

    def test_with_retry_success_Nth_try(self):
        ctr, fail_until, delay = 0, 5, 0.1
        t0 = datetime.now()

        def fn():
            nonlocal ctr
            ctr += 1
            if ctr < fail_until:
                raise leap.errors.RetryError(delay)
            return ctr

        assert fail_until == with_retry(fn)

        # asserts that sleeps are likely happening
        t1 = datetime.now()
        assert (fail_until - 1) * delay < t1.timestamp() - t0.timestamp()

    def test_with_retry_never_succeeds(self):
        ctr, n, delay = 0, 5, 0.1
        t0 = datetime.now()

        def fn():
            nonlocal ctr
            ctr += 1
            raise leap.errors.RetryError(
                delay, "attempt {} failed".format(ctr))

        # asserts that the last error from fn is the one raised from with_retry
        with pytest.raises(leap.errors.RetryError, match="attempt {} failed".format(n)):
            with_retry(fn, n)

        # asserts that sleeps are likely happening
        t1 = datetime.now()
        assert n * delay < t1.timestamp() - t0.timestamp()
