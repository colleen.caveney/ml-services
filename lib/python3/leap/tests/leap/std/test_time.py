# pylint: disable=missing-docstring,redefined-outer-name

from datetime import datetime

import leap.std.time


class TestTime:
    ## Generators

    def test_seq_gen_generates_datetime_instances(self):
        dtiter = leap.std.time.seq_gen()
        dt = next(dtiter)
        assert isinstance(dt, datetime)

    def test_seq_gen_default_start_idx(self):
        dtiter = leap.std.time.seq_gen()
        assert datetime(1970, 1, 1, 0, 0, 0) == next(dtiter)
        assert datetime(1970, 1, 1, 0, 0, 1) == next(dtiter)
        assert datetime(1970, 1, 1, 0, 0, 2) == next(dtiter)
        assert datetime(1970, 1, 1, 0, 0, 3) == next(dtiter)
        assert datetime(1970, 1, 1, 0, 0, 4) == next(dtiter)

    def test_seq_gen_custom_start_time_and_step(self):
        dtiter = leap.std.time.seq_gen(datetime(1984, 6, 15, 12, 30, 0), 10)
        assert datetime(1984, 6, 15, 12, 30, 0)  == next(dtiter)
        assert datetime(1984, 6, 15, 12, 30, 10) == next(dtiter)
        assert datetime(1984, 6, 15, 12, 30, 20) == next(dtiter)
        assert datetime(1984, 6, 15, 12, 30, 30) == next(dtiter)
        assert datetime(1984, 6, 15, 12, 30, 40) == next(dtiter)

    def test_datetime_gen(self):
        now = datetime.now()
        dtiter = leap.std.time.datetime_gen()
        dt1 = next(dtiter)
        assert isinstance(dt1, datetime)
        assert dt1 >= now

        dt2 = next(dtiter)
        assert dt2 >= dt1

        dt3 = next(dtiter)
        assert dt3 >= dt2

    def test_timestamp_gen(self):
        now = datetime.now().timestamp()
        tsiter = leap.std.time.timestamp_gen()
        ts1 = next(tsiter)
        assert isinstance(ts1, float)
        assert ts1 >= now

        ts2 = next(tsiter)
        assert ts2 >= ts1

        ts3 = next(tsiter)
        assert ts3 >= ts2

    ## Factories

    def test_seq_factory_default_start_idx(self):
        dtfactory = leap.std.time.seq_factory()
        assert datetime(1970, 1, 1, 0, 0, 0) == dtfactory()
        assert datetime(1970, 1, 1, 0, 0, 1) == dtfactory()
        assert datetime(1970, 1, 1, 0, 0, 2) == dtfactory()
        assert datetime(1970, 1, 1, 0, 0, 3) == dtfactory()
        assert datetime(1970, 1, 1, 0, 0, 4) == dtfactory()

    def test_seq_factory_custom_start_time_and_step(self):
        dtfactory = leap.std.time.seq_factory(datetime(1984, 6, 15, 12, 30, 0), 10)
        assert datetime(1984, 6, 15, 12, 30, 0)  == dtfactory()
        assert datetime(1984, 6, 15, 12, 30, 10) == dtfactory()
        assert datetime(1984, 6, 15, 12, 30, 20) == dtfactory()
        assert datetime(1984, 6, 15, 12, 30, 30) == dtfactory()
        assert datetime(1984, 6, 15, 12, 30, 40) == dtfactory()

    def test_datetime_factory(self):
        now = datetime.now()
        dtfactory = leap.std.time.datetime_factory()
        dt1 = dtfactory()
        assert isinstance(dt1, datetime)
        assert dt1 >= now

        dt2 = dtfactory()
        assert dt2 >= dt1

        dt3 = dtfactory()
        assert dt3 >= dt2

    def test_timestamp_factory(self):
        now = datetime.now().timestamp()
        tsfactory = leap.std.time.timestamp_factory()
        ts1 = tsfactory()
        assert isinstance(ts1, float)
        assert ts1 >= now

        ts2 = tsfactory()
        assert ts2 >= ts1

        ts3 = tsfactory()
        assert ts3 >= ts2
