# pylint: disable=missing-docstring,redefined-outer-name

import uuid

import leap.std.uuid


class TestUUID:
    ## Generators

    def test_seq_gen_generates_uuid_instances(self):
        uuiditer = leap.std.uuid.seq_gen()
        u = next(uuiditer)
        assert isinstance(u, uuid.UUID)
        assert None == u.version

    def test_seq_gen_default_start_idx(self):
        uuiditer = leap.std.uuid.seq_gen()
        assert uuid.UUID("00000000-0000-0000-0000-000000000000") == next(uuiditer)
        assert uuid.UUID("00000000-0000-0000-0000-000000000001") == next(uuiditer)
        assert uuid.UUID("00000000-0000-0000-0000-000000000002") == next(uuiditer)
        assert uuid.UUID("00000000-0000-0000-0000-000000000003") == next(uuiditer)
        assert uuid.UUID("00000000-0000-0000-0000-000000000004") == next(uuiditer)

    def test_seq_gen_custom_start_idx(self):
        uuiditer = leap.std.uuid.seq_gen(int(1e38))
        assert uuid.UUID("4b3b4ca8-5a86-c400-0000-000000000000") == next(uuiditer)
        assert uuid.UUID("4b3b4ca8-5a86-c400-0000-000000000001") == next(uuiditer)
        assert uuid.UUID("4b3b4ca8-5a86-c400-0000-000000000002") == next(uuiditer)
        assert uuid.UUID("4b3b4ca8-5a86-c400-0000-000000000003") == next(uuiditer)
        assert uuid.UUID("4b3b4ca8-5a86-c400-0000-000000000004") == next(uuiditer)

    def test_seq_gen_idx_rollover(self):
        uuiditer = leap.std.uuid.seq_gen(int(1<<128)-1)
        assert uuid.UUID("ffffffff-ffff-ffff-ffff-ffffffffffff") == next(uuiditer)
        assert uuid.UUID("00000000-0000-0000-0000-000000000000") == next(uuiditer)
        assert uuid.UUID("00000000-0000-0000-0000-000000000001") == next(uuiditer)

    def test_uuid4_gen(self):
        uuiditer = leap.std.uuid.uuid4_gen()
        u = next(uuiditer)
        assert isinstance(u, uuid.UUID)
        assert 4 == u.version
        assert u != next(uuiditer)
        assert next(uuiditer) != next(uuiditer)

    ## Factories

    def test_seq_factory_default_start_idx(self):
        uuidfactory = leap.std.uuid.seq_factory()
        assert uuid.UUID("00000000-0000-0000-0000-000000000000") == uuidfactory()
        assert uuid.UUID("00000000-0000-0000-0000-000000000001") == uuidfactory()
        assert uuid.UUID("00000000-0000-0000-0000-000000000002") == uuidfactory()
        assert uuid.UUID("00000000-0000-0000-0000-000000000003") == uuidfactory()
        assert uuid.UUID("00000000-0000-0000-0000-000000000004") == uuidfactory()

    def test_seq_factory_custom_start_idx(self):
        uuidfactory = leap.std.uuid.seq_factory(int(1e38))
        assert uuid.UUID("4b3b4ca8-5a86-c400-0000-000000000000") == uuidfactory()
        assert uuid.UUID("4b3b4ca8-5a86-c400-0000-000000000001") == uuidfactory()
        assert uuid.UUID("4b3b4ca8-5a86-c400-0000-000000000002") == uuidfactory()
        assert uuid.UUID("4b3b4ca8-5a86-c400-0000-000000000003") == uuidfactory()
        assert uuid.UUID("4b3b4ca8-5a86-c400-0000-000000000004") == uuidfactory()

    def test_uuid4_factory(self):
        uuidfactory = leap.std.uuid.uuid4_factory()
        u = uuidfactory()
        assert isinstance(u, uuid.UUID)
        assert 4 == u.version
        assert u != uuidfactory()
        assert uuidfactory() != uuidfactory()
