## git completion support
source /usr/share/bash-completion/completions/git

## prompt changes with git
test -f ~/.local/contrib/git-prompt.sh && source ~/.local/contrib/git-prompt.sh
