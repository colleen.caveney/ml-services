if [ -z "${NVM_DIR}" ]; then
    if [ -d ${HOME}/.nvm ]; then
        # prefer user's own NVM
        export NVM_DIR="${HOME}/.nvm"
    elif [ -d /usr/local/lib/nvm ]; then
        # defer to system nvm
        export NVM_DIR="/usr/local/lib/nvm"
    else
        echo -e "! NVM_DIR is not set"
    fi
fi

[ -s "${NVM_DIR}/nvm.sh" ] && \. "${NVM_DIR}/nvm.sh"  # load nvm
[ -s "${NVM_DIR}/bash_completion" ] && \. "${NVM_DIR}/bash_completion" # load nvm bash_completion
