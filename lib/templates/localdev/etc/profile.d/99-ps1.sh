color_prompt=yes

path_context=
# include git in path context if git-path function defined
if [ -n "$(type -t __git_ps1)" ]; then
  path_context="\$(__git_ps1 ':%s')"
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
  # default colors
  black='\[\e[0;30m\]'
  bblack='\[\e[01;30m\]'
  grey='\[\e[0;90m\]'
  bgrey='\[\e[01;90m\]'
  red='\[\e[0;31m\]'
  green='\[\e[0;32m\]'
  yellow='\[\e[0;33m\]'
  blue='\[\e[0;34m\]'
  purple='\[\e[0;35m\]'
  cyan='\[\e[0;36m\]'
  white='\[\e[0;37m\]'
  bwhite='\[\e[01;37m\]'
  reset='\[\e[00m\]'
  ymd='$(date +%Y-%m-%d)'
  hms='$(date +%H:%M:%S)'

  PS1="\n${bgrey}${ymd} ${hms} | ${bwhite}\W${cyan}${path_context}\n${red}\u@\h ${bwhite}$ ${reset}"
  unset black bblack grey bfre red green yellow blue purple cyan white bwhite reset ymd hms
fi

unset color_prompt force_color_prompt
