# Service: {{SERVICE_NAME}}

See: [Some doc](https://url/...)
See: [Another doc](https://url/...)

<SERVICE-PURPOSE>

## Development & Testing

Before beginning any new work, make sure that your `pipenv` is up-to-date:

```sh
make setup
```

This will install any missing dependencies (including dev); it won't hurt to run this multiple times. After that, tests are run like so:

```sh
$ make test
```

Manual integration testing can be conducted with the `server` target (see also [server.py](./server.py)):

```sh
$ make server
```

Then you can curl the `/` endpoint ...:

```sh
$ curl -i localhost:5000/

HTTP/1.0 200 OK
Content-Type: application/json
Server: Werkzeug/0.15.4 Python/3.7.1
Date: {{DATE}}
```

You can POST to the the `/` endpoint ...:

```sh
$ curl -i localhost:5000/ \
    -XPOST \
    -H"Content-Type: application/json" \
    -d '{}'

HTTP/1.0 204 NO CONTENT
Content-Type: application/json
Server: Werkzeug/0.15.4 Python/3.7.1
Date: {{DATE}}

"OK"
```

<OTHER EXAMPLES OF TESTING THE SERVICE LOCALLY>

## Architecture

The `{{SERVICE_NAME}}` service is deployed to GCP and is managed with Terraform. This service is comprised of the following resources:

* GCP function: see [main.py](./main.py)
* GCS bucket: where the code for the function will be uploaded for deployment
* <EXAMPLE> BigQuery dataset and table (see [schema.json](./schema.json))

Each of these resources are also defined in the [main terraform file](./main.tf). You will notice, however, that no `provider` for `google` is specified nor does any resource declare its own name. That's because `{{SERVICE_NAME}}` is a general terraform module that is used back in the [Leap Infra repo](https://gitlab.com/leapinc/infra/tree/main/<PROJECT>/services/{{SERVICE_NAME}}). In effect, never `plan` or `apply` anything from this directory.

The only files that should be getting deployed with the function are:

* Defaults: `main.py`, `const.py`, and `requirements.txt`
* <EXTRA-FILES>

`requirements.txt` is a generated file and should not be checked in. You will notice in `main.tf` an `external` provider which calls out to `make dist`; this is when `requirements.txt` gets generated.
