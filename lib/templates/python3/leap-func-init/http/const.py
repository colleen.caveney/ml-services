"""
constants

Constants for use in other modules. These values should not be changed outside
of this module, except under unit testing scenarios.
"""
