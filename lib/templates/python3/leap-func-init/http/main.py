"""
main

This module represents the main entry-point for this code to be used as a
GCloud Function (GFunc). It is assumed to run within a flask app and that
`process` is the entry-point function to be called.
"""

import flask

def process(req: flask.Request) -> flask.Response:
    """Entry-point function processor."""
    if req.method == "POST":
        return flask.Response(b'', mimetype="application/json", status=204)

    return flask.Response(flask.json.dumps("OK"), mimetype="application/json", status=200)
