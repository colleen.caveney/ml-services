"""
server

Acts similar to how gcloud will execute this Function. Meant only for local
development and testing.
"""

# pylint: disable=invalid-name,unused-argument

import leap.app

import main as gfunc

app = leap.app.catch_all(__name__, gfunc.process)
