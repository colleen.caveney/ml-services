# pylint: disable=missing-docstring,redefined-outer-name

import pytest
import server
from flask import json
from flask.testing import FlaskClient


class TestProcess:
    @pytest.fixture
    def app(self) -> FlaskClient:
        server.app.testing = True
        yield server.app.test_client()

    ## GET tests

    def test_get_success(self, app: FlaskClient):
        res = app.get("/")
        assert res.status_code == 200
        assert res.mimetype == "application/json"
        assert b'"OK"' in res.data

    ## POST tests

    def test_post_success(self, app: FlaskClient):
        content_type = "application/json"
        payload_data = json.dumps({})
        res = app.post("/", content_type=content_type, data=payload_data)
        assert res.status_code == 204
        assert res.mimetype == "application/json"
        assert b'' in res.data
