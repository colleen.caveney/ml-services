# Service: {{SERVICE_NAME}}

See: [Some doc](https://url/...)
See: [Another doc](https://url/...)

<SERVICE-PURPOSE>

## Development & Testing

Before beginning any new work, make sure that your `pipenv` is up-to-date:

```sh
make setup
```

This will install any missing dependencies (including dev); it won't hurt to run this multiple times. After that, tests are run like so:

```sh
$ make test
```

Manual integration testing can be conducted with the `trigger` target (see also [trigger.py](./trigger.py)):

```sh
make trigger
# OR
make trigger-debug
```

<OTHER EXAMPLES OF TESTING THE SERVICE LOCALLY>

## Architecture

The `{{SERVICE_NAME}}` service is deployed to GCP and is managed with Terraform. This service is comprised of the following resources:

* GCP function: see [main.py](./main.py)
* GCS bucket: where the code for the function will be uploaded for deployment
* Scheduler job: which writes to topic on some schedule that should just be once per day
* PubSub topic: for triggering the function call on publish
* <EXAMPLE> BigQuery dataset and table (see [schema.json](./schema.json))

Each of these resources are also defined in the [main terraform file](./main.tf). You will notice, however, that no `provider` for `google` is specified nor does any resource declare its own name. That's because `{{SERVICE_NAME}}` is a general terraform module that is used back in the [Leap Infra repo](https://gitlab.com/leapinc/infra/tree/main/<PROJECT>/services/{{SERVICE_NAME}}). In effect, never `plan` or `apply` anything from this directory.

The only files that should be getting deployed with the function are:

* Defaults: `main.py`, `const.py`, and `requirements.txt`
* <EXTRA-FILES>

`requirements.txt` is a generated file and should not be checked in. You will notice in `main.tf` an `external` provider which calls out to `make dist`; this is when `requirements.txt` gets generated.
