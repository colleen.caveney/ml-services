"""
trigger

Acts similar to how gcloud will execute our Function. Meant only for local
development and testing.
"""

import argparse
import http.client
import logging

import main as gfunc


# pylint: disable=missing-docstring

def trigger(debug: bool):
    if debug:
        logging.basicConfig()
        logging.getLogger().setLevel(logging.DEBUG)

        # enable http debugging (delete this if not relevant)
        http.client.HTTPConnection.debuglevel = 1
        requests_log = logging.getLogger("requests.packages.urllib3")
        requests_log.setLevel(logging.DEBUG)
        requests_log.propagate = True

    # trigger the gfunc
    gfunc.process({"event": "local-trigger-test"}, None)

if __name__ == "__main__":
    # execute only if run as a script
    parser = argparse.ArgumentParser() # pylint: disable=invalid-name
    parser.add_argument("--debug",
                        action="store_true",
                        help="enable debug logging")
    args = parser.parse_args() # pylint: disable=invalid-name
    trigger(args.debug)
