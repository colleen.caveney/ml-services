# Service: {{SERVICE_NAME}}

See: [Some doc](https://url/...)
See: [Another doc](https://url/...)

<SERVICE-PURPOSE>

## Development & Testing

Before beginning any new work, make sure that your `pipenv` is up-to-date:

```sh
make setup
```

This will install any missing dependencies (including dev); it won't hurt to run this multiple times. After that, tests are run like so:

```sh
$ make test
```

For quick manual testing, you can run this service via the `server` target and `curl` endpoints at `localhost:8080`:

```
make server
```

This will tell you pretty quickly if anything is wrong.

Manual integration testing of the service similar to how it will run in staging or production -- i.e. as a docker container -- can be conducted with the `wsgi` target (see also [wsgi.py](./wsgi.py)):

```sh
$ make build wsgi
```

You will discover that `docker` is required to be installed, which will not be done for you with `make setup` for obvious reasons; so install `docker` if you haven't already. Then you can `curl` any endpoint:

```sh
$ curl -i localhost:8080/

HTTP/1.1 200 OK
Server: gunicorn/19.9.0
Date: {{DATE}}
Connection: close
Content-Type: application/json
Content-Length: 4

"OK"
```

You can POST to any endpoint ...:

```sh
$ curl -i localhost:8080/ \
    -XPOST \
    -H"Content-Type: application/json" \
    -d '{}'

HTTP/1.0 204 NO CONTENT
Server: gunicorn/19.9.0
Date: {{DATE}}
Connection: close
Content-Type: application/json
```

<OTHER EXAMPLES OF TESTING THE SERVICE LOCALLY>

## Deployment

See: [Cloud Run](https://cloud.google.com/run/)

The `{{SERVICE_NAME}}` service is deployed to GCP as a Run container (hosted and managed kuberenetes) and is mostly managed with Terraform. This service is comprised of the following resources:

* Docker image
* GCR entry: where the images is hosted

The only files that should be getting deployed are managed via the `Dockerfile` and `.dockerignore`. `requirements.txt` is a generated file and should not be checked in.

_**Disclaimer:** Deployment is currently done manually and this needs to be fixed soon. To push a new image up, run: `make build docker-push`._
