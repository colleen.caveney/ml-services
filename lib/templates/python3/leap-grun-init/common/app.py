"""
app

Sets up the processor to be routed to via flask. In this case, we are catching
all requests via the same handler, thus, not routing required at the moment.
"""

import logging.config
import os

# logging setup

if "gunicorn" not in os.environ.get("SERVER_SOFTWARE", ""):
    # we are not running in a gunicorn env
    logging.config.fileConfig("logging.conf")

# app setup

import leap.app

import const
import processor

# handle ALL requests through the provided function
APP = leap.app.catch_all(__name__, processor.process, methods=const.ALLOWED_METHODS)

if __name__ == "__main__":
    APP.run()
