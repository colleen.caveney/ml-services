"""
constants

Constants for use in other modules. These values should not be changed outside
of this module, except under unit testing scenarios.
"""

import logging
import os

ALLOWED_METHODS = ["GET", "POST", "HEAD"]

if os.environ.get("K_SERVICE"): # we are running in a GCP Run container
    logging.debug("replace me with something")
else:
    logging.debug("replace me with something")
