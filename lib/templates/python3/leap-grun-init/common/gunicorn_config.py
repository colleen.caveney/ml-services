import os
from os import path

import multiprocess

cpus = multiprocess.cpu_count()

## gunicorn config

worker_class = "gthread"
workers = cpus*2+1 # a gunicorn recommendation

if os.environ.get("K_SERVICE"): # we are running in a GCP Run container
    # 80 is the the default and max concurrency for Run containers and this
    # calculation will distribute threads roughly evenly across workers
    threads = int(80 / workers)
else:
    # 1 thread per worker
    threads = 1

# can't get any solid documentation on this, so the hope is that a zero timeout
# means we never timeout. in testing withing Run containers, not setting this
# to zero has ended up with gunicorn restarting workers constantly; the
# assumption is that Run is actually opening connections with the service to
# keep it warm.
timeout = 0

# use the custom logging configuration for this service
loglevel = os.environ.get("LOG_LEVEL", "INFO")
logconfig = path.join(path.abspath(os.getcwd()), "logging.conf")

# listen on all NICs
bind = "0.0.0.0:{0}".format(os.environ.get("PORT", 8080))
