"""
processor

...
"""

import json
import logging

from flask import Request, Response


def process(req: Request) -> Response:
    """Entry-point processor."""
    logging.debug("processing event")

    if req.method == "POST":
        return Response("", mimetype="application/json", status=204)

    return Response(json.dumps("OK"), mimetype="application/json", status=200)
