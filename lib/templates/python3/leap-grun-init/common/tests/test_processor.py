# pylint: disable=missing-docstring,redefined-outer-name

import flask
import pytest
from flask.testing import FlaskClient

import app
import const
import leap.app
import leap.std.os
from leap.std.collections import list_sub


class TestProcess:
    ## test setup/teardown

    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    @pytest.fixture
    def app(self) -> FlaskClient:
        app.APP.testing = True
        yield app.APP.test_client()

    def assert_request(self,
                    app: FlaskClient,
                    method: str = "GET",
                    path: str = "/",
                    data: bytes = b"",
                    content_type: str = "application/json",
                    status_code: int = 200) -> flask.Response:
        res = app.open(path, method=method, data=data, content_type=content_type)
        assert res.status_code == status_code
        assert res.mimetype == "application/json"
        return res

    ## tests

    @pytest.mark.parametrize("meth", list_sub(leap.app.ALL_METHODS, const.ALLOWED_METHODS))
    def test_methods_fail(self, app: FlaskClient, meth: str):
        res = self.assert_request(app, method=meth, status_code=405)
        assert sorted(res.headers.get("Allow").split(", ")) == sorted(const.ALLOWED_METHODS)

    def test_get(self, app: FlaskClient):
        res = self.assert_request(app, method="GET", status_code=200)
        assert b'"OK"' == res.data

    def test_post(self, app: FlaskClient):
        res = self.assert_request(app, method="POST", data=b"{}", status_code=204)
        assert not res.data
