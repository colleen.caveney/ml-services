# Script: {{SCRIPT_NAME}}

See: [Some doc](https://url/...)
See: [Another doc](https://url/...)

<SCRIPT-PURPOSE>

## Development & Testing

Before beginning any new work, make sure that your `pipenv` is up-to-date:

```sh
make setup
```

This will install any missing dependencies (including dev); it won't hurt to run this multiple times. After that, tests are run like so:

```sh
$ make test
```

To run your script, simply execute the following:

```sh
$ ./bin/process ...
```

<OTHER EXAMPLES OF TESTING THE SCRIPT LOCALLY>
