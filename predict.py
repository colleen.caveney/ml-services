from typing import Dict, List, Union
from transformers import AutoTokenizer
from google.cloud import aiplatform
from google.protobuf import json_format
from google.protobuf.struct_pb2 import Value
import pandas as pd
import numpy as np


# SCRIPT TO HIT PREDICTION ENDPOINT WITH BERT ALL CATEGORY MODEL


ENDPOINT_ID = ""
PROJECT_ID = "657930556814"
LOCATION = "us-central1"
tokenizer = AutoTokenizer.from_pretrained("distilbert-base-uncased")


label_key = pd.read_csv('keys/1.0/category-all-key.csv')

inputs = [
    "recycled cashmere pullover", "blue khaki shorts", "ballpoint pen", "office lamp", "handmade planter", "terry hoodie"
]


def preprocess(inputs):
    list_encoded = tokenizer(inputs, truncation=True, padding=True)
    result = [{'input_ids': list_encoded['input_ids'][i], 'attention_mask': list_encoded['attention_mask'][i]} for i in range(len(inputs))]
    return result


def predict_custom_trained_model_sample(
    project: str,
    endpoint_id: str,
    instances: Union[Dict, List[Dict]],
    location: str = "us-central1",
    api_endpoint: str = "us-central1-aiplatform.googleapis.com",
):
    """
    `instances` can be either single instance of type dict or a list
    of instances.
    """
    # The AI Platform services require regional API endpoints.
    client_options = {"api_endpoint": api_endpoint}
    # Initialize client that will be used to create and send requests.
    # This client only needs to be created once, and can be reused for multiple requests.
    client = aiplatform.gapic.PredictionServiceClient(client_options=client_options)
    # The format of each instance should conform to the deployed model's prediction input schema.
    instances = instances if type(instances) == list else [instances]
    instances = [
        json_format.ParseDict(instance_dict, Value()) for instance_dict in instances
    ]
    parameters_dict = {}
    parameters = json_format.ParseDict(parameters_dict, Value())
    endpoint = client.endpoint_path(
        project=project, location=location, endpoint=endpoint_id
    )
    response = client.predict(
        endpoint=endpoint, instances=instances, parameters=parameters
    )
    print("response")
    print(" deployed_model_id:", response.deployed_model_id)
    # The predictions are a google.protobuf.Value representation of the model's predictions.
    predictions = response.predictions
    for prediction in predictions:
        prediction_key_idx = np.argmax(prediction)
        pred = label_key.loc[prediction_key_idx]
        print(" prediction:", (pred['key']))



predict_custom_trained_model_sample(
    project=PROJECT_ID,
    endpoint_id=ENDPOINT_ID,
    location=LOCATION,
    instances = preprocess(inputs)
)