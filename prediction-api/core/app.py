from fastapi import FastAPI
from fastapi.openapi.docs import get_redoc_html, get_swagger_ui_html
from fastapi.openapi.utils import get_openapi
import uvicorn, os
from core.utils import preprocess_product_change_event
from core.model import AllCategoryModel, SubCategoryModel, model_path_config
from core.schemas import Product
from core.cloud_storage_client import CloudStorageBucket
from core.config import config


app = FastAPI(
    title=config.TITLE,
    version=config.VERSION,
    docs_url=None if config.ENV == "production" else "/docs",
    redoc_url=None if config.ENV == "production" else "/redoc",
    openapi_url=None if config.ENV == "production" else "/openapi.json",
)

# load 
cloud_storage_bucket = CloudStorageBucket(config.GCP_PROJECT, config.STORAGE_BUCKET)
cloud_storage_bucket.download_blobs(blob_prefix='')

# load models
all_category_model = AllCategoryModel()
all_category_model.load()
for category in model_path_config.keys():
    if category != 'Category All':
        globals()[f"{category}_model"] = SubCategoryModel(category)
        globals()[f"{category}_model"].load()
# AllCategoryModel.get_registered_model_version_sample()


@app.post("/predict")
def predict(product: Product):
    
    text = preprocess_product_change_event(product)
    category = all_category_model.make_prediction(text)
    if category == 'Other':
        return category, category, category
    sub_category, tertiary_category = globals()[f"{category}_model"].make_prediction(text)
    return (category, sub_category, tertiary_category)
    # return category
