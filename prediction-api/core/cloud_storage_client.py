from pathlib import Path
from google.cloud import storage

class CloudStorageBucket:

    def __init__(self, project_name, bucket_name):
        storage_client = storage.Client(project_name)
        self.bucket = storage_client.get_bucket(bucket_name)

    def download_blobs(self, blob_prefix='', skip_if_exists=True):
        print(f'download blobs from {self.bucket.name}')
        blobs = self.bucket.list_blobs(prefix=blob_prefix)  # Get list of files
        for blob in blobs:
            if blob.name.endswith("/"):
                continue
            if skip_if_exists and Path(blob.name).is_file():
                continue
            print(f'downloading blob: {blob.name}')
            file_split = blob.name.split("/")
            directory = "/".join(file_split[0:-1])
            Path(directory).mkdir(parents=True, exist_ok=True)
            blob.download_to_filename(blob.name) 

