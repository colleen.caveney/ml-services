import os

from pydantic import BaseSettings


class Config(BaseSettings):
    ENV: str = "development"
    DEBUG: bool = True
    APP_HOST: str = "0.0.0.0"
    APP_PORT: int = 8000
    TITLE: str = "Product Categorization Prediction API"
    VERSION : str = "0.1.0"

class DevelopmentConfig(Config):
    GCP_PROJECT: str = 'leap-platform-dev'
    STORAGE_BUCKET : str = 'product-categorization-model-storage-dev'


def get_config():
    env = os.getenv("ENV", "dev")
    config_type = {
        "dev": DevelopmentConfig()
    }
    return config_type[env]


config: Config = get_config()