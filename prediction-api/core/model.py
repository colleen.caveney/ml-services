import pandas as pd
import numpy as np
from transformers import TFAutoModelForSequenceClassification, AutoTokenizer
from google.cloud import aiplatform, storage
from typing import Optional
import tensorflow as tf
from core.config import config

MODEL_PATH_KEY = 'model_path'
LABEL_KEY_PATH_KEY = 'key_path'
ALL_CATEGORY_KEY_COLUMN = 'key'
SUB_CATEGORY_KEY_COLUMN = 'sub_category_key'
TERTIARY_CATEGORY_KEY_COLUMN = 'tertiary_category_key'

TOKENIZER = AutoTokenizer.from_pretrained("distilbert-base-uncased")

model_path_config = {
    'Accessories' : {
        'key_path': 'keys/1.0/accessories-key.csv',
        'model_path': "saved_models/distilbert-accessories-wilbert"
    },
    'Bags, Backpacks & Luggage' : {
        'key_path': 'keys/1.0/bags-key.csv',
        'model_path': "saved_models/distilbert-bags-gilbert"
    },
    'Beauty, Health & Personal Care': {
        'key_path': 'keys/1.0/beauty-key.csv',
        'model_path': "saved_models/distilbert-beauty-dagobert"
    },
    'Bottoms' : {
        'key_path': 'keys/1.0/bottoms-key.csv',
        'model_path': "saved_models/distilbert-bottoms-albert"
    },
    'Home' : {
        'key_path': 'keys/1.0/home-key.csv',
        'model_path': "saved_models/distilbert-home-heriberto"
    },
    'Intimates' : {
        'key_path': 'keys/1.0/intimates-key.csv',
        'model_path': "saved_models/distilbert-intimates-albertina"
    },
    'Jewelry & Watches' : {
        'key_path': 'keys/1.0/jewelry-key.csv',
        'model_path': "saved_models/distilbert-jewelry-engelbert"
    },
    'Office & School Supplies' : {
        'key_path': 'keys/1.0/office-key.csv',
        'model_path': "saved_models/distilbert-office-hubert"
    },
    'Other Clothing' : {
        'key_path': 'keys/1.0/other_clothing-key.csv',
        'model_path': "saved_models/distilbert-other_clothing-roberto"
    },
    'Shoes' : {
        'key_path': 'keys/1.0/shoes-key.csv',
        'model_path': "saved_models/distilbert-shoes-dilbert"
    },
    'Tops' : {
        'key_path': 'keys/1.0/tops-key.csv',
        'model_path': "saved_models/distilbert-tops-roberta"
    },
    'Swimsuits & Cover-Ups' : {
        'key_path': 'keys/1.0/swim-key.csv',
        'model_path': "saved_models/distilbert-swim-bertha"
    },
    'Category All' : {
        'key_path': 'keys/1.0/category-all-key.csv',
        'model_path': "saved_models/distilbert-category-all-bert"
    }

}

class CategoryBertModel:

    def __init__(self, category):
        self.category = category
        if category not in model_path_config.keys():
            raise Exception('No existing model for provided category')
        self.model = None
        self.label_key = None
        self.tokenizer = TOKENIZER
    
# code for serving model from_pretrained format

    # def tokenize_str(self, text):
    #     return self.tokenizer.encode(text)

    # def load(self):
    #     path = model_path_config[self.category][MODEL_PATH_KEY]
    #     self.model = TFAutoModelForSequenceClassification.from_pretrained(path)
    #     self.label_key = pd.read_csv(model_path_config[self.category][LABEL_KEY_PATH_KEY])


    def load(self):
        path = model_path_config[self.category][MODEL_PATH_KEY]
        print(f'loading model from {path}')
        self.model = tf.saved_model.load(f'{path}/saved_model/1')
        self.label_key = pd.read_csv(model_path_config[self.category][LABEL_KEY_PATH_KEY])
    
    def tokenize_str(self, text):
        return self.tokenizer(text, truncation=True, padding=True, return_tensors="tf")

    def predict(self, encoded):
        # predictions = self.model.predict(encoded)
        predictions = self.model.signatures["serving_default"](attention_mask = encoded['attention_mask'], input_ids = encoded['input_ids'])
        prediction_key_idx = np.argmax(predictions['logits'][0,:])
        return self.label_key.loc[prediction_key_idx]

    def make_prediction(self, text): 
        pass


class AllCategoryModel(CategoryBertModel):

    def __init__(self):
        super().__init__('Category All')

    def make_prediction(self, text):
        
        if not self.model:
            self.load()
        
        encoded = self.tokenize_str(text)
        prediction = self.predict(encoded)
        return prediction[ALL_CATEGORY_KEY_COLUMN]


class SubCategoryModel(CategoryBertModel):

    def make_prediction(self, text):

        if not self.model:
            self.load()

        encoded = self.tokenize_str(text)
        prediction = self.predict(encoded)
        return prediction[SUB_CATEGORY_KEY_COLUMN], prediction[TERTIARY_CATEGORY_KEY_COLUMN]
