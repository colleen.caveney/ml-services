from pydantic import BaseModel

class Product(BaseModel):
    product_type: str
    product_title: str