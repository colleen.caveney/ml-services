import pandas as pd

def preprocess_product_change_event(product):
    '''
    This function combines the product_title and product_type field.
    '''

    product_type = product.product_type
    product_title = product.product_title
    
    # Combine the optimal features
    text = f"{product_title} {product_type}"
    
    # Post-cleaning of text
    text = text.lower().replace('_', ' ').replace('|', '')
    text = ' '.join(pd.unique(text.split()))
    return text
