from google.cloud import aiplatform, storage
from transformers import AutoTokenizer
import tensorflow as tf
from pathlib import Path

from utils import get_argmax_with_key, format_protobuf_arguments


class CloudStorageBucketClient:

    def __init__(self, project_name, bucket_name):
        storage_client = storage.Client(project_name)
        self.bucket = storage_client.get_bucket(bucket_name)
    
    def download_blobs(self, blob_prefix='', skip_if_exists=True):
        print(f'Downloading blobs')
        blobs = self.bucket.list_blobs(prefix=blob_prefix)  # Get list of files
        for blob in blobs:
            if blob.name.endswith("/"):
                continue
            if skip_if_exists and Path(blob.name).is_file():
                continue
            print(f'Downloading Blob: {blob.name}')
            file_split = blob.name.split("/")
            directory = "/".join(file_split[0:-1])
            Path(directory).mkdir(parents=True, exist_ok=True)
            blob.download_to_filename(blob.name) 
        print(f'Done')


class VertexCategoryPredictionService:

    def __init__(self, project, location, category_endpoint_id, subcategory_endpoint_id_dict, category_label_key, label_key_dict):
        self.project = project
        self.location = location
        self.client = self._prediction_service_client()
        self.category_endpoint = self._endpoint_path(category_endpoint_id)
        self.endpoint_id_dict = {key:self._endpoint_path(value) for (key,value) in subcategory_endpoint_id_dict.items()}
        self.category_label_key = category_label_key
        self.label_key_dict = label_key_dict
        self.tokenizer = AutoTokenizer.from_pretrained("distilbert-base-uncased")

    
    def _prediction_service_client(self):
        return aiplatform.gapic.PredictionServiceClient(client_options={"api_endpoint": f"{self.location}-aiplatform.googleapis.com"})
    

    def _endpoint_path(self, endpoint_id):
        return self.client.endpoint_path(self.project, self.location, endpoint_id)


    def _preprocess(self, inputs):
        list_encoded = self.tokenizer(inputs, truncation=True, padding=True)
        result = [{'input_ids': list_encoded['input_ids'][i], 'attention_mask': list_encoded['attention_mask'][i]} for i in range(len(inputs))]
        result = format_protobuf_arguments(result)
        return result


    def predict(self, input):

        input = self._preprocess([input])
        
        category_prediction = self.client.predict(endpoint=self.category_endpoint, instances=input).predictions[0]

        [category_label] = get_argmax_with_key(category_prediction, self.category_label_key, ['key'])
        
        if category_label == 'Other':
            return 'Other', 'Other', 'Other'
        
        sub_category_model = self.endpoint_id_dict[category_label]

        sub_category_prediction = self.client.predict(endpoint=sub_category_model, instances=input).predictions[0]
        
        [sub_category_label, tert_category_label] = get_argmax_with_key(sub_category_prediction, self.label_key_dict[category_label], ['sub_category_key', 'tertiary_category_key'])
        
        return category_label, sub_category_label, tert_category_label


class DownloadModelPredictionService:

    def __init__(self, cloud_storage_bucket_client, category_model_path, subcategory_model_path_dict, category_label_key, label_key_dict):
        self.cloud_storage_bucket_client = cloud_storage_bucket_client
        self.category_model_path = category_model_path
        self.model_path_dict = subcategory_model_path_dict
        self.category_label_key = category_label_key
        self.label_key_dict = label_key_dict
        self.tokenizer = AutoTokenizer.from_pretrained("distilbert-base-uncased")

        self.all_category_model = None
        self.sub_category_models = {}

        
    def load_category_model(self):
        print(f'loading model from {self.category_model_path}')
        self.all_category_model = tf.saved_model.load(f'{self.category_model_path}')     

    def load_sub_category_model(self, category):
        path = self.model_path_dict[category]
        print(f'loading model from {path}')
        self.sub_category_models[category] = tf.saved_model.load(f'{path}')    

    def _preprocess(self, input):
        return self.tokenizer(input, truncation=True, padding=True, return_tensors="tf")

    def predict(self, input):

        input = self._preprocess(input)

        if not self.all_category_model:
            self.load_category_model()
        
        category_prediction = self.all_category_model.signatures["serving_default"](attention_mask = input['attention_mask'], input_ids = input['input_ids'])

        [category_label] = get_argmax_with_key(category_prediction, self.category_label_key, ['key'])

        if not self.sub_category_models.get(category_label):
            self.load_sub_category_model(category_label)
        
        sub_category_model = self.sub_category_models[category_label]
        sub_category_prediction = sub_category_model.signatures["serving_default"](attention_mask = input['attention_mask'], input_ids = input['input_ids'])
        [sub_category_label, tert_category_label] = get_argmax_with_key(sub_category_prediction, self.label_key_dict[category_label], ['sub_category_key', 'tertiary_category_key'])

        return category_label, sub_category_label, tert_category_label

