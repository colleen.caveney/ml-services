
import numpy as np
import pandas as pd
from google.protobuf import json_format
from google.protobuf.struct_pb2 import Value
import json
import base64


def trigger_to_dict(trigger_type, trigger_data):
    
    if trigger_type == 'cloud_event':
        try:
            data = base64.b64decode(trigger_data.data["message"]["data"]).decode()
            return json.loads(data)
        except:
            raise Exception('cloud event message not structured json')
    elif trigger_type == 'http':
        try:
            return trigger_data.get_json(silent=True)
        except:
            raise Exception('http request does not include json post data')
    
    raise Exception('trigger_type not one of [ "http", "cloud_event" ]')



def get_argmax_with_key(values, label_key = None, key_names=None):
    """
    Return the index of the maximum value in values. If label_key provided,
    return the row in label_key where index is the max index in values. If 
    key_names also provided, return the values in the selected row belonging to 
    key_names columns.
    """
    idx = np.argmax(values)
    print(f'values: {values}')
    print(f'idx: {idx}')
    if label_key is None:
        print('label key is none')
        return idx
    row = label_key.loc[idx]
    print(row)
    if not key_names:
        return row
    return [row[key_name] for key_name in key_names]

def format_protobuf_arguments(inputs):
    """
    Format list of python dicts as protobuf
    """
    return [
        json_format.ParseDict(input_dict, Value()) for input_dict in inputs
    ]

def preprocess_product_change_event(product_title, product_type):
    '''
    This function combines the product_title and product_type field.
    '''
    
    # Combine the optimal features
    text = f"{product_title} {product_type}"
    
    # Post-cleaning of text
    text = text.lower().replace('_', ' ').replace('|', '')
    text = ' '.join(pd.unique(text.split()))
    return text
